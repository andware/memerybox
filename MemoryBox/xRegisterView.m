//
//  xRegisterView.m
//  MemoryBox
//
//  Created by joewu on 5/20/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "xRegisterView.h"

#import "xMainViewController.h"
#import "Constants.h"
#import "DataBase.h"
#pragma mark
@interface xRegisterView ()

@end

@implementation xRegisterView
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setNeedsStatusBarAppearanceUpdate];// 设置status 字体为白色
    
    passWord.secureTextEntry = YES;
    passWord2.secureTextEntry = YES;
 
    //对输入框进行监听
    regirest_userName.delegate=self;
    regirest_email.delegate=self;
    passWord.delegate=self;
    passWord2.delegate=self;
    
}

// set status bar text white
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
	return YES;
}

- (IBAction)_do_regirest:(UIButton *)sender {
    
    if (![regirest_userName.text compare:nil]==NSOrderedSame && ![regirest_userName.text compare:@""]==NSOrderedSame) {
        
    if ([passWord.text compare:passWord2.text] == NSOrderedSame) {//两次密码一致
        NSString *responseState = [[NSString alloc]initWithFormat:@"%@",[self connectToService]];//获得服务器返回的注册结果
            NSLog(@"--->%@",responseState);
            if ([responseState isEqualToString:@"1"]) {//注册成功
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                
                //保存注册成功的用户名和密码
                [userDefaults setObject:regirest_userName.text forKey:@"userName"];
                [userDefaults setObject:passWord.text forKey:@"passWord"];
                [userDefaults synchronize];
                
                xMainViewController *_mainView = [[[xMainViewController alloc]initWithNibName:@"xMainViewController" bundle:nil]autorelease];
                [self presentModalViewController:_mainView animated:NO];
            }else if([responseState compare:@"3.1"] == NSOrderedSame){
                UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"注册提示" message:@"邮箱已被注册，请使用其他邮箱！" delegate:self cancelButtonTitle:@"关闭" otherButtonTitles:nil];
                [alert show];
                [alert release];
            }else if([responseState compare:@"3.2"] == NSOrderedSame){
                UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"注册提示" message:@"很抱歉，用户名已被注册！" delegate:self cancelButtonTitle:@"关闭" otherButtonTitles:nil];
                [alert show];
                [alert release];
            }
    }else{//两次输入密码不一致提示
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"注册提示" message:@"两次输入密码不一致，请从新输入!" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
    }else {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"注册提示" message:@"用户名为空，请输入用户名!" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

- (IBAction)_cancel:(UIButton *)sender {
    xMainViewController *_mainView = [[[xMainViewController alloc]initWithNibName:@"xMainViewController" bundle:nil]autorelease];
    [self presentModalViewController:_mainView animated:NO];

}

// 链接服务器进行注册
-(NSString *)connectToService{
    NSError *error;
    NSString *re_useName = [[NSString alloc]initWithFormat:@"%@",regirest_userName.text];
    NSString *re_email = [[NSString alloc]initWithFormat:@"%@",regirest_email.text];
    NSString *re_passWord = [[NSString alloc]initWithFormat:@"%@",passWord.text];
    NSString *url = [[NSString alloc]initWithFormat:@"%@userRegisterWithEmail/user_name/%@/user_email/%@/user_password/%@",ApiUrl, re_useName,re_email,re_passWord];//api 登陆接口
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    [url release];
    NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    NSDictionary *weatherDic = [NSJSONSerialization JSONObjectWithData:response  options:NSJSONReadingMutableLeaves error:&error];//Json字典
    [re_useName release];
    [re_email release];
    [re_passWord release];
    return [weatherDic objectForKey:@"state"];//解析出返回的状态参数
}

// 输入时自动调整屏幕高度
- (void)moveView:(UITextField *)textField leaveView:(BOOL)leave{
    float screenHeight = 1496; //屏幕尺寸，如果屏幕允许旋转，可根据旋转动态调整
    float keyboardHeight = 748; //键盘尺寸，如果屏幕允许旋转，可根据旋转动态调整
    float statusBarHeight,NavBarHeight,tableCellHeight,textFieldOriginY,textFieldFromButtomHeigth;
    int margin;
    statusBarHeight = [[UIApplication sharedApplication] statusBarFrame].size.height; //屏幕状态栏高度
    NavBarHeight = self.navigationController.navigationBar.frame.size.height; //获取导航栏高度
    
    UITableViewCell *tableViewCell=(UITableViewCell *)textField.superview;
    tableCellHeight = tableViewCell.frame.size.height; //获取单元格高度
    
    CGRect fieldFrame=[self.view convertRect:textField.frame fromView:tableViewCell];
    textFieldOriginY = fieldFrame.origin.y; //获取文本框相对本视图的y轴位置。
    
    //计算文本框到屏幕底部的高度（屏幕高度-顶部状态栏高度-导航栏高度-文本框的的相对y轴位置-单元格高度）
    textFieldFromButtomHeigth = screenHeight - statusBarHeight - NavBarHeight - textFieldOriginY - tableCellHeight;
    
    if(!leave) {
        if(textFieldFromButtomHeigth < keyboardHeight) { //如果文本框到屏幕底部的高度 < 键盘高度
            margin = keyboardHeight - textFieldFromButtomHeigth; // 则计算差距
            keyBoardMargin_ = margin; //keyBoardMargin_ 为成员变量，记录上一次移动的间距,用户离开文本时恢复视图高度
        } else {
            margin= 0;
            keyBoardMargin_ = 0;
        }
    }
    
    float movementDuration = 0.3f; // 动画时间
    
    int movement; //进入时根据差距移动视图，离开时恢复之前的高度
    UIInterfaceOrientation deviceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (deviceOrientation == 3) {
        movement = (leave ? keyBoardMargin_ : -margin); //进入时根据差距移动视图，离开时恢复之前的高度
    }else{
        movement = (leave ? -keyBoardMargin_ : margin); //进入时根据差距移动视图，离开时恢复之前的高度
    }
    
    [UIView beginAnimations: @"textFieldAnim" context: nil]; //添加动画
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, -movement/10, 0);
    [UIView commitAnimations];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [self moveView:textField leaveView:NO];
}

- (void)textFieldDidEndEditing:(UITextField *)textField;{
    [self moveView:textField leaveView:YES];
}

// memory warning
- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//调整高度实现结束
- (void)viewDidUnload{
    [regirest_userName release];
    regirest_userName = nil;
    [regirest_email release];
    regirest_email = nil;
    [passWord release];
    passWord = nil;
    [passWord2 release];
    passWord2 = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)dealloc {
    [regirest_userName release];
    [regirest_email release];
    [passWord release];
    [passWord2 release];
    [super dealloc];
}

@end
