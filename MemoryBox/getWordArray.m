//
//  getWordArray.m
//  MemoryBoxRed
//
//  Created by YangJoe on 3/15/14.
//
//

#import "getWordArray.h"
#import "tools.h"
#import "DataBase.h"
#import "secondList.h"
#import "Word.h"

@implementation getWordArray

-(NSMutableArray *)GetOneDictionaryWord{
    //获得用户信息接口
    tools *tool = [[tools alloc]init];
    NSString *user_id = (NSString*)[tool readObkect:@"user_id"];
    
    NSMutableArray *newWords;
        
        // 获得用户信息接口
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        
        // 获得词库数组中位置
        NSString *dicNum = [userDefaults stringForKey:@"dicNum"];
        int dicIndex = [dicNum intValue];
        
        NSMutableArray *dics = [tool readArray:@"doubleStruct"];
        UserDictionary *OneDic = [[UserDictionary alloc]init];
        int index = 0;
        for (int i = 0; i < [dics count]; i++) {
            secondList *list = [dics objectAtIndex:i];
            NSMutableArray *array = list.secondLevel;
            for (int j = 0; j < [array count]; j++) {
                if (index == dicIndex) {
                    OneDic = [array objectAtIndex:j];
                    index++;
                    break;
                }
                index++;
            }
        }
        
        NSMutableArray *OneArray = [[NSMutableArray alloc]init];
        [OneArray addObject:OneDic];
        
        DataBase *database = [[DataBase alloc]init];
        NSString *days = [database getCompleteTime:OneDic];// 获得完成天数
        NSString *dayWords = [self dayWords:OneDic finishDay:days];
        
        newWords = [database getNewWord:user_id wordCount:dayWords Dictionarys:OneArray];
        [OneArray release];
        [[NSUserDefaults standardUserDefaults] setObject:@"one" forKey:@"finished"];
        [[NSUserDefaults standardUserDefaults] setObject:OneDic.dictionary_name forKey:@"now_use_dictionary"];
        [OneDic release];
    
    return newWords;
}

-(NSMutableArray *)getNewWords{
    //获得用户信息接口
    tools *tool = [[tools alloc]init];
    NSString *user_id = (NSString*)[tool readObkect:@"user_id"];
    NSMutableArray *newWords = [[NSMutableArray alloc]init];
    NSMutableArray *dics = [tool readArray:@"doubleStruct"];// get saved dictionary structure
    
    for (int i = 0; i < [dics count]; i++) {
        secondList *list = [dics objectAtIndex:i];
        NSMutableArray *array = list.secondLevel;
        NSMutableArray *oneDicArray;
        for (int j = 0; j < [array count]; j++) {
            UserDictionary *OneDic = [[UserDictionary alloc]init];
            OneDic = [array objectAtIndex:j];
            
            NSMutableArray *OneArray = [[NSMutableArray alloc]init];
            [OneArray addObject:OneDic];
            
            DataBase *database = [[DataBase alloc]init];
            NSString *days = [database getCompleteTime:OneDic];// 获得完成天数
            NSString *dayWords = [self dayWords:OneDic finishDay:days];
            oneDicArray = [database getNewWord:user_id wordCount:dayWords Dictionarys:OneArray];
            
            if ([oneDicArray count] != 0) {
                [newWords addObjectsFromArray:oneDicArray];
                NSLog(@"get '%lu' word from %@",(unsigned long)[oneDicArray count],OneDic.dictionary_name);
            }
            [OneArray release];
            [OneDic release];
            [database release];
        }
    }
    return newWords;
}

-(NSString*)dayWords:(UserDictionary*) mydic finishDay: (NSString *)finish_day{
    DataBase *wordCount =[[DataBase alloc]init];
    NSString *AllWordscount = [wordCount getDictionaryWordCount:mydic];//得到词库所有单词数量
    int intString = [AllWordscount intValue];
    int finishday = [finish_day intValue];// 完成的天数
    if (finishday == 0) {
        return 0;
    }else{
        if (finishday <= 15) {
            finishday = 15;
        }
        int day = intString/(finishday-14); // 日背诵量 = (总单词数量)/(计划完成的天数-14)
        NSString *days = [NSString stringWithFormat:@"%d",day];
        return days;
    }
    
}

-(NSMutableArray*)GetOldWord{
    tools *tool = [[tools alloc]init];
    //获得用户信息接口
    NSString *user_id = (NSString*)[tool readObkect:@"user_id"];
    
    DataBase *database = [[DataBase alloc]init];
    NSMutableArray *oldWords = [[NSMutableArray alloc]init];
    oldWords = [database getOldWord:user_id];
    NSLog(@"get oldword = %lu",(unsigned long)[oldWords count]);
    return oldWords;
}

-(NSMutableArray*)getAllWord{
    NSMutableArray *wordsArray = [[NSMutableArray alloc]init];
    NSMutableArray *new = [self getNewWords];
    NSMutableArray *old = [self GetOldWord];
   
    for (Word *nWord in new) {
        NSLog(@"new = %@",nWord.word_word);
    }
    
    for (Word *oWord in old) {
        NSLog(@"old = %@",oWord.word_word);
    }
    int newWordIndex = [new count]/3;
    int oldWordIndex = [old count]/3;
    
    if ([new count] == 0 && [old count] != 0) {
        for (Word *word in old) {
            [wordsArray addObject:word];
        }
    }else if([new count] != 0 && [old count] == 0){
        for (Word *word in new) {
            [wordsArray addObject:word];
        }
    }else if(newWordIndex == 0 || oldWordIndex == 0){
        for (Word *word in new) {
            [wordsArray addObject:word];
        }
        for (Word *word in old) {
            [wordsArray addObject:word];
        }
    }else {
        for (int i =0; i < [new count]+ [old count]; i++) {
            if (i < newWordIndex) {
                [wordsArray addObject:[new objectAtIndex:i]];
            }else if ((i >= newWordIndex) && (i < newWordIndex+oldWordIndex)){
                [wordsArray addObject:[old objectAtIndex:i-newWordIndex]];
            }else if ((i >= (newWordIndex+oldWordIndex)) && (i<(2*newWordIndex+oldWordIndex))){
                [wordsArray addObject:[new objectAtIndex:i-oldWordIndex]];
            }else if((i >= (2*newWordIndex+oldWordIndex)) && i< 2*(newWordIndex + oldWordIndex)){
                [wordsArray addObject:[old objectAtIndex:i-2*newWordIndex]];
            }else if((i >= 2*(newWordIndex + oldWordIndex))&&(i <(3*newWordIndex + 2* oldWordIndex))){
                [wordsArray addObject:[new objectAtIndex:i-2*oldWordIndex]];
            }else if(i >= (3*newWordIndex + 2* oldWordIndex)&&i <(3*newWordIndex + 3* oldWordIndex)){
                [wordsArray addObject:[old objectAtIndex:i-3*newWordIndex]];
            }else{
                
            }
        }
        
        int newR = [new count]%3;
        int oldR = [old count]%3;
        
        if (newR ==1) {
            [wordsArray addObject:[new objectAtIndex:[new count]-1]];
        }else if(newR == 2){
            [wordsArray addObject:[new objectAtIndex:[new count]-1]];
            [wordsArray addObject:[new objectAtIndex:[new count]-2]];
        }
        
        if (oldR ==1) {
            [wordsArray addObject:[old objectAtIndex:[old count]-1]];
        }else if(oldR == 2){
            [wordsArray addObject:[old objectAtIndex:[old count]-1]];
            [wordsArray addObject:[old objectAtIndex:[old count]-2]];
        }
    }
    for (Word *oWord in wordsArray) {
        NSLog(@"toger = %@",oWord.word_word);
    }
    return wordsArray;
}
@end
