//
//  secondList.h
//  MemoryBox
//
//  Created by YangJoe on 10/22/13.
//
//

#import <Foundation/Foundation.h>

@interface secondList : NSObject<NSCoding>{
    
}

@property (nonatomic, retain) NSString *firstLevel;
@property (nonatomic, retain) NSMutableArray *secondLevel;

@end
