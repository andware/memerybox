//
//  User.m
//  MemoryBox
//
//  Created by firefly on 13-6-22.
//
//

#import "User.h"
#import "ServiceApi.h"
#import "Constants.h"
@implementation User

- (id) initWithCoder: (NSCoder *)coder {
    if (self = [super init]){
        self.user_id = [coder decodeObjectForKey:@"user_id"];
        self.user_name = [coder decodeObjectForKey:@"user_name"];
        self.user_password = [coder decodeObjectForKey:@"user_password"];
        self.user_email = [coder decodeObjectForKey:@"user_email"];
        self.user_total = [coder decodeObjectForKey:@"user_total"];
        self.user_day_rember_count = [coder decodeObjectForKey:@"user_day_rember_count"];
        self.user_day_rember_count_test = [coder decodeObjectForKey:@"user_day_rember_count_test"];
        self.user_rembe_time = [coder decodeObjectForKey:@"user_rembe_time"];
    }
    return self;
}

- (void) encodeWithCoder: (NSCoder *)coder  {
    [coder encodeObject:self.user_id forKey:@"user_id"];
    [coder encodeObject:self.user_name forKey:@"user_name"];
    [coder encodeObject:self.user_password forKey:@"user_password"];
    [coder encodeObject:self.user_email forKey:@"user_email"];
    [coder encodeObject:self.user_total forKey:@"user_total"];
    [coder encodeObject:self.user_day_rember_count forKey:@"user_day_rember_count"];
    [coder encodeObject:self.user_day_rember_count_test forKey:@"user_day_rember_count_test"];
    [coder encodeObject:self.user_rembe_time forKey:@"user_rembe_time"];
}

- (BOOL)setUserWordRecordToService:(NSString*)wid wordDay:(NSString *)wd{
    NSString *url = [NSString stringWithFormat:@"%@setUserRecord/user_id/%@/word_id/%@/word_day/%@",ApiUrl,self.user_id,wid,wd];
    NSDictionary *info =[ServiceApi getJSON:url];
    NSString *state = [NSString stringWithFormat:@"%@",[info objectForKey:@"state"]];
    if([@"1" compare:state]==NSOrderedSame){
        return YES;
    }else{
        return NO;
    }
}

-(BOOL)cutTotal:(NSString *)user_total{
    NSString *url = [NSString stringWithFormat:@"%@cutUserTotalByUserName/user_name/%@/user_total/%@",ApiUrl,self.user_name,user_total];
    NSDictionary *s = [ServiceApi getJSON:url];
    NSString *state = [NSString stringWithFormat:@"%@",[s objectForKey:@"state"]];
    if ([state compare:@"1"]==NSOrderedSame) {
        float i = [user_total floatValue];
        float j = [self.user_total floatValue];
        self.user_total = [NSString stringWithFormat:@"%lf",j-i];
        return YES;
    }else{
        return NO;
    }
}

-(id)initWithUserName:(NSString *)userName{
    if (self = [super init]) {
        
        NSDictionary *info = [ServiceApi getJSON:[userInfoUrl stringByAppendingString:userName]];
        NSDictionary *state = [info objectForKey:@"state"];
        NSString *stateStr = [NSString stringWithFormat:@"%@",[state objectForKey:@"state"]]; 
        BOOL compare = [@"1" compare:stateStr]==NSOrderedSame;
        if(compare){
            NSDictionary *data = [info objectForKey:@"data"];
            self.user_id = [NSString stringWithFormat:@"%@",[data objectForKey:@"user_id"]];
            self.user_name = [NSString stringWithFormat:@"%@",[data objectForKey:@"user_name"]];
            self.user_password = [NSString stringWithFormat:@"%@",[data objectForKey:@"user_password"]];
            self.user_email = [NSString stringWithFormat:@"%@",[data objectForKey:@"user_email"]];
            self.user_total = [NSString stringWithFormat:@"%@",[data objectForKey:@"user_total"]];
            self.user_day_rember_count = [NSString stringWithFormat:@"%@",[data objectForKey:@"user_day_rember_count"]];
            self.user_day_rember_count_test = [NSString stringWithFormat:@"%@",[data objectForKey:@"user_day_rember_count_test"]];
            self.user_rembe_time = [NSString stringWithFormat:@"%@",[data objectForKey:@"user_rembe_time"]];
        }else{
            return NO;
        }
    }
    return self;
}


//修改用户日背诵词汇
-(BOOL)setDay_rember_count:(NSString *)user_day_rember_count{
    NSString *url = [NSString stringWithFormat:@"%@updateUserDayRemberCount/user_name/%@/user_day_rember_count/%@",ApiUrl,self.user_name,user_day_rember_count];
    NSDictionary *s = [ServiceApi getJSON:url];
    NSString *state = [NSString stringWithFormat:@"%@",[s objectForKey:@"state"]];
    if ([state compare:@"1"]==NSOrderedSame) {
        return YES;
    }else{
        return NO;
    }
    
}
//修改用户积分
-(BOOL)setTotal:(NSString *)user_total{
    NSString *url = [NSString stringWithFormat:@"%@addUserTotalByUserName/user_name/%@/user_total/%@",ApiUrl,self.user_name,user_total];
    NSDictionary *s = [ServiceApi getJSON:url];
    NSString *state = [NSString stringWithFormat:@"%@",[s objectForKey:@"state"]];
    if ([state compare:@"1"]==NSOrderedSame) {
        float i = [user_total floatValue];
        float j = [self.user_total floatValue];
        self.user_total = [NSString stringWithFormat:@"%lf",j+i];
        return YES;
    }else{
        return NO;
    }
}
//修改用户背诵时间
-(BOOL)setRembe_time:(NSString *)user_rembe_time{
    NSString *url = [NSString stringWithFormat:@"%@updateUserRemberTime/user_name/%@/user_rember_time/%@",ApiUrl,self.user_name,user_rembe_time];
    NSDictionary *s = [ServiceApi getJSON:url];
    NSString *state = [NSString stringWithFormat:@"%@",[s objectForKey:@"state"]];
    
    if ([state compare:@"1"]==NSOrderedSame) {
        return YES;
    }else{
        return NO;
    }
}
- (BOOL)findPassword{
    NSString *url = [NSString stringWithFormat:@"%@findPassword/email/%@",ApiUrl,self.user_email];
    NSDictionary *s = [ServiceApi getJSON:url];
    NSString *state = [NSString stringWithFormat:@"%@",[s objectForKey:@"state"]];
    
    if ([state compare:@"1"]==NSOrderedSame) {
        return YES;
    }else{
        return NO;
    }
}
@end
