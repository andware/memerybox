//
//  xMainViewController.m
//  MemoryBox
//
//  Created by joewu on 5/20/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "xMainViewController.h"
#import "xFindBackPassword.h"
#import "xRegisterView.h"
#import "User_center.h"
#import "User_center.h"
#import "start_memory.h"
#import "Reachability.h"
#import "ServiceApi.h"
#import "UserDictionary.h"
@interface xMainViewController ()

@end
@implementation xMainViewController
@synthesize rememberLoding = _rememberLoding;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
   
    _password.secureTextEntry = YES;
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"rememberLoding"]) {
        [self readNSUserDefaults];
        [self.rememberLoding setImage:[UIImage imageNamed:@"choice2.png"] forState:UIControlStateNormal];
    }
    /*
     *TEST
     */
    ServiceApi *api =[[ServiceApi alloc]init];
    NSMutableArray *dics = [api getUserHadBuyDictionary:@"ssj"];
    NSLog(@"sss");
    for (UserDictionary *dic in dics) {
        
        NSLog(@"%@",dic.dictionary_name);
    }
    
}

- (void)viewDidUnload
{
    [_userName release];
    _userName = nil;
    [_password release];
    _password = nil;
    [self setRememberLoding:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
   
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

- (IBAction)_register:(UIButton *)sender {
    xRegisterView *regirest = [[[xRegisterView alloc]initWithNibName:@"xRegisterView" bundle:nil]autorelease];
    [self presentModalViewController:regirest animated:NO];
}

- (IBAction)_forgotPassword:(UIButton *)sender {
    xFindBackPassword *find_back_password = [[[xFindBackPassword alloc]initWithNibName:@"xFindBackPassword" bundle:nil]autorelease];
    [self presentModalViewController:find_back_password animated:NO];
}


- (IBAction)_loading:(UIButton *)sender {
    [self saveNSUserDefaults];
    
    BOOL isExistenceNetwork = [self isExistenceNetwork];
    if (isExistenceNetwork) {
        NSString *responseState = [[NSString alloc]initWithFormat:@"%@",[self connectToService]];
        if ([responseState isEqualToString:@"1"]) {//登陆成功
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"firstLoding"]) {
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"firstLoding"];
            User_center *_User_center = [[[User_center alloc]initWithNibName:@"User_center" bundle:nil]autorelease];
            [self presentModalViewController:_User_center animated:NO];
            }else {
                start_memory *startmemory = [[[start_memory alloc]initWithNibName:@"start_memory" bundle:nil]autorelease];
                [self presentModalViewController:startmemory animated:NO];
            }
            //设置第一次登陆为否
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"firstLaunch"];
            }else if([responseState isEqualToString:@"2.2"]){//密码错误
                //设置提示窗口
                UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"登陆提示" message:@"密码错误" delegate:self cancelButtonTitle:@"关闭" otherButtonTitles:nil];
                [alert show];
                [alert release];
            }else {//用户名不存在
                //设置提示窗口
                UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"登陆提示" message:@"用户名不存在" delegate:self cancelButtonTitle:@"关闭" otherButtonTitles:nil];
                [alert show];
                [alert release];
            }
    }else{
        UIAlertView *myalert = [[UIAlertView alloc] initWithTitle:@"警告" message:@"网络不存在" delegate:self cancelButtonTitle:@"确认" otherButtonTitles:nil,nil];
        [myalert show];
        [myalert release];
    }
}

- (IBAction)_browser:(UIButton *)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://laihoudegouwo.taobao.com/"]];
}
- (void)dealloc {
    [_userName release];
    [_password release];
    [_rememberLoding release];
    [super dealloc];
}

//存储数据到NSUserDefaults中
-(void)saveNSUserDefaults {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setObject:_userName.text forKey:@"userName"];
    [userDefaults setObject:_password.text forKey:@"passWord"];
    [userDefaults synchronize];  
}

//从NSUserDefaults中读取数据
-(void)readNSUserDefaults {
    NSUserDefaults *userDefaultes = [NSUserDefaults standardUserDefaults];
    NSString *userName = [userDefaultes stringForKey:@"userName"];
    NSString *passWord = [userDefaultes stringForKey:@"passWord"];
    _userName.text = userName;
    _password.text = passWord;
}

-(NSString *)connectToService{
    NSError *error;
    NSString *useName = [[NSString alloc]initWithFormat:@"%@",_userName.text];
    NSString *passWord = [[NSString alloc]initWithFormat:@"%@",_password.text];
    NSString *url = [[NSString alloc]initWithFormat:@"http://andware.cn/remberbox/index.php?s=/Api/userLogin/user_name/%@/user_password/%@",useName,passWord];//api 登陆接口
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    NSDictionary *weatherDic = [NSJSONSerialization JSONObjectWithData:response  options:NSJSONReadingMutableLeaves error:&error];//Json字典
    return [weatherDic objectForKey:@"state"];//解析出返回的状态参数
}

- (BOOL)isExistenceNetwork
{
	BOOL isExistenceNetwork;
	Reachability *r = [Reachability reachabilityWithHostName:@"www.apple.com"];
    switch ([r currentReachabilityStatus]) {
        case NotReachable:
			isExistenceNetwork=FALSE;
            //NSLog(@"没有网络");
            break;
        case ReachableViaWWAN:
			isExistenceNetwork=TRUE;
            //NSLog(@"正在使用3G网络");
            break;
        case ReachableViaWiFi:
			isExistenceNetwork=TRUE;
            //NSLog(@"正在使用wifi网络");
            break;
    }
	return isExistenceNetwork;
}

- (IBAction)_rememberLoding:(UIButton *)sender {
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"rememberLoding"]) {
        [sender setImage:[UIImage imageNamed:@"choice1.png"] forState:UIControlStateNormal];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"rememberLoding"];
    }else{
        [sender setImage:[UIImage imageNamed:@"choice2.png"] forState:UIControlStateNormal];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"rememberLoding"];
    }
}

@end
