//
//  PurchaseTest.m
//  MemoryBox
//
//  Created by YangJoe on 13-10-6.
//
//

#import "CBiOSStoreManager.h"
#import "PurchaseTest.h"

#define ProductID_IAP_1 @"com.andware.dic001"
#define ProductID_IAP_2 @"com.cloudbuytest.two"
#define ProductID_IAP_3 @"com.cloudbuytest.three"

@interface PurchaseTest ()

@end

@implementation PurchaseTest

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[CBiOSStoreManager sharedInstance] initialStore];
    [[CBiOSStoreManager sharedInstance] buy:ProductID_IAP_1];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    [[CBiOSStoreManager sharedInstance] releaseStore];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



@end
