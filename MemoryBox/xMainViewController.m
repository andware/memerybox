//
//  xMainViewController.m
//  MemoryBox
//  Loading page
//  Created by joewu on 5/20/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "xMainViewController.h"
#import "xRegisterView.h"
#import "Reachability.h"
#import "ServiceApi.h"
#import "UserDictionary.h"
#import "DataBaseHelper.h"
#import "Constants.h"
#pragma mark
#import "DataBase.h"
#import "User.h"
#import "HomePage.h"
#import "tools.h"
#import "DateUnits.h"

@interface xMainViewController ()

@end
@implementation xMainViewController
@synthesize rememberLoding = _rememberLoding;

tools *tool;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setNeedsStatusBarAppearanceUpdate];// 设置status 字体为白色
    
    _password.secureTextEntry = YES;
    [self readNSUserDefaults]; // 获得保存的用户名和密码

    //对输入框进行监听
    _userName.delegate=self;
    _password.delegate=self;
    
    tool = [[tools alloc]init];
    
    //计算两次的时间差
    DateUnits *dateunits = [[DateUnits alloc]init];
    int time = [dateunits CalculateDateLag];
    [dateunits SaveLoginDate];
    
    NSString *countdays = [[NSString alloc]initWithFormat:@"%d",time];
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"firstLaunch"]) {
    
    // 如果天数不为零就清楚相应的数据
    if (time != 0) {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:@"0" forKey:@"dwNum"];
        [userDefaults removeObjectForKey:@"todayArray"];
    }
    
    // 如果天数大于3就提示用户背诵
    if (time >=3) {
        NSString *days = [[NSString alloc]initWithFormat:@"亲，你已经有%d天没有背诵了，加油吧！",time];
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"背诵提示" message:days delegate:self cancelButtonTitle:@"关闭" otherButtonTitles:nil];
        [days release];
        [alert show];
        [alert release];
    }
    [countdays release];
    [self.view addSubview:[self FindBackPassword]];
        
    }
}

// 找回密码
-(UILabel*)FindBackPassword{
    UILabel *analyze = [[UILabel alloc]initWithFrame:CGRectMake(620.0, 560.0, 100.0, 50.0)];
    analyze.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    analyze.textAlignment = UITextAlignmentCenter;
    analyze.font = [UIFont fontWithName:@"Helvetica" size:15];
    analyze.textColor = [UIColor blueColor];
    analyze.adjustsFontSizeToFitWidth = YES;
    analyze.text = @"找回密码";
    analyze.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(TextClick:)];
    [analyze addGestureRecognizer:singleTap];
    [singleTap release];
    return analyze;
}

// 文字监听响应事件
-(void)TextClick:(id)sender{
    NSString *userName = [[NSUserDefaults standardUserDefaults] stringForKey:@"userName"];
    if (![userName compare:@""]==NSOrderedSame) {
        User *user = [[User alloc]init];
        [user initWithUserName:userName];
        [user findPassword];
        
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"提示" message:@"我们已将新的密码发送到你的邮箱!" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alert show];
        [alert release];
    }

}

// set status bar text white
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

// register button response
- (IBAction)_register:(UIButton *)sender {
    xRegisterView *regirest = [[[xRegisterView alloc]initWithNibName:@"xRegisterView" bundle:nil]autorelease];
    [self presentModalViewController:regirest animated:NO];
}

// delete all dictionary when change user
-(void)DeleteAll{

    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSArray *contents = [fileManager contentsOfDirectoryAtPath:documentsDirectory error:NULL];
    NSEnumerator *e = [contents objectEnumerator];
    NSString *filename;
    while ((filename = [e nextObject])) {
        [fileManager removeItemAtPath:[documentsDirectory stringByAppendingPathComponent:filename] error:NULL];

    }
}

// loading button response
- (IBAction)_loading:(UIButton *)sender {
    BOOL isExistenceNetwork = [tool isExistenceNetwork];
    if (isExistenceNetwork) {
        NSString *responseState = [[NSString alloc]initWithFormat:@"%@",[self connectToService]];
        if ([responseState isEqualToString:@"1"]) {//登陆成功
            [self clearPreUserData];// 清除上一个用户的信息
            [self saveNSUserDefaults];// 保存用户登陆信息
            
            HomePage *startmemory = [[[HomePage alloc]initWithNibName:@"HomePage" bundle:nil]autorelease];
            [self presentModalViewController:startmemory animated:NO];
            [startmemory release];
        }else if([responseState isEqualToString:@"2.2"]){//密码错误
                //设置提示窗口
                UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"登陆提示" message:@"密码错误" delegate:self cancelButtonTitle:@"关闭" otherButtonTitles:nil];
                [alert show];
                [alert release];
            [responseState release];
        }else {//用户名不存在
                //设置提示窗口
                UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"登陆提示" message:@"用户名不存在" delegate:self cancelButtonTitle:@"关闭" otherButtonTitles:nil];
                [alert show];
                [alert release];
            [responseState release];
        }
    }else{
        UIAlertView *myalert = [[UIAlertView alloc] initWithTitle:@"警告" message:@"网络不存在" delegate:self cancelButtonTitle:@"确认" otherButtonTitles:nil,nil];
        [myalert show];
        [myalert release];
    }
    DataBaseHelper *db = [[DataBaseHelper alloc]init];
    [db initDictoinaryTable];//初始化表结构
    [db release];
    
}

// remove all data when change user
-(void)clearPreUserData{
    NSUserDefaults *userDefaultes = [NSUserDefaults standardUserDefaults];
    NSString *userName = [userDefaultes stringForKey:@"userName"];
    
    // 如果是新的用户，就清除历史信息。
    if ([userName compare:_userName.text]!=NSOrderedSame) {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults removeObjectForKey:@"todayArray"];
        [userDefaults removeObjectForKey:@"GoOverWord"];
        [userDefaults removeObjectForKey:@"doubleStruct"];
        [userDefaults removeObjectForKey:@"user_name"];
        [userDefaults removeObjectForKey:@"user_id"];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"newUser"];
        [tool deleteAllData];
    }else{
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"newUser"];
    }
}

// save user info to NSUserDefaults
-(void)saveNSUserDefaults {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setObject:_userName.text forKey:@"userName"];
    [userDefaults setObject:_password.text forKey:@"passWord"];
}

// Read user info from NSUserDefaults
-(void)readNSUserDefaults {
    NSUserDefaults *userDefaultes = [NSUserDefaults standardUserDefaults];
    NSString *userName = [userDefaultes stringForKey:@"userName"];
    NSString *passWord = [userDefaultes stringForKey:@"passWord"];
    _userName.text = userName;
    _password.text = passWord;
}

// Connect to service when login
-(NSString *)connectToService{
    NSError *error;
    NSString *useName = [[NSString alloc]initWithFormat:@"%@",_userName.text];
    NSString *passWord = [[NSString alloc]initWithFormat:@"%@",_password.text];
    NSString *url = [[NSString alloc]initWithFormat:@"%@userLogin/user_name/%@/user_password/%@",ApiUrl,useName,passWord];//api 登陆接口
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    [url release];
    [useName release];
    [passWord release];
    NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    NSDictionary *weatherDic = [NSJSONSerialization JSONObjectWithData:response  options:NSJSONReadingMutableLeaves error:&error];//Json字典
    return [weatherDic objectForKey:@"state"];//解析出返回的状态参数
}

// remember user name and password
- (IBAction)_rememberLoding:(UIButton *)sender {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"rememberLoding"];
}

// change the location when inputing
- (void)moveView:(UITextField *)textField leaveView:(BOOL)leave{
    float screenHeight = 1496; //屏幕尺寸，如果屏幕允许旋转，可根据旋转动态调整
    float keyboardHeight = 748; //键盘尺寸，如果屏幕允许旋转，可根据旋转动态调整
    float statusBarHeight,NavBarHeight,tableCellHeight,textFieldOriginY,textFieldFromButtomHeigth;
    int margin;
    statusBarHeight = [[UIApplication sharedApplication] statusBarFrame].size.height; //屏幕状态栏高度
    NavBarHeight = self.navigationController.navigationBar.frame.size.height; //获取导航栏高度
    
    UITableViewCell *tableViewCell=(UITableViewCell *)textField.superview;
    tableCellHeight = tableViewCell.frame.size.height; //获取单元格高度
    
    CGRect fieldFrame=[self.view convertRect:textField.frame fromView:tableViewCell];
    textFieldOriginY = fieldFrame.origin.y; //获取文本框相对本视图的y轴位置。
    
    //计算文本框到屏幕底部的高度（屏幕高度-顶部状态栏高度-导航栏高度-文本框的的相对y轴位置-单元格高度）
    textFieldFromButtomHeigth = screenHeight - statusBarHeight - NavBarHeight - textFieldOriginY - tableCellHeight;
    
    if(!leave) {
        if(textFieldFromButtomHeigth < keyboardHeight) { //如果文本框到屏幕底部的高度 < 键盘高度
            margin = keyboardHeight - textFieldFromButtomHeigth; // 则计算差距
            keyBoardMargin_ = margin; //keyBoardMargin_ 为成员变量，记录上一次移动的间距,用户离开文本时恢复视图高度
        } else {
            margin= 0;
            keyBoardMargin_ = 0;
        }
    }
    
    float movementDuration = 0.3f; // 动画时间
    
    int movement;
    UIInterfaceOrientation deviceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (deviceOrientation == 3) {
        movement = (leave ? keyBoardMargin_ : -margin); //进入时根据差距移动视图，离开时恢复之前的高度
    }else{
        movement = (leave ? -keyBoardMargin_ : margin); //进入时根据差距移动视图，离开时恢复之前的高度
    }
    
    
    [UIView beginAnimations: @"textFieldAnim" context: nil]; //添加动画
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, -movement/8, 0);
    [UIView commitAnimations];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [self moveView:textField leaveView:NO];
}

- (void)textFieldDidEndEditing:(UITextField *)textField;{
    [self moveView:textField leaveView:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
    
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    
}

// memory warning
- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload{
    [_userName release];
    _userName = nil;
    [_password release];
    _password = nil;
    [self setRememberLoding:nil];
    [self _register:nil];
    [tool release];
    tool = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
}

- (void)dealloc {
    [_userName release];
    [_password release];
    [_rememberLoding release];
    [tool release];
    [super dealloc];
}
@end
