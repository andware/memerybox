//
//  Dictionary.h
//  MemoryBox
//
//  Created by firefly on 13-6-16.
//
//

#import <Foundation/Foundation.h>

#define NONE_VOICE @"0"
#define US_VOICE  @"1"
#define Uk_VOICE  @"2"
#define USUK_VOICE  @"12"

@interface UserDictionary : NSObject
@property (nonatomic, retain) NSString *dictionary_id;
@property (nonatomic, retain) NSString *dictionary_proid;
@property (nonatomic, retain) NSString *buy;
@property (nonatomic, retain) NSString *dictionary_type_id;
@property (nonatomic, retain) NSString *dictionary_type_name;
@property (nonatomic, retain) NSString *dictionary_name;
@property (nonatomic, retain) NSString *dictionary_abc;
@property (nonatomic, retain) NSString *dictionary_has_voice;
@property (nonatomic, retain) NSString *dictionary_time;
@property (nonatomic, retain) NSString *dictionary_has_stick;
@property (nonatomic, retain) NSString *dictionary_state;
@property (nonatomic, retain) NSString *dictionary_price;
@property (nonatomic, retain) NSString *dictionary_pronounce;
@property (nonatomic, retain) NSString *dictionary_complete_time;
@property (nonatomic, retain) NSString *count;
@property (nonatomic, retain) NSString *dictionary_detail;

@end
