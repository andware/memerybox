//
//  DB.m
//  MemoryBox
//
//  Created by Wooden on 1/18/14.
//
//

#import "DB.h"

@implementation DB
@synthesize db;

+ (DB *)getDB{
    static DB *db = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        db = [[DB alloc]init];
    });
    return db;
}

-(id)init{
    if ((self = [super init]))
    {
        NSString *_databaseName = @"db.sqlite";
        NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches"];
        NSString *database_path = [path stringByAppendingPathComponent:_databaseName];
        if (sqlite3_open([database_path UTF8String], &db) != SQLITE_OK)
        {
            [[[UIAlertView alloc]initWithTitle:@"Missing"
                                       message:@"Database file not found"
                                      delegate:nil
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil, nil]show];
        }
    }
    return self;
}

+(void)close{
    DB *database = [DB getDB];
    sqlite3_close(database.db);
}
@end
