//
//  xRegisterView.h
//  MemoryBox
//
//  Created by joewu on 5/20/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface xRegisterView : UIViewController<UITextFieldDelegate>{
    
    IBOutlet UITextField *regirest_userName;
    IBOutlet UITextField *regirest_email;
    IBOutlet UITextField *passWord;
    IBOutlet UITextField *passWord2;
    int keyBoardMargin_;
}
- (IBAction)_do_regirest:(UIButton *)sender;
- (IBAction)_cancel:(UIButton *)sender;
@end
