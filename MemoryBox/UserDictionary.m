//
//  Dictionary.m
//  MemoryBox
//
//  Created by firefly on 13-6-16.
//
//

#import "UserDictionary.h"

@implementation UserDictionary
- (id) initWithCoder: (NSCoder *)coder {
    if (self = [super init]){
        self.dictionary_id = [coder decodeObjectForKey:@"dictionary_id"];
        self.dictionary_proid = [coder decodeObjectForKey:@"dictionary_proid"];
        self.buy = [coder decodeObjectForKey:@"buy"];
        self.dictionary_type_name = [coder decodeObjectForKey:@"dictionary_type_name"];
        self.dictionary_name = [coder decodeObjectForKey:@"dictionary_name"];
        self.dictionary_abc = [coder decodeObjectForKey:@"dictionary_abc"];
        self.dictionary_has_voice = [coder decodeObjectForKey:@"dictionary_has_voice"];
        self.dictionary_time = [coder decodeObjectForKey:@"dictionary_time"];
        self.dictionary_has_stick = [coder decodeObjectForKey:@"dictionary_has_stick"];
        self.dictionary_state = [coder decodeObjectForKey:@"dictionary_state"];
        self.dictionary_price = [coder decodeObjectForKey:@"dictionary_price"];
        self.dictionary_pronounce = [coder decodeObjectForKey:@"dictionary_pronounce"];
        self.dictionary_complete_time = [coder decodeObjectForKey:@"dictionary_complete_time"];
        self.count = [coder decodeObjectForKey:@"count"];
        self.dictionary_detail = [coder decodeObjectForKey:@"dictionary_detail"];
        
    }
    return self;
}

- (void) encodeWithCoder: (NSCoder *)coder  {
    [coder encodeObject:self.dictionary_id forKey:@"dictionary_id"];
    [coder encodeObject:self.dictionary_proid forKey:@"dictionary_proid"];
    [coder encodeObject:self.buy forKey:@"buy"];
    [coder encodeObject:self.dictionary_type_name forKey:@"dictionary_type_name"];
    [coder encodeObject:self.dictionary_name forKey:@"dictionary_name"];
    [coder encodeObject:self.dictionary_abc forKey:@"dictionary_abc"];
    [coder encodeObject:self.dictionary_has_voice forKey:@"dictionary_has_voice"];
    [coder encodeObject:self.dictionary_time forKey:@"dictionary_time"];
    [coder encodeObject:self.dictionary_has_stick forKey:@"dictionary_has_stick"];
    [coder encodeObject:self.dictionary_state forKey:@"dictionary_state"];
    [coder encodeObject:self.dictionary_price forKey:@"dictionary_price"];
    [coder encodeObject:self.dictionary_pronounce forKey:@"dictionary_pronounce"];
    [coder encodeObject:self.dictionary_complete_time forKey:@"dictionary_complete_time"];
    [coder encodeObject:self.count forKey:@"count"];
    [coder encodeObject:self.dictionary_detail forKey:@"dictionary_detail"];
}
@end
															