//
//  Dictionary_Type.h
//  MemoryBox
//
//  Created by firefly on 13-6-22.
//
//

#import <Foundation/Foundation.h>

@interface Dictionary_Type : NSObject
@property (nonatomic, retain) NSString *dictionary_type_id;
@property (nonatomic, retain) NSString *dictionary_type_name;
@end
