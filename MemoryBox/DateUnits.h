//
//  DateUnits.h
//  MemoryBoxRed
//
//  Created by YangJoe on 3/16/14.
//
//

#import <Foundation/Foundation.h>
#import "datetool.h"

@interface DateUnits : NSObject

-(NSDateComponents*)GetSystemDate;
-(void)SaveLoginDate;
-(int)CalculateDateLag;

@end
