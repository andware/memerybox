//
//  NSMutableArray+Helpers.h
//  MemoryBox
//
//  Created by TangMonk on 13-10-13.
//
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (Helpers)
- (void)shuffle;
@end
