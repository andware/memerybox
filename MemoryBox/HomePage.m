//
//  HomePage.m
//  MemoryBox
//
//  Created by iMac on 13-8-30.
//
//

#import "HomePage.h"
#import "ServiceApi.h"
#import "User.h"
#import "UserDictionary.h"
#import "DataBaseHelper.h"
#import <QuartzCore/QuartzCore.h>
#import "DataBase.h"
#import "xMainViewController.h"
#import <AudioToolbox/AudioToolbox.h>
#import "CBiOSStoreManager.h" // 引入词库购买类
#import "tools.h" // 引入工具类[self.window makeKeyAndVisible];
#import "Init.h"
#import "ReciteView.h"
#import "DateUnits.h"
#import "Algorithm.h"
#import "wrongWordRevew.h"

@interface HomePage ()

@end

@implementation HomePage
@synthesize start_dialog;
@synthesize dics;
@synthesize progressHUD = _progressHUD;
// 二级菜单
@synthesize headViewArray;
@synthesize doubleStruct;

NSMutableArray *viewArray1,*viewArray2,*viewArray3;
NSMutableArray *newDownloadDictionaryArray;
ServiceApi *api;
User *user;
NSString *userName;
UserDictionary *dic;
int num;
int records;
NSUserDefaults *userDefaultes;
tools *tool;
bool isPurchase;
BOOL _doneAnything = false;

#pragma mark - View lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _list = [[NSMutableArray alloc] initWithCapacity:1];
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    [self setNeedsStatusBarAppearanceUpdate];// 设置status 字体为白色
    
    // 初始化---start
    Init *init = [[Init alloc]init];
    [init changeWordDay];
    
//    NSMutableArray *ddd = [init GetTodayWords];
    // ------end
    
    tool = [[tools alloc]init];// 初始化工具类
    dic = [[UserDictionary alloc]init];
    
    // 数组初始化
    percent = [[NSMutableArray alloc]init]; // 初始化词库进度数组
    viewArray1=[[NSMutableArray alloc]init];// 初始化左边列表空间元素储存数组
    viewArray2=[[NSMutableArray alloc]init];// 初始化词库详情空间元素数组
    dics = [[NSMutableArray alloc]init]; // 初始化词库列表数组
    dicPause = [[NSMutableArray alloc]init];// 初始化词库暂停使用状态数组
    newDownloadDictionaryArray = [[NSMutableArray alloc]init];
    api =[[ServiceApi alloc]init];     // 初始化数据库操作类
    userDefaultes = [NSUserDefaults standardUserDefaults]; // 初始化 NSUserDefaults
    userName = [userDefaultes stringForKey:@"userName"];// 获得用户名
    user =[[User alloc]init];
    NSString *user_name = (NSString*)[tool readObkect:@"user_name"];// 读取用户信息
    if (user_name == nil) {
        [user initWithUserName:userName]; // 初始化user
        [tool saveObject:user.user_name saveKey:@"user_name"];// 保存用户名
        [tool saveObject:user.user_id saveKey:@"user_id"];// 保存用ID
    }
    doubleStruct = [[NSMutableArray alloc]init];// 初始化有二级结构的词库保存数组
    
    // 判断是不是IOS7版本
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"version"]) {
        // 右边-上面-导航栏
        UIImageView *top_status_bar = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,1024,20)];
        UIImage *status_bar = [UIImage imageNamed:@"status_bar"];
        [top_status_bar initWithImage:status_bar];
        [self.view addSubview:top_status_bar];
        [top_status_bar release];
    }
        
    // 右边-上面-导航栏
    // 判断是不是IOS7版本
    UIImageView *right_top_bar;
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"version"]) {
        right_top_bar = [[UIImageView alloc] initWithFrame:CGRectMake(301,20,723,45)];
    }else{
        right_top_bar = [[UIImageView alloc] initWithFrame:CGRectMake(301,0,723,45)];
    }
    UIImage *right_top = [UIImage imageNamed:@"right_top_bar"];
    [right_top_bar initWithImage:right_top];
    [self.view addSubview:right_top_bar];
    [right_top_bar release];
    
    // 右边—上面-开始背诵
    UIButton *right_top_start_learn=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"version"]) {
        right_top_start_learn.frame=CGRectMake(950,30,25,25);
    }else{
        right_top_start_learn.frame=CGRectMake(950,10,25,25);
    }
    right_top_start_learn.tag = 0;
    UIImage *start_learn_image =[UIImage imageNamed:@"top_right_start_learn"];
    [right_top_start_learn setBackgroundImage:start_learn_image forState:UIControlStateNormal];
    [right_top_start_learn addTarget:self action:@selector(RightTopButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:right_top_start_learn];
    
    // 右边—上面-设置
    UIButton *right_top_setting=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"version"]) {
            right_top_setting.frame=CGRectMake(900,30,25,25);
    }else{
            right_top_setting.frame=CGRectMake(900,10,25,25);
    }
    right_top_setting.tag = 1;
    UIImage *setting_image =[UIImage imageNamed:@"top_right_setting"];
    [right_top_setting setBackgroundImage:setting_image forState:UIControlStateNormal];
    [right_top_setting addTarget:self action:@selector(RightTopButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:right_top_setting];
    
    // 右边—上面-用户中心
    UIButton *right_top_user=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"version"]) {
        right_top_user.frame=CGRectMake(850,30,25,25);
    }else{
        right_top_user.frame=CGRectMake(850,10,25,25);
    }
    right_top_user.tag = 2;
    UIImage *user_image =[UIImage imageNamed:@"top_right_user"];
    [right_top_user setBackgroundImage:user_image forState:UIControlStateNormal];
    [right_top_user addTarget:self action:@selector(RightTopButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:right_top_user];

    // 右边-中心背景
    UIImageView *right_background;
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"version"]) {
        right_background = [[UIImageView alloc] initWithFrame:CGRectMake(301,65,723,703)];
    }else{
        right_background = [[UIImageView alloc] initWithFrame:CGRectMake(301,45,723,703)];
    }
    UIImage *R_background = [UIImage imageNamed:@"right_background"];
    [right_background initWithImage:R_background];
    
    // 右边logo
    UIImageView *right_logo;
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"version"]) {
        right_logo = [[UIImageView alloc] initWithFrame:CGRectMake(150,250,420,181)];
    }else{
        right_logo = [[UIImageView alloc] initWithFrame:CGRectMake(150,230,420,181)];
    }
    UIImage *logo_view = [UIImage imageNamed:@"right_logo"];
    [right_logo initWithImage:logo_view];
    [right_background addSubview:right_logo];
    
    // 记忆盒子链接
    UILabel *dictionary_name = [[UILabel alloc]initWithFrame:CGRectMake(600.0, 660.0, 100.0, 35.0)];
    dictionary_name.text = @"使用帮助";
    dictionary_name.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    dictionary_name.textColor = [UIColor blackColor];
    dictionary_name.font = [UIFont fontWithName:@"Helvetica" size:18];
    UITapGestureRecognizer *tapRecognizer1=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickurl1:)];
    dictionary_name.userInteractionEnabled=YES;
    [dictionary_name addGestureRecognizer:tapRecognizer1];
    [right_background addSubview:dictionary_name];
    [right_background bringSubviewToFront:dictionary_name];
    [right_background setUserInteractionEnabled:YES];

    
    [self.view addSubview:right_background];
    [right_logo release];
    [right_background release];
    
    // 添加左边view
    [self DataPackgeList];
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"firstLaunch"]) {
        [self LargeImage];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"firstLaunch"];// 如果是第一次登陆就取消第一次标记
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"everLaunched"];
    }
}

-(void)clickurl1:(id)sender
{
    NSString* path=[NSString stringWithFormat:@"http://jiyihezi.net/red"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:path]];
}

// 设置 status bar 字体为白色
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

// 创建左边的列表视图
-(void) DataPackgeList{
    dicPause = [[NSMutableArray alloc]init];// 初始化词库暂停使用状态数组
    // -------获得保存的词库和 词库的启用状态信息-------start
    tools *tool2 = [[tools alloc]init];
    doubleStruct = [tool2 readArray:@"doubleStruct"];
    NSString *user_name = (NSString*)[tool2 readObkect:@"user_name"];
    NSString *user_id = (NSString*)[tool2 readObkect:@"user_id"];
    
    // 获得上次刷新的天数
    NSString *refreshDays = [tool2 CountDays];
    int refreshday = [refreshDays intValue];
    
    if ([doubleStruct count] != 0 && refreshday < 3) {
    }else {
        // 获得有二级结构的词库列表
        doubleStruct = [api getUserHadBuyDictionary2SencondList:user_name userId:user_id];
        NSLog(@"---%@,----%@",user_name,user_id);
        [tool2 saveArray:doubleStruct saveKey:@"doubleStruct"];// 保存二级菜单数组
    }
    
    for (int i =0; i < [doubleStruct count]; i++) {
        secondList *list = [doubleStruct objectAtIndex:i];
        for (int j = 0; j < [list.secondLevel count]; j++) {
            dic = [list.secondLevel objectAtIndex:j];
            DataBaseHelper *PrecentNum = [[DataBaseHelper alloc]init];
            BOOL use = [PrecentNum isDictionaryPause:dic.dictionary_id userId:user_id];
            [PrecentNum release];
            if (use) {
                [dicPause addObject:@"1"];
            }else{
                [dicPause addObject:@"0"];
            }
        }
    }

    // -------获得保存的词库和 词库的启用状态信息-------start
    
    [tool2 saveArray:dicPause saveKey:@"usedDicsArray"];// 保存词库的使用和停止状态
    // 左边-上面-导航栏
    // 判断是不是IOS7版本
    UIImageView *left_top_bar;
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"version"]) {
        left_top_bar = [[UIImageView alloc] initWithFrame:CGRectMake(0,20,300,45)];
    }else{
        left_top_bar = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,300,45)];
    }
    UIImage *left_top = [UIImage imageNamed:@"left_top_bar"];
    [left_top_bar initWithImage:left_top];
    [self.view addSubview:left_top_bar];
    [viewArray1 addObject:left_top_bar];
    
    // 左边-上面-刷新
    UIButton *left_top_refresh=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    // 判断是不是IOS7版本
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"version"]) {
        left_top_refresh.frame=CGRectMake(6,28,45,30);
    }else{
        left_top_refresh.frame=CGRectMake(6,8,45,30);
    }
    left_top_refresh.tag = 0;
    UIImage *refresh_image =[UIImage imageNamed:@"left_top_refresh"];
    [left_top_refresh setBackgroundImage:refresh_image forState:UIControlStateNormal];
    [left_top_refresh addTarget:self action:@selector(LeftTopButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:left_top_refresh];
    [viewArray1 addObject:left_top_refresh];
    
    // 左边-背景
    // 判断是不是IOS7版本
    UIImageView *left_background;
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"version"]) {
        left_background = [[UIImageView alloc] initWithFrame:CGRectMake(0,65,300,703)];
    }else{
        left_background = [[UIImageView alloc] initWithFrame:CGRectMake(0,45,300,703)];
    }
    UIImage *L_background = [UIImage imageNamed:@"left_background"];
    [left_background initWithImage:L_background];
    [self.view addSubview:left_background];
    [viewArray1 addObject:left_background];
    [left_background release];
    
    // ---------二级菜单
    [self loadModel];
    // 判断是不是IOS7版本
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"version"]) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 65,300,703) style:UITableViewStyleGrouped];
    }else{
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 45,300,703) style:UITableViewStyleGrouped];
    }
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorColor = [UIColor clearColor];
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    [viewArray1 addObject:_tableView];
    [_tableView release];
    
    // --------二级菜单
    self.view.backgroundColor = [UIColor blackColor];
    
    // --------列表刷新
    if (_refreshHeaderView == nil) {
		EGORefreshTableHeaderView *view = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - self.tableView.bounds.size.height, self.view.frame.size.width, self.tableView.bounds.size.height)];
		view.delegate = self;
		[self.tableView addSubview:view];
		_refreshHeaderView = view;
		[view release];
	}
    // --------列表刷新
}

// -------------------------二级菜单---start

// 添加一级菜单内容
- (void)loadModel {
    _currentRow = -1;
    headViewArray = [[NSMutableArray alloc]init ];
    for(int i = 0;i< [doubleStruct count] ;i++) {
		HeadView* headview = [[HeadView alloc] init];
        headview.delegate = self;
		headview.section = i;

        //获得文件名
        UILabel *dictionary_name = [[UILabel alloc]initWithFrame:CGRectMake(40.0, 5.0, 220.0, 35.0)];
        secondList *list = [doubleStruct objectAtIndex:i];
        dictionary_name.text = [NSString stringWithFormat:@"%@",list.firstLevel];
        dictionary_name.backgroundColor = [UIColor clearColor]; //可以去掉背景色
        dictionary_name.textColor = [UIColor blackColor];
        dictionary_name.font = [UIFont fontWithName:@"Helvetica" size:20];
        [headview.backBtn addSubview:dictionary_name];
        [dictionary_name release];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10,13,20,20)];
        UIImage *image = [UIImage imageNamed:@"book_icon"];
        [imageView initWithImage:image];
        [headview.backBtn addSubview:imageView];
        [imageView release];
        
        // -----添加内容结束
        [self.headViewArray addObject:headview];
	}
}

#pragma mark - TableViewdelegate&&TableViewdataSource

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    HeadView* headView = [self.headViewArray objectAtIndex:indexPath.section];
    
    return headView.open?45:0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 45;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return [self.headViewArray objectAtIndex:section];
}

// 设置每个二级列表的数量
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    HeadView* headView = [self.headViewArray objectAtIndex:section];
    
    tools *tool = [[tools alloc]init];
    NSMutableArray *wordsArray =  [tool readArray:@"doubleStruct"];
    secondList *list = [wordsArray objectAtIndex:section];
    int intString = [list.secondLevel count];
    [tool release];
    return headView.open?intString:0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [self.headViewArray count];
}

// 添加二级菜单的内容
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *indentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:indentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:indentifier];
        UIButton* backBtn=  [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 340, 45)];
        backBtn.tag = 20000;
        [backBtn setBackgroundImage:[UIImage imageNamed:@"btn_on"] forState:UIControlStateHighlighted];
        backBtn.userInteractionEnabled = NO;
        [cell.contentView addSubview:backBtn];
        [backBtn release];
    }
    UIButton* backBtn = (UIButton*)[cell.contentView viewWithTag:20000];
    HeadView* view = [self.headViewArray objectAtIndex:indexPath.section];
    [backBtn setBackgroundImage:[UIImage imageNamed:@"btn_2_nomal"] forState:UIControlStateNormal];
    
    if (view.open) {
        if (indexPath.row == _currentRow) {
            [backBtn setBackgroundImage:[UIImage imageNamed:@"btn_nomal"] forState:UIControlStateNormal];
        }
    }

    tools *tool = [[tools alloc]init];
    NSMutableArray *wordsArray =  [tool readArray:@"doubleStruct"];
    
    int i = 0;
    for (int j = 0; j < [wordsArray count]; j++) {
        secondList *second = [wordsArray objectAtIndex:j];
        NSMutableArray *secondDic = second.secondLevel;
        for (int k = 0 ; k < [secondDic count]; k++) {
            if (j == indexPath.section && k == indexPath.row) {
                break;
                NSLog(@"same");
            }else{
                i++;
            }
        }
        
        if (j == indexPath.section) {
            break;
        }
    }

    
    
    secondList *second = [wordsArray objectAtIndex:indexPath.section];
    NSMutableArray *secondDic = second.secondLevel;
    dic = [secondDic objectAtIndex:indexPath.row];
    
    // 清楚 cell 中的内容
    NSArray *uis = [cell.contentView subviews];
    for (int i = 0; i <[uis count]; i++) {
        [[uis objectAtIndex:i] removeFromSuperview];
    }
    // 添加列表文字
    UILabel *dictionary_name = [[UILabel alloc]initWithFrame:CGRectMake(40.0, 5.0, 220.0, 35.0)];
    dictionary_name.text = [NSString stringWithFormat:@"%@",dic.dictionary_name];
    dictionary_name.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    dictionary_name.textColor = [UIColor blackColor];
    dictionary_name.font = [UIFont fontWithName:@"Helvetica" size:18];
    [cell.contentView addSubview:dictionary_name];
    [dictionary_name release];
    dicPause = [tool readArray:@"usedDicsArray"];
    [tool release];
    NSLog(@"--%d; -%@",i,[dicPause objectAtIndex:i]);
    // 是否启用图标
    NSString *useString = [NSString stringWithFormat:@"%@",[dicPause objectAtIndex:i]];
    if ([useString compare:@"1"]==NSOrderedSame) {
        // 图标
        UIButton *left_list_item_logo=[UIButton buttonWithType:UIButtonTypeRoundedRect];
        left_list_item_logo.frame=CGRectMake(10,15,21,21);
        UIImage *refresh_image =[UIImage imageNamed:@"dic_unusered"];
        left_list_item_logo.tag = (long)dic;
        [left_list_item_logo setBackgroundImage:refresh_image forState:UIControlStateNormal];
        [left_list_item_logo addTarget:self action:@selector(DicState:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:left_list_item_logo];
    }else{
        // 图标
        UIButton *left_list_item_logo=[UIButton buttonWithType:UIButtonTypeRoundedRect];
        left_list_item_logo.frame=CGRectMake(10,15,21,21);
        UIImage *refresh_image =[UIImage imageNamed:@"left_list_logo"];
        left_list_item_logo.tag = (int)dic;
        [left_list_item_logo setBackgroundImage:refresh_image forState:UIControlStateNormal];
        [left_list_item_logo addTarget:self action:@selector(DicState:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:left_list_item_logo];
    }
    // 添加分界线
    UIImageView* line = [[UIImageView alloc]initWithFrame:CGRectMake(0, 44, 340, 1)];
    line.backgroundColor = [UIColor grayColor];
    [cell.contentView addSubview:line];
    [line release];
    return cell;
}

// 二级菜单选中cell 的监听事件
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    _currentRow = indexPath.row;
    [_tableView reloadData];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    tools *tool2 = [[tools alloc]init];
    NSMutableArray *wordsArray =  [tool2 readArray:@"doubleStruct"];
    
    int i = 0;
    for (int j = 0; j < [wordsArray count]; j++) {
        secondList *second = [wordsArray objectAtIndex:j];
        NSMutableArray *secondDic = second.secondLevel;
        for (int k = 0 ; k < [secondDic count]; k++) {
            if (j == indexPath.section && k == indexPath.row) {
                break;
                NSLog(@"same");
            }else{
                i++;
            }
        }
        
        if (j == indexPath.section) {
            break;
        }
    }
    
    secondList *second = [wordsArray objectAtIndex:indexPath.section];
    NSMutableArray *secondDic = second.secondLevel;
    dic = [secondDic objectAtIndex:indexPath.row];

    for (int i = 0; i<[viewArray1 count]; i++) {
        [[viewArray1 objectAtIndex:i] setHidden:YES];
    }
    [self DataPackgeDetail:i uderDic:dic];
    [self releaseDialog];// 取消dialog显示
}

// 一级菜单响应事件
#pragma mark - HeadViewdelegate
-(void)selectedWith:(HeadView *)view{
    [self releaseDialog];// 取消dialog显示
    _currentRow = -1;
    if (view.open) {
        for(int i = 0;i<[headViewArray count];i++) {
            HeadView *head = [headViewArray objectAtIndex:i];
            head.open = NO;
            [head.backBtn setBackgroundImage:[UIImage imageNamed:@"btn_momal"] forState:UIControlStateNormal];
        }
        [_tableView reloadData];
        return;
    }
    _currentSection = view.section;
    [self reset];
}

//界面重置
- (void)reset {
    for(int i = 0;i<[headViewArray count];i++)
    {
        HeadView *head = [headViewArray objectAtIndex:i];
        
        if(head.section == _currentSection)
        {
            head.open = YES;
            [head.backBtn setBackgroundImage:[UIImage imageNamed:@"btn_nomal"] forState:UIControlStateNormal];
            
        }else {
            [head.backBtn setBackgroundImage:[UIImage imageNamed:@"btn_momal"] forState:UIControlStateNormal];
            
            head.open = NO;
        }
    }
    [_tableView reloadData];
    
}
// ----------------------------二级菜单---end

// 创建词库详情视图
-(void) DataPackgeDetail:(int) dicId uderDic:(UserDictionary*)mydic{
    num = dicId;
    
    // 左边-上面-导航栏
    // 判断是不是IOS7版本
    UIImageView *left_top_bar;
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"version"]) {
         left_top_bar = [[UIImageView alloc] initWithFrame:CGRectMake(0,20,300,45)];
    }else{
        left_top_bar = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,300,45)];
    }
    UIImage *left_top = [UIImage imageNamed:@"list_detail"];
    [left_top_bar initWithImage:left_top];
    [self.view addSubview:left_top_bar];
    [viewArray2 addObject:left_top_bar];

    // 左边-上面-返回
    UIButton *left_top_refresh=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    // 判断是不是IOS7版本
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"version"]) {
        left_top_refresh.frame=CGRectMake(6,28,45,30);
    }else{
        left_top_refresh.frame=CGRectMake(6,8,45,30);
    }
    left_top_refresh.tag = 0;
    UIImage *refresh_image =[UIImage imageNamed:@"left_top_back"];
    [left_top_refresh setBackgroundImage:refresh_image forState:UIControlStateNormal];
    [left_top_refresh addTarget:self action:@selector(packageDetail:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:left_top_refresh];
    [viewArray2 addObject:left_top_refresh];
    
    // 左边-背景
    // 判断是不是IOS7版本
    UIImageView *left_background;
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"version"]) {
        left_background = [[UIImageView alloc] initWithFrame:CGRectMake(0,65,300,703)];
    }else{
        left_background = [[UIImageView alloc] initWithFrame:CGRectMake(0,45,300,703)];
    }
    UIImage *L_background = [UIImage imageNamed:@"left_background"];
    [left_background initWithImage:L_background];
    [self.view addSubview:left_background];
    [viewArray2 addObject:left_background];
    
    //显示词库名
    UILabel *dictionary_name = [[UILabel alloc]initWithFrame:CGRectMake(50.0, 100.0, 220.0, 35.0)];
    dictionary_name.text = [NSString stringWithFormat:@"%@",mydic.dictionary_name];
    NSLog(@"last--> %@",mydic.dictionary_name);
    dictionary_name.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    dictionary_name.textColor = [UIColor blackColor];
    dictionary_name.font = [UIFont fontWithName:@"Helvetica" size:23];
    [self.view addSubview:dictionary_name];
    [viewArray2 addObject:dictionary_name];
    
    // 词库总数量
    UILabel *dictionary_count = [[UILabel alloc]initWithFrame:CGRectMake(50.0, 140.0, 220.0, 35.0)];
    dictionary_count.text = [NSString stringWithFormat:@"词库总数量:%@",mydic.count];
    dictionary_count.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    dictionary_count.textColor = [UIColor blackColor];
    dictionary_count.font = [UIFont fontWithName:@"Helvetica" size:18];
    [self.view addSubview:dictionary_count];
    [viewArray2 addObject:dictionary_count];
    
    //已完成数量
    UILabel *dictionary_finished_num = [[UILabel alloc]initWithFrame:CGRectMake(50.0, 160.0, 220.0, 35.0)];
    
    DataBaseHelper *helper = [[DataBaseHelper alloc]init];
    int finishWords = [helper getWordDayNotEqualZero:mydic.dictionary_id userId:user.user_id];
    dictionary_finished_num.text = [NSString stringWithFormat:@"已完成数量:%d",finishWords];
    dictionary_finished_num.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    dictionary_finished_num.textColor = [UIColor blackColor];
    dictionary_finished_num.font = [UIFont fontWithName:@"Helvetica" size:18];
    [self.view addSubview:dictionary_finished_num];
    [viewArray2 addObject:dictionary_finished_num];
    
    /*
     *判断词库文件夹路径是否存在
     */
    NSString *documentsDirectory2 = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches"];
    NSFileManager *fileManager2 = [NSFileManager defaultManager];
    NSString *filePath2 =  [documentsDirectory2 stringByAppendingPathComponent:mydic.dictionary_id];
    BOOL isDirExist = [fileManager2 fileExistsAtPath:filePath2];
    
    
    // 判断是否购买
    if (![mydic.buy compare:@"1"] == NSOrderedSame && !isDirExist) {// 没有购买
        // 购买
        UIButton *BuyDicBut=[UIButton buttonWithType:UIButtonTypeRoundedRect];
        BuyDicBut.frame=CGRectMake(15,300,270,43);
        BuyDicBut.tag = (long)dic;
        NSString *price = [NSString stringWithFormat:@"￥%@",mydic.dictionary_price];
        [BuyDicBut setTitle:price forState:UIControlStateNormal];
        BuyDicBut.titleLabel.font = [UIFont fontWithName:@"ArialRoundedMTBold" size:20];
        [BuyDicBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        UIImage *buy_image =[UIImage imageNamed:@"buy_dictionary"];
        [BuyDicBut setBackgroundImage:buy_image forState:UIControlStateNormal];
        [BuyDicBut addTarget:self action:@selector(Purchase:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:BuyDicBut];
        [viewArray2 addObject:BuyDicBut];
        
        // 显示词库信息
        UILabel *dictionary_information = [[UILabel alloc]initWithFrame:CGRectMake(25, 350, 250, 200)];
        NSString *dicDetial;
        if ([mydic.dictionary_detail compare:nil]==NSOrderedSame) {
            dicDetial = @" ";
        }else{
            dicDetial = mydic.dictionary_detail;
        }
        dictionary_information.text = [NSString stringWithFormat:@"%@",dicDetial];
        NSLog(@"last--> %@",mydic.dictionary_detail);
        [dictionary_information setNumberOfLines:0];
        dictionary_information.lineBreakMode = UILineBreakModeWordWrap;
        dictionary_information.backgroundColor = [UIColor clearColor]; //可以去掉背景色
        dictionary_information.textColor = [UIColor blackColor];
        dictionary_information.font = [UIFont fontWithName:@"Helvetica" size:20];
        [self.view addSubview:dictionary_information];
        [viewArray2 addObject:dictionary_information];
        
        //-------隐藏--start----
        // 下载
        UIButton *download=[UIButton buttonWithType:UIButtonTypeRoundedRect];
        download.frame=CGRectMake(15,300,270,43);
        download.tag = (long)mydic;
        [download setHidden:YES];
        UIImage *download_image =[UIImage imageNamed:@"download"];
        [download setBackgroundImage:download_image forState:UIControlStateNormal];
        [download addTarget:self action:@selector(downloaddic:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:download];
        [viewArray2 addObject:download];
        
        // 设置发音
        UIImageView *set_tone = [[UIImageView alloc] initWithFrame:CGRectMake(15,200,273,50)];
        UIImage *tone = [UIImage imageNamed:@"set_tone"];
        [set_tone initWithImage:tone];
        
        DataBase *pronucations = [[DataBase alloc]init];
        NSString *pronucation = [pronucations getCurrentDictionaryPronounce:mydic];
        // 转换器-设置发音
        UISwitch *switchButton = [[UISwitch alloc] initWithFrame:CGRectMake(190, 12, 20, 10)];
        [set_tone setHidden:YES];
        if ([pronucation compare:@"US"] == NSOrderedSame) {
            [switchButton setOn:YES];
        }else{
            [switchButton setOn:NO];
        }
        [switchButton addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
        [set_tone addSubview:switchButton];
        [set_tone bringSubviewToFront:switchButton];
        [set_tone setUserInteractionEnabled:YES];
        [self.view addSubview:set_tone];
        [viewArray2 addObject:set_tone];
        
        // 设置完成时间
        UIImageView *setFinishTime = [[UIImageView alloc] initWithFrame:CGRectMake(15,270,273,90)];
        UIImage *time = [UIImage imageNamed:@"set_finish_time"];
        [setFinishTime initWithImage:time];
        
        //获得日期和词汇量
        DataBase *dateAndWord = [[DataBase alloc]init];
        NSString *days = [dateAndWord getCompleteTime:mydic];
        
        // 时间
        date = [[UILabel alloc]initWithFrame:CGRectMake(15.0, 8.0, 260.0, 35.0)];
        
        //日背诵量
        count = [[UILabel alloc]initWithFrame:CGRectMake(15.0, 50.0, 260.0, 35.0)];
        
        if ([days compare:nil]!=NSOrderedSame && [days compare:@""]!=NSOrderedSame && [days compare:@"0"]!=NSOrderedSame) {
            NSString *dayWords = [self dayWords:mydic finishDay:days];
            count.text = [NSString stringWithFormat:@"日背诵量: %@",dayWords];
            NSString *finishTime = [self getFinishDate:days];
            date.text = [NSString stringWithFormat:@"预计完成时间: %@",finishTime];
        }else{
            count.text = [NSString stringWithFormat:@"日背诵量: 25"];
            date.text = [NSString stringWithFormat:@"预计完成时间: %@",[self SuggestDate:dic]];
        }
        
        date.backgroundColor = [UIColor clearColor]; //可以去掉背景色
        date.textColor = [UIColor blackColor];
        date.font = [UIFont fontWithName:@"Helvetica" size:18];
        [setFinishTime addSubview:date];
        
        count.backgroundColor = [UIColor clearColor]; //可以去掉背景色
        count.textColor = [UIColor blackColor];
        count.font = [UIFont fontWithName:@"Helvetica" size:18];
        [setFinishTime addSubview:count];
        
        setFinishTime.userInteractionEnabled = YES;
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ImageViewClick)];
        [setFinishTime addGestureRecognizer:singleTap];
        [singleTap release];
        
        [setFinishTime setHidden:YES];
        [self.view addSubview:setFinishTime];
        [viewArray2 addObject:setFinishTime];
        
        // 背诵当个词库
        UIButton *skim=[UIButton buttonWithType:UIButtonTypeRoundedRect];
        skim.frame=CGRectMake(15,600,270,55);
        skim.tag = dicId;
        UIImage *skim_image =[UIImage imageNamed:@"skim_dic"];
        [skim setBackgroundImage:skim_image forState:UIControlStateNormal];
        [skim addTarget:self action:@selector(skimDic:) forControlEvents:UIControlEventTouchUpInside];
        [skim setHidden:YES];
        [self.view addSubview:skim];
        [viewArray2 addObject:skim];
        
        // 删除
        UIButton *delete=[UIButton buttonWithType:UIButtonTypeRoundedRect];
        delete.frame=CGRectMake(15,690,270,43);
        delete.tag = (long)mydic;
        UIImage *delete_image =[UIImage imageNamed:@"left_delete"];
        [delete setBackgroundImage:delete_image forState:UIControlStateNormal];
        [delete addTarget:self action:@selector(Delete:) forControlEvents:UIControlEventTouchUpInside];
        [delete setHidden:YES];
        
        [self.view addSubview:delete];
        [viewArray2 addObject:delete];
        
        //-------隐藏--结束------

    }else{// 已经购买
        /*
         *获得文件名
         */
        NSString *dicName = [NSString stringWithFormat:@"%@.zip",mydic.dictionary_id];
        
        /*
         *判断词库文件夹路径是否存在
         */
        NSString *documentsDirectory2 = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches"];
        NSFileManager *fileManager2 = [NSFileManager defaultManager];
        NSString *filePath2 =  [documentsDirectory2 stringByAppendingPathComponent:mydic.dictionary_id];
        BOOL isDirExist = [fileManager2 fileExistsAtPath:filePath2];
        
        /*
         *获得压缩文件是否存在
         */
        NSString *documentsDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches"];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSString *filePath =  [documentsDirectory stringByAppendingPathComponent:dicName];
        BOOL isFileExist = [fileManager fileExistsAtPath:filePath];
        if((!isFileExist)&&(!isDirExist)) //如果压缩文件不存在并且词库文件夹不在
        {
            // 下载
            UIButton *download=[UIButton buttonWithType:UIButtonTypeRoundedRect];
            download.frame=CGRectMake(15,300,270,43);
            download.tag = (long)mydic;
            UIImage *download_image =[UIImage imageNamed:@"download"];
            [download setBackgroundImage:download_image forState:UIControlStateNormal];
            [download addTarget:self action:@selector(downloaddic:) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:download];
            [viewArray2 addObject:download];
            
            
            //-------隐藏--start----
            // 设置发音
            UIImageView *set_tone = [[UIImageView alloc] initWithFrame:CGRectMake(15,200,273,50)];
            UIImage *tone = [UIImage imageNamed:@"set_tone"];
            [set_tone initWithImage:tone];
            
            DataBase *pronucations = [[DataBase alloc]init];
            NSString *pronucation = [pronucations getCurrentDictionaryPronounce:mydic];
            // 转换器-设置发音
            UISwitch *switchButton = [[UISwitch alloc] initWithFrame:CGRectMake(190, 12, 20, 10)];
            [set_tone setHidden:YES];
            if ([pronucation compare:@"US"] == NSOrderedSame) {
                [switchButton setOn:YES];
            }else{
                [switchButton setOn:NO];
            }
            [switchButton addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
            [set_tone addSubview:switchButton];
            [set_tone bringSubviewToFront:switchButton];
            [set_tone setUserInteractionEnabled:YES];
            [self.view addSubview:set_tone];
            [viewArray2 addObject:set_tone];
            
            // 设置完成时间
            UIImageView *setFinishTime = [[UIImageView alloc] initWithFrame:CGRectMake(15,270,273,90)];
            UIImage *time = [UIImage imageNamed:@"set_finish_time"];
            [setFinishTime initWithImage:time];
            
            //获得日期和词汇量
            DataBase *dateAndWord = [[DataBase alloc]init];
            NSString *days = [dateAndWord getCompleteTime:mydic];

            // 时间
            date = [[UILabel alloc]initWithFrame:CGRectMake(15.0, 8.0, 260.0, 35.0)];
            
            //日背诵量
            count = [[UILabel alloc]initWithFrame:CGRectMake(15.0, 50.0, 260.0, 35.0)];
            
            if ([days compare:nil]!=NSOrderedSame && [days compare:@""]!=NSOrderedSame && [days compare:@"0"]!=NSOrderedSame) {
                NSString *dayWords = [self dayWords:mydic finishDay:days];
                count.text = [NSString stringWithFormat:@"日背诵量: %@",dayWords];
                NSString *finishTime = [self getFinishDate:days];
                date.text = [NSString stringWithFormat:@"预计完成时间: %@",finishTime];
            }else{
                count.text = [NSString stringWithFormat:@"日背诵量: 25"];
                date.text = [NSString stringWithFormat:@"预计完成时间: %@",[self SuggestDate:mydic]];
            }
            
            date.backgroundColor = [UIColor clearColor]; //可以去掉背景色
            date.textColor = [UIColor blackColor];
            date.font = [UIFont fontWithName:@"Helvetica" size:18];
            [setFinishTime addSubview:date];
            
            count.backgroundColor = [UIColor clearColor]; //可以去掉背景色
            count.textColor = [UIColor blackColor];
            count.font = [UIFont fontWithName:@"Helvetica" size:18];
            [setFinishTime addSubview:count];
            
            setFinishTime.userInteractionEnabled = YES;
            UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ImageViewClick)];
            [setFinishTime addGestureRecognizer:singleTap];
            [singleTap release];
            
            [setFinishTime setHidden:YES];
            [self.view addSubview:setFinishTime];
            [viewArray2 addObject:setFinishTime];
            
            // 显示词库信息
            UILabel *dictionary_information = [[UILabel alloc]initWithFrame:CGRectMake(25, 350, 250, 200)];
            NSString *dicDetial;
            if ([mydic.dictionary_detail compare:nil]==NSOrderedSame) {
                dicDetial = @" ";
            }else{
                dicDetial = mydic.dictionary_detail;
            }
            dictionary_information.text = [NSString stringWithFormat:@"%@",dicDetial];
            NSLog(@"last--> %@",mydic.dictionary_detail);
            [dictionary_information setNumberOfLines:0];
            dictionary_information.lineBreakMode = UILineBreakModeWordWrap;
            dictionary_information.backgroundColor = [UIColor clearColor]; //可以去掉背景色
            dictionary_information.textColor = [UIColor blackColor];
            dictionary_information.font = [UIFont fontWithName:@"Helvetica" size:20];
            [self.view addSubview:dictionary_information];
            [viewArray2 addObject:dictionary_information];
            
            // 背诵当个词库
            UIButton *skim=[UIButton buttonWithType:UIButtonTypeRoundedRect];
            skim.frame=CGRectMake(15,600,270,55);
            skim.tag = dicId;
            UIImage *skim_image =[UIImage imageNamed:@"skim_dic"];
            [skim setBackgroundImage:skim_image forState:UIControlStateNormal];
            [skim addTarget:self action:@selector(skimDic:) forControlEvents:UIControlEventTouchUpInside];
            [skim setHidden:YES];
            [self.view addSubview:skim];
            [viewArray2 addObject:skim];
            
            // 删除
            UIButton *delete=[UIButton buttonWithType:UIButtonTypeRoundedRect];
            delete.frame=CGRectMake(15,690,270,43);
            delete.tag = (long)mydic;
            UIImage *delete_image =[UIImage imageNamed:@"left_delete"];
            [delete setBackgroundImage:delete_image forState:UIControlStateNormal];
            [delete addTarget:self action:@selector(Delete:) forControlEvents:UIControlEventTouchUpInside];
            [delete setHidden:YES];
            
            [self.view addSubview:delete];
            [viewArray2 addObject:delete];

            //-------隐藏--结束------
            
        }else{// 已经下载了
            
            if ([dic.dictionary_pronounce compare:@"12"]==NSOrderedSame) {
                // 设置发音
                UIImageView *set_tone = [[UIImageView alloc] initWithFrame:CGRectMake(15,200,273,50)];
                UIImage *tone = [UIImage imageNamed:@"set_tone"];
                [set_tone initWithImage:tone];
                
                DataBase *pronucations = [[DataBase alloc]init];
                NSString *pronucation = [pronucations getCurrentDictionaryPronounce:mydic];
                // 转换器-设置发音
                UISwitch *switchButton = [[UISwitch alloc] initWithFrame:CGRectMake(190, 12, 20, 10)];
                NSLog(@"---0----%@",pronucation);
                if ([pronucation compare:@"US"]==NSOrderedSame) {
                    [switchButton setOn:YES];
                }else{
                    [switchButton setOn:NO];
                }
                [switchButton addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
                [set_tone addSubview:switchButton];
                [set_tone bringSubviewToFront:switchButton];
                [set_tone setUserInteractionEnabled:YES];
                
                [self.view addSubview:set_tone];
                [viewArray2 addObject:set_tone];

            }
            
            // 设置完成时间
            UIImageView *setFinishTime = [[UIImageView alloc] initWithFrame:CGRectMake(15,270,273,90)];
            UIImage *time = [UIImage imageNamed:@"set_finish_time"];
            [setFinishTime initWithImage:time];
            
            //获得日期和词汇量
            DataBase *dateAndWord = [[DataBase alloc]init];
            NSString *days = [dateAndWord getCompleteTime:mydic];// 获得完成天数
            
            // 时间
            date = [[UILabel alloc]initWithFrame:CGRectMake(15.0, 8.0, 260.0, 35.0)];
            
            //日背诵量
            count = [[UILabel alloc]initWithFrame:CGRectMake(15.0, 50.0, 260.0, 35.0)];

            if ([days compare:nil]!=NSOrderedSame && [days compare:@""]!=NSOrderedSame && [days compare:@"0"]!=NSOrderedSame) {
                NSString *dayWords = [self dayWords:mydic finishDay:days];
                count.text = [NSString stringWithFormat:@"日背诵量: %@",dayWords];
                NSString *finishTime = [self getFinishDate:days];
                date.text = [NSString stringWithFormat:@"预计完成时间: %@",finishTime];
            }else{
                count.text = [NSString stringWithFormat:@"日背诵量: 25"];
                date.text = [NSString stringWithFormat:@"预计完成时间: %@",[self SuggestDate:mydic]];
            }
            
            date.backgroundColor = [UIColor clearColor]; //可以去掉背景色
            date.textColor = [UIColor blackColor];
            date.font = [UIFont fontWithName:@"Helvetica" size:18];
            [setFinishTime addSubview:date];
            
            count.backgroundColor = [UIColor clearColor]; //可以去掉背景色
            count.textColor = [UIColor blackColor];
            count.font = [UIFont fontWithName:@"Helvetica" size:18];
            [setFinishTime addSubview:count];
            
            setFinishTime.userInteractionEnabled = YES;
            UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ImageViewClick)];
            [setFinishTime addGestureRecognizer:singleTap];
            [singleTap release];
            
            [self.view addSubview:setFinishTime];
            [viewArray2 addObject:setFinishTime];
            
            // 背诵当个词库
            UIButton *skim=[UIButton buttonWithType:UIButtonTypeRoundedRect];
            skim.frame=CGRectMake(15,600,270,55);
            skim.tag = dicId;
            UIImage *skim_image =[UIImage imageNamed:@"skim_dic"];
            [skim setBackgroundImage:skim_image forState:UIControlStateNormal];
            [skim addTarget:self action:@selector(skimDic:) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:skim];
            [viewArray2 addObject:skim];
            
            // 删除
            UIButton *delete=[UIButton buttonWithType:UIButtonTypeRoundedRect];
            delete.frame=CGRectMake(15,690,270,43);
            delete.tag = (long)mydic;
            UIImage *delete_image =[UIImage imageNamed:@"left_delete"];
            [delete setBackgroundImage:delete_image forState:UIControlStateNormal];
            [delete addTarget:self action:@selector(Delete:) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:delete];
            [viewArray2 addObject:delete];
        }
    }
}

// 浏览词库
-(void)skimDic:(id)sender{
    
    NSUserDefaults *userDefaultes = [NSUserDefaults standardUserDefaults];
//    NSString *OneOrAll = [userDefaultes stringForKey:@"finished"];
//    if (OneOrAll != nil && [OneOrAll compare:@"all"]==NSOrderedSame) {
//        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"背诵提醒" message:@"你还有计划没有完成，请到背诵全部中继续背诵!" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
//        [alert show];
//        [alert release];
//    }else{
        int tag = (long)[sender tag];
        tools *tool2 = [[tools alloc]init];
        NSMutableArray *wordsArray =  [tool2 readArray:@"doubleStruct"];
        [tool2 release];
        int i = 0;
        UserDictionary *tagDic;
        bool isnull = false;
        for (int j = 0; j < [wordsArray count]; j++) {
            secondList *second = [wordsArray objectAtIndex:j];
            NSMutableArray *secondDic = second.secondLevel;
            for (int k = 0 ; k < [secondDic count]; k++) {
//                NSLog(@"%d,%d",i,tag);
                if (i == tag) {
                    tagDic = [secondDic objectAtIndex:k];
                    isnull = true;
                    break;
                }else{
                    i++;
                }
            }
            
            if (isnull) {
                break;
            }
        }
    
    
    
//        NSString *NowUseDictionary = [userDefaultes stringForKey:@"now_use_dictionary"];
    
//        if ([NowUseDictionary compare:tagDic.dictionary_name]==NSOrderedSame||NowUseDictionary == nil) {
            // 保存词库编号的标签
            NSString *dicNum = [NSString stringWithFormat:@"%d",num];
            [[NSUserDefaults standardUserDefaults] setObject:dicNum forKey:@"dicNum"];
            
            // 保存了这个词库每天要背诵的单词数量
            [[NSUserDefaults standardUserDefaults] setObject:@"one-dic" forKey:@"noe-dic"];
    
    
    // call method to initialize the word for today
    DateUnits *lag = [[DateUnits alloc]init];
    int day = [lag CalculateDateLag]; // 'day' is dely days
    [lag SaveLoginDate];
    Init *init = [[Init alloc]init];
    NSMutableArray *array = [init GetTodayWords];
    
    if (day >0 || [array count]==0) {
        [self Loading2];
        NSThread* myThread = [[NSThread alloc] initWithTarget:self
                                                     selector:@selector(myThreadMainMethod:)
                                                       object:nil];
        [myThread start];  // Actually create the thread
        
    }else{
        if ([newDownloadDictionaryArray count] > 0) {
            Algorithm *alg = [[Algorithm alloc]init];
            [alg UpdateDailyWordArray:newDownloadDictionaryArray];
            
            NSString *dailyplan = [[NSString alloc]initWithFormat:@"%f",[alg CountDailyPlan]];
            tools *tool = [[tools alloc]init];
            [tool saveObject:dailyplan saveKey:@"dailyplan"];
        }
        ReciteView *ickImageViewController = [[ReciteView alloc] init];
        [self presentModalViewController:ickImageViewController animated:NO];
        [ickImageViewController release];
        
        // 2 获得现在还剩下多少单词
        NSMutableArray *wordsArray2 = [init GetTodayWords];
        NSMutableArray *newWordArray = [[NSMutableArray alloc]init];
        for (Word *wstr in wordsArray2) {
            if ([wstr.word_dictionary compare:tagDic.dictionary_name]==NSOrderedSame) {
                [newWordArray addObject:wstr];
            }
        }
        NSString *intString = [[NSString alloc]initWithFormat:@"%lu",(unsigned long)[newWordArray count]];
        [[NSUserDefaults standardUserDefaults] setObject:intString forKey:@"plansize"];
        
    }
    
    
}

-(void)myThreadMainMethod:(id)sender{
    Init *init = [[Init alloc]init];
    [init InitTodayWords];// get and save today's word method
    
    // 2 获得现在还剩下多少单词
    NSMutableArray *wordsArray2 = [init GetTodayWords];
    NSMutableArray *newWordArray = [[NSMutableArray alloc]init];
    for (Word *wstr in wordsArray2) {
        if ([wstr.word_dictionary compare:dic.dictionary_name]==NSOrderedSame) {
            [newWordArray addObject:wstr];
        }
    }
    NSString *intString = [[NSString alloc]initWithFormat:@"%lu",(unsigned long)[newWordArray count]];
    [[NSUserDefaults standardUserDefaults] setObject:intString forKey:@"plansize"];
    
    Algorithm *alg = [[Algorithm alloc]init];
    NSString *dailyplan = [[NSString alloc]initWithFormat:@"%f",[alg CountDailyPlan]];
    tools *tool = [[tools alloc]init];
    [tool saveObject:dailyplan saveKey:@"dailyplan"];
    [self finishLoading];
    
    ReciteView *ickImageViewController = [[ReciteView alloc] init];
    [self presentModalViewController:ickImageViewController animated:NO];
    [ickImageViewController release];
    
}

-(void)myThreadMainMethod2:(id)sender{
    Init *init = [[Init alloc]init];
    [init InitTodayWords];// get and save today's word method
    
    // 2 获得现在还剩下多少单词
    NSMutableArray *wordsArray2 = [init GetTodayWords];
    NSString *intString = [[NSString alloc]initWithFormat:@"%lu",(unsigned long)[wordsArray2 count]];
    [[NSUserDefaults standardUserDefaults] setObject:intString forKey:@"plansize"];
    
    Algorithm *alg = [[Algorithm alloc]init];
    NSString *dailyplan = [[NSString alloc]initWithFormat:@"%f",[alg CountDailyPlan]];
    tools *tool = [[tools alloc]init];
    [tool saveObject:dailyplan saveKey:@"dailyplan"];
    [self finishLoading];
    
    ReciteView *ickImageViewController = [[ReciteView alloc] init];
    [self presentModalViewController:ickImageViewController animated:NO];
    [ickImageViewController release];
    
}

// =================下拉刷新方法开始================
#pragma mark -
#pragma mark Data Source Loading / Reloading Methods

- (void)reloadTableViewDataSource{
	_reloading = YES;
}

- (void)doneLoadingTableViewData{
	_reloading = NO;
	[_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView];
}

#pragma mark -
#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
	[_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
	[_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
}

#pragma mark EGORefreshTableHeaderDelegate Methods

//下拉到一定距离，手指放开时调用
- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view{
    [self releaseDialog]; // 注销dialog
	[self reloadTableViewDataSource];
    
    //停止加载，弹回下拉
	[self performSelector:@selector(doneLoadingTableViewData) withObject:nil afterDelay:0.5];
    
    if (_barView == nil) {
        UIImage *img = [[UIImage imageNamed:@"timeline_new_status_background.png"] stretchableImageWithLeftCapWidth:5 topCapHeight:5];
        _barView = [[UIImageView alloc] initWithImage:img];
        _barView.frame = CGRectMake(0, 63, 298, 40);
        [self.view addSubview:_barView];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
        label.tag = 100;
        label.font = [UIFont systemFontOfSize:16.0f];
        label.textColor = [UIColor whiteColor];
        label.backgroundColor = [UIColor clearColor];
        [_barView addSubview:label];
        [label release];
    }
    UILabel *label = (UILabel *)[_barView viewWithTag:100];
    if (![tool isExistenceNetwork]) {
        label.text = [NSString stringWithFormat:@"没有网络"];
    }else{
        label.text = [NSString stringWithFormat:@"同步数据"];
    }
    [label sizeToFit];
    CGRect frame = label.frame;
    frame.origin = CGPointMake((_barView.frame.size.width - frame.size.width)/2, (_barView.frame.size.height - frame.size.height)/2);
    label.frame = frame;
    
    
    [self performSelector:@selector(updateUI) withObject:nil afterDelay:2.0];
    
}

- (void)updateUI {
    [UIView animateWithDuration:0.6 animations:^{
        CGRect frame = _barView.frame;
        frame.origin.y = 5;
        _barView.frame = frame;
    } completion:^(BOOL finished){
        if (finished) {
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDelay:1.0];
            [UIView setAnimationDuration:0.6];
            CGRect frame = _barView.frame;
            frame.origin.y = -40;
            _barView.frame = frame;
            [UIView commitAnimations];
            [_barView removeFromSuperview];
        }
    }];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"msgcome" ofType:@"wav"];
    NSURL *url = [NSURL fileURLWithPath:path];
    SystemSoundID soundId;
    AudioServicesCreateSystemSoundID((CFURLRef)url, &soundId);
    AudioServicesPlaySystemSound(soundId);
    
    tool = [[tools alloc]init];
    // 同步网络数据
    if ([tool isExistenceNetwork]) {
        // 获得有二级结构的词库列表
        doubleStruct = [api getUserHadBuyDictionary2SencondList:userName userId:user.user_id];
        [tool saveArray:doubleStruct saveKey:@"doubleStruct"];// 保存二级菜单数组
        
        dicPause = [[NSMutableArray alloc]init];
        
    for (int i =0; i < [doubleStruct count]; i++) {
        secondList *list = [doubleStruct objectAtIndex:i];
        for (int j = 0; j < [list.secondLevel count]; j++) {
            UserDictionary *secondDic = [list.secondLevel objectAtIndex:j];
            DataBaseHelper *PrecentNum = [[DataBaseHelper alloc]init];
            BOOL use = [PrecentNum isDictionaryPause:secondDic.dictionary_id userId:user.user_id];
            [PrecentNum release];
            if (use) {
                [dicPause addObject:@"1"];
            }else{
                [dicPause addObject:@"0"];
            }
        }
    }
        // -------获得保存的词库和 词库的启用状态信息-------start
        [tool saveArray:dicPause saveKey:@"usedDicsArray"];// 保存词库的使用和停止状态
    
        NSString *user_name = (NSString*)[tool readObkect:@"user_name"];
        NSString *user_id = (NSString*)[tool readObkect:@"user_id"];
        // 获得有二级结构的词库列表
        doubleStruct = [api getUserHadBuyDictionary2SencondList:user_name userId:user_id];
        [tool saveArray:doubleStruct saveKey:@"doubleStruct"];// 保存二级菜单数组
        
        //重新加载数据
        for (int i = 0; i<[viewArray1 count]; i++) {
            [[viewArray1 objectAtIndex:i] removeFromSuperview];
        }
        [self DataPackgeList];
        [self.view bringSubviewToFront:_barView];
    }else{
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"刷新失败" message:@"网络原因导致刷新失败，请联网后重试！" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil,nil];
        [alert show];
        [alert release];
    }
    
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view{
	return _reloading; // should return if data source model is reloading
}

//取得下拉刷新的时间
- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view{
	return [NSDate date]; // should return date data source was last changed
}

//===============下拉刷新方法结束===========

// 词库状态改变监听事件
-(void)DicState:(id)sender{
    UserDictionary *indexDic = (UserDictionary*)[sender tag];
    DataBaseHelper *UseDic = [[DataBaseHelper alloc]init];
    BOOL used = [UseDic isDictionaryPause:indexDic.dictionary_id userId:user.user_id];
    if (used) {
        UIImage *refresh_image =[UIImage imageNamed:@"left_list_logo"];
        [sender setBackgroundImage:refresh_image forState:UIControlStateNormal];
        [UseDic unPauseDictionary:indexDic.dictionary_id userId:user.user_id];
        NSLog(@"--state");
    }else{
        UIImage *refresh_image =[UIImage imageNamed:@"dic_unusered"];
        [sender setBackgroundImage:refresh_image forState:UIControlStateNormal];
        [UseDic pauseDictionary :indexDic.dictionary_id userId:user.user_id];
    }
    [UseDic release];
}

// 按钮监听
-(void)RightTopButton:(id) sender{
    int i = [sender tag];
    switch (i) {
        case 0:
            if (!isShow) {
                [start_dialog removeFromSuperview];
                [self dialog_start_learn];
                isShow = TRUE;
                start_dialog.alpha = 0.0;
                [UIView animateWithDuration:0.5
                                 animations:^{
                                     start_dialog.alpha = 1.0;
                                 }
                                 completion:^(BOOL finished){
                                     
                                 }];
                
            }else{
                [start_dialog removeFromSuperview];
                isShow = FALSE;
            }
            
            break;
        case 1:
            if (!isShow) {
                [start_dialog removeFromSuperview];
                [self dialog_setting];
                isShow = TRUE;
                start_dialog.alpha = 0.0;
                [UIView animateWithDuration:0.5
                                 animations:^{
                                     start_dialog.alpha = 1.0;
                                 }
                                 completion:^(BOOL finished){
                                     
                                 }];
            }else{
                [start_dialog removeFromSuperview];
                isShow = FALSE;
            }
            
            break;
        case 2:
            if (!isShow) {
                [start_dialog removeFromSuperview];
                [self user_setting];
                isShow = TRUE;
                start_dialog.alpha = 0.0;
                [UIView animateWithDuration:0.5
                                 animations:^{
                                     start_dialog.alpha = 1.0;
                                 }
                                 completion:^(BOOL finished){
                                     
                                 }];
            }else{
                [start_dialog removeFromSuperview];
                isShow = FALSE;
            }
            
            break;
        case 3:
            [start_dialog removeFromSuperview];
            NSLog(@"恢复购买请求");
            [[CBiOSStoreManager sharedInstance] initialStore];
            [[CBiOSStoreManager sharedInstance] rePurchase];
            break;
        case 4:
            [start_dialog removeFromSuperview];
            break;
        case 5:
        {
            [start_dialog removeFromSuperview];
            xMainViewController *ickImageViewController = [[xMainViewController alloc] init];
            [self presentModalViewController:ickImageViewController animated:YES];
            [self dismissModalViewControllerAnimated:YES];
            [ickImageViewController release];
        }
            break;
        case 6:
            [start_dialog removeFromSuperview];
            break;
        case 7:
            [start_dialog removeFromSuperview];
            break;
        case 8:
            [start_dialog removeFromSuperview];
            break;
        case 9:
        {
            [start_dialog removeFromSuperview];
            [self LargeImage];
        }
            break;
        case 10:
        {
            
            [start_dialog removeFromSuperview];
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"useAllDics"];//启用全部词库的标记
            
            NSUserDefaults *userDefaultes = [NSUserDefaults standardUserDefaults];
            userName = [userDefaultes stringForKey:@"userName"];
            NSMutableArray *dics2 = [api getUserHadBuyDictionary:userName userId:user.user_id];
            for (int i = 0; i < [dics2 count]; i++) {//开启全部词库
                dic = [dics2 objectAtIndex:i];
                DataBaseHelper *UseDic = [[DataBaseHelper alloc]init];
                [UseDic unPauseDictionary:dic.dictionary_id userId:user.user_id];
                [UseDic release];
            }
            // 设置错误背诵错误单词 为 否
            [userDefaultes setBool:NO forKey:@"useWrongWords"];
            
            ReciteView *ickImageViewController = [[ReciteView alloc] init];
            [self presentModalViewController:ickImageViewController animated:NO];
            [ickImageViewController release];

        }
            break;
        case 11:
        {
            [start_dialog removeFromSuperview];
            
            NSUserDefaults *userDefaultes = [NSUserDefaults standardUserDefaults];
//            NSString *OneOrAll = [userDefaultes stringForKey:@"finished"];
//            NSString *NowUseDictionary = [userDefaultes stringForKey:@"now_use_dictionary"];
//            NSString *display = [[NSString alloc]initWithFormat:@"你今天还有单个词库的单词没有背完，请到《%@》中完成之前的计划",NowUseDictionary];
//            
//            if (OneOrAll != nil && [OneOrAll compare:@"one"]==NSOrderedSame) {
//                UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"背诵提醒" message:display delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
//                [alert show];
//                [alert release];
//            }else{
                // 设置错误背诵错误单词 为 否
                [userDefaultes setBool:NO forKey:@"useWrongWords"];
                
                // 标记背诵全部词库
                [[NSUserDefaults standardUserDefaults] setObject:@"all-dic" forKey:@"noe-dic"];
            
            
            
            // call method to initialize the word for today
            DateUnits *lag = [[DateUnits alloc]init];
            int day = [lag CalculateDateLag]; // 'day' is dely days
            [lag SaveLoginDate];
            
            Init *init = [[Init alloc]init];
            NSMutableArray *array = [init GetTodayWords];
            if (day >0 || [array count]==0) {
                [self Loading2];
                NSThread* myThread = [[NSThread alloc] initWithTarget:self
                                                             selector:@selector(myThreadMainMethod2:)
                                                               object:nil];
                [myThread start];  // Actually create the thread
                
            }else{
                if ([newDownloadDictionaryArray count] > 0) {
                    Algorithm *alg = [[Algorithm alloc]init];
                    [alg UpdateDailyWordArray:newDownloadDictionaryArray];
                    
                    NSString *dailyplan = [[NSString alloc]initWithFormat:@"%f",[alg CountDailyPlan]];
                    tools *tool = [[tools alloc]init];
                    [tool saveObject:dailyplan saveKey:@"dailyplan"];
                }
                ReciteView *ickImageViewController = [[ReciteView alloc] init];
                [self presentModalViewController:ickImageViewController animated:NO];
                [ickImageViewController release];
                
                // 2 获得现在还剩下多少单词
                NSMutableArray *wordsArray2 = [init GetTodayWords];
                NSString *intString = [[NSString alloc]initWithFormat:@"%lu",(unsigned long)[wordsArray2 count]];
                [[NSUserDefaults standardUserDefaults] setObject:intString forKey:@"plansize"];
            }
        }
            break;
        case 12:
        {
            [start_dialog removeFromSuperview];
            // 设置错误背诵错误单词
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"useWrongWords"];
            
            wrongWordRevew *ickImageViewController = [[wrongWordRevew alloc] init];
            [self presentModalViewController:ickImageViewController animated:NO];
            [ickImageViewController release];
        }
            break;
        default:
            break;
    }
}

// 点击后图片放大
-(void)LargeImage{
    UIImage *image = [UIImage imageNamed:@"help_guide"];
    
    bigImage = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,1024,748)];
    [bigImage initWithImage:image];
    
    bigImage.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(UesrClicked:)];
    [bigImage addGestureRecognizer:singleTap];
    [singleTap release];
    NSArray *selfViewArray = [self.view subviews];
    for (int i =0; i <[selfViewArray count]; i++) {
        [[selfViewArray objectAtIndex:i] setHidden:YES];
    }
    [self.view addSubview:bigImage];
}

// 取消图片放大
-(void)UesrClicked:(id)sender{
    [bigImage removeFromSuperview];
    NSArray *selfViewArray = [self.view subviews];
    for (int i =0; i <[selfViewArray count]; i++) {
        [[selfViewArray objectAtIndex:i] setHidden:NO];
    }
}

// 刷新左边列表
-(void)LeftTopButton:(id) sender{
    NSString *user_name = (NSString*)[tool readObkect:@"user_name"];
    NSString *user_id = (NSString*)[tool readObkect:@"user_id"];
    // 获得有二级结构的词库列表
    doubleStruct = [api getUserHadBuyDictionary2SencondList:user_name userId:user_id];
    [tool saveArray:doubleStruct saveKey:@"doubleStruct"];// 保存二级菜单数组
    
    int butId = [sender tag];
    if (butId == 0) {
        for (int i = 0; i<[viewArray1 count]; i++) {
            [[viewArray1 objectAtIndex:i] removeFromSuperview];
        }
        [self DataPackgeList];
    }
}

// 购买请求响应事件
-(void)Purchase:(id) sender{
    _doneAnything = true;
    for (int i = 0; i<[viewArray1 count]; i++) {
        [[viewArray1 objectAtIndex:i] removeFromSuperview];
    }
    if ([tool isExistenceNetwork]) {
        [sender setTitle:@"交易处理中..." forState:UIControlStateNormal];
        UserDictionary *productId = (UserDictionary*)[sender tag];
        NSLog(@"选择第 个产品,产品product id = %@",productId.dictionary_proid);
        //处理购买词库
        
        NSArray *ui2 = [self.view subviews];
        [[CBiOSStoreManager sharedInstance] initialStore];
        [[CBiOSStoreManager sharedInstance] buy:productId.dictionary_proid myArray:ui2 isBuy:1];
    }else{
        //设置提示窗口
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"网络提示" message:@"请联网后购买" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

// 恢复购买响应事件
-(void)reBuy:(id)sender{
    [sender setTitle:@"交易处理中..." forState:UIControlStateNormal];
    UserDictionary *productId = (UserDictionary*)[sender tag];
    NSArray *ui2 = [self.view subviews];
    [[CBiOSStoreManager sharedInstance] initialStore];
    [[CBiOSStoreManager sharedInstance] buy:productId.dictionary_proid myArray:ui2 isBuy:0];
}

// 购买成功回调函数
-(void)changeDicToBuyState:(NSArray *)ViewArray{
    isPurchase = true;
    NSLog(@"购买成功！");
    ServiceApi *buyDics = [[ServiceApi alloc]init];
    NSString *user_id = (NSString*)[tool readObkect:@"user_id"];
    NSLog(@"user_id=%@,dic_id = %@",user_id,dic.dictionary_id);
    [buyDics buyDictionary:dic UserId:user_id];
    [buyDics release];
    for (int i = 0; i < [ViewArray count]; i++) {
        NSLog(@"%d-%@",i,[ViewArray objectAtIndex:i]);
    }
    // 更新购买按钮
    UIButton * button = [ViewArray objectAtIndex:12];
    [button setHidden:YES];
    
    // 更新
    UILabel * lable = [ViewArray objectAtIndex:13];
    [lable setHidden:YES];
    
    // 显示下载按钮
    UIButton * shim = [ViewArray objectAtIndex:14];
    [shim setHidden:NO];
    
    NSString *user_name = (NSString*)[tool readObkect:@"user_name"];
    // 获得有二级结构的词库列表
    doubleStruct = [api getUserHadBuyDictionary2SencondList:user_name userId:user_id];
    [tool saveArray:doubleStruct saveKey:@"doubleStruct"];// 保存二级菜单数组
}

// 设置完成时间图片监听事件
-(void)ImageViewClick{
    
    NSMutableArray *dics2 = [tool readArray:@"doubleStruct"];
    UserDictionary *OneDic = [[UserDictionary alloc]init];
    int index = 0;
    for (int i = 0; i < [dics2 count]; i++) {
        secondList *list = [dics2 objectAtIndex:i];
        NSMutableArray *array = list.secondLevel;
        for (int j = 0; j < [array count]; j++) {
            if (index == num) {
                OneDic = [array objectAtIndex:j];
                index++;
                break;
            }
            index++;
        }
    }
    
    // 初始化UIDatePicker
    UIDatePicker *datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(10, 0, 265, 215)];
    // 设置时区
    [datePicker setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    // 设置当前显示时间
    NSDate *date1 = [NSDate date];
    date1 = [date1 dateByAddingTimeInterval: 14*3600*24];
    // 设置当前时间为最小时间
    [datePicker setMinimumDate:date1];
    
    NSDate *date2 = [NSDate date];
    DataBase *dateAndWord = [[DataBase alloc]init];
    NSString *days1 = [dateAndWord getCompleteTime:OneDic];
    [dateAndWord release];
    int days2 = [days1 intValue];
    date2 = [date2 dateByAddingTimeInterval: days2*3600*24];//增加天数
    
    [datePicker setDate:date2 animated:YES];
    
    // 设置UIDatePicker的显示模式
    [datePicker setDatePickerMode:UIDatePickerModeDate];
    datePicker.tag = (long)OneDic;
    // 当值发生改变的时候调用的方法
    [datePicker addTarget:self action:@selector(timeChange:) forControlEvents:UIControlEventValueChanged];
//    [OneDic release];
    [self.view addSubview:datePicker];
    [datePicker release];
    // 获得当前UIPickerDate所在的时间
//    NSDate *selected = [datePicker date];
//    NSLog(@"date:%@",selected);
    
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 250, 300, 220)];
    [view setBackgroundColor:[UIColor whiteColor]];
    [view addSubview:datePicker];
    
    // 确定修改
    UIButton *modify=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    modify.frame=CGRectMake(15,180,270,43);
    modify.tag = (long)view;
    UIImage *delete_image =[UIImage imageNamed:@"modify_sure_but"];
    [modify setBackgroundImage:delete_image forState:UIControlStateNormal];
    [modify addTarget:self action:@selector(ModifySure:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:modify];
    
    [self.view addSubview:view];
    [view release];
}

// 时间选择响应方法
-(void)timeChange:(UIDatePicker *)datePicker{
    UserDictionary *ModifyDic = (UserDictionary*)[datePicker tag];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"newUser"];
    NSDateFormatter *formatter = [[NSDateFormatter  alloc]init];
    formatter.dateFormat = @"yyyy年MM月dd日";
    NSString *text = [formatter stringFromDate:datePicker.date];
    [formatter release];
    NSString *choiceDate = [NSString stringWithFormat:@"%@",text];
    NSString *finishdate = [NSString stringWithFormat:@"预计完成时间: %@",choiceDate];
    date.text = finishdate; // 设置显示的完成时间
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:text forKey:@"setfinishDate"]; // 保存设置的完成时间
    
    int words = [self DayRemember:text modifyDic:ModifyDic];
    NSString *stringInt = [NSString stringWithFormat:@"%d",words];
    NSString *dayWordsNum = [NSString stringWithFormat:@"日背诵量:%@",stringInt];
    count.text = dayWordsNum;
    [userDefaults setObject:stringInt forKey:@"setdayWords"];
    
    Algorithm *alg = [[Algorithm alloc]init];
    [alg DeleteWordFromArray:ModifyDic];
    
    [newDownloadDictionaryArray addObject:dic];
}

// 计算每日背诵量
-(int)DayRemember:(NSString *)finishDate modifyDic:(UserDictionary *) ModifyDic{
    DataBase *wordCount =[[DataBase alloc]init];
    NSString *AllWordscount = [wordCount getDictionaryWordCount:ModifyDic];
    [wordCount release];
    int intString = [AllWordscount intValue];
    
    //获得时间差
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy年MM月dd日"];
    NSDate *date1=[dateFormatter dateFromString:finishDate];
    NSDate * date2=[NSDate date];
    NSCalendar * cal=[NSCalendar currentCalendar];
    NSUInteger unitFlags=NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit;
    NSDateComponents * conponent= [cal components:unitFlags fromDate:date2];
    NSInteger year=[conponent year];
    NSInteger month=[conponent month];
    NSInteger day=[conponent day];
    NSString * nsDateString= [NSString stringWithFormat:@"%d年%d月%d日",year,month,day];
    NSDate *date3=[dateFormatter dateFromString:nsDateString];
    [dateFormatter release];
    NSTimeInterval time=[date1 timeIntervalSinceDate:date3];
    
    int days=((int)time)/(3600*24);
    
    int words = 25;
    
    if (days < 14) {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"警告" message:@"请务必选择大于14天的背诵时间，以帮助你更好的使用我们的记忆方法!" delegate:self cancelButtonTitle:@"关闭" otherButtonTitles:nil];
        [alert show];
        [alert release];
    }else{
        if (days <= 14) {
            days = 15;
        }
        
        words = intString/(days - 14);
        if (words < 0) {
            words = 25;
        }
        
        // 设置词库背诵天数
        NSString *finishDays = [NSString stringWithFormat:@"%d",days];
        DataBase *dateAndWord = [[DataBase alloc]init];
        [dateAndWord setDictionaryCompleteTime:finishDays dictioanry:ModifyDic];
        [dateAndWord release];
    }
    return words;
}

// 初始完成日期和词汇量
-(NSString*)SuggestDate:(UserDictionary *)userDic{
    DataBase *wordCount =[[DataBase alloc]init];
    NSString *AllWordscount = [wordCount getDictionaryWordCount:dic];// 得到该词库的所有单词数量
    int intString = [AllWordscount intValue];
    
    int days = intString/25+14;
    
    //设置新的时间
    NSDate * senddate=[NSDate date];
    senddate = [senddate dateByAddingTimeInterval: days*3600*24];//增加天数
    NSCalendar * cal=[NSCalendar currentCalendar];
    NSUInteger unitFlags=NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit;
    NSDateComponents * conponent= [cal components:unitFlags fromDate:senddate];
    NSInteger year=[conponent year];
    NSInteger month=[conponent month];
    NSInteger day=[conponent day];
    NSString *nsDateString= [NSString stringWithFormat:@"%4d年%2d月%d日",year,month,day];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:nsDateString forKey:@"setfinishDate"];
    
    NSString *stringInt = [NSString stringWithFormat:@"%d",25];
    [userDefaults setObject:stringInt forKey:@"setdayWords"];
    
    // 给词库添加完成天数
    [wordCount setDictionaryCompleteTime:@"25" dictioanry:userDic];
    [wordCount release];
    return nsDateString;
}

// 计算完成日期
-(NSString*)getFinishDate:(NSString *) finishDays{
    int days = [finishDays intValue];
    //设置新的时间
    NSDate * senddate=[NSDate date];
    senddate = [senddate dateByAddingTimeInterval: days*3600*24];//增加天数
    NSCalendar * cal=[NSCalendar currentCalendar];
    NSUInteger unitFlags=NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit;
    NSDateComponents * conponent= [cal components:unitFlags fromDate:senddate];
    NSInteger year=[conponent year];
    NSInteger month=[conponent month];
    NSInteger day=[conponent day];
    NSString *nsDateString= [NSString stringWithFormat:@"%4d年%2d月%d日",year,month,day];
    return nsDateString;
}

// 根据天数计算每天背诵词汇量
-(NSString*)dayWords:(UserDictionary*) mydic finishDay: (NSString *)finish_day{
    DataBase *wordCount =[[DataBase alloc]init];
    NSString *AllWordscount = [wordCount getDictionaryWordCount:mydic];//得到词库所有单词数量
    [wordCount release];
    int intString = [AllWordscount intValue];
    int finishday = [finish_day intValue];// 完成的天数
    if (finishday == 0) {
        return 0;
    }else{
        if (finishday <= 15) {
            finishday = 15;
        }
        int day = intString/(finishday-14); // 日背诵量 = (总单词数量)/(计划完成的天数-14)
        NSString *days = [NSString stringWithFormat:@"%d",day];
        return days;
    }
}

// 修改时间确定界面
-(void) ModifySure:(id)sender{
    UIView *view = (UIView*)[sender tag];
    [view removeFromSuperview];
}

// 下载前提醒网络使用
-(void)downloaddic:(id) sender{
    record = sender;
    //设置提示窗口
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"下载提示" message:@"程序将使用你的网络下载数据包!" delegate:self cancelButtonTitle:@"关闭" otherButtonTitles:@"确定" ,nil];
    alert.tag = 1;
    [alert show];
    [alert release];
}

// 下载事件
-(void)Download:(id) sender{
    _doneAnything = true;
    for (int i = 0; i<[viewArray1 count]; i++) {
        [[viewArray1 objectAtIndex:i] removeFromSuperview];
    }
    tools *tool2 = [[tools alloc]init];
    if ([tool2 isExistenceNetwork]) {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"afterBuy"];
        /*
         *判断词库文件夹压缩包和路径是否存在
         */
        [record setHidden:YES];
        
        UIProgressView * progress = [ [ UIProgressView   alloc ]
                                     initWithFrame:CGRectMake(50.0,300.0,200.0,30.0)];
        progress.progressViewStyle= UIProgressViewStyleDefault;
        [self.view addSubview:progress];
        [viewArray2 addObject:progress];
        [progress release];
        UILabel *process = [[UILabel alloc]initWithFrame:CGRectMake(80.0, 260.0, 200.0, 30.0)];
        NSString *process2 = [NSString stringWithFormat:@"下载中...%.1f%%",[progress progress]];
        process.text =process2;
        [self.view addSubview:process];
        [viewArray2 addObject:process];
        [process release];
        
        //    dics = [api getUserHadBuyDictionary:userName userId:user.user_id];
        dic = (UserDictionary*)[record tag];
        NSLog(@"dic_id_1 = %@",dic.dictionary_id);
        NSString *documentsDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches"];
        NSFileManager *fileManager = [NSFileManager defaultManager];//文件
        
        NSString *dicName = [NSString stringWithFormat:@"%@.zip",dic.dictionary_id];
        NSString *filePath2 =  [documentsDirectory stringByAppendingPathComponent:dicName];
        BOOL isDirExist = [fileManager fileExistsAtPath:filePath2];
        
        NSString *stringInt = [NSString stringWithFormat:@"%@",dic.dictionary_id];
        NSString *filePath =  [documentsDirectory stringByAppendingPathComponent:stringInt];
        BOOL filedDirExist = [fileManager fileExistsAtPath:filePath];
        
//        NSString *dBfilePath =  [documentsDirectory stringByAppendingPathComponent:@"db.sqlite"];
//        BOOL isDbExist = [fileManager fileExistsAtPath:dBfilePath];
        
        if (filedDirExist||isDirExist) {
            
        }else{
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"downLoading"];
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setObject:stringInt forKey:@"downLoadNum"];
            //
            /*
             *调用下载词库接口
             */
            [self Loading];
            NSArray *ui = [self.view subviews];
            NSString *stringInt3 = [NSString stringWithFormat:@"%@",dic.dictionary_id];
            [api downloadDictionary:stringInt3 processView:progress Label:process myArray:ui];
        }
        
    }else{
        //设置提示窗口
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"网络提示" message:@"请联网后下载" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
    [tool2 release];
}

// 加载圈
-(void)Loading{
    //显示加载等待框
    self.progressHUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:self.progressHUD];
    [_progressHUD release];
    [self.view bringSubviewToFront:self.progressHUD];
    self.progressHUD.delegate = self;
    //    self.progressHUD.labelText = @"同步中...";
    [self.progressHUD show:YES];
}

// 加载圈
-(void)Loading2{
    //显示加载等待框
    self.progressHUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:self.progressHUD];
    [_progressHUD release];
    [self.view bringSubviewToFront:self.progressHUD];
    self.progressHUD.delegate = self;
    self.progressHUD.labelText = @"初始化本日单词...";
    [self.progressHUD show:YES];
}

-(void)EnableDictionary2:(id)sender{
    DataBaseHelper *help = [[DataBaseHelper alloc]init];
    NSLog(@"id--->%@",user.user_id);
    [help sync:user.user_id];
    [help release];
    [self finishLoading];
    
}

//取消加载
-(void)finishLoading{
    if (self.progressHUD){
        [self.progressHUD removeFromSuperview];
        [self.progressHUD release];
        self.progressHUD = nil;
    }
}

// 删除事件
-(void)Delete:(id) sender{
    UserDictionary *secondDic = (UserDictionary*)[sender tag];
    NSString *dicName = [NSString stringWithFormat:@"你将删除词库:%@",secondDic.dictionary_name];
    
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"警告" message:dicName delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定",nil];
    UserDictionary *usDic = (UserDictionary*)[sender tag];
    alert.tag = (long)usDic;
    [alert show];
    [alert release];
}

//下载成功回调函数
-(void)downLoadSuccess:(NSArray*)viewArray{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"downLoading"];
    
    [self ImportData];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:nil forKey:@"setfinishDate"];// 设置默认完成时间为空
     
    NSString *wordNum = [NSString stringWithFormat:@"%d",25];//设置默认背诵词汇量为25.
    [userDefaults setObject:wordNum forKey:@"setdayWords"];
    
    DataBase *database = [[DataBase alloc]init];
    NSString *AllWordscount = [database getDictionaryWordCount:dic];
    
    // 更新词库总数
    UILabel *lable = [viewArray objectAtIndex:10];
    lable.text = [NSString stringWithFormat:@"词库总数量:%@",AllWordscount];
    
    if ([viewArray count] == 22 && isPurchase) {
        NSLog(@"-----购买后立刻下载请求");
        if ([dic.dictionary_pronounce compare:@"12"]==NSOrderedSame) {
            // 更新声音设置
            UIImageView *imageView = [viewArray objectAtIndex:15];
            [imageView setHidden:NO];
        }
        
        //---------------
        //获得日期和词汇量
        DataBase *dateAndWord = [[DataBase alloc]init];
        NSString *days = [dateAndWord getCompleteTime:dic];// 获得完成天数
        
        // 更新时间设置
        UIImageView *TimeimageView = [viewArray objectAtIndex:16];
        [TimeimageView setHidden:NO];
        NSArray *array = [TimeimageView subviews];
        
        if ([days compare:nil]!=NSOrderedSame && [days compare:@""]!=NSOrderedSame && [days compare:@"0"]!=NSOrderedSame) {
            NSString *dayWords = [self dayWords:dic finishDay:days];
            UILabel *day_word = [array objectAtIndex:1];
            day_word.text = [NSString stringWithFormat:@"日背诵量: %@",dayWords];
            NSString *finishTime = [self getFinishDate:days];
            UILabel *finish_time = [array objectAtIndex:0];
            finish_time.text = [NSString stringWithFormat:@"预计完成时间: %@",finishTime];
        }
        
        //---------------
        
        // 更新背诵按钮
        UIButton * button = [viewArray objectAtIndex:17];
        [button setHidden:NO];
        
        // 更新删除按钮
        UIButton * button2 = [viewArray objectAtIndex:18];
        [button2 setHidden:NO];
        
        // 屏蔽
        UILabel *lable2 = [viewArray objectAtIndex:20];
        [lable2 setHidden:YES];
        
        UIProgressView *progress = [viewArray objectAtIndex:19];
        [progress setHidden:YES];
        
        MBProgressHUD *progress2 = [viewArray objectAtIndex:21];
        [progress2 removeFromSuperview];
    }else{
        NSLog(@"-----非购买后下载");
        if ([dic.dictionary_pronounce compare:@"12"]==NSOrderedSame) {
            // 更新声音设置
            UIImageView *imageView = [viewArray objectAtIndex:13];
            [imageView setHidden:NO];
        }
        //---------------
        //获得日期和词汇量
        DataBase *dateAndWord = [[DataBase alloc]init];
        NSString *days = [dateAndWord getCompleteTime:dic];// 获得完成天数
        
        // 更新时间设置
        UIImageView *TimeimageView = [viewArray objectAtIndex:14];
        [TimeimageView setHidden:NO];
        NSArray *array = [TimeimageView subviews];
        
        if ([days compare:nil]!=NSOrderedSame && [days compare:@""]!=NSOrderedSame && [days compare:@"0"]!=NSOrderedSame && [array count]>1) {
            NSString *dayWords = [self dayWords:dic finishDay:days];
            UILabel *day_word = [array objectAtIndex:1];
            day_word.text = [NSString stringWithFormat:@"日背诵量: %@",dayWords];
            NSString *finishTime = [self getFinishDate:days];
            UILabel *finish_time = [array objectAtIndex:0];
            finish_time.text = [NSString stringWithFormat:@"预计完成时间: %@",finishTime];
        }
        
        //---------------
        
        // 屏蔽
        UILabel *lable3 = [viewArray objectAtIndex:15];
        [lable3 setHidden:YES];
        
        // 更新背诵按钮
        UIButton * button = [viewArray objectAtIndex:16];
        [button setHidden:NO];
        
        // 更新删除按钮
        UIButton * button2 = [viewArray objectAtIndex:17];
        [button2 setHidden:NO];
        
        // 屏蔽
        UILabel *lable2 = [viewArray objectAtIndex:19];
        [lable2 setHidden:YES];
        
        UIProgressView *progress = [viewArray objectAtIndex:18];
        [progress setHidden:YES];
        
        MBProgressHUD *progress2 = [viewArray objectAtIndex:20];
        [progress2 removeFromSuperview];
    }
}

// 下载成功后立刻解析xml数据到数据库中
-(void)ImportData{
    /*
     *解压数据包
     */
    NSString *user_id = (NSString*)[tool readObkect:@"user_id"];
    ServiceApi *api =[[ServiceApi alloc]init];
    NSString *stringInt = [NSString stringWithFormat:@"%@",dic.dictionary_id];
    
    [api upZip:stringInt];
    [api release];
    /*
     *导入数据到数据库
     */
    
    DataBaseHelper *db = [[DataBaseHelper alloc]init];
    //        [db initDictoinaryTable];//初始化表结构
    [db importWordXMLToDB:dic userId:user_id];//把id为1的词库插入数据库

    //默认开启词库
    [db unPauseDictionary:dic.dictionary_id userId:user_id];
    
    [db release];
    [tool refreshDicsStatus];
//    [self finishLoading];
    [newDownloadDictionaryArray addObject:dic];
}

//下载失败回调函数
-(void)downLoadFailed:(NSArray*)viewArray{
    //设置提示窗口
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"下载提示" message:@"下载失败" delegate:self cancelButtonTitle:@"关闭" otherButtonTitles:nil];
    alert.tag = 3;
    [alert show];
    [alert release];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"downLoading"];
    
    if ([viewArray count] == 22) {
        MBProgressHUD *progress2 = [viewArray objectAtIndex:21];
        [progress2 removeFromSuperview];
    }else{
        MBProgressHUD *progress2 = [viewArray objectAtIndex:19];
        [progress2 removeFromSuperview];
    }
    
    NSString *stringInt = [NSString stringWithFormat:@"%@",dic.dictionary_id];
    [api removeDictionary:stringInt];
}

// 按钮监听
-(void)packageDetail:(id) sender{
    int i = [sender tag];
    switch (i) {
        case 0:
            [self releaseDialog]; // 注销dialog
            if (![[NSUserDefaults standardUserDefaults] boolForKey:@"downLoading"]) {
                for (int i = 0; i<[viewArray2 count]; i++) {
                    [[viewArray2 objectAtIndex:i] removeFromSuperview];
                }
                for (int i = 0; i<[viewArray1 count]; i++) {
                    [[viewArray1 objectAtIndex:i] setHidden:NO];
                }
                [viewArray2 removeAllObjects];
                if (_doneAnything) {
                    [self DataPackgeList];
                }
            }
            break;
        case 1:
            
            break;
        case 2:
            
            break;
        default:
            break;
    }
}

// 转换事件监听-转换词库的发音
-(void)switchAction:(id)sender{
    dics = [api getUserHadBuyDictionary:userName userId:user.user_id];
    UserDictionary *dic_pronucation = [dics objectAtIndex:num];
    DataBase *pronucations = [[DataBase alloc]init];
    NSString *pronucation = [pronucations getCurrentDictionaryPronounce:dic_pronucation];
    
    UISwitch *switchButton = (UISwitch*)sender;
    BOOL isButtonOn = [switchButton isOn];
    if (isButtonOn) {
        NSLog(@"---9----%@",pronucation);
        [pronucations switchCurrentDictionaryPronounce:dic_pronucation];
    }else {
        NSLog(@"---92----%@",pronucation);
        [pronucations switchCurrentDictionaryPronounce:dic_pronucation];
    }
    [pronucations release];
}

// 开始学习弹出框
-(void)dialog_start_learn{
    //
    // 判断是不是IOS7版本
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"version"]) {
        start_dialog = [[UIImageView alloc] initWithFrame:CGRectMake(670,68,332,183)];
    }else{
        start_dialog = [[UIImageView alloc] initWithFrame:CGRectMake(670,48,332,183)];
    }
    UIImage *start = [UIImage imageNamed:@"dialog_start"];
    [start_dialog initWithImage:start];
    
    // button-1
    UIButton *learn_all_but=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    learn_all_but.frame=CGRectMake(11,71,306,44);
    learn_all_but.tag = 11;
    UIImage *all_image =[UIImage imageNamed:@"but_background2"];
    [learn_all_but setBackgroundImage:all_image forState:UIControlStateNormal];
    [learn_all_but addTarget:self action:@selector(RightTopButton:) forControlEvents:UIControlEventTouchUpInside];
    
    //---添加文字--start
    UILabel *lable1 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 0.0, 200.0, 20.0)];
    lable1.text = [NSString stringWithFormat:@"背诵全部"];
    lable1.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    lable1.textColor = [UIColor blackColor];
    lable1.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    [learn_all_but addSubview:lable1];
    [lable1 release];
    UILabel *lable2 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 22.0, 200.0, 20.0)];
    lable2.text = [NSString stringWithFormat:@"将背诵所有已经下载并启用的单词"];
    lable2.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    lable2.textColor = [UIColor grayColor];
    lable2.font = [UIFont fontWithName:@"Helvetica" size:13];
    [learn_all_but addSubview:lable2];
    [lable2 release];
    //---添加文字--end
    
    [start_dialog addSubview:learn_all_but];
    [start_dialog bringSubviewToFront:learn_all_but];
    [start_dialog setUserInteractionEnabled:YES];
    
    // button-2
    UIButton *learn_wrong_words=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    learn_wrong_words.frame=CGRectMake(11,122,306,44);
    learn_wrong_words.tag = 12;
    UIImage *wrong_image =[UIImage imageNamed:@"but_background"];
    [learn_wrong_words setBackgroundImage:wrong_image forState:UIControlStateNormal];
    [learn_wrong_words addTarget:self action:@selector(RightTopButton:) forControlEvents:UIControlEventTouchUpInside];
    
    //---添加文字--start
    UILabel *lable3 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 0.0, 200.0, 20.0)];
    lable3.text = [NSString stringWithFormat:@"翻阅当日错词"];
    lable3.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    lable3.textColor = [UIColor blackColor];
    lable3.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    [learn_wrong_words addSubview:lable3];
    [lable3 release];
    UILabel *lable4 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 22.0, 200.0, 20.0)];
    lable4.text = [NSString stringWithFormat:@"将只翻阅当日背诵错误的单词"];
    lable4.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    lable4.textColor = [UIColor grayColor];
    lable4.font = [UIFont fontWithName:@"Helvetica" size:13];
    [learn_wrong_words addSubview:lable4];
    [lable4 release];
    //---添加文字--end
    
    [start_dialog addSubview:learn_wrong_words];
    [start_dialog bringSubviewToFront:learn_wrong_words];
    [start_dialog setUserInteractionEnabled:YES];
    [self.view addSubview:start_dialog];
    
}

// 设置弹出框
-(void)dialog_setting{
    // 判断是不是IOS7版本
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"version"]) {
        start_dialog = [[UIImageView alloc] initWithFrame:CGRectMake(655,68,332,300)];
    }else{
        start_dialog = [[UIImageView alloc] initWithFrame:CGRectMake(655,48,332,300)];
    }
    UIImage *start = [UIImage imageNamed:@"system_setting"];
    [start_dialog initWithImage:start];
    
    // button-1
    UIButton *learn_all_but=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    learn_all_but.frame=CGRectMake(11,81,306,44);
    learn_all_but.tag = 6;
    UIImage *all_image =[UIImage imageNamed:@"but_background2"];
    [learn_all_but setBackgroundImage:all_image forState:UIControlStateNormal];
    
    // 开关转换器
    UISwitch *switchButton = [[UISwitch alloc] initWithFrame:CGRectMake(215, 6, 20, 10)];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"notification"]) {
        [switchButton setOn:YES];
    }else{
        [switchButton setOn:NO];
    }
    [switchButton addTarget:self action:@selector(notifition:) forControlEvents:UIControlEventValueChanged];
    [learn_all_but addSubview:switchButton];
    [learn_all_but bringSubviewToFront:switchButton];
    [learn_all_but setUserInteractionEnabled:YES];
    
    //---添加文字--start
    UILabel *lable = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 0.0, 200.0, 20.0)];
    lable.text = [NSString stringWithFormat:@"通知"];
    lable.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    lable.textColor = [UIColor blackColor];
    lable.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    [learn_all_but addSubview:lable];
    [lable release];
    UILabel *lable2 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 22.0, 200.0, 20.0)];
    lable2.text = [NSString stringWithFormat:@"打开或关闭提醒"];
    lable2.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    lable2.textColor = [UIColor grayColor];
    lable2.font = [UIFont fontWithName:@"Helvetica" size:13];
    [learn_all_but addSubview:lable2];
    [lable2 release];
    //---添加文字--end
    
    [start_dialog addSubview:learn_all_but];

    // button-2
    UIButton *learn_choice_but=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    learn_choice_but.frame=CGRectMake(11,132,306,44);
    learn_choice_but.tag = 7;
    UIImage *choice_image =[UIImage imageNamed:@"but_background2"];
    [learn_choice_but setBackgroundImage:choice_image forState:UIControlStateNormal];
    //开发转换器
    UISwitch *switchButton2 = [[UISwitch alloc] initWithFrame:CGRectMake(215, 6, 20, 10)];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"sound"]) {
        [switchButton2 setOn:YES];
    }else{
        [switchButton2 setOn:NO];
    }
    [switchButton2 addTarget:self action:@selector(sound:) forControlEvents:UIControlEventValueChanged];
    [learn_choice_but addSubview:switchButton2];
    [learn_choice_but bringSubviewToFront:switchButton2];
    [learn_choice_but setUserInteractionEnabled:YES];
    
    //---添加文字--start
    UILabel *lable3 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 0.0, 200.0, 20.0)];
    lable3.text = [NSString stringWithFormat:@"声音"];
    lable3.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    lable3.textColor = [UIColor blackColor];
    lable3.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    [learn_choice_but addSubview:lable3];
    [lable3 release];
    UILabel *lable4 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 22.0, 200.0, 20.0)];
    lable4.text = [NSString stringWithFormat:@"打开或关闭声音"];
    lable4.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    lable4.textColor = [UIColor grayColor];
    lable4.font = [UIFont fontWithName:@"Helvetica" size:13];
    [learn_choice_but addSubview:lable4];
    [lable4 release];
    //---添加文字--end
    
    [start_dialog addSubview:learn_choice_but];

    // button-3
    UIButton *learn_wrong_words=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    learn_wrong_words.frame=CGRectMake(11,181,306,44);
    learn_wrong_words.tag = 8;
    UIImage *wrong_image =[UIImage imageNamed:@"but_background2"];
    [learn_wrong_words setBackgroundImage:wrong_image forState:UIControlStateNormal];
    //开关转换器
    UISwitch *switchButton3 = [[UISwitch alloc] initWithFrame:CGRectMake(215, 6, 20, 10)];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"autoplay"]) {
        [switchButton3 setOn:YES];
    }else{
        [switchButton3 setOn:NO];
    }
    [switchButton3 addTarget:self action:@selector(autoplay:) forControlEvents:UIControlEventValueChanged];
    [learn_wrong_words addSubview:switchButton3];
    [learn_wrong_words bringSubviewToFront:switchButton3];
    [learn_wrong_words setUserInteractionEnabled:YES];
    
    //---添加文字--start
    UILabel *lable5 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 0.0, 200.0, 20.0)];
    lable5.text = [NSString stringWithFormat:@"声音自动播放"];
    lable5.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    lable5.textColor = [UIColor blackColor];
    lable5.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    [learn_wrong_words addSubview:lable5];
    [lable5 release];
    UILabel *lable6 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 22.0, 200.0, 20.0)];
    lable6.text = [NSString stringWithFormat:@"打开此项，背诵时将自动播放音频"];
    lable6.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    lable6.textColor = [UIColor grayColor];
    lable6.font = [UIFont fontWithName:@"Helvetica" size:13];
    [learn_wrong_words addSubview:lable6];
    [lable6 release];
    //---添加文字--end
    
    [start_dialog addSubview:learn_wrong_words];
    // button-4
    UIButton *setting_help=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    setting_help.frame=CGRectMake(11,238,306,25.5);
    setting_help.tag = 9;
    UIImage *help_image =[UIImage imageNamed:@"but_background"];
    [setting_help setBackgroundImage:help_image forState:UIControlStateNormal];
    [setting_help addTarget:self action:@selector(RightTopButton:) forControlEvents:UIControlEventTouchUpInside];
    
    //---添加文字--start
    UILabel *lable7 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 3.0, 200.0, 20.0)];
    lable7.text = [NSString stringWithFormat:@"使用帮助"];
    lable7.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    lable7.textColor = [UIColor blackColor];
    lable7.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    [setting_help addSubview:lable7];
    [lable7 release];
    //---添加文字--end
    
    [start_dialog addSubview:setting_help];
    [start_dialog bringSubviewToFront:setting_help];
    [start_dialog setUserInteractionEnabled:YES];
//    [setting_help release];
    [self.view addSubview:start_dialog];
}

// 用户设置弹出框
-(void)user_setting{
    // 判断是不是IOS7版本
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"version"]) {
        start_dialog = [[UIImageView alloc] initWithFrame:CGRectMake(605,68,332,164)];
    }else{
        start_dialog = [[UIImageView alloc] initWithFrame:CGRectMake(605,48,332,164)];
    }
    UIImage *start = [UIImage imageNamed:@"user_setting"];
    [start_dialog initWithImage:start];
    
    // button-1
    UIButton *synchronization_but=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    synchronization_but.frame=CGRectMake(11,50,306,44);
    synchronization_but.tag = 3;
    UIImage *all_image =[UIImage imageNamed:@"but_background2"];
    [synchronization_but setBackgroundImage:all_image forState:UIControlStateNormal];
    [synchronization_but addTarget:self action:@selector(RightTopButton:) forControlEvents:UIControlEventTouchUpInside];
    
    //---添加文字--start
    UILabel *lable = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 0.0, 200.0, 20.0)];
    lable.text = [NSString stringWithFormat:@"恢复购买"];
    lable.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    lable.textColor = [UIColor blackColor];
    lable.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    [synchronization_but addSubview:lable];
    [lable release];
    UILabel *lable2 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 22.0, 200.0, 20.0)];
    lable2.text = [NSString stringWithFormat:@"恢复已购买的词库"];
    lable2.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    lable2.textColor = [UIColor grayColor];
    lable2.font = [UIFont fontWithName:@"Helvetica" size:13];
    [synchronization_but addSubview:lable2];
    [lable2 release];
    //---添加文字--end
    
    //button-1 添加到view中
    [start_dialog addSubview:synchronization_but];
    [start_dialog bringSubviewToFront:synchronization_but];
    [start_dialog setUserInteractionEnabled:YES];
//    [synchronization_but release];
    // button-3
    UIButton *sing_out=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    sing_out.frame=CGRectMake(11,100,306,44);
    sing_out.tag = 5;
    UIImage *sing_out_image =[UIImage imageNamed:@"but_background"];
    [sing_out setBackgroundImage:sing_out_image forState:UIControlStateNormal];
    [sing_out addTarget:self action:@selector(RightTopButton:) forControlEvents:UIControlEventTouchUpInside];
    
    //---添加文字--start
    UILabel *lable3 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 0.0, 200.0, 20.0)];
    lable3.text = [NSString stringWithFormat:@"退出登录"];
    lable3.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    lable3.textColor = [UIColor blackColor];
    lable3.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    [sing_out addSubview:lable3];
    [lable3 release];
    
    UILabel *lable4 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 22.0, 200.0, 20.0)];
    lable4.text = [NSString stringWithFormat:@"退出登录，或者切换用户名"];
    lable4.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    lable4.textColor = [UIColor grayColor];
    lable4.font = [UIFont fontWithName:@"Helvetica" size:13];
    [sing_out addSubview:lable4];
    [lable4 release];
    //---添加文字--end
    
    [start_dialog addSubview:sing_out];
    [start_dialog bringSubviewToFront:sing_out];
    [start_dialog setUserInteractionEnabled:YES];
//    [sing_out release];
    [self.view addSubview:start_dialog];
 }

// 注销弹出框方法
-(void)releaseDialog{
    if (start_dialog) {
        [start_dialog removeFromSuperview];// 取消弹出框
        isShow = FALSE;// 标记显示状态
    }
}

// 通知开关事件
-(void)notifition:(id) sender{
    UISwitch *switchButton = (UISwitch*)sender;
    BOOL isButtonOn = [switchButton isOn];
    if (isButtonOn) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"notification"];
    }else {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"notification"];
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    }
}

// 声音开关事件
-(void)sound:(id) sender{
    UISwitch *switchButton = (UISwitch*)sender;
    BOOL isButtonOn = [switchButton isOn];
    if (isButtonOn) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"sound"];
    }else {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"sound"];
    }
}

// 自动播放声音
-(void)autoplay:(id) sender{
    UISwitch *switchButton = (UISwitch*)sender;
    BOOL isButtonOn = [switchButton isOn];
    if (isButtonOn) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"autoplay"];
    }else {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"autoplay"];
    }
}

// 弹出框事件监听
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if ([alertView tag] == 1) {
        if (buttonIndex == 0) {
            
        }else if(buttonIndex == 1){
            [self Download:(id)[alertView tag]];
        }
    }else{
        if (buttonIndex == 0) {
            
        }else if (buttonIndex == 1){
            _doneAnything = true;
            for (int i = 0; i<[viewArray1 count]; i++) {
                [[viewArray1 objectAtIndex:i] removeFromSuperview];
            }
            UserDictionary *dic2 = (UserDictionary*)[alertView tag];
            
            NSString *documentsDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches"];
            NSFileManager *fileManager = [NSFileManager defaultManager];//文件
            
            NSString *dicName = [NSString stringWithFormat:@"%@.zip",dic2.dictionary_id];
            NSString *filePath2 =  [documentsDirectory stringByAppendingPathComponent:dicName];
            BOOL isDirExist = [fileManager fileExistsAtPath:filePath2];
            
            NSString *stringInt = [NSString stringWithFormat:@"%@",dic2.dictionary_id];
            NSString *filePath =  [documentsDirectory stringByAppendingPathComponent:stringInt];
            BOOL filedDirExist = [fileManager fileExistsAtPath:filePath];
            
            NSString *dBfilePath =  [documentsDirectory stringByAppendingPathComponent:@"db.sqlite"];
            BOOL isDbExist = [fileManager fileExistsAtPath:dBfilePath];

            if (filedDirExist||isDirExist) {
                Algorithm *alg = [[Algorithm alloc]init];
                [alg DeleteWordFromArray:dic2];
                
                NSString *stringInt = [NSString stringWithFormat:@"%@",dic2.dictionary_id];
                [api removeDictionary:stringInt];
                
                if (isDbExist) {
                    DataBaseHelper *api2 =[[DataBaseHelper alloc]init];
                    [api2 deleteDictionaryInDB:stringInt];
                    
                    NSString *user_id = (NSString*)[tool readObkect:@"user_id"];
                    //暂停词库
                    [api2 pauseDictionary:dic2.dictionary_id userId:user_id];
                    [api2 release];
                    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                    [userDefaults setObject:@"0" forKey:@"dwNum"];
                    [userDefaults removeObjectForKey:@"todayArray"];
                    [userDefaults removeObjectForKey:@"wrongWords"];
                    [userDefaults removeObjectForKey:@"now_use_dictionary"];
                    [userDefaults removeObjectForKey:@"finished"];

                }
                
            }else{
                
            }
            //        }
            for (int i= 0; i <[viewArray2 count]; i++) {
                [[viewArray2 objectAtIndex:i] removeFromSuperview];
            }
            [self DataPackgeList];
        }else if (buttonIndex == 2){
            
        }
    }
}

// 屏幕旋转处理
#define degreesToRadian(x) (M_PI * (x) / 180.0)
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    //旋转方向判断
    if (toInterfaceOrientation==UIDeviceOrientationLandscapeRight) {
    }
}

// ------支付------start
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated{
	[super viewDidDisappear:animated];
}
// ------支付------end

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [super viewDidUnload];
//    _tableView= nil;
//    [dic release];
//    [percent release];
//    [dics release];
//    [dicPause release];
//    [api release];
//    [_barView release];
//    [_list release];
//    [start_dialog release];
//    [_progress release];
//    [_process release];
//    [_progressHUD release];
//    [headViewArray release];
//    [doubleStruct release];
//    [_refreshHeaderView release];
//    [bigImage release];
}

-(void)dealloc {
//    [dic release];
//    [percent release];
//    [dics release];
//    [dicPause release];
//    [api release];
//    [doubleStruct release];
//    [_barView release];
//    [_list release];
//    [start_dialog release];
//    [_progress release];
//    [_process release];
//    [_progressHUD release];
//    [headViewArray release];
//    [doubleStruct release];
//    [_refreshHeaderView release];
//    [bigImage release];
    [super dealloc];
}
@end
