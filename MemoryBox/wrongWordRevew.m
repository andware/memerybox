//
//  wrongWordRevew.m
//  MemoryBoxRed
//
//  Created by YangJoe on 4/27/14.
//
//

#import "wrongWordRevew.h"
#import "Algorithm.h"
#import "tools.h"
#import "wordStructure.h"
#import "DateUnits.h"
#import "Init.h"
#import "Animations.h"
#import "DataBase.h"
#import "wrongWords.h"
#import "QuestionC.h"

@interface wrongWordRevew ()

@end

@implementation wrongWordRevew

@synthesize progressHUD = _progressHUD;
BOOL backLaseView;
BOOL ClickToChooseAnsware;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // initialize variables
    GlobalWord = [[Word alloc]init];
    viewArray = [[NSMutableArray alloc]init];
    OneViewCache= [[UIView alloc]init];
    count = 0;
    
    // judge use one or all dictionary and get a less than 20 words array
    DayWordArray = [[NSMutableArray alloc]init];
    tools *tool = [[tools alloc]init];
    DayWordArray = [tool readArray:@"wrongWordArray"];
    NSLog(@"size = %lu",(unsigned long)[DayWordArray count]);
    
    if ([DayWordArray count] > 0) {
        // add the first view when start memory
        Word *word = [DayWordArray objectAtIndex:0];
        [self ViewType:word];
        [self.view addSubview:[viewArray objectAtIndex:0]];
        GlobalWord = word;
        [Animations moveLeft:[viewArray objectAtIndex:0] andAnimationDuration:0.3 andWait:YES andLength:1000.0];
        
        //添加手势滑动的事件
        UISwipeGestureRecognizer *recognizer;
        recognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handleSwipeFrom:)];
        [recognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
        [[self view] addGestureRecognizer:recognizer];
        [recognizer release];
        
        recognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handleSwipeFrom:)];
        [recognizer setDirection:(UISwipeGestureRecognizerDirectionLeft)];
        [[self view] addGestureRecognizer:recognizer];
        [recognizer release];
        
    }else{
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"提示" message:@"今天没有错误单词需要复习哦!" delegate:self cancelButtonTitle:nil  otherButtonTitles:@"确定",nil];
        alert.tag = 1;
        [alert show];
        [alert release];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)Back:(id)sender {
    [self dismissModalViewControllerAnimated:NO];
}

- (IBAction)Recite:(id)sender {
    if (avAudioPlayer) {
        [avAudioPlayer release];
    }
    
    //把音频文件转换成url格式
    NSURL *url = [NSURL fileURLWithPath:GlobalWord.word_us_voice1];
    //初始化音频类 并且添加播放文件
    NSError *avPlayerError = nil;
    avAudioPlayer = [[[AVAudioPlayer alloc]init] initWithContentsOfURL:url error:&avPlayerError];
    if (avPlayerError)
    {
        NSLog(@"Error: %@", [avPlayerError description]);
    }
    //设置代理
    avAudioPlayer.delegate = self;
    //设置初始音量大小
    avAudioPlayer.volume = 1;
    //设置音乐播放次数  -1为一直循环
    avAudioPlayer.numberOfLoops = 0;
    //预播放
    [avAudioPlayer prepareToPlay];
    [avAudioPlayer play];
}
- (void)Recite{
    bool sunndOff = [[NSUserDefaults standardUserDefaults] boolForKey:@"sound"];
    if (sunndOff) {
        if (avAudioPlayer) {
            [avAudioPlayer release];
        }
        
        //把音频文件转换成url格式
        NSURL *url = [NSURL fileURLWithPath:GlobalWord.word_us_voice1];
        //初始化音频类 并且添加播放文件
        NSError *avPlayerError = nil;
        avAudioPlayer = [[[AVAudioPlayer alloc]init] initWithContentsOfURL:url error:&avPlayerError];
        if (avPlayerError)
        {
            NSLog(@"Error: %@", [avPlayerError description]);
        }
        
        //设置代理
        avAudioPlayer.delegate = self;
        //设置初始音量大小
        avAudioPlayer.volume = 1;
        //设置音乐播放次数  -1为一直循环
        avAudioPlayer.numberOfLoops = 0;
        //预播放
        [avAudioPlayer prepareToPlay];
        [avAudioPlayer play];
    }
}

// 加载圈
-(void)Loading{
    //显示加载等待框
    self.progressHUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:self.progressHUD];
    [_progressHUD release];
    [self.view bringSubviewToFront:self.progressHUD];
    self.progressHUD.delegate = self;
    //    self.progressHUD.labelText = @"同步中...";
    [self.progressHUD show:YES];
}


- (IBAction)Pre:(UIButton *)sender {
    wordStructure *wordStruct = [DayWordArray objectAtIndex:0];
    Algorithm *alg = [[Algorithm alloc]init];
    DayWordArray = [alg WrongUpdateWordArray:DayWordArray word:wordStruct];
}

- (IBAction)Next:(UIButton *)sender {
    // 获得当前单词
    wordStructure *wordStruct = [DayWordArray objectAtIndex:0];
    Algorithm *alg = [[Algorithm alloc]init];
    
    // 判断被动的是单个词库还是全部词库
    NSString *getDicType = [[NSUserDefaults standardUserDefaults] stringForKey:@"noe-dic"];
    if ([getDicType compare:@"one-dic"]==NSOrderedSame) {
        DayWordArray = [alg UpdateOneDicArray:DayWordArray word:wordStruct];
    }else{
        DayWordArray = [alg UpdateAllDicArray:DayWordArray word:wordStruct];
    }
    
    
}

// The Dictinoary title lable
-(UILabel*)AddDictionaryLable:(Word*) word1{
    UILabel *Word_dicName = [[UILabel alloc]initWithFrame:CGRectMake(760.0, 580.0, 200.0, 30.0)];
    Word_dicName.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    Word_dicName.textAlignment = UITextAlignmentCenter;
    Word_dicName.adjustsFontSizeToFitWidth = YES;
    Word_dicName.font = [UIFont fontWithName:@"Helvetica" size:15];
    Word_dicName.textColor = [UIColor grayColor];
    NSLog(@"--%@",word1.word_dictionary);
    if ([word1.word_dictionary compare:@"nil"]==NSOrderedSame) {
        NSLog(@"dd");
    }
    Word_dicName.text = word1.word_dictionary;
    return Word_dicName;
}
-(UILabel*)AddDictionaryLable2:(Word*) word1{
    UILabel *Word_dicName = [[UILabel alloc]initWithFrame:CGRectMake(760.0, 520.0, 200.0, 30.0)];
    Word_dicName.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    Word_dicName.textAlignment = UITextAlignmentCenter;
    Word_dicName.adjustsFontSizeToFitWidth = YES;
    Word_dicName.font = [UIFont fontWithName:@"Helvetica" size:15];
    Word_dicName.textColor = [UIColor grayColor];
    Word_dicName.text = word1.word_dictionary;
    return Word_dicName;
}

// Show answer analyse mothed
-(UILabel*)analyzeText:(Word*)word{
    UILabel *analyze = [[UILabel alloc]initWithFrame:CGRectMake(67.0, 515.0, 100.0, 50.0)];
    analyze.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    analyze.textAlignment = UITextAlignmentCenter;
    analyze.font = [UIFont fontWithName:@"Helvetica" size:20];
    analyze.textColor = [UIColor blueColor];
    analyze.adjustsFontSizeToFitWidth = YES;
    analyze.text = @"答案解析";
    analyze.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(TextClick2:)];
    [analyze addGestureRecognizer:singleTap];
    UIView *singleTapView = [singleTap view];
    singleTapView.tag = (long)word;
    [singleTap release];
    // 字段解析释放为空
    if (word.word_analyze.length == 0) {
        [analyze setHidden:YES];
    }
    return analyze;
}

-(UILabel*)analyzeTextBC:(Word*)word{
    UILabel *analyze = [[UILabel alloc]initWithFrame:CGRectMake(67.0, 515.0, 100.0, 50.0)];
    analyze.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    analyze.textAlignment = UITextAlignmentCenter;
    analyze.font = [UIFont fontWithName:@"Helvetica" size:20];
    analyze.textColor = [UIColor blueColor];
    analyze.adjustsFontSizeToFitWidth = YES;
    analyze.text = @"答案解析";
    analyze.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(TextClick2:)];
    [analyze addGestureRecognizer:singleTap];
    UIView *singleTapView = [singleTap view];
    singleTapView.tag = (long)word;
    [singleTap release];
    // 字段解析释放为空
    [analyze setHidden:YES];
    return analyze;
}

// Text click response method
-(void)TextClick2:(id)sender{
    UITapGestureRecognizer *singleTap = (UITapGestureRecognizer *)sender;
    Word *new_word = (Word*)[singleTap view].tag;
    [self analyze:new_word.word_analyze];
}
// Dialog for answer analyse
-(void)analyze:(NSString*)analyze_text{
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"答案解析" message:analyze_text delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
    alert.tag = 0;
    [alert show];
    [alert release];
}

//Sliding events response method
-(void)handleSwipeFrom:(UISwipeGestureRecognizer *)recognizer{
    if(recognizer.direction==UISwipeGestureRecognizerDirectionRight) {//向右滑动
        if ([[OneViewCache subviews] count] >0 && !backLaseView) {
            backLaseView = true;
            [self.view addSubview:OneViewCache];
            [Animations moveRight:[viewArray objectAtIndex:0] andAnimationDuration:0.3 andWait:YES andLength:1000.0];
            [Animations moveRight:OneViewCache andAnimationDuration:0.3 andWait:YES andLength:1000.0];
            [self Recite];
        }
    }
    
    if(recognizer.direction==UISwipeGestureRecognizerDirectionLeft) {//向左滑动
        if (!ClickToChooseAnsware) {
            if (backLaseView) {
                backLaseView = false;
                [Animations moveLeft:OneViewCache andAnimationDuration:0.3 andWait:YES andLength:1000.0];
                [Animations moveLeft:[viewArray objectAtIndex:0] andAnimationDuration:0.3 andWait:YES andLength:1000.0];
                [self Recite];
            }else if ([viewArray count] >=2 ){
                ClickToChooseAnsware = true;
//                [self progressImage];
                [self.view addSubview:[viewArray objectAtIndex:1]];
                [Animations moveLeft:[viewArray objectAtIndex:0] andAnimationDuration:0.3 andWait:YES andLength:1000.0];
                [Animations moveLeft:[viewArray objectAtIndex:1] andAnimationDuration:0.3 andWait:YES andLength:1000.0];
                [[viewArray objectAtIndex:0] removeFromSuperview];
                OneViewCache = [viewArray objectAtIndex:0];
                [viewArray removeObjectAtIndex:0];
                ClickToChooseAnsware =false;
//                [self Recite];
            }
        }
    }
}

-(void)progressImage{
    if (processing) {
        [processing removeFromSuperview];
        processing = nil;
    }
    Algorithm *alg = [[Algorithm alloc]init];
    NSString *getDicType = [[NSUserDefaults standardUserDefaults] stringForKey:@"noe-dic"];
    float progressInt;
    if ([getDicType compare:@"one-dic"]==NSOrderedSame) {
        progressInt = [alg CountOneDicProgress:DayWordArray word:GlobalWord];
    }else{
        progressInt = [alg CountAllDicProgress:DayWordArray];
    }
    if (progressInt <0) {
        progressInt = 0;
    }
    processing = [[UIImageView alloc] initWithFrame:CGRectMake(62,705,942*progressInt,23)];
    processing.image = [UIImage imageNamed:@"process"];//加载入图片
    [self.view addSubview:processing];
}

-(void)progressEndImage{
    if (processing) {
        [processing removeFromSuperview];
        processing = nil;
    }
    float progressInt = 1.00;
    processing = [[UIImageView alloc] initWithFrame:CGRectMake(62,705,942*progressInt,23)];
    processing.image = [UIImage imageNamed:@"process"];//加载入图片
    [self.view addSubview:processing];
}

-(void)ViewType:(Word*) word{
    int value2 = arc4random() % 2;//单词随机题型
    int value = arc4random() % 3;//单词随机题型
    int value3 = arc4random() % 4;//ABCD型
    
    // 查询单词目前的天数
    tools *tool = [[tools alloc]init];
    NSString *user_id = (NSString*)[tool readObkect:@"user_id"];
    DataBase *database = [[DataBase alloc]init];
    NSString *days = [database getWordDay:word.word_id userId:user_id];
    
    int worday = [days intValue];
    
    if ((([word.abc compare:@"1"]==NSOrderedSame)||([word.abc compare:@"12"]==NSOrderedSame)||([word.abc compare:@"13"]==NSOrderedSame)||([word.abc compare:@"14"]==NSOrderedSame)||([word.abc compare:@"123"]==NSOrderedSame)||([word.abc compare:@"124"]==NSOrderedSame)||([word.abc compare:@"134"]==NSOrderedSame)||([word.abc compare:@"1234"]==NSOrderedSame)) && worday ==0) {
        //题型A
        [self QuestionA:word];
    }else if([word.abc compare:@"1234"]==NSOrderedSame){
        switch (value3) {
            case  0:
                //题型A
                [self QuestionA:word];
                break;
            case  1:
                //题型B
                [self QuestionB:word];
                break;
            case  2:
                //题型C
                [self QuestionC:word];
                break;
            case  3:
                //题型D
                [self QuestionD:word];
                break;
        }
    }else if([word.abc compare:@"124"]==NSOrderedSame){
        switch (value) {
            case  0:
                //题型A
                [self QuestionA:word];
                break;
            case  1:
                //题型B
                [self QuestionB:word];
                break;
            case  2:
                //题型D
                [self QuestionD:word];
                break;
        }
    }else if([word.abc compare:@"134"]==NSOrderedSame){
        switch (value) {
            case  0:
                //题型A
                [self QuestionA:word];
                break;
            case  1:
                //题型C
                [self QuestionC:word];
                break;
            case  2:
                //题型D
                [self QuestionD:word];
                break;
        }
    }else if([word.abc compare:@"234"]==NSOrderedSame){
        switch (value) {
            case  0:
                //题型B
                [self QuestionB:word];
                break;
            case  1:
                //题型C
                [self QuestionC:word];
                break;
            case  2:
                //题型D
                [self QuestionD:word];
                break;
        }
    }else if((([word.abc compare:@"123"]==NSOrderedSame)) && worday !=0){
        switch (value) {
            case  0:
                //题型A
                [self QuestionA:word];
                break;
            case  1:
                //题型B
                [self QuestionB:word];
                break;
            case  2:
                //题型C
                [self QuestionC:word];
                break;
        }
    }else if([word.abc compare:@"14"]==NSOrderedSame){
        switch (value2) {
            case 0:
                //题型A
                [self QuestionA:word];
                break;
                
            case 1:
                //题型D
                [self QuestionD:word];
                
                break;
        }
    }else if([word.abc compare:@"13"]==NSOrderedSame){
        switch (value2) {
            case 0:
                //题型A
                [self QuestionA:word];
                break;
                
            case 1:
                //题型C
                [self QuestionC:word];
                
                break;
        }
    }else if([word.abc compare:@"24"]==NSOrderedSame){
        switch (value2) {
            case 0:
                //题型B
                [self QuestionB:word];
                break;
                
            case 1:
                //题型D
                [self QuestionD:word];
                
                break;
        }
    }else if([word.abc compare:@"34"]==NSOrderedSame){
        switch (value2) {
            case 0:
                //题型C
                [self QuestionC:word];
                break;
                
            case 1:
                //题型D
                [self QuestionD:word];
                break;
        }
    }else if(([word.abc compare:@"12"]==NSOrderedSame)){
        switch (value2) {
            case 0:
                //题型A
                [self QuestionA:word];
                break;
                
            case 1:
                //题型B
                [self QuestionB:word];
                break;
        }
    }else if(([word.abc compare:@"23"]==NSOrderedSame)){
        switch (value2) {
            case  0:
                //题型B
                [self QuestionB:word];
                break;
            case  1:
                //题型C
                [self QuestionC:word];
                break;
        }
    }else if([word.abc compare:@"4"]==NSOrderedSame){
        //题型D
        [self QuestionD:word];
    }else if([word.abc compare:@"3"]==NSOrderedSame){
        //题型C
        [self QuestionC:word];
    }else if([word.abc compare:@"2"]==NSOrderedSame){
        //题型B
        [self QuestionB:word];
    }else if([word.abc compare:@"1"]==NSOrderedSame){
        //题型A
        [self QuestionA:word];
    }
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if ([alertView tag] == 0) {
        
    }else if([alertView tag] == 1){
        [self dismissModalViewControllerAnimated:NO];
    }
}

// response methods for Type A & D
-(void)AorDRight:(id) sender{
    OneViewCache = nil;
    ClickToChooseAnsware = true;//mark for 'Click' to protect view error
    tools *tool = [[tools alloc]init];
    DayWordArray = [tool readArray:@"wrongWordArray"];
    if ([DayWordArray count] > 1) {
        count ++;
        if (count < [DayWordArray count]) {
        Word *word = [DayWordArray objectAtIndex:count];
        [self ViewType:word];// update view array
        GlobalWord = word;
        // change the view by animation
        [self.view addSubview:[viewArray objectAtIndex:1]];
        [Animations moveLeft:[viewArray objectAtIndex:0] andAnimationDuration:0.3 andWait:YES andLength:1000.0];
        [Animations moveLeft:[viewArray objectAtIndex:1] andAnimationDuration:0.3 andWait:YES andLength:1000.0];
        [[viewArray objectAtIndex:0] removeFromSuperview];
        [viewArray removeObjectAtIndex:0];
        [self Recite];
        }else{
            [self progressEndImage];
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"复习提示" message:@"你已经复习完今天所有的错误单词了!" delegate:self cancelButtonTitle:nil  otherButtonTitles:@"确定",nil];
            alert.tag = 1;
            [alert show];
            [alert release];
        }
    }else{
        [self progressEndImage];
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"复习提示" message:@"你已经复习完今天所有的错误单词了!" delegate:self cancelButtonTitle:nil  otherButtonTitles:@"确定",nil];
        alert.tag = 1;
        [alert show];
        [alert release];
    }
    ClickToChooseAnsware = false;//mark for 'Click' to protect view error
}

-(void)AorDWrong:(id) sender{
    OneViewCache = nil;
    ClickToChooseAnsware = true;//mark for 'Click' to protect view error
    // update word array when answared right
    
    tools *tool = [[tools alloc]init];
    DayWordArray = [tool readArray:@"wrongWordArray"];
    
    count++;
    if (count < [DayWordArray count]) {
    Word *word = [DayWordArray objectAtIndex:count];
    [self ViewType:word];// update view array
    GlobalWord = word;
    
    // change the view by animation
    [self.view addSubview:[viewArray objectAtIndex:1]];
    [Animations moveLeft:[viewArray objectAtIndex:0] andAnimationDuration:0.3 andWait:YES andLength:1000.0];
    [Animations moveLeft:[viewArray objectAtIndex:1] andAnimationDuration:0.3 andWait:YES andLength:1000.0];
    [[viewArray objectAtIndex:0] removeFromSuperview];
    [viewArray removeObjectAtIndex:0];
    ClickToChooseAnsware = false;//mark for 'Click' to protect view error
    }else{
        [self progressEndImage];
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"复习提示" message:@"你已经复习完今天所有的错误单词了!" delegate:self cancelButtonTitle:nil  otherButtonTitles:@"确定",nil];
        alert.tag = 1;
        [alert show];
        [alert release];
    }
}

// response methods for Type B
-(void)BChoice:(id)sender{
    // get word from the 'sender'
    wrongWords *getWord = (wrongWords *)[sender tag];//得到结构
    Word *word = getWord.myword;//得到单词
    
    // get choice place and mark
    UIView *view = [viewArray objectAtIndex:0];
    NSArray *ui = [view subviews];
    int i = [getWord.randomTag intValue];//正确答案是第几个
    int j;
    switch (i) {
        case 0:
            //标记选择的答案
            if ([getWord.answare compare:word.word_b_explana]==NSOrderedSame) {
                j=1;
            }else if([getWord.answare compare:word.word_b_explanb]==NSOrderedSame){
                j=2;
            }else if([getWord.answare compare:word.word_b_explanc]==NSOrderedSame){
                j=3;
            }else if([getWord.answare compare:word.word_b_expland]==NSOrderedSame){
                j=4;
            }else {
                j=5;
            }
            break;
        case 1:
            //标记选择的答案
            if ([getWord.answare compare:word.word_b_explanb]==NSOrderedSame) {
                j=1;
            }else if([getWord.answare compare:word.word_b_explana]==NSOrderedSame){
                j=2;
            }else if([getWord.answare compare:word.word_b_explanc]==NSOrderedSame){
                j=3;
            }else if([getWord.answare compare:word.word_b_expland]==NSOrderedSame){
                j=4;
            }else {
                j=5;
            }
            break;
        case 2:
            //标记选择的答案
            if ([getWord.answare compare:word.word_b_explanc]==NSOrderedSame) {
                j=1;
            }else if([getWord.answare compare:word.word_b_explanb]==NSOrderedSame){
                j=2;
            }else if([getWord.answare compare:word.word_b_explana]==NSOrderedSame){
                j=3;
            }else if([getWord.answare compare:word.word_b_expland]==NSOrderedSame){
                j=4;
            }else {
                j=5;
            }
            break;
        case 3:
            //标记选择的答案
            if ([getWord.answare compare:word.word_b_expland]==NSOrderedSame) {
                j=1;
            }else if([getWord.answare compare:word.word_b_explanb]==NSOrderedSame){
                j=2;
            }else if([getWord.answare compare:word.word_b_explanc]==NSOrderedSame){
                j=3;
            }else if([getWord.answare compare:word.word_b_explane]==NSOrderedSame){
                j=4;
            }else {
                j=5;
            }
            break;
        case 4:
            //标记选择的答案
            if ([getWord.answare compare:word.word_b_explane]==NSOrderedSame) {
                j=1;
            }else if([getWord.answare compare:word.word_b_explanb]==NSOrderedSame){
                j=2;
            }else if([getWord.answare compare:word.word_b_explanc]==NSOrderedSame){
                j=3;
            }else if([getWord.answare compare:word.word_b_expland]==NSOrderedSame){
                j=4;
            }else {
                j=5;
            }
            break;
    }
    
    if ([ui count] == 9 && j == 5) {
        j = 4;
    }
    // mark the wrong answare as red
    UIButton *la2 = [ui objectAtIndex:j+1];
    [la2 setBackgroundColor:[UIColor redColor]];
    
    if ([[la2.titleLabel.text substringFromIndex:5] compare:word.word_b_right]==NSOrderedSame) {
    }else{
        [la2 addSubview:[self BWrong]];
    }
    
    // mark the right answare as green
    if (word.word_b_explane.length == 0) {
        for (int n =0; n <4; n++) {
            UIButton *bt = [ui objectAtIndex:n+2];
            NSString *title = [bt.titleLabel.text substringFromIndex:5];
            if ([title compare:word.word_b_right]==NSOrderedSame) {
                [bt setBackgroundColor:[UIColor greenColor]];
                [bt addSubview:[self BRight]];
            }
        }
    }else{
        for (int n =0; n <5; n++) {
            UIButton *bt = [ui objectAtIndex:n+2];
            NSString *title = [bt.titleLabel.text substringFromIndex:5];
            if ([title compare:word.word_b_right]==NSOrderedSame) {
                [bt setBackgroundColor:[UIColor greenColor]];
                [bt addSubview:[self BRight]];
            }
        }
    }
    
    // Hide chicken andware button
    UIButton *la22 = [ui objectAtIndex:[ui count]-2];
    [la22 setHidden:YES];
    
    // user can just click once
    if ([ui count] == 8) {
        for (int h = 2 ; h < 6; h++) {
            UIButton *but = [ui objectAtIndex:h];
            [but setUserInteractionEnabled:NO];
        }
        UIButton *but = [ui objectAtIndex:7];
        [but setHidden:YES];
    }else if([ui count] == 9){
        for (int h = 2 ; h < 7; h++) {
            UIButton *but = [ui objectAtIndex:h];
            [but setUserInteractionEnabled:NO];
        }
        UIButton *but = [ui objectAtIndex:8];
        [but setHidden:YES];
    }
    
    // show answare analyse
    if (word.word_analyze.length != 0 && [getWord.answare compare:word.word_b_right]!=NSOrderedSame) {
        UILabel *analyze = [ui objectAtIndex:[ui count]-3];
        analyze.userInteractionEnabled = YES;
        [analyze setHidden:NO];
    }
    
    [NSTimer scheduledTimerWithTimeInterval: 0.5
                                     target: self
                                   selector: @selector(handleTimer:)
                                   userInfo: getWord
                                    repeats: NO];
}

-(void)handleTimer:(NSTimer *)sender{
    wrongWords *wrong = sender.userInfo;
    Word *word = wrong.myword;//得到单词
    
    ClickToChooseAnsware = true;//mark for 'Click' to protect view error
    
    //如果回答正确
    if ([wrong.answare compare:word.word_b_right]==NSOrderedSame) {
        if ([DayWordArray count] > 1) {
            // update word array when answared right
            count ++;
            
            if (count < [DayWordArray count]) {
            Word *word = [DayWordArray objectAtIndex:count];
            [self ViewType:word];// update view array
            GlobalWord = word;
            
            // change the view by animation
            [self.view addSubview:[viewArray objectAtIndex:1]];
            [Animations moveLeft:[viewArray objectAtIndex:0] andAnimationDuration:0.3 andWait:YES andLength:1000.0];
            [Animations moveLeft:[viewArray objectAtIndex:1] andAnimationDuration:0.3 andWait:YES andLength:1000.0];
            [[viewArray objectAtIndex:0] removeFromSuperview];
            [viewArray removeObjectAtIndex:0];
            [self Recite];
            }else{
                [self progressEndImage];
                UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"复习提示" message:@"你已经复习完今天所有的错误单词了!" delegate:self cancelButtonTitle:nil  otherButtonTitles:@"确定",nil];
                alert.tag = 1;
                [alert show];
                [alert release];
            }
        }else{
            [self progressEndImage];
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"复习提示" message:@"恭喜你，你已经复习玩今天的所有错误单词了!" delegate:self cancelButtonTitle:nil  otherButtonTitles:@"确定",nil];
            alert.tag = 1;
            [alert show];
            [alert release];
        }
    }else{//如果回答错误
        // update word array when answared right
        
        tools *tool = [[tools alloc]init];
        DayWordArray = [tool readArray:@"wrongWordArray"];
        
        count ++;
        if (count < [DayWordArray count]) {
        if (count < [DayWordArray count]) {
            Word *word = [DayWordArray objectAtIndex:count];
            [self ViewType:word];// update view array
            GlobalWord = word;
        }else{
            [self progressEndImage];
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"复习提示" message:@"恭喜你，你已经复习玩今天的所有错误单词了!" delegate:self cancelButtonTitle:nil  otherButtonTitles:@"确定",nil];
            alert.tag = 1;
            [alert show];
            [alert release];
        }
        }else{
            [self progressEndImage];
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"复习提示" message:@"你已经复习完今天所有的错误单词了!" delegate:self cancelButtonTitle:nil  otherButtonTitles:@"确定",nil];
            alert.tag = 1;
            [alert show];
            [alert release];
        }
    }
    ClickToChooseAnsware = false;//mark for 'Click' to protect view error
}

// chick answare button
-(void)BChickAnsware:(id)sender{
    [sender setHidden:true];
    wrongWords *getWord = (wrongWords *)[sender tag];
    Word *word = getWord.myword;
    UIView *view = [viewArray objectAtIndex:0];
    NSArray *ui = [view subviews];
    
    // show the right answare as green
    int i = [getWord.randomTag intValue];//正确答案是第几个
    UIButton *la = [ui objectAtIndex:i+2];
    [la setBackgroundColor:[UIColor greenColor]];
    
    // if andware was checked, then user can not choise any more
    if ([ui count] == 8) {
        for (int h = 2 ; h < 6; h++) {
            UIButton *but = [ui objectAtIndex:h];
            [but setUserInteractionEnabled:NO];
        }
        UIButton *but = [ui objectAtIndex:9];
        [but setHidden:YES];
    }else if([ui count] == 9){
        for (int h = 2 ; h < 7; h++) {
            UIButton *but = [ui objectAtIndex:h];
            [but setUserInteractionEnabled:NO];
        }
        UIButton *but = [ui objectAtIndex:8];
        [but setHidden:YES];
    }
    
    // show answare analyse
    if (word.word_analyze.length != 0) {
        UILabel *analyze = [ui objectAtIndex:[ui count]-3];
        analyze.userInteractionEnabled = YES;
        [analyze setHidden:NO];
    }
    
    // update word array when answared right
    
    tools *tool = [[tools alloc]init];
    DayWordArray = [tool readArray:@"wrongWordArray"];
    
    count ++;
    if (count < [DayWordArray count]) {
    Word *word2 = [DayWordArray objectAtIndex:count];
    [self ViewType:word2];// update view array
    GlobalWord = word2;
    }else{
        [self progressEndImage];
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"复习提示" message:@"恭喜你，你已经复习玩今天的所有错误单词了!" delegate:self cancelButtonTitle:nil  otherButtonTitles:@"确定",nil];
        alert.tag = 1;
        [alert show];
        [alert release];
    }
}

-(UIImageView*)BRight{
    UIImage *image =[UIImage imageNamed:@"choice_right"];
    
    UIImageView *rightImageView = [[UIImageView alloc] initWithFrame:CGRectMake(800,5,50,50)];
    [rightImageView initWithImage:image];
    return rightImageView;
}

-(UIImageView*)BWrong{
    UIImage *image =[UIImage imageNamed:@"choice_wrong"];
    UIImageView *rightImageView = [[UIImageView alloc] initWithFrame:CGRectMake(800,5,50,50)];
    [rightImageView initWithImage:image];
    return rightImageView;
}

// response methods for Type C
// C judge the answare is right or not
-(void) CAnswerJudge:(UITextField *) getTextField2{
    NSTimer *timer;
    timer = [NSTimer scheduledTimerWithTimeInterval: 1.0
                                             target: self
                                           selector: @selector(CJudge:)
                                           userInfo: getTextField2
                                            repeats: NO];
}

-(void)CJudge:(NSTimer *)sender{
    UITextField *textfield = (UITextField*)[sender userInfo];
    
    ClickToChooseAnsware = true;//mark for 'Click' to protect view error
    
    Word *word = (Word *)[textfield tag];
    if(word==nil){
        return;
    }
    if ([word.word_c_answer compare:textfield.text]==NSOrderedSame) {
        textfield.text = word.word_c_answer;
        textfield.textColor = [UIColor greenColor];
        
        if ([DayWordArray count] > 1) {
            // update word array when answared right
            
            tools *tool = [[tools alloc]init];
            DayWordArray = [tool readArray:@"wrongWordArray"];
            count ++;
            
            if (count < [DayWordArray count]) {
            Word *word = [DayWordArray objectAtIndex:count];
            [self ViewType:word];// update view array
            GlobalWord = word;
            
            // change the view by animation
            [self.view addSubview:[viewArray objectAtIndex:1]];
            [Animations moveLeft:[viewArray objectAtIndex:0] andAnimationDuration:0.3 andWait:YES andLength:1000.0];
            [Animations moveLeft:[viewArray objectAtIndex:1] andAnimationDuration:0.3 andWait:YES andLength:1000.0];
            [[viewArray objectAtIndex:0] removeFromSuperview];
            [viewArray removeObjectAtIndex:0];
            [self Recite];
            }else{
                [self progressEndImage];
                UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"复习提示" message:@"恭喜你，你已经复习玩今天的所有错误单词了!" delegate:self cancelButtonTitle:nil  otherButtonTitles:@"确定",nil];
                alert.tag = 1;
                [alert show];
                [alert release];
            }
        }else{
            [self progressEndImage];
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"复习提示" message:@"恭喜你，你已经复习玩今天的所有错误单词了!" delegate:self cancelButtonTitle:nil  otherButtonTitles:@"确定",nil];
            alert.tag = 1;
            [alert show];
            [alert release];
        }
    }else{
        UIView *view = [viewArray objectAtIndex:0];
        NSArray *ui = [view subviews];
        UILabel *text = [ui objectAtIndex:2];
        text.text = word.word_c_answer;
        text.textColor = [UIColor redColor];
        
        if (word.word_analyze.length != 0) {
            // show answer analyse
            UILabel *analyze = [ui objectAtIndex:[ui count]-3];
            [analyze setHidden:NO];
        }
        
        // hidde the check answare button
        UIButton *showAnsware = [ui objectAtIndex:[ui count]-2];
        [showAnsware setHidden:YES];
        
        // update word array when answared right
        tools *tool = [[tools alloc]init];
        DayWordArray = [tool readArray:@"wrongWordArray"];
        count++;
        if (count < [DayWordArray count]) {
        Word *word = [DayWordArray objectAtIndex:count];
        [self ViewType:word];// update view array
        GlobalWord = word;
        }else{
            [self progressEndImage];
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"复习提示" message:@"恭喜你，你已经复习玩今天的所有错误单词了!" delegate:self cancelButtonTitle:nil  otherButtonTitles:@"确定",nil];
            alert.tag = 1;
            [alert show];
            [alert release];
        }
    }
    ClickToChooseAnsware = false;//mark for 'Click' to protect view error
}

// when choice 'I don't know'
-(void)CCheckAnswer:(id)sender{
    [sender setHidden:YES];
    QuestionC *qc = (QuestionC *)[sender tag];
    Word *word = qc.wordC;
    
    UIView *view = [viewArray objectAtIndex:0];
    NSArray *ui = [view subviews];
    UILabel *text = [ui objectAtIndex:2];
    text.text = word.word_c_answer;
    text.textColor = [UIColor redColor];
    
    // show answer analyse
    if (word.word_analyze.length != 0) {
        UILabel *analyze = [ui objectAtIndex:[ui count]-3];
        [analyze setHidden:NO];
    }
    
    // update word array when answared right
    tools *tool = [[tools alloc]init];
    DayWordArray = [tool readArray:@"wrongWordArray"];
    count++;
    if (count < [DayWordArray count]){
    Word *word2 = [DayWordArray objectAtIndex:count];
    [self ViewType:word2];// update view array
    GlobalWord = word2;
    }else{
        [self progressEndImage];
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"复习提示" message:@"恭喜你，你已经复习玩今天的所有错误单词了!" delegate:self cancelButtonTitle:nil  otherButtonTitles:@"确定",nil];
        alert.tag = 1;
        [alert show];
        [alert release];
    }
}

// 点击后图片放大
-(void)LargeImage:(id)sender{
    UIImage *image = [(UIGestureRecognizer *)sender view].tag;
    int layoutWidth = image.size.width;
    int layoutHeight = image.size.height;
    if (layoutHeight > layoutWidth) {
        layoutWidth = 748*layoutWidth/layoutHeight;
        layoutHeight = 748;
    }else{
        layoutHeight = 1024*layoutHeight/layoutWidth;
        layoutWidth = 1024;
    }
    int layoutX = (1024 - layoutWidth)/2;
    int layoutY = (748 - layoutHeight)/2;
    bigImage = [[UIImageView alloc] initWithFrame:CGRectMake(layoutX,layoutY,layoutWidth,layoutHeight)];
    [bigImage initWithImage:image];
    
    bigImage.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(UesrClicked:)];
    [bigImage addGestureRecognizer:singleTap];
    [singleTap release];
    NSArray *selfViewArray = [self.view subviews];
    for (int i =0; i <[selfViewArray count]; i++) {
        [[selfViewArray objectAtIndex:i] setHidden:YES];
    }
    [self.view addSubview:bigImage];
}

// 取消图片放大
-(void)UesrClicked:(id)sender{
    [bigImage removeFromSuperview];
    NSArray *selfViewArray = [self.view subviews];
    for (int i =0; i <[selfViewArray count]; i++) {
        [[selfViewArray objectAtIndex:i] setHidden:NO];
    }
}

//-----------初始化A型题view
-(void)QuestionA:(Word *)word{
    UIView *dwView=[[UIView alloc] initWithFrame:CGRectMake(1020, 50, 975.0, 620)];
    UIImage*img =[UIImage imageNamed:@"remember_words_background"];
    [dwView setBackgroundColor:[UIColor colorWithPatternImage:img]];
    dwView.tag = (long)word;
    
    UILabel *label1 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 130.0, 950.0, 300.0)];
    label1.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label1.textAlignment = UITextAlignmentCenter;
    label1.font = [UIFont fontWithName:@"Helvetica" size:60];
    label1.textColor = [UIColor blackColor];
    label1.adjustsFontSizeToFitWidth = YES;
    label1.lineBreakMode = UILineBreakModeWordWrap;
    label1.numberOfLines = 0;
    NSString *wordString = [word.word_word stringByReplacingOccurrencesOfString:@"@$" withString:@"\n"];
    if (wordString.length > 60) {
        if (wordString.length > 140) {
            label1.font = [UIFont fontWithName:@"Helvetica" size:30];
        }else{
            label1.font = [UIFont fontWithName:@"Helvetica" size:40];
        }
        
    }
    label1.text = wordString;
    UILabel *label2 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 430.0, 950.0, 30.0)];
    label2.adjustsFontSizeToFitWidth = YES;
    if (![word.word_word_class compare:@""]==NSOrderedSame) {
        NSString *text = [NSString stringWithFormat:@"%@.",word.word_word_class];
        label2.text = text;
    }
    label2.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label2.textAlignment = UITextAlignmentCenter;
    label2.font = [UIFont fontWithName:@"Helvetica" size:20];
    label2.textColor = [UIColor blackColor];
    
    UIImage *image2 = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@", word.word_d_picture1]];
    
    int width1 = image2.size.height;
    if (width1 > 300) {
        width1 = 300;
    }
    int height1 = width1*(image2.size.height/image2.size.width);
    UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(920-width1,570-height1,width1,height1)];
    [imageView2 initWithImage:image2];
    imageView2.tag = (long)image2;
    imageView2.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(LargeImage:)];
    [imageView2 addGestureRecognizer:singleTap];
    [singleTap release];
    if (image2) {
        [dwView bringSubviewToFront:imageView2];
        [dwView setUserInteractionEnabled:YES];
        [dwView addSubview:imageView2];
    }
    
    [dwView addSubview:[self AddDictionaryLable:word]];
    [dwView addSubview:label1];
    [dwView addSubview:label2];
    [viewArray addObject:dwView];
    
    //体型A的答案界面
    UIView *dwView2=[[UIView alloc] initWithFrame:CGRectMake(1020, 50, 975.0, 620)];
    UIImage*img2 =[UIImage imageNamed:@"remember_words_background"];
    [dwView2 setBackgroundColor:[UIColor colorWithPatternImage:img2]];
    dwView2.tag = (long)word;
    
    UILabel *label3 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 100, 950.0, 300)];
    label3.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label3.adjustsFontSizeToFitWidth = YES;
    label3.textAlignment = UITextAlignmentCenter;
    label3.font = [UIFont fontWithName:@"Helvetica" size:40];
    label3.textColor = [UIColor blackColor];
    label3.lineBreakMode = UILineBreakModeWordWrap;
    label3.numberOfLines = 0;
    NSString *word2String = [word.word_a_explan1 stringByReplacingOccurrencesOfString:@"@$" withString:@"\n"];
    if (word2String.length > 60) {
        if (word2String.length > 140) {
            label1.font = [UIFont fontWithName:@"Helvetica" size:30];
        }else{
            label1.font = [UIFont fontWithName:@"Helvetica" size:40];
        }
        
    }
    label3.text = word2String;
    
    UILabel *label4 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 400, 950.0, 30.0)];
    label4.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label4.textAlignment = UITextAlignmentCenter;
    label4.adjustsFontSizeToFitWidth = YES;
    label4.font = [UIFont fontWithName:@"Helvetica" size:20];
    label4.textColor = [UIColor blackColor];
    NSArray *array2 = [word.word_statement componentsSeparatedByString:@"@$"]; //从字符A中分隔成2个元素的数组
    NSString *newString2 = [[NSString alloc]init];
    for (int i = 0; i < [array2 count]; i++) {
        newString2 = [NSString stringWithFormat:@"%@%@",newString2,[array2 objectAtIndex:i]];
    }
    label4.text = newString2;
    
    UIImage *image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@", word.word_d_picture2]];
    int width2 = image.size.height;
    if (width2 > 300) {
        width2 = 300;
    }
    int height2 = width2*(image.size.height/image.size.width);
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(12,550-height2,width2,height2)];
    [imageView initWithImage:image];
    imageView.tag = (long)image;
    imageView.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(LargeImage:)];
    [imageView addGestureRecognizer:singleTap2];
    [singleTap2 release];
    [dwView2 bringSubviewToFront:imageView];
    [dwView2 setUserInteractionEnabled:YES];
    
    UIButton *Btn1=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    Btn1.frame=CGRectMake(10,565,200,43);
    UIImage*btnImage =[UIImage imageNamed:@"memory_easy"];
    [Btn1 setBackgroundImage:btnImage forState:UIControlStateNormal];
    Btn1.tag=(long)word;
    [Btn1 addTarget:self action:@selector(AorDRight:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *Btn2=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    Btn2.frame=CGRectMake(762,565,200,43);
    Btn2.tag=(long)word;
    UIImage*btnImage2 =[UIImage imageNamed:@"memory_i_know"];
    [Btn2 setBackgroundImage:btnImage2 forState:UIControlStateNormal];
    [Btn2 addTarget:self action:@selector(AorDWrong:) forControlEvents:UIControlEventTouchUpInside];
    
    [dwView2 addSubview:label3];
    [dwView2 addSubview:label4];
    if (image) {
        [dwView2 addSubview:imageView];
    }
    
    [dwView2 addSubview:Btn1];
    [dwView2 addSubview:Btn2];
    [dwView2 addSubview:[self AddDictionaryLable2:word]];
    
    [dwView2 addSubview:[self analyzeText:word]];
    
    [self.view bringSubviewToFront:dwView2];
    [self.view setUserInteractionEnabled:YES];
    [self.view addSubview:dwView2];
    
    [viewArray addObject:dwView2];
    
}

//-----------初始化B型题view
-(void)QuestionB:(Word *)word{
    UIView *dwView=[[UIView alloc] initWithFrame:CGRectMake(1020, 50, 975.0, 620)];
    UIImage*img =[UIImage imageNamed:@"remember_words_background"];
    [dwView setBackgroundColor:[UIColor colorWithPatternImage:img]];
    dwView.tag = (long)word;
    
    int question;//体型随机处理
    if (![word.word_b_explane compare:@""]==NSOrderedSame) {
        question = arc4random() % 5;
    }else {
        question = arc4random() % 4;
    }
    NSString *intString = [NSString stringWithFormat:@"%d",question];
    NSArray *list;
    
    switch (question) {
        case 0:
            list = [NSArray arrayWithObjects:word.word_b_explana,word.word_b_explanb,word.word_b_explanc,word.word_b_expland,word.word_b_explane, nil];
            break;
        case 1:
            list = [NSArray arrayWithObjects:word.word_b_explanb,word.word_b_explana,word.word_b_explanc,word.word_b_expland,word.word_b_explane, nil];
            break;
        case 2:
            list = [NSArray arrayWithObjects:word.word_b_explanc,word.word_b_explanb,word.word_b_explana,word.word_b_expland,word.word_b_explane, nil];
            break;
        case 3:
            list = [NSArray arrayWithObjects:word.word_b_expland,word.word_b_explanb,word.word_b_explanc,word.word_b_explana,word.word_b_explane, nil];
            break;
        case 4:
            list = [NSArray arrayWithObjects:word.word_b_explane,word.word_b_explanb,word.word_b_explanc, word.word_b_expland,word.word_b_explana,nil];
            break;
    }
    
    UILabel *label1 = [[UILabel alloc]initWithFrame:CGRectMake(60.0, 20.0, 860.0, 80.0)];
    label1.font = [UIFont fontWithName:@"Helvetica" size:40];
    label1.lineBreakMode = UILineBreakModeWordWrap;
    label1.numberOfLines = 0;
    NSString *text = word.word_word;
    if (text.length > 21) {
        if (text.length > 56) {
            label1.font = [UIFont fontWithName:@"Helvetica" size:20];
        }else{
            label1.font = [UIFont fontWithName:@"Helvetica" size:30];
        }
    }
    label1.text = text;
    label1.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label1.adjustsFontSizeToFitWidth = YES;
    label1.textAlignment = UITextAlignmentLeft;
    label1.textColor = [UIColor blackColor];
    
    UILabel *label2 = [[UILabel alloc]initWithFrame:CGRectMake(74.0, 100.0, 846.0, 30.0)];
    NSString *text_n = [NSString stringWithFormat:@"%@",word.word_word_class];
    label2.text = text_n;
    label2.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label2.textAlignment = UITextAlignmentLeft;
    label2.font = [UIFont fontWithName:@"Helvetica" size:20];
    label2.adjustsFontSizeToFitWidth = YES;
    label2.textColor = [UIColor blackColor];
    
    UIImage *image2 = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@", word.word_pircture1]];
    int width = 160*(image2.size.width/image2.size.height);
    UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(670,20,width,160)];
    [imageView2 initWithImage:image2];
    imageView2.tag = (long)image2;
    imageView2.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(LargeImage:)];
    [imageView2 addGestureRecognizer:singleTap];
    [singleTap release];
    [dwView bringSubviewToFront:imageView2];
    [dwView setUserInteractionEnabled:YES];
    
    UIButton *Btn1=[UIButton buttonWithType:UIButtonTypeCustom];
    Btn1.frame=CGRectMake(50.0, 150.0, 880.0, 60.0);
    UIImage*btnImage =[UIImage imageNamed:@"memory_d_background"];
    [Btn1 setBackgroundImage:btnImage forState:UIControlStateNormal];
    NSString *text1 = [NSString stringWithFormat:@"  A. %@",[list objectAtIndex:0]];
    wrongWords *wrong = [[wrongWords alloc]init];
    wrong.randomTag = intString;//标记正确答案是第几个
    wrong.answare = [list objectAtIndex:0];//保存正确答案
    wrong.myword = word;
    Btn1.tag = (long)wrong;
    Btn1.titleLabel.font = [UIFont systemFontOfSize: 40];
    Btn1.titleLabel.adjustsFontSizeToFitWidth = YES;
    Btn1.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft ;
    [Btn1 setTitle:text1 forState:UIControlStateNormal];
    [Btn1 addTarget:self action:@selector(BChoice:) forControlEvents:UIControlEventTouchUpInside];
    [Btn1 setTitleColor:[UIColor colorWithRed:37.99/255 green:37.99/255 blue:41.05/255 alpha:1]forState:UIControlStateNormal];
    
    UIButton *Btn2=[UIButton buttonWithType:UIButtonTypeCustom];
    Btn2.frame=CGRectMake(50.0, 225.0, 880.0, 60.0);
    UIImage*btnImage2 =[UIImage imageNamed:@"memory_d_background"];
    [Btn2 setBackgroundImage:btnImage2 forState:UIControlStateNormal];
    NSString *text2 = [NSString stringWithFormat:@"  B. %@",[list objectAtIndex:1]];
    wrongWords *wrong2 = [[wrongWords alloc]init];
    wrong2.randomTag = intString;
    wrong2.answare = [list objectAtIndex:1];
    wrong2.myword = word;
    Btn2.tag = (long)wrong2;
    Btn2.titleLabel.font = [UIFont systemFontOfSize: 40];
    Btn2.titleLabel.adjustsFontSizeToFitWidth = YES;
    Btn2.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft ;
    [Btn2 setTitle:text2 forState:UIControlStateNormal];
    [Btn2 addTarget:self action:@selector(BChoice:) forControlEvents:UIControlEventTouchUpInside];
    [Btn2 setTitleColor:[UIColor colorWithRed:37.99/255 green:37.99/255 blue:41.05/255 alpha:1]forState:UIControlStateNormal];
    
    UIButton *Btn3=[UIButton buttonWithType:UIButtonTypeCustom];
    Btn3.frame=CGRectMake(50.0, 300.0, 880.0, 60.0);
    UIImage*btnImage3 =[UIImage imageNamed:@"memory_d_background"];
    [Btn3 setBackgroundImage:btnImage3 forState:UIControlStateNormal];
    NSString *text3 = [NSString stringWithFormat:@"  C. %@",[list objectAtIndex:2]];
    Btn3.titleLabel.font = [UIFont systemFontOfSize: 40];
    Btn3.titleLabel.adjustsFontSizeToFitWidth = YES;
    wrongWords *wrong3 = [[wrongWords alloc]init];
    wrong3.randomTag = intString;
    wrong3.answare = [list objectAtIndex:2];
    wrong3.myword = word;
    Btn3.tag = (long)wrong3;
    Btn3.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft ;
    [Btn3 setTitle:text3 forState:UIControlStateNormal];
    [Btn3 addTarget:self action:@selector(BChoice:) forControlEvents:UIControlEventTouchUpInside];
    [Btn3 setTitleColor:[UIColor colorWithRed:37.99/255 green:37.99/255 blue:41.05/255 alpha:1]forState:UIControlStateNormal];
    
    UIButton *Btn4=[UIButton buttonWithType:UIButtonTypeCustom];
    Btn4.frame=CGRectMake(50.0, 375.0, 880.0, 60.0);
    UIImage*btnImage4 =[UIImage imageNamed:@"memory_d_background"];
    [Btn4 setBackgroundImage:btnImage4 forState:UIControlStateNormal];
    NSString *text4 = [NSString stringWithFormat:@"  D. %@",[list objectAtIndex:3]];
    Btn4.titleLabel.font = [UIFont systemFontOfSize: 40];
    Btn4.titleLabel.adjustsFontSizeToFitWidth = YES;
    wrongWords *wrong4 = [[wrongWords alloc]init];
    wrong4.randomTag = intString;
    wrong4.answare = [list objectAtIndex:3];
    wrong4.myword = word;
    Btn4.tag = (long)wrong4;
    Btn4.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft ;
    [Btn4 setTitle:text4 forState:UIControlStateNormal];
    [Btn4 addTarget:self action:@selector(BChoice:) forControlEvents:UIControlEventTouchUpInside];
    [Btn4 setTitleColor:[UIColor colorWithRed:37.99/255 green:37.99/255 blue:41.05/255 alpha:1]forState:UIControlStateNormal];
    
    
    UIButton *Btne=[UIButton buttonWithType:UIButtonTypeCustom];
    Btne.frame=CGRectMake(50.0, 450.0, 880.0, 60.0);
    UIImage*btnImagee =[UIImage imageNamed:@"memory_d_background"];
    [Btne setBackgroundImage:btnImagee forState:UIControlStateNormal];
    NSString *texte = [NSString stringWithFormat:@"  E. %@",[list objectAtIndex:4]];
    Btne.titleLabel.font = [UIFont systemFontOfSize: 40];
    Btne.titleLabel.adjustsFontSizeToFitWidth = YES;
    wrongWords *wronge = [[wrongWords alloc]init];
    wronge.randomTag = intString;
    wronge.answare = [list objectAtIndex:4];
    wronge.myword = word;
    Btne.tag = (long)wronge;
    Btne.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft ;
    [Btne setTitle:texte forState:UIControlStateNormal];
    [Btne addTarget:self action:@selector(BChoice:) forControlEvents:UIControlEventTouchUpInside];
    [Btne setTitleColor:[UIColor colorWithRed:37.99/255 green:37.99/255 blue:41.05/255 alpha:1]forState:UIControlStateNormal];
    
    UIButton *Btn5=[UIButton buttonWithType:UIButtonTypeCustom];
    Btn5.frame=CGRectMake(730,560,200,43);
    
    wrongWords *wrong5 = [[wrongWords alloc]init];
    wrong5.randomTag = intString;//标记正确答案是第几个
    wrong5.answare = [list objectAtIndex:question];//保存正确答案
    wrong5.myword = word;
    Btn5.tag = (long)wrong5;
    UIImage*btnImage5 =[UIImage imageNamed:@"memory_i_do_not_know"];
    [Btn5 setBackgroundImage:btnImage5 forState:UIControlStateNormal];
    [Btn5 addTarget:self action:@selector(BChickAnsware:) forControlEvents:UIControlEventTouchUpInside];
    
    [dwView addSubview:label1];
    [dwView addSubview:label2];
    [dwView addSubview:Btn1];
    [dwView addSubview:Btn2];
    [dwView addSubview:Btn3];
    [dwView addSubview:Btn4];
    if (![word.word_b_explane compare:@""]==NSOrderedSame) {
        [dwView addSubview:Btne];
    }
    if (image2) {
        [dwView addSubview:imageView2];
    }
    [dwView addSubview:[self analyzeTextBC:word]];
    [dwView addSubview:Btn5];
    [dwView addSubview:[self AddDictionaryLable2:word]];
    [viewArray addObject:dwView];
}

//-----------初始化C型题view
-(void)QuestionC:(Word *)word{
    UIView *dwView=[[UIView alloc] initWithFrame:CGRectMake(1020, 50, 975.0, 620)];
    UIImage*img =[UIImage imageNamed:@"remember_words_background"];
    [dwView setBackgroundColor:[UIColor colorWithPatternImage:img]];
    dwView.tag = (long)word;
    
    UILabel *label1 = [[UILabel alloc]initWithFrame:CGRectMake(52.0, 120.0, 900.0, 120.0)];
    label1.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label1.textAlignment = UITextAlignmentCenter;
    label1.lineBreakMode = UILineBreakModeWordWrap;
    label1.numberOfLines = 0;
    label1.font = [UIFont fontWithName:@"Helvetica" size:50];
    label1.textColor = [UIColor blackColor];
    NSString *text = [NSString stringWithFormat:@"%@",word.word_c_explan];
    if (text.length > 45) {
        if (text.length > 90) {
            label1.font = [UIFont fontWithName:@"Helvetica" size:20];
        }else{
            label1.font = [UIFont fontWithName:@"Helvetica" size:30];
        }
    }
    label1.text = text;
    
    UILabel *label2 = [[UILabel alloc]initWithFrame:CGRectMake(52.0, 240.0, 900.0, 30.0)];
    NSString *text2 = [NSString stringWithFormat:@"%@",word.word_word_class];
    label2.text = text2;
    label2.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label2.textAlignment = UITextAlignmentCenter;
    label2.font = [UIFont fontWithName:@"Helvetica" size:20];
    label2.textColor = [UIColor blackColor];
    
    UITextField *textFiled = [[UITextField alloc] initWithFrame:CGRectMake(350, 300, 280.0f, 40)];
    [textFiled setBorderStyle:UITextBorderStyleRoundedRect]; //外框类型
    textFiled.font = [UIFont fontWithName:@"helvetica" size:20];
    textFiled.placeholder = @"请输入答案";
    textFiled.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    textFiled.textColor = [UIColor blackColor];
    textFiled.tag = (long)word;
    textFiled.delegate=self;
    [textFiled addTarget:self action:@selector(CAnswerJudge:) forControlEvents:UIControlEventEditingDidEnd];
    textFiled.autocapitalizationType = UITextAutocapitalizationTypeNone;
    
    UILabel *label3 = [[UILabel alloc]initWithFrame:CGRectMake(640, 300, 280.0f, 34)];
    label3.text = @"";
    label3.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label3.font = [UIFont fontWithName:@"Helvetica" size:30];
    label3.textColor = [UIColor blackColor];
    
    UIImage *image2 = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@", word.word_d_picture1]];
    int width = 160*(image2.size.width/image2.size.height);
    UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(360,350,width,160)];
    [imageView2 initWithImage:image2];
    imageView2.tag = (long)image2;
    imageView2.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(LargeImage:)];
    [imageView2 addGestureRecognizer:singleTap];
    [singleTap release];
    [dwView bringSubviewToFront:imageView2];
    [dwView setUserInteractionEnabled:YES];
    
    UIButton *Btn1=[UIButton buttonWithType:UIButtonTypeCustom];
    Btn1.frame=CGRectMake(750,550,200,43);
    UIImage *btnImage5 =[UIImage imageNamed:@"memory_c_button"];
    [Btn1 setBackgroundImage:btnImage5 forState:UIControlStateNormal];
    QuestionC *qc = [[QuestionC alloc]init];
    qc.answareC = textFiled.text;
    qc.wordC = word;
    Btn1.tag=(long)qc;
    [Btn1 addTarget:self action:@selector(CCheckAnswer:) forControlEvents:UIControlEventTouchUpInside];
    
    [dwView addSubview:label1];
    [dwView addSubview:label2];
    [dwView addSubview:label3];
    [dwView addSubview:textFiled];
    if (image2) {
        [dwView addSubview:imageView2];
    }
    [dwView addSubview:[self analyzeTextBC:word]];
    [dwView addSubview:Btn1];
    [dwView addSubview:[self AddDictionaryLable2:word]];
    [viewArray addObject:dwView];
    
}

//-----------初始化D型题view
-(void)QuestionD:(Word *)word{
    UIView *dwView=[[UIView alloc] initWithFrame:CGRectMake(1020, 50, 975.0, 620)];
    UIImage*img =[UIImage imageNamed:@"remember_words_background"];
    [dwView setBackgroundColor:[UIColor colorWithPatternImage:img]];
    dwView.tag = (long)word;
    UILabel *label1 = [[UILabel alloc]initWithFrame:CGRectMake(40.0, 40.0, 900.0, 110.0)];
    label1.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label1.textAlignment = UITextAlignmentLeft;
    label1.font = [UIFont fontWithName:@"Helvetica" size:40];
    label1.textColor = [UIColor blackColor];
    label1.adjustsFontSizeToFitWidth = YES;
    label1.lineBreakMode = UILineBreakModeWordWrap;
    label1.numberOfLines = 0;
    NSString *wordString = [word.word_word stringByReplacingOccurrencesOfString:@"@$" withString:@"\n"];
    if (wordString.length > 44) {
        if (wordString.length > 90) {
            label1.font = [UIFont fontWithName:@"Helvetica" size:20];
        }else{
            label1.font = [UIFont fontWithName:@"Helvetica" size:30];
        }
    }
    label1.text = wordString;
    
    UILabel *label2 = [[UILabel alloc]initWithFrame:CGRectMake(40.0, 150.0, 900.0, 30.0)];
    label2.adjustsFontSizeToFitWidth = YES;
    if (![word.word_word_class compare:@""]==NSOrderedSame) {
        NSString *text = [NSString stringWithFormat:@"%@.",word.word_word_class];
        label2.text = text;
    }
    label2.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label2.textAlignment = UITextAlignmentLeft;
    label2.font = [UIFont fontWithName:@"Helvetica" size:20];
    label2.textColor = [UIColor blackColor];
    
    UIImage *image2 = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@", word.word_d_picture1]];
    
    int layoutWith = image2.size.width;
    int layoutHeight = image2.size.height;
    
    if (layoutWith > 700) {
        layoutWith = 700;
        layoutHeight = layoutHeight*700/layoutWith;
    }else if(layoutHeight > 450){
        layoutHeight = 450;
        layoutWith = layoutWith*450/layoutHeight;
    }
    int layoutX = (975.0 - layoutWith)/2;
    int layoutY = (620 - layoutHeight)/2;
    
    UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(layoutX,layoutY,layoutWith,layoutHeight)];
    [imageView2 initWithImage:image2];
    imageView2.tag = (long)image2;
    imageView2.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(LargeImage:)];
    [imageView2 addGestureRecognizer:singleTap];
    [singleTap release];
    [dwView bringSubviewToFront:imageView2];
    [dwView setUserInteractionEnabled:YES];
    if (image2) {
        [dwView addSubview:imageView2];
    }
    [dwView addSubview:label1];
    [dwView addSubview:label2];
    [dwView addSubview:[self AddDictionaryLable:word]];
    [viewArray addObject:dwView];
    
    //体型D的答案界面
    UIView *dwView2=[[UIView alloc] initWithFrame:CGRectMake(1020, 50, 975.0, 620)];
    UIImage*img2 =[UIImage imageNamed:@"remember_words_background"];
    [dwView2 setBackgroundColor:[UIColor colorWithPatternImage:img2]];
    dwView2.tag = (long)word;
    
    UILabel *label3 = [[UILabel alloc]initWithFrame:CGRectMake(20.0, 40.0, 940.0, 120.0)];
    label3.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label3.textAlignment = UITextAlignmentLeft;
    label3.adjustsFontSizeToFitWidth = YES;
    label3.font = [UIFont fontWithName:@"Helvetica" size:40];
    label3.textColor = [UIColor blackColor];
    label3.lineBreakMode = UILineBreakModeWordWrap;
    label3.numberOfLines = 0;
    NSString *newString = [word.word_a_explan1 stringByReplacingOccurrencesOfString:@"@$" withString:@"\n"];
    if (newString.length > 46) {
        if (newString.length > 93) {
            label3.font = [UIFont fontWithName:@"Helvetica" size:20];
        }else{
            label3.font = [UIFont fontWithName:@"Helvetica" size:30];
        }
    }
    label3.text = newString;
    
    UILabel *label4 = [[UILabel alloc]initWithFrame:CGRectMake(20.0, 160.0, 940.0, 30.0)];
    label4.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label4.textAlignment = UITextAlignmentLeft;
    label4.adjustsFontSizeToFitWidth = YES;
    label4.font = [UIFont fontWithName:@"Helvetica" size:20];
    label4.textColor = [UIColor blackColor];
    NSArray *array2 = [word.word_a_explan2 componentsSeparatedByString:@"@$"]; //从字符A中分隔成2个元素的数组
    NSString *newString2 = [[NSString alloc]init];
    for (int i = 0; i < [array2 count]; i++) {
        newString2 = [NSString stringWithFormat:@"%@%@",newString2,[array2 objectAtIndex:i]];
    }
    label4.text = newString2;
    
    UIImage *image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@", word.word_d_picture2]];
    
    int layoutWith2 = image.size.width;
    int layoutHeight2 = image.size.height;
    
    if (layoutWith2 > 700) {
        layoutWith2 = 700;
        layoutHeight2 = layoutHeight2*700/layoutWith2;
    }else if(layoutHeight2 > 450){
        layoutHeight2 = 450;
        layoutWith2 = layoutWith2*450/layoutHeight2;
    }
    int layoutX2 = (975.0 - layoutWith2)/2;
    int layoutY2 = (620 - layoutHeight2)/2;
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(layoutX2,layoutY2,layoutWith2,layoutHeight2)];
    [imageView initWithImage:image];
    imageView.tag = (long)image;
    imageView.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(LargeImage:)];
    [imageView addGestureRecognizer:singleTap2];
    [singleTap2 release];
    [dwView2 bringSubviewToFront:imageView];
    [dwView2 setUserInteractionEnabled:YES];
    
    UIButton *Btn1=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    Btn1.frame=CGRectMake(10,565,200,43);
    UIImage*btnImage =[UIImage imageNamed:@"memory_easy"];
    [Btn1 setBackgroundImage:btnImage forState:UIControlStateNormal];
    Btn1.tag=(long)word;
    [Btn1 addTarget:self action:@selector(AorDRight:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *Btn2=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    Btn2.frame=CGRectMake(762,565,200,43);
    Btn2.tag=(long)word;
    UIImage*btnImage2 =[UIImage imageNamed:@"memory_i_know"];
    [Btn2 setBackgroundImage:btnImage2 forState:UIControlStateNormal];
    [Btn2 addTarget:self action:@selector(AorDWrong:) forControlEvents:UIControlEventTouchUpInside];
    
    [dwView2 addSubview:label3];
    [dwView2 addSubview:label4];
    if (image) {
        [dwView2 addSubview:imageView];
    }
    [dwView2 addSubview:[self analyzeText:word]];
    [dwView2 addSubview:Btn1];
    [dwView2 addSubview:Btn2];
    [dwView2 addSubview:[self AddDictionaryLable2:word]];
    [self.view bringSubviewToFront:dwView2];
    [self.view setUserInteractionEnabled:YES];
    [viewArray addObject:dwView2];
}

// The following three method will automatic adjust the 'UIEditView' location
-(void)moveView:(UITextField *)textField leaveView:(BOOL)leave{
    float screenHeight = 1496; //屏幕尺寸，如果屏幕允许旋转，可根据旋转动态调整
    float keyboardHeight = 748; //键盘尺寸，如果屏幕允许旋转，可根据旋转动态调整
    float statusBarHeight,NavBarHeight,tableCellHeight,textFieldOriginY,textFieldFromButtomHeigth;
    int margin;
    statusBarHeight = [[UIApplication sharedApplication] statusBarFrame].size.height; //屏幕状态栏高度
    NavBarHeight = self.navigationController.navigationBar.frame.size.height; //获取导航栏高度
    
    UITableViewCell *tableViewCell=(UITableViewCell *)textField.superview;
    tableCellHeight = tableViewCell.frame.size.height; //获取单元格高度
    
    CGRect fieldFrame=[self.view convertRect:textField.frame fromView:tableViewCell];
    textFieldOriginY = fieldFrame.origin.y; //获取文本框相对本视图的y轴位置。
    
    //计算文本框到屏幕底部的高度（屏幕高度-顶部状态栏高度-导航栏高度-文本框的的相对y轴位置-单元格高度）
    textFieldFromButtomHeigth = screenHeight - statusBarHeight - NavBarHeight - textFieldOriginY - tableCellHeight;
    
    if(!leave) {
        if(textFieldFromButtomHeigth < keyboardHeight) { //如果文本框到屏幕底部的高度 < 键盘高度
            margin = keyboardHeight - textFieldFromButtomHeigth; // 则计算差距
            keyBoardMargin_ = margin; //keyBoardMargin_ 为成员变量，记录上一次移动的间距,用户离开文本时恢复视图高度
        } else {
            margin= 0;
            keyBoardMargin_ = 0;
        }
    }
    
    float movementDuration = 0.3f; // 动画时间
    
    int movement; //进入时根据差距移动视图，离开时恢复之前的高度
    UIInterfaceOrientation deviceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (deviceOrientation == 3) {
        movement = (leave ? keyBoardMargin_ : -margin); //进入时根据差距移动视图，离开时恢复之前的高度
    }else{
        movement = (leave ? -keyBoardMargin_ : margin); //进入时根据差距移动视图，离开时恢复之前的高度
    }
    
    [UIView beginAnimations: @"textFieldAnim" context: nil]; //添加动画
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, -movement/8, 0);
    [UIView commitAnimations];
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    [self moveView:textField leaveView:NO];
}
-(void)textFieldDidEndEditing:(UITextField *)textField;{
    [self moveView:textField leaveView:YES];
}

@end
