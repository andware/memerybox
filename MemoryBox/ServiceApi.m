
//
//  ServiceApi.m
//  MemoryBox
//
//  Created by firefly on 13-6-12.
//
//

#import "ServiceApi.h"
#import "GDataXMLNode.h"
#import "UserDictionary.h"
#import "Constants.h"
#import "DataBaseHelper.h"
#import "HomePage.h"

@implementation ServiceApi
-(void)removeDictionary:(NSString *)dictionaryId{
    DataBaseHelper *dbHelper = [[DataBaseHelper alloc]init];
    [dbHelper deleteDictionaryInDB:dictionaryId];
    NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches"];

    
    NSString* file = [path stringByAppendingString:[NSString stringWithFormat:@"/%@",dictionaryId]] ;
    NSString* zip_file = [path stringByAppendingString:[NSString stringWithFormat:@"/%@.zip",dictionaryId]] ;

    NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager removeItemAtPath:file error:nil];
    [fileManager removeItemAtPath:zip_file error:nil];
    [dbHelper release];
}
+ (NSDictionary *)getJSON:(NSString *)url{
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    NSDictionary *weatherDic = [NSJSONSerialization JSONObjectWithData:response  options:NSJSONReadingMutableLeaves error:nil];//Json字典
    return weatherDic;
}
+ (NSDictionary *)getJSONFromData:(NSData *)json{
    NSDictionary *weatherDic = [NSJSONSerialization JSONObjectWithData:json  options:NSJSONReadingMutableLeaves error:nil];//Json字典
    return weatherDic;

}//从 String 获取

//读取XML，返回包含多个NSMutableDictionary的NSMutableArray
- (NSMutableArray *)readXML:(NSData *)data ElementName:(NSString *)EleName{
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:data  options:0 error:nil];
   	 GDataXMLElement *rootElement = [doc rootElement];
    NSArray *element = [rootElement elementsForName:EleName];
    
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:100];

    
    for (GDataXMLElement *ele in element) {
        NSMutableDictionary *KeyAndValue = [NSMutableDictionary dictionaryWithCapacity:1];

        for (int i=0; i<[ele childCount]; i++) {
            GDataXMLNode *subElement = [ele childAtIndex:i];
            NSString *key = [subElement name];
            NSString *value = [subElement stringValue];

            [KeyAndValue setObject:value forKey:key];
        }
        [array addObject:KeyAndValue];
    }

    return array;
    
}

- (NSMutableArray *) testgetUserHadBuyDictionary:(NSString *)userName{
    NSMutableArray *dics = [NSMutableArray arrayWithCapacity:1];
    NSString *url = [NSString stringWithFormat:@"%@%@%@",ApiUrl,@"getUserDictionary/user_name/",userName];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    NSMutableArray *data = [self readXML:response ElementName:@"dictionary"];
    
    for (NSMutableDictionary *dic in data) {
        NSArray *allKeys = [dic allKeys];
        for (NSString *key in allKeys) {
            NSString *TheKey = key;
            NSString *TheValue = [dic objectForKey:key];
            NSLog(@"%@:%@",TheKey,TheValue);
        }

    }
    return nil;
}

- (NSMutableArray *) getUserHadBuyDictionary:(NSString *)userName userId:(NSString *)uid{
    NSMutableArray *dics = [NSMutableArray arrayWithCapacity:1];
    
    
    NSString *url = [NSString stringWithFormat:@"%@%@%@",ApiUrl,@"getUserDictionary/user_name/",userName];
    
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:response  options:0 error:nil];
    GDataXMLElement *rootElement = [doc rootElement];
    
    NSArray *dictionarys = [rootElement elementsForName:@"dictionary"];
    
    for (GDataXMLElement *dic in dictionarys) {
        UserDictionary *tempDic = [[UserDictionary alloc] init];
        NSString *buy = [[[dic elementsForName:@"buy"] objectAtIndex:0] stringValue];
//#warning TODO:以下注释使所有词库都可以下载
        if (buy==nil) {
            buy=@"0";
        }
        NSString *dic_id = [[[dic elementsForName:@"dictionary_id"] objectAtIndex:0] stringValue];
        NSString *dictionary_proid = [[[dic elementsForName:@"dictionary_proid"] objectAtIndex:0] stringValue];

        NSString *dic_type_id = [[[dic elementsForName:@"dictionary_type_id"] objectAtIndex:0] stringValue];
        NSString *dic_name = [[[dic elementsForName:@"dictionary_name"] objectAtIndex:0]stringValue];
        NSString *dic_abc = [[[dic elementsForName:@"dictionary_abc"] objectAtIndex:0] stringValue];
        NSString *dic_hasVoice = [[[dic elementsForName:@"dictionary_has_voice"] objectAtIndex:0]stringValue];
        NSString *dic_time = [[[dic elementsForName:@"dictionary_time"] objectAtIndex:0] stringValue];
        NSString *dic_hasStick = [[[dic elementsForName:@"dictionary_has_stick"] objectAtIndex:0]stringValue];
        NSString *dic_state = [[[dic elementsForName:@"dictionary_state"] objectAtIndex:0]stringValue];
        NSString *dic_type_name = [[[dic elementsForName:@"dictionary_type_name"] objectAtIndex:0]stringValue];
        NSString *dictionary_price = [[[dic elementsForName:@"dictionary_price"] objectAtIndex:0]stringValue];
        NSString *dictionary_pronounce = [[[dic elementsForName:@"dictionary_pronounce"] objectAtIndex:0]stringValue];
        NSString *dictionary_complete_time = [[[dic elementsForName:@"dictionary_complete_time"] objectAtIndex:0]stringValue];
         NSString *count = [[[dic elementsForName:@"count"] objectAtIndex:0]stringValue];
        NSString *dictionary_detail = [[[dic elementsForName:@"dictionary_detail"] objectAtIndex:0]stringValue];

        tempDic.buy = buy;
        tempDic.dictionary_proid = dictionary_proid;
        tempDic.dictionary_id = dic_id;
        tempDic.dictionary_type_id = dic_type_id;
        tempDic.dictionary_name = dic_name;
        tempDic.dictionary_abc = dic_abc;
        tempDic.dictionary_has_voice = dic_hasVoice;
        tempDic.dictionary_time = dic_time;
        tempDic.dictionary_has_stick = dic_hasStick;
        tempDic.dictionary_state = dic_state;
        tempDic.dictionary_type_name = dic_type_name;
        tempDic.dictionary_price = dictionary_price;
        tempDic.dictionary_pronounce = dictionary_pronounce;
        tempDic.dictionary_complete_time = dictionary_complete_time;
        tempDic.count = count;
        tempDic.dictionary_detail = dictionary_detail;
        if ([dic_id compare:@"12"] == NSOrderedSame) {
            NSLog(@"%@",tempDic.dictionary_abc );
        }
        [dics addObject:tempDic];
        if([buy compare:@"1"]==NSOrderedSame){
            [self saveUserDictionary:uid dictionaryId:dic_id];

        }
    }
    NSLog(@"%@",dics);
    return dics;

}
- (NSMutableArray *) getUserHadBuyDictionary2SencondList:(NSString *)userName userId:(NSString *)uid{
    NSMutableArray *dics = [NSMutableArray arrayWithCapacity:1];
    
    
    NSString *url = [NSString stringWithFormat:@"%@%@%@",ApiUrl,@"getUserDictionary2/user_name/",userName];
    
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:response  options:0 error:nil];
    GDataXMLElement *rootElement = [doc rootElement];
    
    NSArray *dictionarys = [rootElement elementsForName:@"dictionary"];
    
    NSMutableDictionary *tempDictionary = [NSMutableDictionary dictionaryWithCapacity:10];
    
    for (GDataXMLElement *dic in dictionarys) {
        UserDictionary *tempDic = [[UserDictionary alloc] init];
        NSString *buy = [[[dic elementsForName:@"buy"] objectAtIndex:0] stringValue];
        //#warning TODO:以下注释使所有词库都可以下载
        if (buy==nil) {
            buy=@"0";
        }
        NSString *dic_id = [[[dic elementsForName:@"dictionary_id"] objectAtIndex:0] stringValue];
        NSString *dictionary_proid = [[[dic elementsForName:@"dictionary_proid"] objectAtIndex:0] stringValue];
        
        NSString *dic_type_id = [[[dic elementsForName:@"dictionary_type_id"] objectAtIndex:0] stringValue];
        NSString *dic_name = [[[dic elementsForName:@"dictionary_name"] objectAtIndex:0]stringValue];
        NSString *dic_abc = [[[dic elementsForName:@"dictionary_abc"] objectAtIndex:0] stringValue];
        NSString *dic_hasVoice = [[[dic elementsForName:@"dictionary_has_voice"] objectAtIndex:0]stringValue];
        NSString *dic_time = [[[dic elementsForName:@"dictionary_time"] objectAtIndex:0] stringValue];
        NSString *dic_hasStick = [[[dic elementsForName:@"dictionary_has_stick"] objectAtIndex:0]stringValue];
        NSString *dic_state = [[[dic elementsForName:@"dictionary_state"] objectAtIndex:0]stringValue];
        NSString *dic_type_name = [[[dic elementsForName:@"dictionary_type_name"] objectAtIndex:0]stringValue];
        NSString *dictionary_price = [[[dic elementsForName:@"dictionary_price"] objectAtIndex:0]stringValue];
        NSString *dictionary_pronounce = [[[dic elementsForName:@"dictionary_pronounce"] objectAtIndex:0]stringValue];
        NSString *dictionary_complete_time = [[[dic elementsForName:@"dictionary_complete_time"] objectAtIndex:0]stringValue];
        NSString *count = [[[dic elementsForName:@"count"] objectAtIndex:0]stringValue];
        NSString *dictionary_detail = [[[dic elementsForName:@"dictionary_detail"] objectAtIndex:0]stringValue];
        
        tempDic.buy = buy;
        tempDic.dictionary_proid = dictionary_proid;
        tempDic.dictionary_id = dic_id;
        tempDic.dictionary_type_id = dic_type_id;
        tempDic.dictionary_name = dic_name;
        tempDic.dictionary_abc = dic_abc;
        tempDic.dictionary_has_voice = dic_hasVoice;
        tempDic.dictionary_time = dic_time;
        tempDic.dictionary_has_stick = dic_hasStick;
        tempDic.dictionary_state = dic_state;
        tempDic.dictionary_type_name = dic_type_name;
        tempDic.dictionary_price = dictionary_price;
        tempDic.dictionary_pronounce = dictionary_pronounce;
        tempDic.dictionary_complete_time = dictionary_complete_time;
        tempDic.count = count;
        tempDic.dictionary_detail = dictionary_detail;
        if ([dic_id compare:@"12"] == NSOrderedSame) {
            NSLog(@"%@",tempDic.dictionary_abc );
        }
        [dics addObject:tempDic];
        if([buy compare:@"1"]==NSOrderedSame){
            [self saveUserDictionary:uid dictionaryId:dic_id];
        }
        
        if ([tempDictionary objectForKey:dic_type_name]!=nil) {
            NSMutableArray *tempArray = [NSMutableArray arrayWithArray:[tempDictionary objectForKey:dic_type_name]];
            [tempArray addObject:tempDic];
            [tempDictionary setObject:tempArray forKey:dic_type_name];
        }else{
            NSMutableArray *tempArray = [NSMutableArray arrayWithCapacity:1];
            [tempArray addObject:tempDic];
            [tempDictionary setObject:tempArray forKey:dic_type_name];
        }

    }
    NSMutableArray *MainArray = [NSMutableArray arrayWithCapacity:1];
    for (NSString* key in tempDictionary) {
        secondList *sl = [[secondList alloc]init];
        sl.firstLevel =key;
        NSMutableArray *arrays = [NSMutableArray arrayWithCapacity:1];
        for (id object in [tempDictionary objectForKey:key]) {
            [arrays addObject:object];
        }
        sl.secondLevel = arrays;
        [MainArray addObject:sl];
    }
    NSArray *sortedArray;
    sortedArray = [MainArray sortedArrayUsingSelector:@selector(compare:)];
    return sortedArray;
}//获取用户购买的词库，返回NSMutableArray数组，数组包含UserDictionary对象


- (BOOL)downloadDictionary:(NSString *)dictionaryUrl{
    ASINetworkQueue   *que = [[ASINetworkQueue alloc] init];
    NSString *name = [NSString stringWithFormat:@"%@.zip",dictionaryUrl];
    dictionaryUrl = [NSString stringWithFormat:@"%@%@.zip",DownloadUrl,dictionaryUrl];
    
    self.netWorkQueue = que;
    
    [que release];
    
    
    
    [self.netWorkQueue reset];
    
    [self.netWorkQueue setShowAccurateProgress:YES];
    
    [self.netWorkQueue go];
    
    //   创建存放路径
    
    //初始化Documents路径
    
    NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    
    //初始化临时文件路径
    
    NSString *folderPath = [path stringByAppendingPathComponent:@"temp"];
    NSLog(@"文件存放：%@",folderPath);
    //创建文件管理器
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    //判断temp文件夹是否存在
    
    BOOL fileExists = [fileManager fileExistsAtPath:folderPath];
    
    if (!fileExists) {//如果不存在说创建,因为下载时,不会自动创建文件夹
        
        [fileManager createDirectoryAtPath:folderPath
         
               withIntermediateDirectories:YES
         
                                attributes:nil
         
                                     error:nil];
        
    }
   
    
    NSLog(@"filePath=%@",dictionaryUrl);
    
    //初始下载路径
    
    NSURL *url = [NSURL URLWithString:dictionaryUrl];
    
    //设置下载路径
    
    ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:url];
    
    //设置ASIHTTPRequest代理
    
    request.delegate = self;
    
    //初始化保存ZIP文件路径
    
    NSString *savePath = [path stringByAppendingPathComponent:name];
    
    //初始化临时文件路径
    
    NSString *tempPath = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"temp/dictionary.zip.temp"]];
    
    //设置文件保存路径
    
    [request setDownloadDestinationPath:savePath];
    
    //设置临时文件路径
    
    [request setTemporaryFileDownloadPath:tempPath];
    
    
    
    //设置进度条的代理,
    
  
    
    //设置是是否支持断点下载
    
    [request setAllowResumeForFileDownloads:NO];
    [request setDelegate:self]; 
    [request setDidFinishSelector:@selector(requestDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    
    
    NSLog(@"UserInfo=%@",request.userInfo);
    
    //添加到ASINetworkQueue队列去下载
    
    [self.netWorkQueue addOperation:request];
    
    //收回request
    
    [request release];
    return YES;

}
- (BOOL)downloadDictionary:(NSString *)dictionaryUrl processView:(UIProgressView *)process Label:(UILabel *)label myArray:(NSArray *)ViewArray{
    ui = [[NSArray alloc]init];
    ui = ViewArray;
    ASINetworkQueue   *que = [[ASINetworkQueue alloc] init];
    NSString *name = [NSString stringWithFormat:@"%@.zip",dictionaryUrl];
    dictionaryUrl = [NSString stringWithFormat:@"%@%@.zip",DownloadUrl,dictionaryUrl];
    NSFileHandle *fileHandle;
    self.netWorkQueue = que;
    self.label = label;
    [que release];
    
    [self.netWorkQueue reset];
    
    [self.netWorkQueue setShowAccurateProgress:YES];
    
    [self.netWorkQueue go];
    
    //   创建存放路径
    
    //初始化Documents路径
    
    NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches"];
    
    //初始化临时文件路径
    
    NSString *folderPath = [path stringByAppendingPathComponent:@"tmp"];
    NSLog(@"文件存放：%@",folderPath);
    //创建文件管理器
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    //判断temp文件夹是否存在
    
    BOOL fileExists = [fileManager fileExistsAtPath:folderPath];
    
    if (!fileExists) {//如果不存在说创建,因为下载时,不会自动创建文件夹
        
        [fileManager createDirectoryAtPath:folderPath
         
               withIntermediateDirectories:YES
         
                                attributes:nil
         
                                     error:nil];
        
    }    
    
    
    NSLog(@"filePath=%@",dictionaryUrl);
    
    //init download url
    
    NSURL *url = [NSURL URLWithString:dictionaryUrl];
    
    
    ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:url];
    
    //set ASIHTTPRequest delegate
    
    request.delegate = self;
    
    //init file save path
    
    NSString *savePath = [path stringByAppendingPathComponent:name];
    
    bool b=[ fileManager createFileAtPath :savePath contents : nil attributes : nil ];
    if (b){
        fileHandle=[ NSFileHandle fileHandleForWritingAtPath :savePath];
    }
    
    [request setAllowResumeForFileDownloads:NO];
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setShouldContinueWhenAppEntersBackground:YES];
    [request setDataReceivedBlock :^( NSData * data){
        [fileHandle seekToEndOfFile ];
        [fileHandle writeData :data];
        [ label setText :[ NSString stringWithFormat : @"下载中...%.1f %%" , process . progress * 100 ]];

    }];

    [request setDownloadProgressDelegate:process];
    
    //add to ASINetworkQueue
    
    [self.netWorkQueue addOperation:request];
    
    //relase request
    
    [request release];
    return YES;

}//下载,第一个参数是下载的URL，第二个参数是保存zip的名字,下载保存在Document下，以词典ID命名

- (BOOL)upZip:(NSString *)fileName{
    ZipArchive *zip = [[ZipArchive alloc] init];
    NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches"];

    
    NSString* l_zipfile = [path stringByAppendingString:[NSString stringWithFormat:@"/%@.zip",fileName]] ;
    NSString* unzipto = [path stringByAppendingString:[NSString stringWithFormat:@"/%@",fileName]] ;
    
    NSLog(@"%@",unzipto);
    
    if( [zip UnzipOpenFile:l_zipfile] )
    {
        BOOL ret = [zip UnzipFileTo:unzipto overWrite:YES];
        
        if( NO==ret )
        {
            return NO;
        }
        [zip UnzipCloseFile];
    }
    NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager removeItemAtPath:l_zipfile error:nil];
    [zip release];
    return YES;
}

- (NSMutableArray *)readLocalWordXml:(NSString *)id{
    NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches"];
    NSString* xmlfile = [path stringByAppendingString:[NSString stringWithFormat:@"/%@/word.XML",id]] ;
    
    NSData *data = [NSData dataWithContentsOfFile:xmlfile];
    
    NSMutableArray *words = [self readXML:data ElementName:@"word"];
    return words;
    
}
+ (NSDictionary *)readLocalWordJSON:(NSString *)id{
    NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches"];
    NSString* xmlfile = [path stringByAppendingString:[NSString stringWithFormat:@"/%@/word.XML",id]] ;
    
    NSData *data = [NSData dataWithContentsOfFile:xmlfile];
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    return json;
}
- (NSString*) allFilesAtPath:(NSString*) dirString {
    NSMutableArray* array = [NSMutableArray arrayWithCapacity:10];
    NSFileManager* fileMgr = [NSFileManager defaultManager];
    NSArray* tempArray = [fileMgr contentsOfDirectoryAtPath:dirString error:nil];
    for (NSString* fileName in tempArray) {
        BOOL flag = YES;
        NSString* fullPath = [dirString stringByAppendingPathComponent:fileName];
        if ([fileMgr fileExistsAtPath:fullPath isDirectory:&flag]) {
            if (!flag) {
                if([fullPath rangeOfString:@"word.XML"].length>0){
                    NSLog(@"xmlfile:%@",fullPath);
                    return fullPath;
                }
            }
        }
    }
 
    
}

//获取Document目录
- (NSString *)getDocumentsPath{
    NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [path objectAtIndex:0];
    return documentsDir;
    
    //获取临时文件tmp路径的代码：return NSTemporaryDirectory（）；
}

//下载成功回调函数
- (void)requestDone:(ASIHTTPRequest *)request
{
    NSLog(@"回调函数size -- %d",[ui count]);
    for (int i =0; i <[ui count]; i++) {
        NSLog(@"%d--- %@",i,[ui objectAtIndex:i]);
    }
    
    self.label.text = @"下载成功: 100%";
    NSLog(@"下载成功！");
    HomePage *home =[[HomePage alloc]init];
    [home downLoadSuccess:ui];

}

//下载失败回调函数
- (void)requestWentWrong:(ASIHTTPRequest *)request
{
    HomePage *home =[[HomePage alloc]init];
    [home downLoadFailed:ui];
    NSLog(@"下载失败！");

}
- (void)saveUserDictionary:(NSString *)userId dictionaryId:(NSString *)dicId{
    DataBaseHelper *db = [[DataBaseHelper alloc]init];
    [db openDB];
   
    NSString *sql = [NSString stringWithFormat:@"INSERT INTO 'have_buy_dictionary' ('have_buy_dictionary_user_id','have_buy_dictionary_dictionary') VALUES (%@,%@)",userId,dicId];
    [db execSql:sql];
    
    [db close];
    [db release];
}
- (BOOL)buyDictionary:(UserDictionary *)dictionary UserId:(NSString *)user_id{
    NSString *url = [[NSString alloc]initWithFormat:@"%@user_id/%@/dictionary_id/%@",BuyUrl,user_id,dictionary.dictionary_id];
    NSLog(@"%@",url);
    
    NSDictionary *json = [ServiceApi getJSON:url];
    NSString *state = [NSString stringWithFormat:@"%@",[json objectForKey:@"state"]];
    NSLog(@"%@",state);
    if ([state compare:@"0"]==NSOrderedSame) {
        return YES;
    }else{
        return NO;
    }
}
@end
