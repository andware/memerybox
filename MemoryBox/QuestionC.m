//
//  QuestionC.m
//  MemoryBox
//
//  Created by appstone on 13-7-2.
//
//

#import "QuestionC.h"

@implementation QuestionC

- (id) initWithCoder: (NSCoder *)coder {
    if (self = [super init]){
        self.answareC = [coder decodeObjectForKey:@"answareC"];
        self.wordC = [coder decodeObjectForKey:@"wordC"];
    }
    return self;
}

- (void) encodeWithCoder: (NSCoder *)coder  {
    [coder encodeObject:self.answareC forKey:@"answareC"];
    [coder encodeObject:self.wordC forKey:@"wordC"];
}
@end
