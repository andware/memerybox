//
//  DataBase.m
//  MemoryBox
//
//  Created by firefly on 13-6-24.
//
//

#import "DataBase.h"
#import "Word.h"
#import "NSMutableArray+Helpers.h"
#import "DB.h"
@implementation DataBase
- (id)init{
    DB *databases = [DB getDB];
    database = databases.db;
    return self;
}

- (void)setWordNotEqualZeroPlusNumber:(NSString *)userId PlusNumber:(NSString *)plus{
    NSString * sql = [NSString stringWithFormat:@"update word_day set word_day=word_day+%@ where word_day!=0 and user_id =%@;",plus,userId];
    [self openDB];
    [self execSql:sql];
    [self closeDB];
}

- (void)wordDelayPlus:(NSString *)wordId howManyPlus:(NSString *)plus userId:(NSString *)uid{
    NSString *sql = [NSString stringWithFormat:@"update word_day set word_delay=word_delay+%@ where word_id=%@ and user_id =%@;",plus,wordId,uid];
    [self openDB];
    [self execSql:sql];
    [self closeDB];
}//将单词的delay在原本的数值上加

- (NSString *)getWordDelay:(NSString *)wordId userId:(NSString *)uid{
    NSString *sql =[NSString stringWithFormat:@"select word_delay from word_day where word_id=%@ and user_id=%@",wordId,uid];
    [self openDB];
    sqlite3_stmt * statement;
    
    
    if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &statement, nil) == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            char *word_day = (char*)sqlite3_column_text(statement, 0);
            NSString *_word_day = [[NSString alloc]initWithUTF8String:word_day];
            return _word_day;
        }
    }else{
        return @"0";
    }
    sqlite3_finalize(statement);
    [self closeDB];
}//获取某个单词的DELAY

- (NSString *)getWordDay:(NSString *)wordId userId:(NSString *)uid{
    NSString *sql =[NSString stringWithFormat:@"select word_day from word_day where word_id=%@ and user_id=%@",wordId,uid];
    [self openDB];
    sqlite3_stmt * statement;
    
    if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &statement, nil) == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            char *word_day = (char*)sqlite3_column_text(statement, 0);
            NSString *_word_day = [[NSString alloc]initWithUTF8String:word_day];
            sqlite3_finalize(statement);
            [self closeDB];
            return _word_day;
        }
    }else{
        return @"0";
    }
    while (YES) {
        if(sqlite3_finalize(statement)){
            continue;
        }else{
            break;
        }
    }
    return @"0";
    [self closeDB];
}

- (void)wordDayPlus:(NSString *)wordId howManyPlus:(NSString *)manyPlus userId:(NSString *)uid{
    NSString *sql = [NSString stringWithFormat:@"update word_day set word_day=word_day+%@ where word_id=%@ and user_id =%@;",manyPlus,wordId,uid];
    [self openDB];
    [self execSql:sql];
    [self closeDB];
}
- (NSMutableArray *)getNewWord:(NSString *)userId wordCount:(NSString *)count{
    [self openDB];
    NSString *sql = [NSString stringWithFormat:@"SELECT DISTINCT  w.word_id,w.word_word,w.word_symbol,w.word_word_class,w.word_statement,w.word_cn_statement,w.word_a_explan1,w.word_a_explan2,w.word_a_explan3,w.word_b_explana,w.word_b_explanb,w.word_b_explanc,w.word_b_expland,w.word_b_explane,w.word_b_right,w.word_c_explan,w.word_c_answer,d.dictionary_id,d.dictionary_abc,word_analyze FROM dictionary d join dictionary_with_word dw on dw.dictionary_id=d.dictionary_id join word w on w.word_id=dw.word_id join word_day wd on  wd.word_id=w.word_id join have_buy_dictionary hbd on hbd.have_buy_dictionary_dictionary=d.dictionary_id where wd.user_id=%@ and wd.word_day=0 and hbd.is_stop!=1 order by random() limit %@",userId,count];
    
    sqlite3_stmt * statement;
      sqlite3_exec(database, "BEGIN EXCLUSIVE TRANSACTION", 0, 0, 0);
    NSMutableArray *words = [NSMutableArray arrayWithCapacity:6];
    int error=sqlite3_prepare_v2(database, [sql UTF8String], -1, &statement, nil);
    if ( error== SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            
            char *word_id = (char*)sqlite3_column_text(statement, 0);
            char *word_word = (char*)sqlite3_column_text(statement, 1);
            char *word_symbol = (char*)sqlite3_column_text(statement, 2);
            char *word_word_class = (char*)sqlite3_column_text(statement, 3);
            char *word_statement = (char*)sqlite3_column_text(statement, 4);
            char *word_cn_statement = (char*)sqlite3_column_text(statement, 5);
            char *word_a_explan1 = (char*)sqlite3_column_text(statement, 6);
            char *word_a_explan2 = (char*)sqlite3_column_text(statement, 7);
            char *word_a_explan3 = (char*)sqlite3_column_text(statement, 8);
            char *word_b_explana = (char*)sqlite3_column_text(statement, 9);
            char *word_b_explanb = (char*)sqlite3_column_text(statement, 10);
            char *word_b_explanc = (char*)sqlite3_column_text(statement, 11);
            char *word_b_expland = (char*)sqlite3_column_text(statement, 12);
            char *word_b_explane = (char*)sqlite3_column_text(statement, 13);
            char *word_b_right = (char*)sqlite3_column_text(statement, 14);
            char *word_c_explan = (char*)sqlite3_column_text(statement, 15);
            char *word_c_answer = (char*)sqlite3_column_text(statement, 16);
            char *dictionary_id = (char*)sqlite3_column_text(statement, 17);
            char *dictionary_abc = (char*)sqlite3_column_text(statement, 18);
            char *word_analyze = (char*)sqlite3_column_text(statement, 19);
            
            NSString *_dictionary_id = [[NSString alloc]initWithUTF8String:dictionary_id];
            NSString *_word_id = [[NSString alloc]initWithUTF8String:word_id];
            NSString *_word_word = [[NSString alloc]initWithUTF8String:word_word];
            NSString *_word_symbol = [[NSString alloc]initWithUTF8String:word_symbol];
            
            NSString *_word_word_class = [[NSString alloc]initWithUTF8String:word_word_class];
            NSString *_word_statement = [[NSString alloc]initWithUTF8String:word_statement];
            NSString *_word_cn_statement = [[NSString alloc]initWithUTF8String:word_cn_statement];
            NSString *_word_a_explan1 = [[NSString alloc]initWithUTF8String:word_a_explan1];
            NSString *_word_a_explan2 = [[NSString alloc]initWithUTF8String:word_a_explan2];
            NSString *_word_a_explan3 = [[NSString alloc]initWithUTF8String:word_a_explan3];
            NSString *_word_b_explana = [[NSString alloc]initWithUTF8String:word_b_explana];
            NSString *_word_b_explanb = [[NSString alloc]initWithUTF8String:word_b_explanb];
            NSString *_word_b_explanc = [[NSString alloc]initWithUTF8String:word_b_explanc];
            NSString *_word_b_expland = [[NSString alloc]initWithUTF8String:word_b_expland];
            NSString *_word_b_explane = [[NSString alloc]initWithUTF8String:word_b_explane];
            NSString *_word_b_right = [[NSString alloc]initWithUTF8String:word_b_right];
            NSString *_word_c_explan = [[NSString alloc]initWithUTF8String:word_c_explan];
            NSString *_word_c_answer = [[NSString alloc]initWithUTF8String:word_c_answer];
            NSString *abc = [[NSString alloc]initWithUTF8String:dictionary_abc];
            NSString *_word_analyze = [[NSString alloc]initWithUTF8String:word_analyze];

            
            Word *word = [[Word alloc]init];
            word.dictionary_id=_dictionary_id;
            word.word_id = _word_id;
            word.word_word = _word_word;
            word.word_symbol = _word_symbol;
            word.word_word_class = _word_word_class;
            word.word_statement = _word_statement;
            word.word_cn_statement = _word_cn_statement;
            word.word_a_explan1 = _word_a_explan1;
            word.word_a_explan2 = _word_a_explan2;
            word.word_a_explan3 = _word_a_explan3;
            word.word_b_explana = _word_b_explana;
            word.word_b_explanb = _word_b_explanb;
            word.word_b_explanc = _word_b_explanc;
            word.word_b_expland = _word_b_expland;
            word.word_b_explane = _word_b_explane;
            word.word_b_right = _word_b_right;
            word.word_c_explan = _word_c_explan;
            word.word_c_answer = _word_c_answer;
            word.abc=abc;
            word.word_analyze = _word_analyze;
            [words addObject:word];
        }
        
    }else{
        NSLog(@"error%d",error);
    }
    sqlite3_finalize(statement);
      if (sqlite3_exec(database, "COMMIT TRANSACTION", 0, 0, 0) != SQLITE_OK) NSLog(@"SQL Error: %s",sqlite3_errmsg(database));
    [self closeDB];
    
    return words;

}

- (NSMutableArray *)getOldWord:(NSString * )uid{
    NSString *sql = [NSString stringWithFormat:@"SELECT DISTINCT  w.word_id,w.word_word,w.word_symbol,w.word_word_class,w.word_statement,w.word_cn_statement,w.word_a_explan1,w.word_a_explan2,w.word_a_explan3,w.word_b_explana,w.word_b_explanb,w.word_b_explanc,w.word_b_expland,w.word_b_explane,w.word_b_right,w.word_c_explan,w.word_c_answer,d.dictionary_id,d.dictionary_abc,word_analyze  FROM dictionary d join dictionary_with_word dw on dw.dictionary_id=d.dictionary_id join word w on w.word_id=dw.word_id join word_day wd on  wd.word_id=w.word_id join have_buy_dictionary hbd on hbd.have_buy_dictionary_dictionary=d.dictionary_id where wd.user_id=%@ and wd.word_day=1 or wd.word_day=2 or wd.word_day=4 or wd.word_day=7 or wd.word_day=14 or wd.word_day=28 or wd.word_day=56 and hbd.is_stop!=1",uid];
    
    [self openDB];
    NSMutableArray *words = [NSMutableArray arrayWithCapacity:6];
    sqlite3_stmt * statement;
      sqlite3_exec(database, "BEGIN EXCLUSIVE TRANSACTION", 0, 0, 0);
    if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &statement, nil) == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            char *word_id = (char*)sqlite3_column_text(statement, 0);
            char *word_word = (char*)sqlite3_column_text(statement, 1);
            char *word_symbol = (char*)sqlite3_column_text(statement, 2);
            char *word_word_class = (char*)sqlite3_column_text(statement, 3);
            char *word_statement = (char*)sqlite3_column_text(statement, 4);
            char *word_cn_statement = (char*)sqlite3_column_text(statement, 5);
            char *word_a_explan1 = (char*)sqlite3_column_text(statement, 6);
            char *word_a_explan2 = (char*)sqlite3_column_text(statement, 7);
            char *word_a_explan3 = (char*)sqlite3_column_text(statement, 8);
            char *word_b_explana = (char*)sqlite3_column_text(statement, 9);
            char *word_b_explanb = (char*)sqlite3_column_text(statement, 10);
            char *word_b_explanc = (char*)sqlite3_column_text(statement, 11);
            char *word_b_expland = (char*)sqlite3_column_text(statement, 12);
            char *word_b_explane = (char*)sqlite3_column_text(statement, 13);
            char *word_b_right = (char*)sqlite3_column_text(statement, 14);
            char *word_c_explan = (char*)sqlite3_column_text(statement, 15);
            char *word_c_answer = (char*)sqlite3_column_text(statement, 16);
            char *dictionary_id = (char*)sqlite3_column_text(statement, 17);
            char *dictionary_abc = (char*)sqlite3_column_text(statement, 18);
            char *word_analyze = (char*)sqlite3_column_text(statement, 19);
            
            NSString *_dictionary_id = [[NSString alloc]initWithUTF8String:dictionary_id];
            NSString *_word_id = [[NSString alloc]initWithUTF8String:word_id];
            NSString *_word_word = [[NSString alloc]initWithUTF8String:word_word];
            NSString *_word_symbol = [[NSString alloc]initWithUTF8String:word_symbol];
            
            NSString *_word_word_class = [[NSString alloc]initWithUTF8String:word_word_class];
            NSString *_word_statement = [[NSString alloc]initWithUTF8String:word_statement];
            NSString *_word_cn_statement = [[NSString alloc]initWithUTF8String:word_cn_statement];
            NSString *_word_a_explan1 = [[NSString alloc]initWithUTF8String:word_a_explan1];
            NSString *_word_a_explan2 = [[NSString alloc]initWithUTF8String:word_a_explan2];
            NSString *_word_a_explan3 = [[NSString alloc]initWithUTF8String:word_a_explan3];
            NSString *_word_b_explana = [[NSString alloc]initWithUTF8String:word_b_explana];
            NSString *_word_b_explanb = [[NSString alloc]initWithUTF8String:word_b_explanb];
            NSString *_word_b_explanc = [[NSString alloc]initWithUTF8String:word_b_explanc];
            NSString *_word_b_expland = [[NSString alloc]initWithUTF8String:word_b_expland];
            NSString *_word_b_explane = [[NSString alloc]initWithUTF8String:word_b_explane];
            NSString *_word_b_right = [[NSString alloc]initWithUTF8String:word_b_right];
            NSString *_word_c_explan = [[NSString alloc]initWithUTF8String:word_c_explan];
            NSString *_word_c_answer = [[NSString alloc]initWithUTF8String:word_c_answer];
            NSString *abc = [[NSString alloc]initWithUTF8String:dictionary_abc];
            NSString *_word_analyze = [[NSString alloc]initWithUTF8String:word_analyze];

            
            Word *word = [[Word alloc]init];
            word.dictionary_id=_dictionary_id;
            
            word.word_id = _word_id;
            word.word_word = _word_word;
            word.word_symbol = _word_symbol;
            
            word.word_word_class = _word_word_class;
            word.word_statement = _word_statement;
            word.word_cn_statement = _word_cn_statement;
            word.word_a_explan1 = _word_a_explan1;
            word.word_a_explan2 = _word_a_explan2;
            word.word_a_explan3 = _word_a_explan3;
            word.word_b_explana = _word_b_explana;
            word.word_b_explanb = _word_b_explanb;
            word.word_b_explanc = _word_b_explanc;
            word.word_b_expland = _word_b_expland;
            word.word_b_explane = _word_b_explane;
            word.word_b_right = _word_b_right;
            word.word_c_explan = _word_c_explan;
            word.word_c_answer = _word_c_answer;
            word.abc=abc;
            word.word_analyze = _word_analyze;
   
            
            [words addObject:word];
        }
        
    }else{
        NSLog(@"error");
    }
    sqlite3_finalize(statement);
      if (sqlite3_exec(database, "COMMIT TRANSACTION", 0, 0, 0) != SQLITE_OK) NSLog(@"SQL Error: %s",sqlite3_errmsg(database));
    [self closeDB];
    return words;
    


}//根据词库获取旧单词
- (NSMutableArray *)getNewWord:(NSString *)userId wordCount:(NSString *)count Dictionarys:(NSArray *)dics{
    NSMutableArray *words = [NSMutableArray arrayWithCapacity:6];
    NSMutableString *sql = [[NSMutableString alloc] initWithFormat:@"SELECT DISTINCT  w.word_id,w.word_word,w.word_symbol,w.word_word_class,w.word_statement,w.word_cn_statement,w.word_a_explan1,w.word_a_explan2,w.word_a_explan3,w.word_b_explana,w.word_b_explanb,w.word_b_explanc,w.word_b_expland,w.word_b_explane,w.word_b_right,w.word_c_explan,w.word_c_answer,d.dictionary_id,d.dictionary_abc,word_analyze FROM dictionary d join dictionary_with_word dw on dw.dictionary_id=d.dictionary_id join word w on w.word_id=dw.word_id join word_day wd on  wd.word_id=w.word_id join have_buy_dictionary hbd on hbd.have_buy_dictionary_dictionary=d.dictionary_id where wd.user_id=%@ and wd.word_day=0 and hbd.is_stop!=1 ",userId];
    
    for (UserDictionary *dic in dics) {
        [sql appendFormat:@" and d.dictionary_id=%@ ",dic.dictionary_id];
    }
   
    //get max and min then generate random sql
    int random = [self getDictionaryRandomIdInRange:dics];
    [sql appendFormat:@"and w.word_id>%d limit %@",random - [count intValue],count];
    sqlite3_stmt * statement;
    sqlite3_exec(database, "BEGIN EXCLUSIVE TRANSACTION", 0, 0, 0);
    if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &statement, nil) == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            char *word_id = (char*)sqlite3_column_text(statement, 0);
            char *word_word = (char*)sqlite3_column_text(statement, 1);
            char *word_symbol = (char*)sqlite3_column_text(statement, 2);
            char *word_word_class = (char*)sqlite3_column_text(statement, 3);
            char *word_statement = (char*)sqlite3_column_text(statement, 4);
            char *word_cn_statement = (char*)sqlite3_column_text(statement, 5);
            char *word_a_explan1 = (char*)sqlite3_column_text(statement, 6);
            char *word_a_explan2 = (char*)sqlite3_column_text(statement, 7);
            char *word_a_explan3 = (char*)sqlite3_column_text(statement, 8);
            char *word_b_explana = (char*)sqlite3_column_text(statement, 9);
            char *word_b_explanb = (char*)sqlite3_column_text(statement, 10);
            char *word_b_explanc = (char*)sqlite3_column_text(statement, 11);
            char *word_b_expland = (char*)sqlite3_column_text(statement, 12);
            char *word_b_explane = (char*)sqlite3_column_text(statement, 13);
            char *word_b_right = (char*)sqlite3_column_text(statement, 14);
            char *word_c_explan = (char*)sqlite3_column_text(statement, 15);
            char *word_c_answer = (char*)sqlite3_column_text(statement, 16);
            char *dictionary_id = (char*)sqlite3_column_text(statement, 17);
            char *dictionary_abc = (char*)sqlite3_column_text(statement, 18);
            char *word_analyze = (char*)sqlite3_column_text(statement, 19);
            
            NSString *_dictionary_id = [[NSString alloc]initWithUTF8String:dictionary_id];
            NSString *_word_id = [[NSString alloc]initWithUTF8String:word_id];
            NSString *_word_word = [[NSString alloc]initWithUTF8String:word_word];
            NSString *_word_symbol = [[NSString alloc]initWithUTF8String:word_symbol];
            NSString *_word_word_class = [[NSString alloc]initWithUTF8String:word_word_class];
            NSString *_word_statement = [[NSString alloc]initWithUTF8String:word_statement];
            NSString *_word_cn_statement = [[NSString alloc]initWithUTF8String:word_cn_statement];
            NSString *_word_a_explan1 = [[NSString alloc]initWithUTF8String:word_a_explan1];
            NSString *_word_a_explan2 = [[NSString alloc]initWithUTF8String:word_a_explan2];
            NSString *_word_a_explan3 = [[NSString alloc]initWithUTF8String:word_a_explan3];
            NSString *_word_b_explana = [[NSString alloc]initWithUTF8String:word_b_explana];
            NSString *_word_b_explanb = [[NSString alloc]initWithUTF8String:word_b_explanb];
            NSString *_word_b_explanc = [[NSString alloc]initWithUTF8String:word_b_explanc];
            NSString *_word_b_expland = [[NSString alloc]initWithUTF8String:word_b_expland];
            NSString *_word_b_explane = [[NSString alloc]initWithUTF8String:word_b_explane];
            NSString *_word_b_right = [[NSString alloc]initWithUTF8String:word_b_right];
            NSString *_word_c_explan = [[NSString alloc]initWithUTF8String:word_c_explan];
            NSString *_word_c_answer = [[NSString alloc]initWithUTF8String:word_c_answer];
            NSString *abc = [[NSString alloc]initWithUTF8String:dictionary_abc];
            NSString *_word_analyze = [[NSString alloc]initWithUTF8String:word_analyze];
            
            Word *word = [[Word alloc]init];
            word.dictionary_id=_dictionary_id;
            word.word_id = _word_id;
            word.word_word = _word_word;
            word.word_symbol = _word_symbol;
            word.word_word_class = _word_word_class;
            word.word_statement = _word_statement;
            word.word_cn_statement = _word_cn_statement;
            word.word_a_explan1 = _word_a_explan1;
            word.word_a_explan2 = _word_a_explan2;
            word.word_a_explan3 = _word_a_explan3;
            word.word_b_explana = _word_b_explana;
            word.word_b_explanb = _word_b_explanb;
            word.word_b_explanc = _word_b_explanc;
            word.word_b_expland = _word_b_expland;
            word.word_b_explane = _word_b_explane;
            word.word_b_right = _word_b_right;
            word.word_c_explan = _word_c_explan;
            word.word_c_answer = _word_c_answer;
            word.abc=abc;
            word.word_analyze = _word_analyze;
            [words addObject:word];
        }
        
    }else{
        NSLog(@"error");
    }
    sqlite3_finalize(statement);
    if (sqlite3_exec(database, "COMMIT TRANSACTION", 0, 0, 0) != SQLITE_OK) NSLog(@"SQL Error: %s",sqlite3_errmsg(database));

    return words;
}

-(int)getRandomNumberBetween:(int)from to:(int)to {
    return (int)from + arc4random() % (to-from+1);
}
-(int)getDictionaryRandomIdInRange:(NSArray *)dics{
    NSMutableString *sql = [[NSMutableString alloc] initWithFormat:@"SELECT max(w.word_id) as max, min(w.word_id) as min  FROM  dictionary_with_word dw  join word w on w.word_id=dw.word_id  where "];
    
    for (int i=0; i<dics.count; i++) {
        if (i>0) {
            [sql appendString:@" and "];
        }
        [sql appendFormat:@" dw.dictionary_id=%@ ",((UserDictionary *)[dics objectAtIndex:i]).dictionary_id];
    }
  
    int random = 0;
    sqlite3_stmt * statement;
    sqlite3_exec(database, "BEGIN EXCLUSIVE TRANSACTION", 0, 0, 0);
    if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &statement, nil) == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            int max = sqlite3_column_int(statement, 0);
            int min = sqlite3_column_int(statement, 1);
            random = [self getRandomNumberBetween:min to:max];
        }
        
    }else{
        NSLog(@"error");
    }
    sqlite3_finalize(statement);
    if (sqlite3_exec(database, "COMMIT TRANSACTION", 0, 0, 0) != SQLITE_OK) NSLog(@"SQL Error: %s",sqlite3_errmsg(database));
    return random;
}
//- (NSMutableArray *)getNewWord:(NSString *)userId wordCount:(NSString *)count Dictionarys:(NSArray *)dics{
//
//    [self openDB];
//    NSMutableString *sql = [[NSMutableString alloc] initWithFormat:@"SELECT  w.word_id,w.word_word,w.word_symbol,w.word_word_class,w.word_statement,w.word_cn_statement,w.word_a_explan1,w.word_a_explan2,w.word_a_explan3,w.word_b_explana,w.word_b_explanb,w.word_b_explanc,w.word_b_expland,w.word_b_explane,w.word_b_right,w.word_c_explan,w.word_c_answer,d.dictionary_id,d.dictionary_abc,w.word_d_picture1,word_d_picture2 FROM dictionary d  join dictionary_with_word dw on dw.dictionary_id=d.dictionary_id  join word w on w.word_id=dw.word_id  join word_day wd on  wd.word_id=w.word_id  join have_buy_dictionary hbd on hbd.have_buy_dictionary_dictionary=d.dictionary_id   where wd.user_id=%@ and wd.word_day=0 ",userId];
//    for (UserDictionary *dic in dics) {
//        [sql appendFormat:@" and d.dictionary_id=%@ ",dic.dictionary_id];
//        NSLog(@"count:%@",[self getDictionaryWordCount:dic]);
//        [self openDB];
//    }
//    
//    [sql appendFormat:@" and hbd.is_stop!=1"];
//
//    sqlite3_stmt * statement;
//    sqlite3_exec(database, "BEGIN EXCLUSIVE TRANSACTION", 0, 0, 0);
//    NSMutableArray *words = [NSMutableArray arrayWithCapacity:6];
//    int error=sqlite3_prepare_v2(database, [sql UTF8String], -1, &statement, nil);
//    int index_count = 0;
//    if ( error== SQLITE_OK) {
//        while (sqlite3_step(statement) == SQLITE_ROW) {
//            
//            char *word_id = (char*)sqlite3_column_text(statement, 0);
//            char *word_word = (char*)sqlite3_column_text(statement, 1);
//            char *word_symbol = (char*)sqlite3_column_text(statement, 2);
//            char *word_word_class = (char*)sqlite3_column_text(statement, 3);
//            char *word_statement = (char*)sqlite3_column_text(statement, 4);
//            char *word_cn_statement = (char*)sqlite3_column_text(statement, 5);
//            char *word_a_explan1 = (char*)sqlite3_column_text(statement, 6);
//            char *word_a_explan2 = (char*)sqlite3_column_text(statement, 7);
//            char *word_a_explan3 = (char*)sqlite3_column_text(statement, 8);
//            char *word_b_explana = (char*)sqlite3_column_text(statement, 9);
//            char *word_b_explanb = (char*)sqlite3_column_text(statement, 10);
//            char *word_b_explanc = (char*)sqlite3_column_text(statement, 11);
//            char *word_b_expland = (char*)sqlite3_column_text(statement, 12);
//            char *word_b_explane = (char*)sqlite3_column_text(statement, 13);
//            char *word_b_right = (char*)sqlite3_column_text(statement, 14);
//            char *word_c_explan = (char*)sqlite3_column_text(statement, 15);
//            char *word_c_answer = (char*)sqlite3_column_text(statement, 16);
//            char *dictionary_id = (char*)sqlite3_column_text(statement, 17);
//            char *dictionary_abc = (char*)sqlite3_column_text(statement, 18);
//            
//            
//            
//            NSString *_dictionary_id = [[NSString alloc]initWithUTF8String:dictionary_id];
//            NSString *_word_id = [[NSString alloc]initWithUTF8String:word_id];
//            NSString *_word_word = [[NSString alloc]initWithUTF8String:word_word];
//            NSString *_word_symbol = [[NSString alloc]initWithUTF8String:word_symbol];
//            
//            NSString *_word_word_class = [[NSString alloc]initWithUTF8String:word_word_class];
//            NSString *_word_statement = [[NSString alloc]initWithUTF8String:word_statement];
//            NSString *_word_cn_statement = [[NSString alloc]initWithUTF8String:word_cn_statement];
//            NSString *_word_a_explan1 = [[NSString alloc]initWithUTF8String:word_a_explan1];
//            NSString *_word_a_explan2 = [[NSString alloc]initWithUTF8String:word_a_explan2];
//            NSString *_word_a_explan3 = [[NSString alloc]initWithUTF8String:word_a_explan3];
//            NSString *_word_b_explana = [[NSString alloc]initWithUTF8String:word_b_explana];
//            NSString *_word_b_explanb = [[NSString alloc]initWithUTF8String:word_b_explanb];
//            NSString *_word_b_explanc = [[NSString alloc]initWithUTF8String:word_b_explanc];
//            NSString *_word_b_expland = [[NSString alloc]initWithUTF8String:word_b_expland];
//            NSString *_word_b_explane = [[NSString alloc]initWithUTF8String:word_b_explane];
//            NSString *_word_b_right = [[NSString alloc]initWithUTF8String:word_b_right];
//            NSString *_word_c_explan = [[NSString alloc]initWithUTF8String:word_c_explan];
//            NSString *_word_c_answer = [[NSString alloc]initWithUTF8String:word_c_answer];
//            NSString *abc = [[NSString alloc]initWithUTF8String:dictionary_abc];
//            
//            
//            
//            Word *word = [[Word alloc]init];
//            word.dictionary_id=_dictionary_id;
//            word.word_id = _word_id;
//            word.word_word = _word_word;
//            word.word_symbol = _word_symbol;
//            word.word_word_class = _word_word_class;
//            word.word_statement = _word_statement;
//            word.word_cn_statement = _word_cn_statement;
//            word.word_a_explan1 = _word_a_explan1;
//            word.word_a_explan2 = _word_a_explan2;
//            word.word_a_explan3 = _word_a_explan3;
//            word.word_b_explana = _word_b_explana;
//            word.word_b_explanb = _word_b_explanb;
//            word.word_b_explanc = _word_b_explanc;
//            word.word_b_expland = _word_b_expland;
//            word.word_b_explane = _word_b_explane;
//            word.word_b_right = _word_b_right;
//            word.word_c_explan = _word_c_explan;
//            word.word_c_answer = _word_c_answer;
//            word.abc=abc;
//            
//            [words addObject:word];
//            index_count++;
//        }
//        
//    }else{
//        NSLog(@"error%d",error);
//    }
//    sqlite3_finalize(statement);
//    if (sqlite3_exec(database, "COMMIT TRANSACTION", 0, 0, 0) != SQLITE_OK) NSLog(@"SQL Error: %s",sqlite3_errmsg(database));
//    [self closeDB];
//    NSLog(@"inde_coun:%d",index_count);
//    NSLog(@"%lu",(unsigned long)[words count]);
//
//    [words shuffle];
//    NSLog(@"%lu",(unsigned long)[words count]);
//    return [words objectsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, [count intValue])]];
//
//}//从所有词库中获取新单词，25个，wordCount为要返回的单词数量

- (NSMutableArray *)getOldWord:(NSString * )uid Dictionarys:(NSArray *)dics{
    NSString *sql = [NSString stringWithFormat:@"SELECT DISTINCT  w.word_id,w.word_word,w.word_symbol,w.word_word_class,w.word_statement,w.word_cn_statement,w.word_a_explan1,w.word_a_explan2,w.word_a_explan3,w.word_b_explana,w.word_b_explanb,w.word_b_explanc,w.word_b_expland,w.word_b_explane,w.word_b_right,w.word_c_explan,w.word_c_answer,d.dictionary_id,d.dictionary_abc   FROM dictionary d join dictionary_with_word dw on dw.dictionary_id=d.dictionary_id join word w on w.word_id=dw.word_id join word_day wd on  wd.word_id=w.word_id join have_buy_dictionary hbd on hbd.have_buy_dictionary_dictionary=d.dictionary_id where wd.user_id=%@ and wd.word_day=1 or wd.word_day=2 or wd.word_day=4 or wd.word_day=7 or wd.word_day=14 or wd.word_day=28 or wd.word_day=56 and hbd.is_stop!=1",uid];
    
    for (UserDictionary *dic in dics) {
        sql = [NSString stringWithFormat:@"%@ and d.dictionary_id='%@'",sql,dic.dictionary_id];
    }
    
    [self openDB];
    NSMutableArray *words = [NSMutableArray arrayWithCapacity:6];
    sqlite3_stmt * statement;
    sqlite3_exec(database, "BEGIN EXCLUSIVE TRANSACTION", 0, 0, 0);
    if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &statement, nil) == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            char *word_id = (char*)sqlite3_column_text(statement, 0);
            char *word_word = (char*)sqlite3_column_text(statement, 1);
            char *word_symbol = (char*)sqlite3_column_text(statement, 2);
            char *word_word_class = (char*)sqlite3_column_text(statement, 3);
            char *word_statement = (char*)sqlite3_column_text(statement, 4);
            char *word_cn_statement = (char*)sqlite3_column_text(statement, 5);
            char *word_a_explan1 = (char*)sqlite3_column_text(statement, 6);
            char *word_a_explan2 = (char*)sqlite3_column_text(statement, 7);
            char *word_a_explan3 = (char*)sqlite3_column_text(statement, 8);
            char *word_b_explana = (char*)sqlite3_column_text(statement, 9);
            char *word_b_explanb = (char*)sqlite3_column_text(statement, 10);
            char *word_b_explanc = (char*)sqlite3_column_text(statement, 11);
            char *word_b_expland = (char*)sqlite3_column_text(statement, 12);
            char *word_b_explane = (char*)sqlite3_column_text(statement, 13);
            char *word_b_right = (char*)sqlite3_column_text(statement, 14);
            char *word_c_explan = (char*)sqlite3_column_text(statement, 15);
            char *word_c_answer = (char*)sqlite3_column_text(statement, 16);
            char *dictionary_id = (char*)sqlite3_column_text(statement, 17);
            char *dictionary_abc = (char*)sqlite3_column_text(statement, 18);
            
            NSString *_dictionary_id = [[NSString alloc]initWithUTF8String:dictionary_id];
            NSString *_word_id = [[NSString alloc]initWithUTF8String:word_id];
            NSString *_word_word = [[NSString alloc]initWithUTF8String:word_word];
            NSString *_word_symbol = [[NSString alloc]initWithUTF8String:word_symbol];
            
            NSString *_word_word_class = [[NSString alloc]initWithUTF8String:word_word_class];
            NSString *_word_statement = [[NSString alloc]initWithUTF8String:word_statement];
            NSString *_word_cn_statement = [[NSString alloc]initWithUTF8String:word_cn_statement];
            NSString *_word_a_explan1 = [[NSString alloc]initWithUTF8String:word_a_explan1];
            NSString *_word_a_explan2 = [[NSString alloc]initWithUTF8String:word_a_explan2];
            NSString *_word_a_explan3 = [[NSString alloc]initWithUTF8String:word_a_explan3];
            NSString *_word_b_explana = [[NSString alloc]initWithUTF8String:word_b_explana];
            NSString *_word_b_explanb = [[NSString alloc]initWithUTF8String:word_b_explanb];
            NSString *_word_b_explanc = [[NSString alloc]initWithUTF8String:word_b_explanc];
            NSString *_word_b_expland = [[NSString alloc]initWithUTF8String:word_b_expland];
            NSString *_word_b_explane = [[NSString alloc]initWithUTF8String:word_b_explane];
            NSString *_word_b_right = [[NSString alloc]initWithUTF8String:word_b_right];
            NSString *_word_c_explan = [[NSString alloc]initWithUTF8String:word_c_explan];
            NSString *_word_c_answer = [[NSString alloc]initWithUTF8String:word_c_answer];
            NSString *abc = [[NSString alloc]initWithUTF8String:dictionary_abc];
            
            Word *word = [[Word alloc]init];
            word.dictionary_id=_dictionary_id;
            
            word.word_id = _word_id;
            word.word_word = _word_word;
            word.word_symbol = _word_symbol;
            
            word.word_word_class = _word_word_class;
            word.word_statement = _word_statement;
            word.word_cn_statement = _word_cn_statement;
            word.word_a_explan1 = _word_a_explan1;
            word.word_a_explan2 = _word_a_explan2;
            word.word_a_explan3 = _word_a_explan3;
            word.word_b_explana = _word_b_explana;
            word.word_b_explanb = _word_b_explanb;
            word.word_b_explanc = _word_b_explanc;
            word.word_b_expland = _word_b_expland;
            word.word_b_explane = _word_b_explane;
            word.word_b_right = _word_b_right;
            word.word_c_explan = _word_c_explan;
            word.word_c_answer = _word_c_answer;
            word.abc=abc;
            
            
            
            [words addObject:word];
        }
        
    }else{
        NSLog(@"error");
    }
    sqlite3_finalize(statement);
    if (sqlite3_exec(database, "COMMIT TRANSACTION", 0, 0, 0) != SQLITE_OK) NSLog(@"SQL Error: %s",sqlite3_errmsg(database));
    [self closeDB];
    return words;
    

}//根据词库获取旧单词，wordCount为要返回的单词数量

- (void)answerRightAndSaveToDB:(Word *)word{
    [self openDB];
    NSString *word_id = word.word_id;
    NSString *word_word = word.word_word;
    NSString *sql = [NSString stringWithFormat:@"INSERT INTO 'complete_word' ('word_id','word_word') VALUES (%@,'%@')",word_id,word_word];
    [self execSql:sql];
    [self closeDB];
    
}//回答正确，把word单词放进以背诵表里




/*
 *数据库操作方法
 */

- (BOOL)openDB{
   
}
- (void)closeDB{
  
}
- (BOOL)execSql:(NSString *)sql{
    char *err;
    while (YES) {
        if (sqlite3_exec(database, [sql UTF8String], NULL, NULL, &err) != SQLITE_OK) {
            
            NSLog(@"数据库操作数据失败!:%s SQL:%@",sqlite3_errmsg(database),sql);
            
            if(strcmp(sqlite3_errmsg(database), "database is locked")==0){
                NSLog(@"!!!database is locked sleep 1s");
                sleep(0.3);
                continue;
            }
            break;
        }else{
            break;
        }
    }

    [self closeDB];
    NSLog(@"数据库操作数据成!");
    return YES;
}
- (NSString *)getAllWordCount{
    NSString *sql =@"select count(*) from word";
    [self openDB];
    sqlite3_stmt * statement;
    
    if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &statement, nil) == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            char *count = (char*)sqlite3_column_text(statement, 0);
            NSString *c = [[NSString alloc]initWithUTF8String:count];
            sqlite3_finalize(statement);
            [self closeDB];
            return c;
        }
    }else{
        sqlite3_finalize(statement);
        [self closeDB];
        return 0;
    }
   
}

- (NSString *)getCompleteTime:(UserDictionary *)dictionary{
    NSString *sql =[NSString stringWithFormat:@"select dictionary_complete_time from dictionary where dictionary_id=%@",dictionary.dictionary_id];
    [self openDB];
    sqlite3_stmt * statement;
    
    if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &statement, nil) == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            char *count = (char*)sqlite3_column_text(statement, 0);
            NSString *c = [[NSString alloc]initWithUTF8String:count];
            sqlite3_finalize(statement);
            [self closeDB];
            return c;
        }
    }else{
        NSLog(@"!!error: %s",sqlite3_errmsg(database));
    }
        sqlite3_finalize(statement);
        [self closeDB];
        return nil;
    
}//获取完成时间
- (void)setDictionaryCompleteTime:(NSString *)howManyTime dictioanry:(UserDictionary *)dic{
    NSString *sql = [NSString stringWithFormat:@"UPDATE dictionary set dictionary_complete_time=%@ where dictionary_id=%@",howManyTime,dic.dictionary_id];
    [self openDB];
    [self execSql:sql];
    [self closeDB];
}//直接设置完成时间

- (void)dictionaryAddCompleteTime:(NSString *)howManyTime dictionary:(UserDictionary *)dic{
    NSString *sql = [NSString stringWithFormat:@"UPDATE dictionary set dictionary_complete_time=dictionary_complete_time+%@ where dictionary_id=%@",howManyTime,dic.dictionary_id];
    [self openDB];
    [self execSql:sql];
    [self closeDB];
}//给词库增加完成时间

- (void)dictionarySubCompleteTime:(NSString *)howManyTime dictionary:(UserDictionary *)dic{
    NSString *sql = [NSString stringWithFormat:@"UPDATE dictionary set dictionary_complete_time=dictionary_complete_time-%@ where dictionary_id=%@",howManyTime,dic.dictionary_id];
    [self openDB];
    [self execSql:sql];
    [self closeDB];
}//给词库减少完成时间

- (NSString *)getDictionaryWordCount:(UserDictionary *)dictioanry{
    NSString *sql =[NSString stringWithFormat:@"SELECT count(*) FROM word w left join  dictionary_with_word dww where dww.dictionary_id=%@ and dww.word_id = w.word_id",dictioanry.dictionary_id];
    [self openDB];
    sqlite3_stmt * statement;
    
    if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &statement, nil) == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            char *count = (char*)sqlite3_column_text(statement, 0);
            NSString *c = [[NSString alloc]initWithUTF8String:count];
            sqlite3_finalize(statement);
            [self closeDB];
            return c;
        }
    }
    sqlite3_finalize(statement);
    [self closeDB];
    return nil;
}//获取词库总数
- (void)setDictionaryPronounce:(UserDictionary *)dictionary whatPronounce:(NSString *)pronounce{
    NSString *sql =[NSString stringWithFormat:@"UPDATE 'dictionary' SET 'dictionary_pronounce' = %@ WHERE  'dictionary_id' = %@",pronounce,dictionary.dictionary_id];
    [self openDB];
    [self execSql:sql];
    [self closeDB];
}//设置词库的发音
- (NSString *)getDictionaryPronounce:(UserDictionary *)dictionary{
    NSString *sql =[NSString stringWithFormat:@"SELECT dictionary_pronounce FROM dictionary where dictionary_id=%@",dictionary.dictionary_id];
    [self openDB];
    sqlite3_stmt * statement;
    
    if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &statement, nil) == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            char *count = (char*)sqlite3_column_text(statement, 0);
            NSString *c = [[NSString alloc]initWithUTF8String:count];
            sqlite3_finalize(statement);
            [self closeDB];
            return c;
        }
    }
    sqlite3_finalize(statement);
    [self closeDB];
    return nil;
}//获取词库的发音
- (NSString *)switchCurrentDictionaryPronounce:(UserDictionary *)dictionary{
    NSString *sql =[NSString stringWithFormat:@"update dictionary set current_pronounce = case when current_pronounce = 1 then 0 else 1 end where dictionary.dictionary_id = '%@'",dictionary.dictionary_id];
    [self openDB];
    [self execSql:sql];
    [self closeDB];
}//切换词库发音
- (NSString *)getCurrentDictionaryPronounce:(UserDictionary *)dictionary{
    NSString *sql =[NSString stringWithFormat:@"SELECT current_pronounce FROM dictionary where dictionary_id=%@",dictionary.dictionary_id];
    [self openDB];
    sqlite3_stmt * statement;
    NSString *c = nil;
    if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &statement, nil) == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            char *count = (char*)sqlite3_column_text(statement, 0);
            c = [[NSString alloc]initWithUTF8String:count];
            sqlite3_finalize(statement);
            
        }
    }
    sqlite3_finalize(statement);
    [self closeDB];
    if ([c compare:@"0"]==NSOrderedSame) {
        return @"US";
    }else{
        return @"UK";
    }
}//获取词库当前已经切换的发音

@end
