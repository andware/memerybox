//
//  remember_view.m
//  MemoryBox
//
//  Created by joewu on 5/27/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "remember_view.h"
#import <QuartzCore/CATransaction.h>
#import "Word.h"
#import "User.h"
#import "wrongWords.h"
#import "QuestionC.h"
#import "Constants.h"
#import "HomePage.h"
#import "ServiceApi.h"
#import "DataBaseHelper.h"
#import "tools.h"
#import "secondList.h"
#import "getWordArray.h"
#import "Algorithm.h"
#import "wordStructure.h"

@interface remember_view ()

@end

@implementation remember_view
@synthesize dwNum;//表示界面的编号，防止越界
@synthesize viewNum;
int newwordscount;
bool isGoOver;
tools *tool;

Word *b_word; // b型题错误后处理
BOOL *b = false;

Word *c_word;
bool *c = false;

NSUserDefaults *userDefaultes;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    tool = [[tools alloc]init];// 初始化工具类
    userDefaultes = [NSUserDefaults standardUserDefaults];// 初始化存储工具
    database = [[DataBase alloc]init];// 初始化数据库
    wordsArray = [[NSMutableArray alloc]init];// 初始化当日需要背诵的数组
    GoOver = [[NSMutableArray alloc]init]; // 初始化复习单词的数字
    isGoOver = false; // 默认复习为否
    NSUserDefaults *userDefaultes = [NSUserDefaults standardUserDefaults];

    // 判断是不是被动错误单词
    BOOL memory = [userDefaultes boolForKey:@"useWrongWords"];
    if (memory) {
        wordsArray = [tool readArray:@"wrongWordArray"];
        NSLog(@"背诵的是错误的单词,数量是:%d",[wordsArray count]);
        //如果错误单词数量为0,弹出提示
        if ([wordsArray count] == 0) {
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"错误单词背诵提醒" message:@"现在还没有可背诵的错误单词，先学习新的单词吧！" delegate:self cancelButtonTitle:@"关闭" otherButtonTitles:nil];
            [alert show];
            [alert release];
        }
    }else{
//        Algorithm *algorithm = [[Algorithm alloc]init];
//        NSMutableArray * array = [algorithm DynamicArray];
//        
//        getWordArray *getwords = [[getWordArray alloc]init];
//        newWordsArray= [getwords getNewWords];
//        NSLog(@"%lu",(unsigned long)[newWordsArray count]);
//        
//        oldWordsArray = [getwords GetOldWord];
//        NSLog(@"%lu",(unsigned long)[oldWordsArray count]);
//        
//        [getwords getAllWord];
    }
    //================================================end
    
    // 系统时间处理
    // =================================================start
    // 获得系统当前时间
//    NSDate *  senddate=[NSDate date];
//    NSCalendar  * cal=[NSCalendar  currentCalendar];
//    NSUInteger  unitFlags=NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit;
//    NSDateComponents * conponent= [cal components:unitFlags fromDate:senddate];
//    NSInteger nowMonth=[conponent month];
//    NSInteger nowDay=[conponent day];
    
    // 获得上次登录时间
//    NSString *lastmoth = [userDefaultes stringForKey:@"lastLoginMonth"];
//    NSString *lastday = [userDefaultes stringForKey:@"lastLoginDay"];
//    int oldmonth = [lastmoth intValue];
//    int oldday = [lastday intValue];
    
    // 计算两次的时间差 ,单位: 天
//    int time = 30*(nowMonth - oldmonth) + (nowDay - oldday);
    
    // 保存本次登录时间
//    NSString *lastLoginMonth = [[NSString alloc]initWithFormat:@"%d",nowMonth];
//    NSString *lastLoginDay = [[NSString alloc]initWithFormat:@"%d",nowDay];
//    [userDefaultes setObject:lastLoginMonth forKey:@"lastLoginMonth"];
//    [userDefaultes setObject:lastLoginDay forKey:@"lastLoginDay"];
//    [lastLoginMonth release];
    // ===================================================end
    
    /*
     * 开始动态生成单词的view,得到保存的单词记录标签
     * ------------------------------------------------------------
     */

    NSString *process = [userDefaultes stringForKey:@"dwNum"];
//    [lastLoginDay release];
    int intString = [process intValue];
    if (![process compare:@""]==NSOrderedSame && ![[NSUserDefaults standardUserDefaults] boolForKey:@"useWrongWords"]) {
        dwNum=intString;//得到保存的单词标签
    }else{
        dwNum=0;
    }
    
    // 整合当日需要学习的词汇，如新旧单词穿插组合
    // ===================================================start
    if (dwNum !=0 && ![[NSUserDefaults standardUserDefaults] boolForKey:@"newUser"] && ![[NSUserDefaults standardUserDefaults] boolForKey:@"useWrongWords"]) {//如果同一天，不是新用户，不是背诵当日错误词库
        
        // 获得保存的当日背诵的单词数组
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSData *newArray = [userDefaults objectForKey:@"todayArray"];
        
        // 如果存在当日没有背完的单词就背诵当天的单词
        if(newArray!=nil && ![[NSUserDefaults standardUserDefaults] boolForKey:@"isGoOver"]){
            NSArray *getNewArray = [NSKeyedUnarchiver unarchiveObjectWithData:newArray];
            wordsArray =  [NSMutableArray arrayWithArray:getNewArray];
            [GoOver addObjectsFromArray:wordsArray];
            NSLog(@"继续背诵当日没有背诵完的单词！---%d",[wordsArray count]);
        
            // 如果当日单词已经背完，就复习当日词汇
        }else{
            NSData *newArray = [userDefaults objectForKey:@"GoOverWord"];
            NSArray *getNewArray = [NSKeyedUnarchiver unarchiveObjectWithData:newArray];
            wordsArray =  [NSMutableArray arrayWithArray:getNewArray];
            isGoOver = true;// 标记复习状态为真
            if ([wordsArray count] == 0) {
                UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"回顾提醒" message:@"今天没有需要复习的单词!" delegate:self cancelButtonTitle:@"关闭" otherButtonTitles:nil];
                [alert show];
                [alert release];
            }
            NSLog(@"复习当天的单词--->%d",[wordsArray count]);
        }
        
        // 如果当日没有没背完的单词,也不是背诵错误单词，
    }else if(![[NSUserDefaults standardUserDefaults] boolForKey:@"useWrongWords"]){
        
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isGoOver"]; // 存储复习状态为否
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"dwNum"];// 存储单词标记从0开始.
        
//    if ([newWordsArray count]!=0 || [oldWordsArray count]!=0) {// 保证新单词数组或旧单词数组不为空
//        if ([oldWordsArray count] ==0) {//如果旧单词数组为空新单词数组一定不为空
//            wordsArray = newWordsArray;
//            NSLog(@"只有新单词--%d",[wordsArray count]);
//            
//        }else if([newWordsArray count] ==0){//如果新单词数组为空旧单词数组一定不为空
//            wordsArray = oldWordsArray;
//            NSLog(@"只有旧单词--%d",[wordsArray count]);
//            
//        }else{// 将新旧单词数组整合到一个数组中
//            int newWordIndex = [newWordsArray count]/3;
//            int oldWordIndex = [oldWordsArray count]/3;
//            NSLog(@"---###----旧单词数量=%d",[oldWordsArray count]);
//            for (int i = 0; i < [oldWordsArray count]; i++) {
//                Word *word = [oldWordsArray objectAtIndex:i];
//                NSLog(@"- %@",word.word_word);
//            }
//            if (newWordIndex == 0 || oldWordIndex ==0) {
//                for (int i = 0; i < [newWordsArray count]; i++) {
//                    [wordsArray addObject:[newWordsArray objectAtIndex:i]];
//                }
//                for (int i = 0; i < [oldWordsArray count]; i++) {
//                    [wordsArray addObject:[oldWordsArray objectAtIndex:i]];
//                }
//            }else{
//            for (int i =0; i < [newWordsArray count]+ [oldWordsArray count]; i++) {
//                if (i <= newWordIndex) {
//                    [wordsArray addObject:[newWordsArray objectAtIndex:i]];
//                }else if ((i > newWordIndex) && (i<=newWordIndex+oldWordIndex)){
//                    [wordsArray addObject:[oldWordsArray objectAtIndex:i-newWordIndex]];
//                }else if ((i >(newWordIndex+oldWordIndex)) && (i<=(2*newWordIndex+oldWordIndex))){
//                    [wordsArray addObject:[newWordsArray objectAtIndex:i-newWordIndex-oldWordIndex]];
//                }else if((i > (2*newWordIndex+oldWordIndex)) && i<= 2*(newWordIndex + oldWordIndex)){
//                    [wordsArray addObject:[oldWordsArray objectAtIndex:i-2*newWordIndex-oldWordIndex]];
//                }else if((i >2*(newWordIndex + oldWordIndex))&&(i <(3*newWordIndex + 2* oldWordIndex))){
//                    [wordsArray addObject:[newWordsArray objectAtIndex:i-2*(newWordIndex+oldWordIndex)]];
//                }else{
//                    [wordsArray addObject:[oldWordsArray objectAtIndex:i-3*newWordIndex-2*oldWordIndex]];
//                }
//            }
//            }
//            NSLog(@"新旧组合在单词一起,数组大小为：%d",[wordsArray count]);
//        }// 新旧单词整合完毕
        
//    }else{//如果没有获得新单词同时也没有获得旧单词
//        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"背诵提示" message:@"没有可背诵单词或者没有启用词库，请检查词库!" delegate:self cancelButtonTitle:@"关闭" otherButtonTitles:nil];
//        [alert show];
//        [alert release];
//    }
        // 将新的单词数组保存下来, 提供给复习时使用
        NSArray * myarray = [[NSArray alloc] initWithArray:wordsArray];
        NSData *udObject = [NSKeyedArchiver archivedDataWithRootObject:myarray];
        [userDefaultes setObject:udObject forKey:@"GoOverWord"];
    }
    // ===================================================end
    
    /*
     * ------------------------------------------------------------
     * 初始化当日背诵单词数（新单词，复习单词，错误单词）组结束
     */
    
    // 超过100的会分段处理
    int word_count = [wordsArray count];
    if ([wordsArray count] > 100) {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"警告" message:@"亲，你一次性背诵的单词超过了100个，我们已将单词做了分段处理，在背完之后请选择继续背诵" delegate:self cancelButtonTitle:@"确定"  otherButtonTitles:nil,nil];
        [alert show];
        [alert release];
        word_count = 100;
    }
    // 初始化view数组和answer数组
    viewArray=[[NSMutableArray alloc]init];
    isAnsArray=[[NSMutableArray alloc]init];
    // 将单词生成数组
    for (int i=0; i< word_count; i++) {
        
        Word *word = [wordsArray objectAtIndex:i];
        int value2 = arc4random() % 2;//单词随机题型
        int value = arc4random() % 3;//单词随机题型
        int value3 = arc4random() % 4;//ABCD型
        
        // 查询单词目前的天数
        NSString *days = [database getWordDay:word.word_id userId:user.user_id];
        
        int worday = [days intValue];
        
        if ((([word.abc compare:@"1"]==NSOrderedSame)||([word.abc compare:@"12"]==NSOrderedSame)||([word.abc compare:@"13"]==NSOrderedSame)||([word.abc compare:@"14"]==NSOrderedSame)||([word.abc compare:@"123"]==NSOrderedSame)||([word.abc compare:@"124"]==NSOrderedSame)||([word.abc compare:@"134"]==NSOrderedSame)||([word.abc compare:@"1234"]==NSOrderedSame)) && worday ==0) {
            //题型A
            [self QuestionA:word Num:i];
        }else if([word.abc compare:@"1234"]==NSOrderedSame){
            switch (value3) {
                case  0:
                    //题型A
                    [self QuestionA:word Num:i];
                    break;
                case  1:
                    //题型B
                    [self QuestionB:word Num:i];
                    break;
                case  2:
                    //题型C
                    [self QuestionC:word Num:i];
                    break;
                case  3:
                    //题型D
                    [self QuestionD:word Num:i];
                    break;
            }
        }else if([word.abc compare:@"124"]==NSOrderedSame){
            switch (value) {
                case  0:
                    //题型A
                    [self QuestionA:word Num:i];
                    break;
                case  1:
                    //题型B
                    [self QuestionB:word Num:i];
                    break;
                case  2:
                    //题型D
                    [self QuestionD:word Num:i];
                    break;
            }
        }else if([word.abc compare:@"134"]==NSOrderedSame){
            switch (value) {
                case  0:
                    //题型A
                    [self QuestionA:word Num:i];
                    break;
                case  1:
                    //题型C
                    [self QuestionC:word Num:i];
                    break;
                case  2:
                    //题型D
                    [self QuestionD:word Num:i];
                    break;
            }
        }else if([word.abc compare:@"234"]==NSOrderedSame){
            switch (value) {
                case  0:
                    //题型B
                    [self QuestionB:word Num:i];
                    break;
                case  1:
                    //题型C
                    [self QuestionC:word Num:i];
                    break;
                case  2:
                    //题型D
                    [self QuestionD:word Num:i];
                    break;
            }
        }else if((([word.abc compare:@"123"]==NSOrderedSame)) && worday !=0){
            switch (value) {
                case  0:
                    //题型A
                    [self QuestionA:word Num:i];
                    break;
                case  1:
                    //题型B
                    [self QuestionB:word Num:i];
                    break;
                case  2:
                    //题型C
                    [self QuestionC:word Num:i];
                    break;
            }
        }else if([word.abc compare:@"14"]==NSOrderedSame){
            switch (value2) {
                case 0:
                    //题型A
                    [self QuestionA:word Num:i];
                    break;
                    
                case 1:
                    //题型D
                    [self QuestionD:word Num:i];
                    
                    break;
            }
        }else if([word.abc compare:@"13"]==NSOrderedSame){
            switch (value2) {
                case 0:
                    //题型A
                    [self QuestionA:word Num:i];
                    break;
                    
                case 1:
                    //题型C
                    [self QuestionC:word Num:i];
                    
                    break;
            }
        }else if([word.abc compare:@"24"]==NSOrderedSame){
            switch (value2) {
                case 0:
                    //题型B
                    [self QuestionB:word Num:i];
                    break;
                    
                case 1:
                    //题型D
                    [self QuestionD:word Num:i];
                    
                    break;
            }
        }else if([word.abc compare:@"34"]==NSOrderedSame){
            switch (value2) {
                case 0:
                    //题型C
                    [self QuestionC:word Num:i];
                    break;
                    
                case 1:
                    //题型D
                    [self QuestionD:word Num:i];
                    break;
            }
        }else if(([word.abc compare:@"12"]==NSOrderedSame)){
            switch (value2) {
                case 0:
                    //题型A
                    [self QuestionA:word Num:i];
                    break;
                    
                case 1:
                    //题型B
                    [self QuestionB:word Num:i];
                    break;
            }
        }else if(([word.abc compare:@"23"]==NSOrderedSame)){
            switch (value2) {
                case  0:
                    //题型B
                    [self QuestionB:word Num:i];
                    break;
                case  1:
                    //题型C
                    [self QuestionC:word Num:i];
                    break;
            }
        }else if([word.abc compare:@"4"]==NSOrderedSame){
            //题型D
            [self QuestionD:word Num:i];
        }else if([word.abc compare:@"3"]==NSOrderedSame){
            //题型C
            [self QuestionC:word Num:i];
        }else if([word.abc compare:@"2"]==NSOrderedSame){
            //题型B
            [self QuestionB:word Num:i];
        }else if([word.abc compare:@"1"]==NSOrderedSame){
            //题型A
            [self QuestionA:word Num:i];
        }
    }
    
    viewNum = [viewArray count];//设置界面的个数标签
    
    // 如果数组大小为0就不添加滑动监听事件
    if ([viewArray count]!=0) {
        if (dwNum >= [viewArray count]-1) {
            dwNum = [viewArray count]-1;
        }
        [self.view bringSubviewToFront:[viewArray objectAtIndex:dwNum]];//将当前view放在最上面
        
        //添加手势滑动的事件
        UISwipeGestureRecognizer *recognizer;
        recognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handleSwipeFrom:)];
        [recognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
        [[self view] addGestureRecognizer:recognizer];
        [recognizer release];
        
        recognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handleSwipeFrom:)];
        [recognizer setDirection:(UISwipeGestureRecognizerDirectionLeft)];
        [[self view] addGestureRecognizer:recognizer];
        [recognizer release];
        
        //将当前的单词数组保存下来
        NSArray * myarray = [[NSArray alloc] initWithArray:wordsArray];
        NSData *udObject = [NSKeyedArchiver archivedDataWithRootObject:myarray];
        [userDefaultes setObject:udObject forKey:@"todayArray"];
        [myarray release];
    }
    
    // 如果是背诵错误单词，将单词位置标签设置为0
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"useWrongWords"]) {
        dwNum = 0;
    }
    
    // 初始化进度条
    [processing removeFromSuperview];
    if (viewNum != 0){
        // 进度条图片
        double x1 = (942.00/viewNum)*(dwNum);
        processing = [[UIImageView alloc] initWithFrame:CGRectMake(62,705,x1,23)];
        processing.image = [UIImage imageNamed:@"process"];//加载入图片
        [self.view addSubview:processing];
        
        // 百分比文字
        [processText removeFromSuperview];
        processText = [[UILabel alloc]initWithFrame:CGRectMake(150, 701, 150, 28)];
        NSString *text2 = [NSString stringWithFormat:@"已完成 %d%@",((dwNum+1)*100)/viewNum,@"%"];
        processText.text = text2;
        processText.backgroundColor = [UIColor clearColor]; //可以去掉背景色
        processText.textAlignment = UITextAlignmentLeft;
        processText.font = [UIFont fontWithName:@"Helvetica" size:15];
        processText.textColor = [UIColor blackColor];
        [self.view addSubview:processText];
    }
    
    //播放音频
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"autoplay"] && [[NSUserDefaults standardUserDefaults] boolForKey:@"sound"]) {
        [self addAudio];
        [avAudioPlayer play];
    }
}

-(UILabel*)AddDictionaryLable:(Word*) word1{
    UILabel *Word_dicName = [[UILabel alloc]initWithFrame:CGRectMake(760.0, 580.0, 200.0, 30.0)];
    Word_dicName.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    Word_dicName.textAlignment = UITextAlignmentCenter;
    Word_dicName.adjustsFontSizeToFitWidth = YES;
    Word_dicName.font = [UIFont fontWithName:@"Helvetica" size:15];
    Word_dicName.textColor = [UIColor grayColor];
    Word_dicName.text = word1.word_dictionary;
    return Word_dicName;
}

-(UILabel*)AddDictionaryLable2:(Word*) word1{
    UILabel *Word_dicName = [[UILabel alloc]initWithFrame:CGRectMake(760.0, 520.0, 200.0, 30.0)];
    Word_dicName.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    Word_dicName.textAlignment = UITextAlignmentCenter;
    Word_dicName.adjustsFontSizeToFitWidth = YES;
    Word_dicName.font = [UIFont fontWithName:@"Helvetica" size:15];
    Word_dicName.textColor = [UIColor grayColor];
    Word_dicName.text = word1.word_dictionary;
    return Word_dicName;
}

// 答案解析文字
-(UILabel*)analyzeText:(Word*)word{
    UILabel *analyze = [[UILabel alloc]initWithFrame:CGRectMake(67.0, 515.0, 100.0, 50.0)];
    analyze.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    analyze.textAlignment = UITextAlignmentCenter;
    analyze.font = [UIFont fontWithName:@"Helvetica" size:20];
    analyze.textColor = [UIColor blueColor];
    analyze.adjustsFontSizeToFitWidth = YES;
    analyze.text = @"答案解析";
    analyze.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(TextClick2:)];
    [analyze addGestureRecognizer:singleTap];
    UIView *singleTapView = [singleTap view];
    singleTapView.tag = (int)word;
    [singleTap release];
    // 字段解析释放为空
    NSLog(@"%@",word.word_analyze);
    if (word.word_analyze.length == 0) {
        [analyze setHidden:YES];
    }
    return analyze;
}

// 答案解析文字
-(UILabel*)analyzeTextBC:(Word*)word{
    UILabel *analyze = [[UILabel alloc]initWithFrame:CGRectMake(67.0, 515.0, 100.0, 50.0)];
    analyze.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    analyze.textAlignment = UITextAlignmentCenter;
    analyze.font = [UIFont fontWithName:@"Helvetica" size:20];
    analyze.textColor = [UIColor blueColor];
    analyze.adjustsFontSizeToFitWidth = YES;
    analyze.text = @"答案解析";
    analyze.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(TextClick2:)];
    [analyze addGestureRecognizer:singleTap];
    UIView *singleTapView = [singleTap view];
    singleTapView.tag = (int)word;
    [singleTap release];
    // 字段解析释放为空
//    if (word.word_analyze.length == 0) {
        [analyze setHidden:YES];
//    }
    return analyze;
}

// 文字监听响应事件
-(void)TextClick2:(id)sender{
    UITapGestureRecognizer *singleTap = (UITapGestureRecognizer *)sender;
    Word *new_word = (Word*)[singleTap view].tag;
    [self analyze:new_word.word_analyze];
    NSLog(@"dddd");
}

// 答案解析对话框
-(void)analyze:(NSString*)analyze_text{
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"答案解析" message:analyze_text delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
    alert.tag = 0;
    [alert show];
    [alert release];
}

//-----------初始化A型题view
-(void)QuestionA:(Word *)word Num:(int) i{
    UIView *dwView=[[UIView alloc] initWithFrame:CGRectMake(20, 50, 975.0, 620)];
    UIImage*img =[UIImage imageNamed:@"remember_words_background"];
    [dwView setBackgroundColor:[UIColor colorWithPatternImage:img]];
    dwView.tag = (int)word;
    
    UILabel *label1 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 160.0, 950.0, 170.0)];
    label1.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label1.textAlignment = UITextAlignmentCenter;
    label1.font = [UIFont fontWithName:@"Helvetica" size:80];
    label1.textColor = [UIColor blackColor];
    label1.adjustsFontSizeToFitWidth = YES;
    NSString *wordString = [word.word_word stringByReplacingOccurrencesOfString:@"@$" withString:@"\n"];
    label1.text = wordString;
    
    UILabel *label2 = [[UILabel alloc]initWithFrame:CGRectMake(100.0, 330.0, 800.0, 60.0)];
    label2.adjustsFontSizeToFitWidth = YES;
    if (![word.word_word_class compare:@""]==NSOrderedSame) {
        NSString *text = [NSString stringWithFormat:@"%@.",word.word_word_class];
        label2.text = text;
    }
    label2.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label2.textAlignment = UITextAlignmentCenter;
    label2.font = [UIFont fontWithName:@"Helvetica" size:40];
    label2.textColor = [UIColor blackColor];
    
    UIImage *image2 = [self loadImage:word.word_d_picture1];
    
    int width1 = image2.size.height;
    if (width1 > 300) {
        width1 = 300;
    }
    int height1 = width1*(image2.size.height/image2.size.width);
    UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(920-width1,570-height1,width1,height1)];
    [imageView2 initWithImage:image2];
    imageView2.tag = (int)image2;
    imageView2.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(LargeImage:)];
    [imageView2 addGestureRecognizer:singleTap];
    [singleTap release];
    if (image2) {
        [dwView bringSubviewToFront:imageView2];
        [dwView setUserInteractionEnabled:YES];
        [dwView addSubview:imageView2];
    }
    
    [dwView addSubview:[self AddDictionaryLable:word]];
    [dwView addSubview:label1];
    [dwView addSubview:label2];
    [viewArray addObject:dwView];
    [isAnsArray addObject:@"1"];
    [self.view addSubview:dwView];
    
    [label1 release];
    [label2 release];
    [dwView release];
    
    //体型A的答案界面
    UIView *dwView2=[[UIView alloc] initWithFrame:CGRectMake(20, 50, 975.0, 620)];
    UIImage*img2 =[UIImage imageNamed:@"remember_words_background"];
    [dwView2 setBackgroundColor:[UIColor colorWithPatternImage:img2]];
    dwView2.tag = (int)word;
    
    UILabel *label3 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 100, 950.0, 300)];
    label3.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label3.adjustsFontSizeToFitWidth = YES;
    label3.textAlignment = UITextAlignmentCenter;
    label3.font = [UIFont fontWithName:@"Helvetica" size:40];
    label3.textColor = [UIColor blackColor];
    label3.lineBreakMode = UILineBreakModeWordWrap;
    label3.numberOfLines = 0;
    NSLog(@"1---%@",word.word_a_explan1);
    NSString *word2String = [word.word_a_explan1 stringByReplacingOccurrencesOfString:@"@$" withString:@"\n"];
    NSLog(@"2---%@",word2String);
    label3.text = word2String;
    
    UILabel *label4 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 400, 950.0, 30.0)];
    label4.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label4.textAlignment = UITextAlignmentCenter;
    label4.adjustsFontSizeToFitWidth = YES;
    label4.font = [UIFont fontWithName:@"Helvetica" size:20];
    label4.textColor = [UIColor blackColor];
    NSArray *array2 = [word.word_statement componentsSeparatedByString:@"@$"]; //从字符A中分隔成2个元素的数组
    NSString *newString2 = [[NSString alloc]init];
    for (int i = 0; i < [array2 count]; i++) {
        newString2 = [NSString stringWithFormat:@"%@%@",newString2,[array2 objectAtIndex:i]];
    }
    label4.text = newString2;
    
    UIImage *image = [self loadImage:word.word_d_picture2];
    int width2 = image.size.height;
    if (width2 > 300) {
        width2 = 300;
    }
    int height2 = width2*(image.size.height/image.size.width);
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(12,550-height2,width2,height2)];
    [imageView initWithImage:image];
    imageView.tag = (int)image;
    imageView.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(LargeImage:)];
    [imageView addGestureRecognizer:singleTap2];
    [singleTap2 release];
    [dwView2 bringSubviewToFront:imageView];
    [dwView2 setUserInteractionEnabled:YES];
    
    UIButton *Btn1=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    Btn1.frame=CGRectMake(10,565,200,43);
    UIImage*btnImage =[UIImage imageNamed:@"memory_easy"];
    [Btn1 setBackgroundImage:btnImage forState:UIControlStateNormal];
    Btn1.tag=(int)word;
    [Btn1 addTarget:self action:@selector(rightAnsware:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *Btn2=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    Btn2.frame=CGRectMake(762,565,200,43);
    Btn2.tag=(int)word;
    UIImage*btnImage2 =[UIImage imageNamed:@"memory_i_know"];
    [Btn2 setBackgroundImage:btnImage2 forState:UIControlStateNormal];
    [Btn2 addTarget:self action:@selector(wrongAnsware:) forControlEvents:UIControlEventTouchUpInside];
    
    [dwView2 addSubview:label3];
    [dwView2 addSubview:label4];
    if (image) {
        [dwView2 addSubview:imageView];
    }
    
    [dwView2 addSubview:Btn1];
    [dwView2 addSubview:Btn2];
    [dwView2 addSubview:[self AddDictionaryLable2:word]];
    
    [dwView2 addSubview:[self analyzeText:word]];
    
    [self.view bringSubviewToFront:dwView2];
    [self.view setUserInteractionEnabled:YES];
    [viewArray addObject:dwView2];
    [isAnsArray addObject:@"0"];
    [self.view addSubview:dwView2];
    
    [label3 release];
    [label4 release];
    [Btn1 release];
    [Btn2 release];
    [dwView2 release];
}

//-----------初始化B型题view
-(void)QuestionB:(Word *)word Num:(int) i{
    UIView *dwView=[[UIView alloc] initWithFrame:CGRectMake(20, 50, 975.0, 620)];
    UIImage*img =[UIImage imageNamed:@"remember_words_background"];
    [dwView setBackgroundColor:[UIColor colorWithPatternImage:img]];
    dwView.tag = (int)word;
    
    int question;//体型随机处理
    if (![word.word_b_explane compare:@""]==NSOrderedSame) {
        question = arc4random() % 5;
    }else {
        question = arc4random() % 4;
    }
    NSString *intString = [NSString stringWithFormat:@"%d",question];
    NSArray *list;
    switch (question) {
        case 0:
            list = [NSArray arrayWithObjects:word.word_b_explana,word.word_b_explanb,word.word_b_explanc,word.word_b_expland,word.word_b_explane, nil];
            break;
        case 1:
            list = [NSArray arrayWithObjects:word.word_b_explanb,word.word_b_explana,word.word_b_explanc,word.word_b_expland,word.word_b_explane, nil];
            break;
        case 2:
            list = [NSArray arrayWithObjects:word.word_b_explanc,word.word_b_explanb,word.word_b_explana,word.word_b_expland,word.word_b_explane, nil];
            break;
        case 3:
            list = [NSArray arrayWithObjects:word.word_b_expland,word.word_b_explanb,word.word_b_explanc,word.word_b_explana,word.word_b_explane, nil];
            break;
        case 4:
            list = [NSArray arrayWithObjects:word.word_b_explane,word.word_b_explanb,word.word_b_explanc, word.word_b_expland,word.word_b_explana,nil];
            break;
    }
    
    UILabel *label1 = [[UILabel alloc]initWithFrame:CGRectMake(74.0, 20.0, 800.0, 70.0)];
    label1.font = [UIFont fontWithName:@"Helvetica" size:50];
    NSString *text = [word.word_word stringByReplacingOccurrencesOfString:@"@$" withString:@"\n"];
    if (text.length > 100) {
        label1.lineBreakMode = UILineBreakModeWordWrap;
        label1.numberOfLines = 0;
        label1.font = [UIFont fontWithName:@"Helvetica" size:20];
    }
    label1.text = text;
    label1.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label1.adjustsFontSizeToFitWidth = YES;
    label1.textAlignment = UITextAlignmentLeft;
    label1.textColor = [UIColor blackColor];
    
    UILabel *label2 = [[UILabel alloc]initWithFrame:CGRectMake(74.0, 90.0, 400.0, 65.0)];
    NSString *text_n = [NSString stringWithFormat:@"%@",word.word_word_class];
    label2.text = text_n;
    label2.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label2.textAlignment = UITextAlignmentLeft;
    label2.font = [UIFont fontWithName:@"Helvetica" size:40];
    label2.adjustsFontSizeToFitWidth = YES;
    label2.textColor = [UIColor blackColor];
    
    UIImage *image2 = [self loadImage:word.word_pircture1];
    int width = 160*(image2.size.width/image2.size.height);
    UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(670,20,width,160)];
    [imageView2 initWithImage:image2];
    imageView2.tag = (int)image2;
    imageView2.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(LargeImage:)];
    [imageView2 addGestureRecognizer:singleTap];
    [singleTap release];
    [dwView bringSubviewToFront:imageView2];
    [dwView setUserInteractionEnabled:YES];
    
    UIButton *Btn1=[UIButton buttonWithType:UIButtonTypeCustom];
    Btn1.frame=CGRectMake(50.0, 150.0, 880.0, 60.0);
    UIImage*btnImage =[UIImage imageNamed:@"memory_d_background"];
    [Btn1 setBackgroundImage:btnImage forState:UIControlStateNormal];
    NSString *text1 = [NSString stringWithFormat:@"  A. %@",[list objectAtIndex:0]];
    wrongWords *wrong = [[wrongWords alloc]init];
    wrong.randomTag = intString;//标记正确答案是第几个
    wrong.answare = [list objectAtIndex:0];//保存正确答案
    wrong.myword = word;
    Btn1.tag = (int)wrong;
    Btn1.titleLabel.font = [UIFont systemFontOfSize: 40];
    Btn1.titleLabel.adjustsFontSizeToFitWidth = YES;
    Btn1.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft ;
    [Btn1 setTitle:text1 forState:UIControlStateNormal];
    [Btn1 addTarget:self action:@selector(buttonClickB:) forControlEvents:UIControlEventTouchUpInside];
    [Btn1 setTitleColor:[UIColor colorWithRed:37.99/255 green:37.99/255 blue:41.05/255 alpha:1]forState:UIControlStateNormal];
    
    UIButton *Btn2=[UIButton buttonWithType:UIButtonTypeCustom];
    Btn2.frame=CGRectMake(50.0, 225.0, 880.0, 60.0);
    UIImage*btnImage2 =[UIImage imageNamed:@"memory_d_background"];
    [Btn2 setBackgroundImage:btnImage2 forState:UIControlStateNormal];
    NSString *text2 = [NSString stringWithFormat:@"  B. %@",[list objectAtIndex:1]];
    wrongWords *wrong2 = [[wrongWords alloc]init];
    wrong2.randomTag = intString;
    wrong2.answare = [list objectAtIndex:1];
    wrong2.myword = word;
    Btn2.tag = (int)wrong2;
    Btn2.titleLabel.font = [UIFont systemFontOfSize: 40];
    Btn2.titleLabel.adjustsFontSizeToFitWidth = YES;
    Btn2.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft ;
    [Btn2 setTitle:text2 forState:UIControlStateNormal];
    [Btn2 addTarget:self action:@selector(buttonClickB:) forControlEvents:UIControlEventTouchUpInside];
    [Btn2 setTitleColor:[UIColor colorWithRed:37.99/255 green:37.99/255 blue:41.05/255 alpha:1]forState:UIControlStateNormal];
    
    UIButton *Btn3=[UIButton buttonWithType:UIButtonTypeCustom];
    Btn3.frame=CGRectMake(50.0, 300.0, 880.0, 60.0);
    UIImage*btnImage3 =[UIImage imageNamed:@"memory_d_background"];
    [Btn3 setBackgroundImage:btnImage3 forState:UIControlStateNormal];
    NSString *text3 = [NSString stringWithFormat:@"  C. %@",[list objectAtIndex:2]];
    Btn3.titleLabel.font = [UIFont systemFontOfSize: 40];
    Btn3.titleLabel.adjustsFontSizeToFitWidth = YES;
    wrongWords *wrong3 = [[wrongWords alloc]init];
    wrong3.randomTag = intString;
    wrong3.answare = [list objectAtIndex:2];
    wrong3.myword = word;
    Btn3.tag = (int)wrong3;
    Btn3.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft ;
    [Btn3 setTitle:text3 forState:UIControlStateNormal];
    [Btn3 addTarget:self action:@selector(buttonClickB:) forControlEvents:UIControlEventTouchUpInside];
    [Btn3 setTitleColor:[UIColor colorWithRed:37.99/255 green:37.99/255 blue:41.05/255 alpha:1]forState:UIControlStateNormal];
    
    UIButton *Btn4=[UIButton buttonWithType:UIButtonTypeCustom];
    Btn4.frame=CGRectMake(50.0, 375.0, 880.0, 60.0);
    UIImage*btnImage4 =[UIImage imageNamed:@"memory_d_background"];
    [Btn4 setBackgroundImage:btnImage4 forState:UIControlStateNormal];
    NSString *text4 = [NSString stringWithFormat:@"  D. %@",[list objectAtIndex:3]];
    Btn4.titleLabel.font = [UIFont systemFontOfSize: 40];
    Btn4.titleLabel.adjustsFontSizeToFitWidth = YES;
    wrongWords *wrong4 = [[wrongWords alloc]init];
    wrong4.randomTag = intString;
    wrong4.answare = [list objectAtIndex:3];
    wrong4.myword = word;
    Btn4.tag = (int)wrong4;
    Btn4.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft ;
    [Btn4 setTitle:text4 forState:UIControlStateNormal];
    [Btn4 addTarget:self action:@selector(buttonClickB:) forControlEvents:UIControlEventTouchUpInside];
    [Btn4 setTitleColor:[UIColor colorWithRed:37.99/255 green:37.99/255 blue:41.05/255 alpha:1]forState:UIControlStateNormal];
    
    
    UIButton *Btne=[UIButton buttonWithType:UIButtonTypeCustom];
    Btne.frame=CGRectMake(50.0, 450.0, 880.0, 60.0);
    UIImage*btnImagee =[UIImage imageNamed:@"memory_d_background"];
    [Btne setBackgroundImage:btnImagee forState:UIControlStateNormal];
    NSString *texte = [NSString stringWithFormat:@"  E. %@",[list objectAtIndex:4]];
    Btne.titleLabel.font = [UIFont systemFontOfSize: 40];
    Btne.titleLabel.adjustsFontSizeToFitWidth = YES;
    wrongWords *wronge = [[wrongWords alloc]init];
    wronge.randomTag = intString;
    wronge.answare = [list objectAtIndex:4];
    wronge.myword = word;
    Btne.tag = (int)wronge;
    Btne.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft ;
    [Btne setTitle:texte forState:UIControlStateNormal];
    [Btne addTarget:self action:@selector(buttonClickB:) forControlEvents:UIControlEventTouchUpInside];
    [Btne setTitleColor:[UIColor colorWithRed:37.99/255 green:37.99/255 blue:41.05/255 alpha:1]forState:UIControlStateNormal];
    
    UIButton *Btn5=[UIButton buttonWithType:UIButtonTypeCustom];
    Btn5.frame=CGRectMake(730,560,200,43);
    
    wrongWords *wrong5 = [[wrongWords alloc]init];
    wrong5.randomTag = intString;//标记正确答案是第几个
    wrong5.answare = [list objectAtIndex:question];//保存正确答案
    wrong5.myword = word;
    Btn5.tag = (int)wrong5;
    UIImage*btnImage5 =[UIImage imageNamed:@"memory_i_do_not_know"];
    [Btn5 setBackgroundImage:btnImage5 forState:UIControlStateNormal];
    [Btn5 addTarget:self action:@selector(buttonClickBWrong:) forControlEvents:UIControlEventTouchUpInside];
    
    [dwView addSubview:label1];
    [dwView addSubview:label2];
    [dwView addSubview:Btn1];
    [dwView addSubview:Btn2];
    [dwView addSubview:Btn3];
    [dwView addSubview:Btn4];
    if (![word.word_b_explane compare:@""]==NSOrderedSame) {
     [dwView addSubview:Btne];
//        [Btne release];
    }
    if (image2) {
        [dwView addSubview:imageView2];
    }
    [dwView addSubview:[self analyzeTextBC:word]];
    [dwView addSubview:Btn5];
    [dwView addSubview:[self AddDictionaryLable2:word]];
    [viewArray addObject:dwView];
    [isAnsArray addObject:@"0"];
    [self.view addSubview:dwView];
}

//-----------初始化C型题view
-(void)QuestionC:(Word *)word Num:(int) i{
    UIView *dwView=[[UIView alloc] initWithFrame:CGRectMake(20, 50, 975.0, 620)];
    UIImage*img =[UIImage imageNamed:@"remember_words_background"];
    [dwView setBackgroundColor:[UIColor colorWithPatternImage:img]];
    dwView.tag = (int)word;
    
    UILabel *label1 = [[UILabel alloc]initWithFrame:CGRectMake(52.0, 150.0, 900.0, 70.0)];
    NSString *text = [NSString stringWithFormat:@"%@",word.word_c_explan];
    label1.text = text;
    label1.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label1.textAlignment = UITextAlignmentCenter;
    label1.font = [UIFont fontWithName:@"Helvetica" size:50];
    label1.textColor = [UIColor blackColor];
    
    UILabel *label2 = [[UILabel alloc]initWithFrame:CGRectMake(52.0, 220.0, 900.0, 50.0)];
    NSString *text2 = [NSString stringWithFormat:@"%@",word.word_word_class];
    label2.text = text2;
    label2.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label2.textAlignment = UITextAlignmentCenter;
    label2.font = [UIFont fontWithName:@"Helvetica" size:30];
    label2.textColor = [UIColor blackColor];
    
    UITextField *textFiled = [[UITextField alloc] initWithFrame:CGRectMake(350, 300, 280.0f, 40)];
    [textFiled setBorderStyle:UITextBorderStyleRoundedRect]; //外框类型
    textFiled.font = [UIFont fontWithName:@"helvetica" size:20];
    textFiled.placeholder = @"请输入答案";
    textFiled.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter; 
    textFiled.textColor = [UIColor blackColor];
    textFiled.tag = (int)word;
    textFiled.delegate=self;
    [textFiled addTarget:self action:@selector(textFieldEnd:) forControlEvents:UIControlEventEditingDidEnd];
    textFiled.autocapitalizationType = UITextAutocapitalizationTypeNone;
    
    UILabel *label3 = [[UILabel alloc]initWithFrame:CGRectMake(640, 300, 280.0f, 34)];
    label3.text = @"";
    label3.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label3.font = [UIFont fontWithName:@"Helvetica" size:30];
    label3.textColor = [UIColor blackColor];

    UIImage *image2 = [self loadImage:word.word_d_picture1];//加载入图片
    int width = 160*(image2.size.width/image2.size.height);
    UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(360,350,width,160)];
    [imageView2 initWithImage:image2];
    imageView2.tag = (int)image2;
    imageView2.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(LargeImage:)];
    [imageView2 addGestureRecognizer:singleTap];
    [singleTap release];
    [dwView bringSubviewToFront:imageView2];
    [dwView setUserInteractionEnabled:YES];
    
    UIButton *Btn1=[UIButton buttonWithType:UIButtonTypeCustom];
    Btn1.frame=CGRectMake(750,550,200,43);
    UIImage *btnImage5 =[UIImage imageNamed:@"memory_c_button"];
    [Btn1 setBackgroundImage:btnImage5 forState:UIControlStateNormal];
    QuestionC *qc = [[QuestionC alloc]init];
    qc.answareC = textFiled.text;
    qc.wordC = word;
    Btn1.tag=(int)qc;
    //[Btn1 setTitle:[titleArray objectAtIndex:i] forState:UIControlStateNormal];
    [Btn1 addTarget:self action:@selector(buttonClickC:) forControlEvents:UIControlEventTouchUpInside];
    
    [dwView addSubview:label1];
    [dwView addSubview:label2];
    [dwView addSubview:label3];
    [dwView addSubview:textFiled];
    if (image2) {
        [dwView addSubview:imageView2];
    }
    [dwView addSubview:[self analyzeTextBC:word]];
    [dwView addSubview:Btn1];
    [dwView addSubview:[self AddDictionaryLable2:word]];
    [viewArray addObject:dwView];
    [isAnsArray addObject:@"0"];
    [self.view addSubview:dwView];
    
//    [label1 release];
//    [label2 release];
//    [label3 release];
//    [textFiled release];
//    [image2 release];
//    [dwView release];

}

//-----------初始化D型题view
-(void)QuestionD:(Word *)word Num:(int) i{
    UIView *dwView=[[UIView alloc] initWithFrame:CGRectMake(20, 50, 975.0, 620)];
    UIImage*img =[UIImage imageNamed:@"remember_words_background"];
    [dwView setBackgroundColor:[UIColor colorWithPatternImage:img]];
    dwView.tag = (int)word;
    UILabel *label1 = [[UILabel alloc]initWithFrame:CGRectMake(40.0, 40.0, 900.0, 100.0)];
    label1.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label1.textAlignment = UITextAlignmentLeft;
    label1.font = [UIFont fontWithName:@"Helvetica" size:30];
    label1.textColor = [UIColor blackColor];
    label1.adjustsFontSizeToFitWidth = YES;
    label1.lineBreakMode = UILineBreakModeWordWrap;
    label1.numberOfLines = 0;
    NSString *wordString = [word.word_word stringByReplacingOccurrencesOfString:@"@$" withString:@"\n"];
    label1.text = wordString;
    
    UILabel *label2 = [[UILabel alloc]initWithFrame:CGRectMake(40.0, 140.0, 800.0, 30.0)];
    label2.adjustsFontSizeToFitWidth = YES;
    if (![word.word_word_class compare:@""]==NSOrderedSame) {
        NSString *text = [NSString stringWithFormat:@"%@.",word.word_word_class];
        label2.text = text;
    }
    label2.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label2.textAlignment = UITextAlignmentLeft;
    label2.font = [UIFont fontWithName:@"Helvetica" size:20];
    label2.textColor = [UIColor blackColor];
    
    UIImage *image2 = [self loadImage:word.word_d_picture1];
    
    int layoutWith = image2.size.width;
    int layoutHeight = image2.size.height;
    
    if (layoutWith > 700) {
        layoutWith = 700;
        layoutHeight = layoutHeight*700/layoutWith;
    }else if(layoutHeight > 450){
        layoutHeight = 450;
        layoutWith = layoutWith*450/layoutHeight;
    }
    int layoutX = (975.0 - layoutWith)/2;
    int layoutY = (620 - layoutHeight)/2;
    
    UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(layoutX,layoutY,layoutWith,layoutHeight)];
    [imageView2 initWithImage:image2];
    imageView2.tag = (int)image2;
    imageView2.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(LargeImage:)];
    [imageView2 addGestureRecognizer:singleTap];
    [singleTap release];
    [dwView bringSubviewToFront:imageView2];
    [dwView setUserInteractionEnabled:YES];
    if (image2) {
        [dwView addSubview:imageView2];
    }
    [dwView addSubview:label1];
    [dwView addSubview:label2];
    [dwView addSubview:[self AddDictionaryLable:word]];
    [viewArray addObject:dwView];
    [isAnsArray addObject:@"1"];
    [self.view addSubview:dwView];
    
    [label1 release];
    [label2 release];
    [dwView release];
    
    //体型D的答案界面
    UIView *dwView2=[[UIView alloc] initWithFrame:CGRectMake(20, 50, 975.0, 620)];
    UIImage*img2 =[UIImage imageNamed:@"remember_words_background"];
    [dwView2 setBackgroundColor:[UIColor colorWithPatternImage:img2]];
    dwView2.tag = (int)word;
    
    UILabel *label3 = [[UILabel alloc]initWithFrame:CGRectMake(40.0, 40.0, 950.0, 120.0)];
    label3.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label3.textAlignment = UITextAlignmentLeft;
    label3.adjustsFontSizeToFitWidth = YES;
    label3.font = [UIFont fontWithName:@"Helvetica" size:30];
    label3.textColor = [UIColor blackColor];
    label3.lineBreakMode = UILineBreakModeWordWrap;
    label3.numberOfLines = 0;
    NSString *newString = [word.word_a_explan1 stringByReplacingOccurrencesOfString:@"@$" withString:@"\n"];
    label3.text = newString;
    
    UILabel *label4 = [[UILabel alloc]initWithFrame:CGRectMake(40.0, 160.0, 950.0, 30.0)];
    label4.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label4.textAlignment = UITextAlignmentLeft;
    label4.adjustsFontSizeToFitWidth = YES;
    label4.font = [UIFont fontWithName:@"Helvetica" size:20];
    label4.textColor = [UIColor blackColor];
    NSArray *array2 = [word.word_a_explan2 componentsSeparatedByString:@"@$"]; //从字符A中分隔成2个元素的数组
    NSString *newString2 = [[NSString alloc]init];
    for (int i = 0; i < [array2 count]; i++) {
        newString2 = [NSString stringWithFormat:@"%@%@",newString2,[array2 objectAtIndex:i]];
    }
    label4.text = newString2;
    
    UIImage *image = [self loadImage:word.word_d_picture2];
    int layoutWith2 = image.size.width;
    int layoutHeight2 = image.size.height;
    
    if (layoutWith2 > 700) {
        layoutWith2 = 700;
        layoutHeight2 = layoutHeight2*700/layoutWith2;
    }else if(layoutHeight2 > 450){
        layoutHeight2 = 450;
        layoutWith2 = layoutWith2*450/layoutHeight2;
    }
    int layoutX2 = (975.0 - layoutWith2)/2;
    int layoutY2 = (620 - layoutHeight2)/2;
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(layoutX2,layoutY2,layoutWith2,layoutHeight2)];
    [imageView initWithImage:image];
    imageView.tag = (int)image;
    imageView.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(LargeImage:)];
    [imageView addGestureRecognizer:singleTap2];
    [singleTap2 release];
    [dwView2 bringSubviewToFront:imageView];
    [dwView2 setUserInteractionEnabled:YES];

    
    UIButton *Btn1=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    Btn1.frame=CGRectMake(10,565,200,43);
    UIImage*btnImage =[UIImage imageNamed:@"memory_easy"];
    [Btn1 setBackgroundImage:btnImage forState:UIControlStateNormal];
    Btn1.tag=(int)word;
    [Btn1 addTarget:self action:@selector(rightAnsware:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *Btn2=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    Btn2.frame=CGRectMake(762,565,200,43);
    Btn2.tag=(int)word;
    UIImage*btnImage2 =[UIImage imageNamed:@"memory_i_know"];
    [Btn2 setBackgroundImage:btnImage2 forState:UIControlStateNormal];
    [Btn2 addTarget:self action:@selector(D_wrongAnsware:) forControlEvents:UIControlEventTouchUpInside];
    
    [dwView2 addSubview:label3];
    [dwView2 addSubview:label4];
    if (image) {
        [dwView2 addSubview:imageView];
    }
    [dwView2 addSubview:[self analyzeText:word]];
    [dwView2 addSubview:Btn1];
    [dwView2 addSubview:Btn2];
    [dwView2 addSubview:[self AddDictionaryLable2:word]];
    [self.view bringSubviewToFront:dwView2];
    [self.view setUserInteractionEnabled:YES];
    [viewArray addObject:dwView2];
    [isAnsArray addObject:@"0"];
    [self.view addSubview:dwView2];
    [label3 release];
    [label4 release];
//    [image release];
    [dwView2 release];
//    [Btn1 release];
//    [Btn2 release];
}

// 读取本地保存的图片方法封装
-(UIImage *) loadImage:(NSString *)filePathName {
    UIImage * result = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@", filePathName]];
    return result;
}

//得到当日需要背诵的新单词数组
-(NSMutableArray *) getNewWordArray{
    //获得用户信息接口
    tool = [[tools alloc]init];
    NSString *user_id = (NSString*)[tool readObkect:@"user_id"];
    
    NSString *getDicType = [[NSUserDefaults standardUserDefaults] stringForKey:@"noe-dic"];

    NSMutableArray *newWords;
    if ([getDicType compare:@"one-dic"]==NSOrderedSame) {
        
        return nil;
//        NSLog(@"----背诵的是单个的词库");
//        // 获得用户信息接口
//        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//        
//        // 获得词库数组中位置
//        NSString *dicNum = [userDefaults stringForKey:@"dicNum"];
//        int dicIndex = [dicNum intValue];
//        
//        NSMutableArray *dics = [tool readArray:@"doubleStruct"];
//        UserDictionary *OneDic = [[UserDictionary alloc]init];
//        int index = 0;
//        for (int i = 0; i < [dics count]; i++) {
//            secondList *list = [dics objectAtIndex:i];
//            NSMutableArray *array = list.secondLevel;
//            for (int j = 0; j < [array count]; j++) {
//                if (index == dicIndex) {
//                    OneDic = [array objectAtIndex:j];
////                    NSLog(@"i= %d,j=%d,%@",i,j,OneDic.dictionary_name);
//                    index++;
//                    break;
//                }
//                index++;
////                NSLog(@"xx-i= %d,j=%d,%@",i,j,OneDic.dictionary_name);
//            }
//        }
//
//        NSMutableArray *OneArray = [[NSMutableArray alloc]init];
//        [OneArray addObject:OneDic];
//        
//        // 活动词库每天的背诵量
//        //获得这个词库的日期和词汇量
//        NSString *days = [database getCompleteTime:OneDic];// 获得完成天数
//        NSString *dayWords = [self dayWords:OneDic finishDay:days];
//        
//        newWords = [database getNewWord:user_id wordCount:dayWords Dictionarys:OneArray];
//        [OneArray release];
//        NSLog(@"背诵词库名字为%@,词库数量是%@",OneDic.dictionary_name,dayWords);
//        [[NSUserDefaults standardUserDefaults] setObject:@"one" forKey:@"finished"];
//        [[NSUserDefaults standardUserDefaults] setObject:OneDic.dictionary_name forKey:@"now_use_dictionary"];
//        [OneDic release];
    }else{
        NSLog(@"----背诵的是所有词库");
        // ---------------------获得所有词库的背诵量的总和
        NSMutableArray *dics2 = [tool readArray:@"doubleStruct"];
        
        DataBase *database2 =[[DataBase alloc]init];
        int dayWordsCount = 0; //  统计要背诵的单词总数
        for (int i = 0; i < [dics2 count]; i++) {
            secondList *second = [dics2 objectAtIndex:i];
            NSMutableArray *array = second.secondLevel;
            for (int j=0; j < [array count]; j++) {
                UserDictionary *dic = [array objectAtIndex:j];
                
                NSString *AllWordscount = [database2 getDictionaryWordCount:dic];
                NSString *allDays = [database2 getCompleteTime:dic];
                
                if ([AllWordscount compare:nil] != NSOrderedSame && [allDays compare:nil] != NSOrderedSame) {
                    int intString = [AllWordscount intValue];
                    int intString2 = [allDays intValue];
                    if (intString2 <= 0 ) {
                        intString2 = 25;
                    }
                    int dicWords = 0;
                    if (intString2 > 14) {
                        dicWords = intString/(intString2-14);
                    }
                    dayWordsCount+=dicWords;
                }
            }
        }
        
        NSString *wordCount = [NSString stringWithFormat:@"%d",dayWordsCount];
        //----------------------------end
        
        // 如果获得的单词数量都为零，标记下次可以再次获取单词
        if ([wordCount compare:@"0"]==NSOrderedSame) {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"newUser"];
        }
        
        // 从数据库中获取相应地单词保存到数组
        newWords = [database getNewWord:user_id wordCount:wordCount];
        
        NSLog(@"背诵的单词总数是%@",wordCount);
        [[NSUserDefaults standardUserDefaults] setObject:@"all" forKey:@"finished"];
        
    }
    return newWords;
}

// 根据天数计算每天背诵词汇量
//-(NSString*)dayWords:(UserDictionary*) mydic finishDay: (NSString *)finish_day{
//    DataBase *wordCount =[[DataBase alloc]init];
//    NSString *AllWordscount = [wordCount getDictionaryWordCount:mydic];//得到词库所有单词数量
//    int intString = [AllWordscount intValue];
//    int finishday = [finish_day intValue];// 完成的天数
//    if (finishday == 0) {
//        return 0;
//    }else{
//        if (finishday <= 15) {
//            finishday = 15;
//        }
//        int day = intString/(finishday-14); // 日背诵量 = (总单词数量)/(计划完成的天数-14)
//        NSString *days = [NSString stringWithFormat:@"%d",day];
//        return days;
//    }
//    
//}

////得到当日需要复习的新单词数组
//-(NSMutableArray *) getOldWordArray{
//    //获得用户信息接口
//    NSString *user_id = (NSString*)[tool readObkect:@"user_id"];
//    
//    NSMutableArray *oldWords = [[NSMutableArray alloc]init];
//    oldWords = [database getOldWord:user_id];
//    NSLog(@"从所有词库中得到新单词");
//    return oldWords;
//}

// 音频播放事件
-(IBAction)playSound:(UIButton *)sender {
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"sound"]) {
        [avAudioPlayer play];
        NSLog(@"play audio");
    }
}

//返回到主页
- (IBAction)back_to_home:(UIButton *)sender {
    [database release];
    database = nil;
    [isAnsArray release];
    isAnsArray = nil;
    [tool release];
    tool = nil;
    [myView release];
    myView = nil;
    [bigImage release];
    bigImage = nil;
//    [wordsArray release];
    wordsArray = nil;
    [GoOver release];
    GoOver = nil;
//    [wrongWordArray release];
    wordsArray = nil;
    [processing release];
    processing = nil;
    [processText release];
    processText = nil;
    [avAudioPlayer release];
    avAudioPlayer = nil;
    [pubuser release];
    pubuser = nil;
    [user release];
    user = nil;
    [viewArray release];
    viewArray = nil;
//    HomePage *homepage = [[HomePage alloc] init];
//    [self presentModalViewController:homepage animated:NO];
//    [homepage release];
    
    [self dismissModalViewControllerAnimated:NO];
//    [self.navigationController popViewControllerAnimated:YES];
}

//手势响应事件
-(void)handleSwipeFrom:(UISwipeGestureRecognizer *)recognizer{
    if(recognizer.direction==UISwipeGestureRecognizerDirectionRight) {//向右滑动
        NSString *canTurn = [isAnsArray objectAtIndex:dwNum];
        if (![canTurn compare:@"1"]==NSOrderedSame) {
            if(dwNum<0){
                dwNum=0;
            }
            else if(dwNum!=0)
                dwNum--;
            //修改进度
            [processing removeFromSuperview];
            double x1 = (942.00/viewNum)*(dwNum);
            processing = [[UIImageView alloc] initWithFrame:CGRectMake(62,705,x1,23)];
            processing.image = [UIImage imageNamed:@"process"];//加载入图片
            [self.view addSubview:processing];
            [processText removeFromSuperview];
            processText = [[UILabel alloc]initWithFrame:CGRectMake(150, 701, 150, 28)];
            NSUserDefaults *userDefaultes = [NSUserDefaults standardUserDefaults];
            NSString *pricess = [userDefaultes stringForKey:@"dwNum"];
            int intString = [pricess intValue];
            NSString *text2 = [NSString stringWithFormat:@"已完成 %d%@",((intString+1)*100)/viewNum,@"%"];
            processText.text = text2;
            processText.backgroundColor = [UIColor clearColor]; //可以去掉背景色
            processText.textAlignment = UITextAlignmentLeft;
            processText.font = [UIFont fontWithName:@"Helvetica" size:15];
            processText.textColor = [UIColor blackColor];
            [self.view addSubview:processText];
            
            [UIView beginAnimations:nil context:nil];
            //持续时间
            [UIView setAnimationDuration:1.0];
            //在出动画的时候减缓速度
            [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
            //添加动画开始及结束的代理
            [UIView setAnimationDelegate:self];
            [UIView setAnimationWillStartSelector:@selector(begin)];
            [UIView setAnimationDidStopSelector:@selector(stopAni)];
            //动画效果
            [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:[viewArray objectAtIndex:dwNum] cache:YES];
            //View切换
            for (int i=0; i<viewNum; i++) {
                if(dwNum!=i){
                    [[viewArray objectAtIndex:i] removeFromSuperview];
                    
                }
                else
                    [self.view insertSubview:[viewArray objectAtIndex:i] atIndex:6];
            }
            
            [UIView commitAnimations];
        }
    }
    
    if(recognizer.direction==UISwipeGestureRecognizerDirectionLeft) {//向左滑动
        [self AnimationToLeft];
    }
}

//一轮背完后判断是继续背诵还是回到主页
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if ([alertView tag] == 0) {
        
    }else{
    if (buttonIndex == 0) {
        [userDefaults setObject:@"0" forKey:@"dwNum"];
        [userDefaults removeObjectForKey:@"todayArray"];
        [userDefaults removeObjectForKey:@"wrongWords"];
        [userDefaults removeObjectForKey:@"now_use_dictionary"];
        [userDefaults removeObjectForKey:@"finished"];
        [self dismissModalViewControllerAnimated:NO];
    }else{
        [userDefaults setObject:@"0" forKey:@"dwNum"];
        [userDefaults removeObjectForKey:@"todayArray"];
        [userDefaults removeObjectForKey:@"wrongWords"];
        [userDefaults removeObjectForKey:@"now_use_dictionary"];
        [userDefaults removeObjectForKey:@"finished"];
        remember_view *userCenter = [[[remember_view alloc]initWithNibName:@"remember_view" bundle:nil]autorelease];
        [self presentModalViewController:userCenter animated:NO];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isGoOver"];
    }
    
//    // 修改单个词库还是所有词库状态
//    NSString *OneOrAll = [userDefaults stringForKey:@"finished"];
//    if ([OneOrAll compare:@"one"]==NSOrderedSame) {
//        [[NSUserDefaults standardUserDefaults] setObject:@"all" forKey:@"finished"];
//    }else{
//        [[NSUserDefaults standardUserDefaults] setObject:@"one" forKey:@"finished"];
//    }
        
    }
}

//向左选择切换动画
-(void)AnimationToLeft{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
//    // ----测试数据
//    
//    UIView *testviews = [viewArray objectAtIndex:dwNum+1];
//    NSArray *testarray = [testviews subviews];
//    UILabel *testLable = [testarray objectAtIndex:0];
//    NSString *testword = testLable.text;
//    
//    //获得保存的单词数组
//    NSData *test_newArray = [userDefaults objectForKey:@"todayArray"];
//    if(test_newArray!=nil){
//        NSArray *getNewArray = [NSKeyedUnarchiver unarchiveObjectWithData:test_newArray];
//        wordsArray =  [NSMutableArray arrayWithArray:getNewArray];
//    }
//    
//    // 单词一共多少个
//    int wordConatinNum = 0;
//    for (int i = 0; i < [wordsArray count]; i++) {
//        Word *wrongWord = [wordsArray objectAtIndex:i];
//        if ([wrongWord.word_word compare:testword]==NSOrderedSame) {
//            wordConatinNum += 1;
//        }
//    }
//    
//    int nowConatinNum = 0;
//    for (int i = 0; i <= dwNum; i++) {
//        Word *wrongWord = [wordsArray objectAtIndex:i];
//        if ([wrongWord.word_word compare:testword]==NSOrderedSame) {
//            nowConatinNum += 1;
//        }
//    }
//    
//    NSData *arraydata = [[NSUserDefaults standardUserDefaults] objectForKey:@"wrongWordArray"];
//    NSArray *tempArray = [NSKeyedUnarchiver unarchiveObjectWithData:arraydata];
//    NSMutableArray *Array =  [NSMutableArray arrayWithArray:tempArray];
//    
//    int wrongNum = 0;
//    for (int i = 0; i < [Array count]; i++) {
//        Word *wrongWord = [Array objectAtIndex:i];
//        if ([wrongWord.word_word compare:testword]==NSOrderedSame) {
//            wrongNum += 1;
//        }
//    }
//    
//    NSString *show = [NSString stringWithFormat:@"该单词一共有%d个，现在已经出现%d个，错误数量%d个,单词是：%@,dwView =%d",wordConatinNum,nowConatinNum,wrongNum,testword,dwNum];
//    // ----测试数据, 该单词一共有?个，现在是第？个，错误？次。
    
    if (b_word && b) {
        //更新当日学习单词数组，并且保存
        [self updeateWordArray:b_word];
        //更新viewArray数组，同时更新viewNum
        [self updateWrongWordView:b_word];
        [b_word release];
        b = FALSE;
    }
    
    if (c_word && c) {
        //更新当日学习单词数组，并且保存
        [self updeateWordArray:c_word];
        //更新viewArray数组，同时更新viewNum
        [self updateWrongWordView:c_word];
        [c_word release];
        c = FALSE;
    }
    
    NSLog(@"%d",dwNum);
    
    NSString *canTurn = [isAnsArray objectAtIndex:dwNum];

    if ([canTurn compare:@"1"]==NSOrderedSame) {//如果背诵完了
        if (dwNum == viewNum-1) {
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"背诵提示提示" message:@"亲，你已经背诵完当日的单词计划，加油哦！" delegate:self cancelButtonTitle:@"休息一下"  otherButtonTitles:@"继续背诵新单词",nil];
            alert.tag = 1;
            [alert show];
            [alert release];
        }else if(dwNum>viewNum-1){
            dwNum=viewNum-1;
        }else if(dwNum!=viewNum-1)
            dwNum++;
        
        //修改进度条显示
        [processing removeFromSuperview];
        [processing removeFromSuperview];
        double x2 = (942.00/viewNum)*(dwNum+1);
        processing = [[UIImageView alloc] initWithFrame:CGRectMake(62,705,x2,23)];
        processing.image = [UIImage imageNamed:@"process"];//加载入图片
        [self.view addSubview:processing];
        
        [processText removeFromSuperview];
        processText = [[UILabel alloc]initWithFrame:CGRectMake(150, 701, 750, 28)];
        NSString *text2 = [NSString stringWithFormat:@"已完成 %d%@",((dwNum+1)*100)/viewNum,@"%"];
        //获得最高纪录，如果小于最高纪录就不变
        NSString *process = [userDefaults stringForKey:@"dwNum"];
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"useWrongWords"]) {
            
        }
        
        int intString = 0;
                    @try {
                intString = [process intValue];
            }
            @catch (NSException *exception) {
                NSLog(@"error 3");
            }
      
        
        if (intString < dwNum) {
            processText.text = text2;
            NSString *text = [NSString stringWithFormat:@"%d",dwNum];
            [userDefaults setObject:text forKey:@"dwNum"];
        }else if([[NSUserDefaults standardUserDefaults] boolForKey:@"useWrongWords"]){
            int j = ((dwNum+1)*100)/viewNum;
            NSString *text2 = [NSString stringWithFormat:@"已完成 %d%@",j,@"%"];
            processText.text = text2;
        }else{
            int i = ((intString+1)*100)/viewNum;
            if (i>100) {
                i = 100;
            }
            NSString *text3 = [NSString stringWithFormat:@"已完成 %d%@",i,@"%"];
            processText.text = text3;
        }
        
        processText.backgroundColor = [UIColor clearColor]; //可以去掉背景色
        processText.textAlignment = UITextAlignmentLeft;
        processText.font = [UIFont fontWithName:@"Helvetica" size:15];
        processText.textColor = [UIColor blackColor];
        [self.view addSubview:processText];
        
        
        //切换动画
        [UIView beginAnimations:nil context:nil];
        //持续时间
        [UIView setAnimationDuration:1.0];
        //在出动画的时候减缓速度
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        //添加动画开始及结束的代理
        [UIView setAnimationDelegate:self];
        [UIView setAnimationWillStartSelector:@selector(begin)];
        [UIView setAnimationDidStopSelector:@selector(stopAni)];
        //动画效果
        [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:[viewArray objectAtIndex:dwNum] cache:YES];
        //View切换
        NSLog(@"r--> dwNum = %d, array = %d, viewNum = %d",dwNum,[viewArray count],viewNum);
        for (int j = 0; j < [viewArray count]; j++) {
            NSLog(@"log%d---%@",j,[viewArray objectAtIndex:j]);
        }
        for (int i=0; i<[viewArray count]; i++) {
            if(dwNum!=i){
                NSLog(@"----%d",i);
                [[viewArray objectAtIndex:i] removeFromSuperview];
            } else{
                NSLog(@"2----%d",[[self.view subviews] count]);
                [self.view insertSubview:[viewArray objectAtIndex:i] atIndex:10];
            }
        }
        [UIView commitAnimations];
    }
    
     [self addAudio];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"autoplay"] && [[NSUserDefaults standardUserDefaults] boolForKey:@"sound"]) {
       
        [avAudioPlayer play];
    }
    
    NSData *newArray = [userDefaults objectForKey:@"todayArray"];
    if(newArray!=nil){//如果存在当日没有背完的单词就背诵当天的单词
        NSArray *getNewArray = [NSKeyedUnarchiver unarchiveObjectWithData:newArray];
        wordsArray =  [NSMutableArray arrayWithArray:getNewArray];
        NSLog(@"继续背诵当日没有背诵完的单词！");
    }
    
    int notification = [wordsArray count] - dwNum;
    
    NSLog(@"00@----> %d; dwNum: %d",notification,dwNum);
    if (notification !=0) {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"notification"]) {
            [[UIApplication sharedApplication] setApplicationIconBadgeNumber:notification];
        }else{
            [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
        }
    }
    
}

//添加音频文件
-(void)addAudio{
    if (avAudioPlayer) {
        [avAudioPlayer release];
    }
    if (viewArray !=nil && [viewArray count] != 0) {
        //初始化单词音频文件
        UIView *view = [viewArray objectAtIndex:dwNum];
        Word *word = (Word*) [view tag];
        
        NSArray *viewType = [view subviews];
        NSLog(@"这个界面子控件的个数 是 - %d",[viewType count]);
        
        // 判断是英音还是美音
        
        // 判读文件是否存在
        NSFileManager *fileManager = [NSFileManager defaultManager];
        BOOL isFileExist = [fileManager fileExistsAtPath:word.word_us_voice2];

        NSString *text2;
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"sound"]) {
            // 判断是否是a形题和d型题的第二面
            if ([viewType count] == 5) {
                
                if (!isFileExist) {
                    text2 = [NSString stringWithFormat:@"%@",word.word_us_voice1];
                }else{
                    text2 = [NSString stringWithFormat:@"%@",word.word_us_voice2];
                }
            }else{
                text2 = [NSString stringWithFormat:@"%@",word.word_us_voice1];
            }
        }else{
            // 判断是否是a形题和d型题的第二面
            if ([viewType count] == 5) {
                if (!isFileExist) {
                    text2 = [NSString stringWithFormat:@"%@",word.word_uk_voice1];
                }else{
                    text2 = [NSString stringWithFormat:@"%@",word.word_uk_voice2];
                }
            }else{
                text2 = [NSString stringWithFormat:@"%@",word.word_uk_voice1];
            }
        }
        
        //把音频文件转换成url格式
        NSURL *url = [NSURL fileURLWithPath:text2];
        //初始化音频类 并且添加播放文件
        NSError *avPlayerError = nil;
        avAudioPlayer = [[[AVAudioPlayer alloc]init] initWithContentsOfURL:url error:&avPlayerError];
        if (avPlayerError)
        {
            NSLog(@"Error: %@", [avPlayerError description]);
        }
        //设置代理
        avAudioPlayer.delegate = self;
        //设置初始音量大小
        avAudioPlayer.volume = 1;
        //设置音乐播放次数  -1为一直循环
        avAudioPlayer.numberOfLoops = 0;
        //预播放
        [avAudioPlayer prepareToPlay];

    }
    }

//错误单词插入到当日背诵的单词数组中
-(void)updeateWordArray:(Word *)word{
    //给单词数组插入错误的单词
    wordsArray = [[NSMutableArray alloc]init];
    //获得保存的单词数组
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSData *newArray = [userDefaults objectForKey:@"todayArray"];
    if(newArray!=nil){
        NSArray *getNewArray = [NSKeyedUnarchiver unarchiveObjectWithData:newArray];
        wordsArray =  [NSMutableArray arrayWithArray:getNewArray];
    
        // 得到单词数组中单词的重复次数
        int wordConatinNum = 0;
        for (int i = dwNum; i < [wordsArray count]; i++) {
            Word *wrongWord = [wordsArray objectAtIndex:i];
            if ([wrongWord.word_word compare:word.word_word]==NSOrderedSame) {
                wordConatinNum += 1;
            }
        }
    NSLog(@"1--&&***这个单词在后面出现次数为----> %d",wordConatinNum);
        
        if (wordConatinNum <= 3) {
            int recordId =0;    //获得并记录单词在数组中的位置
            for (int i =0; i < [wordsArray count]; i++) {
                Word *myword = [wordsArray objectAtIndex:i];
                if ([myword.word_id compare:word.word_id]==NSOrderedSame) {
                    recordId =i;
                }
            }
            if (([wordsArray count]-recordId)>=10) {
                [wordsArray insertObject:word atIndex:recordId+5];
                [wordsArray insertObject:word atIndex:recordId+10];
            }else if(([wordsArray count]-recordId)>=5 && ([wordsArray count]-recordId)<10){
                [wordsArray insertObject:word atIndex:recordId+5];
                [wordsArray insertObject:word atIndex:[wordsArray count]];
            }else{
                [wordsArray insertObject:word atIndex:[wordsArray count]];
                [wordsArray insertObject:word atIndex:[wordsArray count]];
            }
            //将当前的单词数组保存下来
            [tool saveArray:wordsArray saveKey:@"todayArray"];
            //-------------保存当天错误单词到数组------
            wrongWordArray = [[NSMutableArray alloc]init];
            //得到旧的数组----------
            wrongWordArray = [tool readArray:@"wrongWordArray"];
            
            //添加新的错误单词到数组
            [wrongWordArray insertObject:word atIndex:[wrongWordArray count]];
            NSLog(@"错误的单词：%d",[wrongWordArray count]);
            //保存新的数组
            [tool saveArray:wrongWordArray saveKey:@"wrongWordArray"];
            //--------------------------------------
        }
    }
}

//更新viewArray数组
-(void)updateWrongWordView: (Word *) word{
    int value = arc4random() % 3;//生成题型的随机数
    int value2 = arc4random() % 2;//单词随机题型
    int value3 = arc4random() % 4;//生成题型的随机数
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSData *newArray = [userDefaults objectForKey:@"todayArray"];
        if(newArray!=nil){
            NSArray *getNewArray = [NSKeyedUnarchiver unarchiveObjectWithData:newArray];
            wordsArray =  [NSMutableArray arrayWithArray:getNewArray];
        }
        
        // 得到单词数组中单词的重复次数
        int wordViewNum = 0;
        for (int i = dwNum; i < [wordsArray count]; i++) {
            Word *wrongWord = [wordsArray objectAtIndex:i];
            if ([wrongWord.word_word compare:word.word_word]==NSOrderedSame) {
                wordViewNum += 1;
            }
        }
        
        NSLog(@"2---&&***这个单词在后面出现次数为----> %d",wordViewNum);
        if (wordViewNum <= 4 ) {
            //得到旧的数组----------
            NSData *arraydata = [[NSUserDefaults standardUserDefaults] objectForKey:@"wrongWordArray"];
            NSArray *tempArray = [NSKeyedUnarchiver unarchiveObjectWithData:arraydata];
            NSMutableArray *Array =  [NSMutableArray arrayWithArray:tempArray];
            
            int wrongNum = 0;
            for (int i = 0; i < [Array count]; i++) {
                Word *wrongWord = [Array objectAtIndex:i];
                if ([wrongWord.word_word compare:word.word_word]==NSOrderedSame) {
                    wrongNum += 1;
                }
            }
            
            if ([word.abc compare:@"1"]==NSOrderedSame) {
                [self addWrongViewA:word wrongWordNum:wrongNum];
            }else if([word.abc compare:@"2"]==NSOrderedSame){
                [self addWrongViewB:word];
            }else if([word.abc compare:@"3"]==NSOrderedSame){
                [self addWrongViewC:word];
            }else if([word.abc compare:@"4"]==NSOrderedSame){
                [self addWrongViewD:word wrongWordNum:wrongNum];
            }else if([word.abc compare:@"12"]==NSOrderedSame){
                switch (value2) {
                    case 0:
                        [self addWrongViewA:word wrongWordNum:wrongNum];
                        break;
                    case 1:
                        [self addWrongViewB:word];
                        break;
                }
            }else if([word.abc compare:@"13"]==NSOrderedSame){
                switch (value2) {
                    case 0:
                        [self addWrongViewA:word wrongWordNum:wrongNum];
                        break;
                    case 1:
                        [self addWrongViewC:word];
                        break;
                }
            }else if([word.abc compare:@"14"]==NSOrderedSame){
                switch (value2) {
                    case 0:
                        [self addWrongViewA:word wrongWordNum:wrongNum];
                        break;
                    case 1:
                        [self addWrongViewD:word wrongWordNum:wrongNum];
                        break;
                }
            }else if([word.abc compare:@"23"]==NSOrderedSame){
                switch (value2) {
                    case 0:
                        [self addWrongViewB:word];
                        break;
                    case 1:
                        [self addWrongViewC:word];
                        break;
                }
            }else if([word.abc compare:@"24"]==NSOrderedSame){
                switch (value2) {
                    case 0:
                        [self addWrongViewB:word];
                        break;
                    case 1:
                        [self addWrongViewD:word wrongWordNum:wrongNum];
                        break;
                }
            }else if([word.abc compare:@"34"]==NSOrderedSame){
                switch (value2) {
                    case 0:
                        [self addWrongViewC:word];
                        break;
                    case 1:
                        [self addWrongViewD:word wrongWordNum:wrongNum];
                        break;
                }
            }else if([word.abc compare:@"123"]==NSOrderedSame){
                switch (value) {
                    case 0:
                        [self addWrongViewA:word wrongWordNum:wrongNum];
                        break;
                    case 1:
                        [self addWrongViewB:word];
                        break;
                    case 2:
                        [self addWrongViewC:word];
                        break;
                }
            }else if([word.abc compare:@"124"]==NSOrderedSame){
                switch (value) {
                    case 0:
                        [self addWrongViewA:word wrongWordNum:wrongNum];
                        break;
                    case 1:
                        [self addWrongViewB:word];
                        break;
                    case 2:
                        [self addWrongViewD:word wrongWordNum:wrongNum];
                        break;
                }
                
            }else if([word.abc compare:@"134"]==NSOrderedSame){
                switch (value) {
                    case 0:
                        [self addWrongViewA:word wrongWordNum:wrongNum];
                        break;
                    case 1:
                        [self addWrongViewC:word];
                        break;
                    case 2:
                        [self addWrongViewD:word wrongWordNum:wrongNum];
                        break;
                }
                
            }else if([word.abc compare:@"234"]==NSOrderedSame){
                switch (value) {
                    case 0:
                        [self addWrongViewB:word];
                        break;
                    case 1:
                        [self addWrongViewC:word];
                        break;
                    case 2:
                        [self addWrongViewD:word wrongWordNum:wrongNum];
                        break;
                }
                
            }else if([word.abc compare:@"1234"]==NSOrderedSame){
                switch (value3) {
                    case 0:
                        [self addWrongViewA:word wrongWordNum:wrongNum];
                        break;
                    case 1:
                        [self addWrongViewB:word];
                        break;
                    case 2:
                        [self addWrongViewC:word];
                        break;
                    case 3:
                        [self addWrongViewD:word wrongWordNum:wrongNum];
                        break;
                }
            }
        }
}

// A形题错误处理
-(void)addWrongViewA:(Word *) word wrongWordNum:(int) wrongNum{
    UIView *dwView1=[[UIView alloc] initWithFrame:CGRectMake(20, 50, 975.0, 620)];
    UIImage*img =[UIImage imageNamed:@"remember_words_background"];
    [dwView1 setBackgroundColor:[UIColor colorWithPatternImage:img]];
    dwView1.tag = (int)word;
    
    UILabel *label1 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 160.0, 950.0, 170.0)];
    label1.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label1.textAlignment = UITextAlignmentCenter;
    label1.font = [UIFont fontWithName:@"Helvetica" size:80];
    label1.textColor = [UIColor blackColor];
    label1.adjustsFontSizeToFitWidth = YES;
    NSString *wordString = [word.word_word stringByReplacingOccurrencesOfString:@"@$" withString:@"\n"];
    label1.text = wordString;
    
    UILabel *label2 = [[UILabel alloc]initWithFrame:CGRectMake(100.0, 330.0, 800.0, 60.0)];
    label2.adjustsFontSizeToFitWidth = YES;
    if (![word.word_word_class compare:@""]==NSOrderedSame) {
        NSString *text = [NSString stringWithFormat:@"%@.",word.word_word_class];
        label2.text = text;
    }
    label2.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label2.textAlignment = UITextAlignmentCenter;
    label2.font = [UIFont fontWithName:@"Helvetica" size:55];
    label2.textColor = [UIColor blackColor];
   
    UIImage *image4 = [self loadImage:word.word_d_picture1];
    int width1 = image4.size.height;
    if (width1 > 300) {
        width1 = 300;
    }
    int height1 = width1*(image4.size.height/image4.size.width);
    UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(920-width1,570-height1,width1,height1)];
    [imageView2 initWithImage:image4];
    imageView2.tag = (int)image4;
    imageView2.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(LargeImage:)];
    [imageView2 addGestureRecognizer:singleTap];
    [singleTap release];
    [dwView1 bringSubviewToFront:imageView2];
    [dwView1 setUserInteractionEnabled:YES];
    if (image4) {
        [dwView1 addSubview:imageView2];
    }
    [dwView1 addSubview:[self AddDictionaryLable:word]];
    [dwView1 addSubview:label1];
    [dwView1 addSubview:label2];
    
    
    int record;
    if ((viewNum - dwNum) >=12) {//添加到第五个单词后面
        UIView *view = [viewArray objectAtIndex:dwNum +10];
        NSArray *views = [view subviews];
        if ([views count] == 3) {
            [viewArray insertObject:dwView1 atIndex:dwNum+12];
            [self.view addSubview:dwView1];
            [isAnsArray insertObject:@"1" atIndex:dwNum+12];
            record=dwNum+12;
        }else{
            [viewArray insertObject:dwView1 atIndex:dwNum+11];
            [self.view addSubview:dwView1];
            [isAnsArray insertObject:@"1" atIndex:dwNum+11];
            record=dwNum+11;
        }
    }else{
        record = viewNum;
        [viewArray insertObject:dwView1 atIndex:viewNum];
        [self.view addSubview:dwView1];
        [isAnsArray insertObject:@"1" atIndex:viewNum];
    }
    viewNum +=1;
    
    //体型A的答案界面
    UIView *dwView2=[[UIView alloc] initWithFrame:CGRectMake(20, 50, 975.0, 620)];
    UIImage*img2 =[UIImage imageNamed:@"remember_words_background"];
    [dwView2 setBackgroundColor:[UIColor colorWithPatternImage:img2]];
    dwView2.tag = (int)word;
    
    // create word explan 1 label
    UILabel *label3 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 100, 950.0, 300)];
    label3.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label3.textAlignment = UITextAlignmentCenter;
    label3.adjustsFontSizeToFitWidth = YES;
    label3.font = [UIFont fontWithName:@"Helvetica" size:50];
    label3.textColor = [UIColor blackColor];
    label3.lineBreakMode = UILineBreakModeWordWrap;
    label3.numberOfLines = 0;
    NSString *word2String = [word.word_a_explan1 stringByReplacingOccurrencesOfString:@"@$" withString:@"\n"];
    label3.text = word2String;
    
    UILabel *label4 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 400.0, 950.0, 30.0)];
    label4.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label4.textAlignment = UITextAlignmentCenter;
    label4.adjustsFontSizeToFitWidth = YES;
    label4.font = [UIFont fontWithName:@"Helvetica" size:20];
    label4.textColor = [UIColor blackColor];
    NSArray *array2 = [word.word_statement componentsSeparatedByString:@"@$"]; //从字符A中分隔成2个元素的数组
    NSString *newString2 = [[NSString alloc]init];
    for (int i = 0; i < [array2 count]; i++) {
        newString2 = [NSString stringWithFormat:@"%@%@",newString2,[array2 objectAtIndex:i]];
    }
    label4.text = newString2;
    
    if (wrongNum ==1) {
        UILabel *label5 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 310.0, 950.0, 30.0)];
        label5.backgroundColor = [UIColor clearColor]; //可以去掉背景色
        label5.textAlignment = UITextAlignmentCenter;
        label5.adjustsFontSizeToFitWidth = YES;
        label5.font = [UIFont fontWithName:@"Helvetica" size:20];
        label5.textColor = [UIColor blackColor];
        NSArray *array = [word.word_a_explan2 componentsSeparatedByString:@"@$"]; //从字符A中分隔成2个元素的数组
        NSString *newString = [[NSString alloc]init];
        for (int i = 0; i < [array count]; i++) {
            newString = [NSString stringWithFormat:@"%@%@",newString,[array objectAtIndex:i]];
        }
        label5.text = newString;
        [dwView2 addSubview:label5];
    }else if(wrongNum >= 2){
        UILabel *label5 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 310.0, 950.0, 30.0)];
        label5.backgroundColor = [UIColor clearColor]; //可以去掉背景色
        label5.textAlignment = UITextAlignmentCenter;
        label5.adjustsFontSizeToFitWidth = YES;
        label5.font = [UIFont fontWithName:@"Helvetica" size:20];
        label5.textColor = [UIColor blackColor];
        NSArray *array = [word.word_a_explan2 componentsSeparatedByString:@"@$"]; //从字符A中分隔成2个元素的数组
        NSString *newString = [[NSString alloc]init];
        for (int i = 0; i < [array count]; i++) {
            newString = [NSString stringWithFormat:@"%@%@",newString,[array objectAtIndex:i]];
        }
        label5.text = newString;
        [dwView2 addSubview:label5];
        
        UILabel *label6 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 340.0, 950.0, 30.0)];
        label6.backgroundColor = [UIColor clearColor]; //可以去掉背景色
        label6.textAlignment = UITextAlignmentCenter;
        label6.adjustsFontSizeToFitWidth = YES;
        label6.font = [UIFont fontWithName:@"Helvetica" size:20];
        label6.textColor = [UIColor blackColor];
        NSArray *array2 = [word.word_a_explan3 componentsSeparatedByString:@"@$"]; //从字符A中分隔成2个元素的数组
        NSString *newString2 = [[NSString alloc]init];
        for (int i = 0; i < [array2 count]; i++) {
            newString2 = [NSString stringWithFormat:@"%@%@",newString2,[array2 objectAtIndex:i]];
        }
        label6.text = newString2;
        [dwView2 addSubview:label6];
    }
    
    UIImage *image = [self loadImage:word.word_d_picture2];
    int width2 = image.size.height;
    if (width2 > 300) {
        width2 = 300;
    }
    int height2 = width2*(image.size.height/image.size.width);
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(12,550-height2,width2,height2)];
    [imageView initWithImage:image];
    imageView.tag = (int)image;
    imageView.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(LargeImage:)];
    [imageView addGestureRecognizer:singleTap2];
    [singleTap2 release];
    [dwView2 bringSubviewToFront:imageView];
    [dwView2 setUserInteractionEnabled:YES];
    
    UIButton *Btn1=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    Btn1.frame=CGRectMake(10,565,200,43);
    UIImage*btnImage =[UIImage imageNamed:@"memory_easy"];
    [Btn1 setBackgroundImage:btnImage forState:UIControlStateNormal];
    Btn1.tag=(int)word;
    [Btn1 addTarget:self action:@selector(rightAnsware:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *Btn2=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    Btn2.frame=CGRectMake(762,565,200,43);
    Btn2.tag=(int)word;
    UIImage*btnImage2 =[UIImage imageNamed:@"memory_i_know"];
    [Btn2 setBackgroundImage:btnImage2 forState:UIControlStateNormal];
    [Btn2 addTarget:self action:@selector(wrongAnsware:) forControlEvents:UIControlEventTouchUpInside];
    
    [dwView2 addSubview:label3];
    [dwView2 addSubview:label4];
    if (image) {
        [dwView2 addSubview:imageView];
    }
    [dwView2 addSubview:[self AddDictionaryLable2:word]];
    [dwView2 addSubview:Btn1];
    [dwView2 addSubview:Btn2];
    [viewArray insertObject:dwView2 atIndex:record+1];
    [self.view addSubview:dwView2];
    [isAnsArray insertObject:@"1" atIndex:record+1];
    viewNum +=1;
    
    //第二次添加

    UIView *ansView1=[[UIView alloc] initWithFrame:CGRectMake(20, 50, 975.0, 620)];
    UIImage*img3 =[UIImage imageNamed:@"remember_words_background"];
    [ansView1 setBackgroundColor:[UIColor colorWithPatternImage:img3]];
    ansView1.tag = (int)word;
    
    UILabel *label11 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 160.0, 950.0, 90.0)];
    label11.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label11.textAlignment = UITextAlignmentCenter;
    label11.font = [UIFont fontWithName:@"Helvetica" size:80];
    label11.textColor = [UIColor blackColor];
    label11.adjustsFontSizeToFitWidth = YES;
    NSString *word3String = [word.word_word stringByReplacingOccurrencesOfString:@"@$" withString:@"\n"];
    label11.text = word3String;
    
    UILabel *label22 = [[UILabel alloc]initWithFrame:CGRectMake(100.0, 250.0, 800.0, 60.0)];
    label22.adjustsFontSizeToFitWidth = YES;
    if (![word.word_word_class compare:@""]==NSOrderedSame) {
        NSString *text = [NSString stringWithFormat:@"%@.",word.word_word_class];
        label22.text = text;
    }
    label22.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label22.textAlignment = UITextAlignmentCenter;
    label22.font = [UIFont fontWithName:@"Helvetica" size:55];
    label22.textColor = [UIColor blackColor];
    
    UIImage *image3 = [self loadImage:word.word_d_picture1];
    int width12 = image3.size.height;
    if (width12 > 300) {
        width12 = 300;
    }
    int height12 = width12*(image3.size.height/image3.size.width);
    UIImageView *imageView22 = [[UIImageView alloc] initWithFrame:CGRectMake(920-width12,570-height12,width12,height12)];
    [imageView22 initWithImage:image3];
    imageView22.tag = (int)image3;
    imageView22.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(LargeImage:)];
    [imageView22 addGestureRecognizer:singleTap3];
    [singleTap3 release];
    [ansView1 bringSubviewToFront:imageView22];
    [ansView1 setUserInteractionEnabled:YES];
    if (image3) {
        [ansView1 addSubview:imageView22];
    }
    [ansView1 addSubview:[self AddDictionaryLable:word]];
    [ansView1 addSubview:label11];
    [ansView1 addSubview:label22];
    int record2;
    if ((viewNum - dwNum)>=24) {
        UIView *view = [viewArray objectAtIndex:dwNum+22];
        NSArray *views = [view subviews];
        if ([views count] == 3) {
            [viewArray insertObject:ansView1 atIndex:dwNum+24];
            [self.view addSubview:ansView1];
            [isAnsArray insertObject:@"1" atIndex:dwNum+24];
            record2 = dwNum+24;
        }else{
            [viewArray insertObject:ansView1 atIndex:dwNum+23];
            [self.view addSubview:ansView1];
            [isAnsArray insertObject:@"1" atIndex:dwNum+23];
            record2 = dwNum+23;
        }
        
    }else{
        record2 = viewNum;
        [viewArray insertObject:ansView1 atIndex:viewNum];
        [self.view addSubview:ansView1];
        [isAnsArray insertObject:@"1" atIndex:viewNum];
    }
    viewNum +=1;
    
    //体型A的答案界面
    UIView *ansView2=[[UIView alloc] initWithFrame:CGRectMake(20, 50, 975.0, 620)];
    UIImage*img4 =[UIImage imageNamed:@"remember_words_background"];
    [ansView2 setBackgroundColor:[UIColor colorWithPatternImage:img4]];
    ansView2.tag = (int)word;
    
    UILabel *label33 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 100, 950.0, 300)];
    label33.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label33.textAlignment = UITextAlignmentCenter;
    label33.adjustsFontSizeToFitWidth = YES;
    label33.font = [UIFont fontWithName:@"Helvetica" size:50];
    label33.textColor = [UIColor blackColor];
    NSArray *array3 = [word.word_a_explan1 componentsSeparatedByString:@"@$"]; //从字符A中分隔成2个元素的数组
    NSString *newString3 = [[NSString alloc]init];
    for (int i = 0; i < [array3 count]; i++) {
        newString3 = [NSString stringWithFormat:@"%@%@",newString3,[array3 objectAtIndex:i]];
    }
    NSString *word4String = [newString3 stringByReplacingOccurrencesOfString:@"@$" withString:@"\n"];
    label33.text = word4String;
    
    UILabel *label44 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 400, 950.0, 30.0)];
    label44.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label44.textAlignment = UITextAlignmentCenter;
    label44.adjustsFontSizeToFitWidth = YES;
    label44.font = [UIFont fontWithName:@"Helvetica" size:20];
    label44.textColor = [UIColor blackColor];
    NSArray *array4 = [word.word_statement componentsSeparatedByString:@"@$"]; //从字符A中分隔成2个元素的数组
    NSString *newString4 = [[NSString alloc]init];
    for (int i = 0; i < [array4 count]; i++) {
        newString4 = [NSString stringWithFormat:@"%@%@",newString4,[array4 objectAtIndex:i]];
    }
    label44.text = newString4;
    
    if (wrongNum ==1) {
        UILabel *label55 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 310.0, 950.0, 30.0)];
        label55.backgroundColor = [UIColor clearColor]; //可以去掉背景色
        label55.textAlignment = UITextAlignmentCenter;
        label55.adjustsFontSizeToFitWidth = YES;
        label55.font = [UIFont fontWithName:@"Helvetica" size:20];
        label55.textColor = [UIColor blackColor];
        NSArray *array = [word.word_a_explan2 componentsSeparatedByString:@"@$"]; //从字符A中分隔成2个元素的数组
        NSString *newString = [[NSString alloc]init];
        for (int i = 0; i < [array count]; i++) {
            newString = [NSString stringWithFormat:@"%@%@",newString,[array objectAtIndex:i]];
        }
        label55.text = newString;
        [ansView2 addSubview:label55];
    }else if(wrongNum >= 2){
        UILabel *label55 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 310.0, 950.0, 30.0)];
        label55.backgroundColor = [UIColor clearColor]; //可以去掉背景色
        label55.textAlignment = UITextAlignmentCenter;
        label55.adjustsFontSizeToFitWidth = YES;
        label55.font = [UIFont fontWithName:@"Helvetica" size:20];
        label55.textColor = [UIColor blackColor];
        NSArray *array = [word.word_a_explan2 componentsSeparatedByString:@"@$"]; //从字符A中分隔成2个元素的数组
        NSString *newString = [[NSString alloc]init];
        for (int i = 0; i < [array count]; i++) {
            newString = [NSString stringWithFormat:@"%@%@",newString,[array objectAtIndex:i]];
        }
        label55.text = newString;
        [ansView2 addSubview:label55];
        
        UILabel *label66 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 340.0, 950.0, 30.0)];
        label66.backgroundColor = [UIColor clearColor]; //可以去掉背景色
        label66.textAlignment = UITextAlignmentCenter;
        label66.adjustsFontSizeToFitWidth = YES;
        label66.font = [UIFont fontWithName:@"Helvetica" size:20];
        label66.textColor = [UIColor blackColor];
        NSArray *array2 = [word.word_a_explan1 componentsSeparatedByString:@"@$"]; //从字符A中分隔成2个元素的数组
        NSString *newString2 = [[NSString alloc]init];
        for (int i = 0; i < [array2 count]; i++) {
            newString2 = [NSString stringWithFormat:@"%@%@",newString2,[array2 objectAtIndex:i]];
        }
        label66.text = newString2;
        [ansView2 addSubview:label66];
    }

    UIImage *image2 = [self loadImage:word.word_d_picture2];
    int width22 = image2.size.height;
    if (width22 > 300) {
        width22 = 300;
    }
    int height22 = width22*(image2.size.height/image2.size.width);
    
    UIImageView *imageView33 = [[UIImageView alloc] initWithFrame:CGRectMake(12,550-height22,width22,height22)];
    [imageView33 initWithImage:image2];
    imageView33.tag = (int)image2;
    imageView33.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(LargeImage:)];
    [imageView33 addGestureRecognizer:singleTap4];
    [singleTap4 release];
    [ansView2 bringSubviewToFront:imageView33];
    [ansView2 setUserInteractionEnabled:YES];
    
    UIButton *Btn3=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    Btn3.frame=CGRectMake(10,565,200,43);
    UIImage*btnImage3 =[UIImage imageNamed:@"memory_easy"];
    [Btn3 setBackgroundImage:btnImage3 forState:UIControlStateNormal];
    Btn3.tag=(int)word;
    [Btn3 addTarget:self action:@selector(rightAnsware:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *Btn4=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    Btn4.frame=CGRectMake(762,565,200,43);
    Btn4.tag=(int)word;
    UIImage*btnImage4 =[UIImage imageNamed:@"memory_i_know"];
    [Btn4 setBackgroundImage:btnImage4 forState:UIControlStateNormal];
    [Btn4 addTarget:self action:@selector(wrongAnsware:) forControlEvents:UIControlEventTouchUpInside];
    [ansView2 addSubview:[self AddDictionaryLable2:word]];
    [ansView2 addSubview:label33];
    [ansView2 addSubview:label44];
    if (image2) {
        [ansView2 addSubview:imageView33];
    }
    [ansView2 addSubview:Btn3];
    [ansView2 addSubview:Btn4];
    [viewArray insertObject:ansView2 atIndex:record2+1];
    [self.view addSubview:ansView2];
    [isAnsArray insertObject:@"1" atIndex:record2+1];

    viewNum +=1;
}

// B形题错误处理
-(void)addWrongViewB:(Word *) word{
    UIView *dwView=[[UIView alloc] initWithFrame:CGRectMake(20, 50, 975.0, 620)];
    UIImage*img =[UIImage imageNamed:@"remember_words_background"];
    [dwView setBackgroundColor:[UIColor colorWithPatternImage:img]];
    dwView.tag = (int)word;
    
    UIView *ansView=[[UIView alloc] initWithFrame:CGRectMake(20, 50, 975.0, 620)];
    UIImage*img2 =[UIImage imageNamed:@"remember_words_background"];
    [ansView setBackgroundColor:[UIColor colorWithPatternImage:img2]];
    ansView.tag = (int)word;
    
    int question;//体型随机处理
    if (![word.word_b_explane compare:@""]==NSOrderedSame) {
        question = arc4random() % 5;
    }else {
        question = arc4random() % 4;
    }
    NSString *intString = [NSString stringWithFormat:@"%d",question];
    NSArray *list;
    switch (question) {
        case 0:
            list = [NSArray arrayWithObjects:word.word_b_explana,word.word_b_explanb,word.word_b_explanc,word.word_b_expland,word.word_b_explane, nil];
            break;
        case 1:
            list = [NSArray arrayWithObjects:word.word_b_explanb,word.word_b_explana,word.word_b_explanc,word.word_b_expland,word.word_b_explane, nil];
            break;
        case 2:
            list = [NSArray arrayWithObjects:word.word_b_explanc,word.word_b_explanb,word.word_b_explana,word.word_b_expland,word.word_b_explane, nil];
            break;
        case 3:
            list = [NSArray arrayWithObjects:word.word_b_expland,word.word_b_explanb,word.word_b_explanc,word.word_b_explana,word.word_b_explane, nil];
            break;
        case 4:
            list = [NSArray arrayWithObjects:word.word_b_explane,word.word_b_explanb,word.word_b_explanc, word.word_b_expland,word.word_b_explana,nil];
            break;
    }
    
    
    UILabel *label1 = [[UILabel alloc]initWithFrame:CGRectMake(74.0, 20.0, 800.0, 70.0)];
    label1.font = [UIFont fontWithName:@"Helvetica" size:60];
    NSString *text = [word.word_word stringByReplacingOccurrencesOfString:@"@$" withString:@"\n"];
    if (text.length > 100) {
        label1.lineBreakMode = UILineBreakModeWordWrap;
        label1.numberOfLines = 0;
        label1.font = [UIFont fontWithName:@"Helvetica" size:20];
    }
    label1.text = text;
    label1.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label1.textAlignment = UITextAlignmentLeft;
    label1.adjustsFontSizeToFitWidth = YES;
    label1.textColor = [UIColor blackColor];
    
    UILabel *label2 = [[UILabel alloc]initWithFrame:CGRectMake(74.0, 90.0, 400.0, 65.0)];
    NSString *text_n = [NSString stringWithFormat:@"%@",word.word_word_class];
    label2.text = text_n;
    label2.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label2.adjustsFontSizeToFitWidth = YES;
    label2.textAlignment = UITextAlignmentLeft;
    label2.font = [UIFont fontWithName:@"Helvetica" size:40];
    label2.textColor = [UIColor blackColor];
    
    UIImage *image2 = [self loadImage:word.word_d_picture1];
    int width2 = 160*(image2.size.width/image2.size.height);
    UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(670,20,width2,160)];
    [imageView2 initWithImage:image2];
    imageView2.tag = (int)image2;
    imageView2.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(LargeImage:)];
    [imageView2 addGestureRecognizer:singleTap];
    [singleTap release];
    [dwView bringSubviewToFront:imageView2];
    [dwView setUserInteractionEnabled:YES];
    
    UIButton *Btn1=[UIButton buttonWithType:UIButtonTypeCustom];
    Btn1.frame=CGRectMake(50.0, 150.0, 880.0, 60.0);
    UIImage*btnImage =[UIImage imageNamed:@"memory_d_background"];
    [Btn1 setBackgroundImage:btnImage forState:UIControlStateNormal];
    NSString *text1 = [NSString stringWithFormat:@"  A. %@",[list objectAtIndex:0]];
    Btn1.titleLabel.font = [UIFont systemFontOfSize: 40];
    Btn1.titleLabel.adjustsFontSizeToFitWidth = YES;
    wrongWords *wrong = [[wrongWords alloc]init];
    wrong.randomTag =intString;
    wrong.answare = [list objectAtIndex:0];
    wrong.myword = word;
    Btn1.tag = (int)wrong;
    Btn1.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft ;
    [Btn1 setTitle:text1 forState:UIControlStateNormal];
    [Btn1 addTarget:self action:@selector(buttonClickB:) forControlEvents:UIControlEventTouchUpInside];
    [Btn1 setTitleColor:[UIColor colorWithRed:37.99/255 green:37.99/255 blue:41.05/255 alpha:1]forState:UIControlStateNormal];
    
    UIButton *Btn2=[UIButton buttonWithType:UIButtonTypeCustom];
    Btn2.frame=CGRectMake(50.0, 225.0, 880.0, 60.0);
    UIImage*btnImage2 =[UIImage imageNamed:@"memory_d_background"];
    [Btn2 setBackgroundImage:btnImage2 forState:UIControlStateNormal];
    NSString *text2 = [NSString stringWithFormat:@"  B. %@",[list objectAtIndex:1]];
    Btn2.titleLabel.font = [UIFont systemFontOfSize: 40];
    Btn2.titleLabel.adjustsFontSizeToFitWidth = YES;
    wrongWords *wrong2 = [[wrongWords alloc]init];
    wrong2.randomTag =intString;
    wrong2.answare = [list objectAtIndex:1];
    wrong2.myword = word;
    Btn2.tag = (int)wrong2;
    Btn2.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft ;
    [Btn2 setTitle:text2 forState:UIControlStateNormal];
    [Btn2 addTarget:self action:@selector(buttonClickB:) forControlEvents:UIControlEventTouchUpInside];
    [Btn2 setTitleColor:[UIColor colorWithRed:37.99/255 green:37.99/255 blue:41.05/255 alpha:1]forState:UIControlStateNormal];
    
    UIButton *Btn3=[UIButton buttonWithType:UIButtonTypeCustom];
    Btn3.frame=CGRectMake(50.0, 300.0, 880.0, 60.0);
    UIImage*btnImage3 =[UIImage imageNamed:@"memory_d_background"];
    [Btn3 setBackgroundImage:btnImage3 forState:UIControlStateNormal];
    NSString *text3 = [NSString stringWithFormat:@"  C. %@",[list objectAtIndex:2]];
    Btn3.titleLabel.font = [UIFont systemFontOfSize: 40];
    Btn3.titleLabel.adjustsFontSizeToFitWidth = YES;
    wrongWords *wrong3 = [[wrongWords alloc]init];
    wrong3.randomTag =intString;
    wrong3.answare = [list objectAtIndex:2];
    wrong3.myword = word;
    Btn3.tag = (int)wrong3;
    Btn3.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft ;
    [Btn3 setTitle:text3 forState:UIControlStateNormal];
    [Btn3 addTarget:self action:@selector(buttonClickB:) forControlEvents:UIControlEventTouchUpInside];
    [Btn3 setTitleColor:[UIColor colorWithRed:37.99/255 green:37.99/255 blue:41.05/255 alpha:1]forState:UIControlStateNormal];
    
    UIButton *Btn4=[UIButton buttonWithType:UIButtonTypeCustom];
    Btn4.frame=CGRectMake(50.0, 375.0, 880.0, 60.0);
    UIImage*btnImage4 =[UIImage imageNamed:@"memory_d_background"];
    [Btn4 setBackgroundImage:btnImage4 forState:UIControlStateNormal];
    NSString *text4 = [NSString stringWithFormat:@"  D. %@",[list objectAtIndex:3]];
    Btn4.titleLabel.font = [UIFont systemFontOfSize: 40];
    Btn4.titleLabel.adjustsFontSizeToFitWidth = YES;
    wrongWords *wrong4 = [[wrongWords alloc]init];
    wrong4.randomTag =intString;
    wrong4.answare = [list objectAtIndex:3];
    wrong4.myword = word;
    Btn4.tag = (int)wrong4;
    Btn4.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft ;
    [Btn4 setTitle:text4 forState:UIControlStateNormal];
    [Btn4 addTarget:self action:@selector(buttonClickB:) forControlEvents:UIControlEventTouchUpInside];
    [Btn4 setTitleColor:[UIColor colorWithRed:37.99/255 green:37.99/255 blue:41.05/255 alpha:1]forState:UIControlStateNormal];
    
    UIButton *Btne=[UIButton buttonWithType:UIButtonTypeCustom];
    Btne.frame=CGRectMake(50.0, 450.0, 880.0, 60.0);
    UIImage*btnImagee =[UIImage imageNamed:@"memory_d_background"];
    [Btne setBackgroundImage:btnImagee forState:UIControlStateNormal];
    NSString *texte = [NSString stringWithFormat:@"  E. %@",[list objectAtIndex:4]];
    Btne.titleLabel.font = [UIFont systemFontOfSize: 40];
    Btne.titleLabel.adjustsFontSizeToFitWidth = YES;
    wrongWords *wronge = [[wrongWords alloc]init];
    wronge.randomTag = intString;
    wronge.answare = [list objectAtIndex:4];
    wronge.myword = word;
    Btne.tag = (int)wronge;
    Btne.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft ;
    [Btne setTitle:texte forState:UIControlStateNormal];
    [Btne addTarget:self action:@selector(buttonClickB:) forControlEvents:UIControlEventTouchUpInside];
    [Btne setTitleColor:[UIColor colorWithRed:37.99/255 green:37.99/255 blue:41.05/255 alpha:1]forState:UIControlStateNormal];

    UIButton *Btn5=[UIButton buttonWithType:UIButtonTypeCustom];
    Btn5.frame=CGRectMake(730,560,200,43);
    wrongWords *wrong51 = [[wrongWords alloc]init];
    wrong51.randomTag = intString;//标记正确答案是第几个
    wrong51.answare = [list objectAtIndex:0];//保存正确答案
    wrong51.myword = word;
    Btn5.tag = (int)wrong51;
    UIImage*btnImage5 =[UIImage imageNamed:@"memory_i_do_not_know"];
    [Btn5 setBackgroundImage:btnImage5 forState:UIControlStateNormal];
    [Btn5 addTarget:self action:@selector(buttonClickBWrong:) forControlEvents:UIControlEventTouchUpInside];
    
    [dwView addSubview:label1];
    [dwView addSubview:label2];
    [dwView addSubview:Btn1];
    [dwView addSubview:Btn2];
    [dwView addSubview:Btn3];
    [dwView addSubview:Btn4];
    if (![word.word_b_explane compare:@""]==NSOrderedSame) {
        [dwView addSubview:Btne];
//        [Btne release];
    }
    if (image2) {
        [dwView addSubview:imageView2];
    }
    [dwView addSubview:Btn5];
    [dwView addSubview:[self AddDictionaryLable2:word]];
    int record;
    if ((viewNum - dwNum)>=12) {
        UIView *view = [viewArray objectAtIndex:dwNum+10];
        NSLog(@"%@",view);
        NSArray *views = [view subviews];
        if ([views count]==3) {
            [viewArray insertObject:dwView atIndex:dwNum+12];
            [self.view addSubview:dwView];
            [isAnsArray insertObject:@"0" atIndex:dwNum+12];
            record = dwNum+12;
        }else{
            [viewArray insertObject:dwView atIndex:dwNum+11];
            [self.view addSubview:dwView];
            [isAnsArray insertObject:@"0" atIndex:dwNum+11];
            record = dwNum+11;
        }

    }else{
        [viewArray insertObject:dwView atIndex:viewNum];
        [self.view addSubview:dwView];
        [isAnsArray insertObject:@"0" atIndex:viewNum];
        record=viewNum;
    }
    viewNum +=1;
    
    
    // 第二次加入
    UILabel *label11 = [[UILabel alloc]initWithFrame:CGRectMake(74.0, 20.0, 400.0, 70.0)];
    label11.font = [UIFont fontWithName:@"Helvetica" size:50];
    NSString *texta = [word.word_word stringByReplacingOccurrencesOfString:@"@$" withString:@"\n"];
    if (texta.length > 100) {
        label11.lineBreakMode = UILineBreakModeWordWrap;
        label11.numberOfLines = 0;
        label11.font = [UIFont fontWithName:@"Helvetica" size:20];
    }
    label11.text = texta;
    label11.adjustsFontSizeToFitWidth = YES;
    label11.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label11.textAlignment = UITextAlignmentLeft;
    label11.font = [UIFont fontWithName:@"Helvetica" size:60];
    label11.textColor = [UIColor blackColor];
    
    UILabel *label22 = [[UILabel alloc]initWithFrame:CGRectMake(74.0, 90.0, 400.0, 65.0)];
    NSString *text_n2 = [NSString stringWithFormat:@"%@",word.word_word_class];
    label22.text = text_n2;
    label22.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label22.adjustsFontSizeToFitWidth = YES;
    label22.textAlignment = UITextAlignmentLeft;
    label22.font = [UIFont fontWithName:@"Helvetica" size:40];
    label22.textColor = [UIColor blackColor];
    
    UIImage *image3 = [self loadImage:word.word_d_picture1];
    int width3 = 160*(image3.size.width/image3.size.height);
    UIImageView *imageView22 = [[UIImageView alloc] initWithFrame:CGRectMake(670,20,width3,160)];
    //    UIImage *image2 = [UIImage imageNamed:@"set_finish_time"];
    [imageView22 initWithImage:image3];
    imageView22.tag = (int)image2;
    imageView22.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(LargeImage:)];
    [imageView22 addGestureRecognizer:singleTap2];
    [singleTap2 release];
    [ansView bringSubviewToFront:imageView22];
    [ansView setUserInteractionEnabled:YES];
    
    UIButton *Btn11=[UIButton buttonWithType:UIButtonTypeCustom];
    Btn11.frame=CGRectMake(50.0, 150.0, 880.0, 60.0);
    UIImage*btnImagea =[UIImage imageNamed:@"memory_d_background"];
    [Btn11 setBackgroundImage:btnImagea forState:UIControlStateNormal];
    NSString *text1a = [NSString stringWithFormat:@"  A. %@",[list objectAtIndex:0]];
    Btn11.titleLabel.font = [UIFont systemFontOfSize: 40];
    Btn11.titleLabel.adjustsFontSizeToFitWidth = YES;
    wrongWords *wrong5 = [[wrongWords alloc]init];
    wrong5.randomTag = intString;
    wrong5.answare = [list objectAtIndex:0];
    wrong5.myword = word;
    Btn11.tag = (int)wrong5;
    Btn11.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft ;
    [Btn11 setTitle:text1a forState:UIControlStateNormal];
    [Btn11 addTarget:self action:@selector(buttonClickB:) forControlEvents:UIControlEventTouchUpInside];
    [Btn11 setTitleColor:[UIColor colorWithRed:37.99/255 green:37.99/255 blue:41.05/255 alpha:1]forState:UIControlStateNormal];
    
    UIButton *Btn22=[UIButton buttonWithType:UIButtonTypeCustom];
    Btn22.frame=CGRectMake(50.0, 225.0, 880.0, 60.0);
    UIImage*btnImage2a =[UIImage imageNamed:@"memory_d_background"];
    [Btn22 setBackgroundImage:btnImage2a forState:UIControlStateNormal];
    NSString *text2a = [NSString stringWithFormat:@"  B. %@",[list objectAtIndex:1]];
    Btn22.titleLabel.font = [UIFont systemFontOfSize: 40];
    Btn22.titleLabel.adjustsFontSizeToFitWidth = YES;
    wrongWords *wrong6 = [[wrongWords alloc]init];
    wrong6.randomTag = intString;
    wrong6.answare = [list objectAtIndex:1];
    wrong6.myword = word;
    Btn22.tag = (int)wrong6;
    Btn22.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft ;
    [Btn22 setTitle:text2a forState:UIControlStateNormal];
    [Btn22 addTarget:self action:@selector(buttonClickB:) forControlEvents:UIControlEventTouchUpInside];
    [Btn22 setTitleColor:[UIColor colorWithRed:37.99/255 green:37.99/255 blue:41.05/255 alpha:1]forState:UIControlStateNormal];
    
    UIButton *Btn33=[UIButton buttonWithType:UIButtonTypeCustom];
    Btn33.frame=CGRectMake(50.0, 300.0, 880.0, 60.0);
    UIImage*btnImage3a =[UIImage imageNamed:@"memory_d_background"];
    [Btn33 setBackgroundImage:btnImage3a forState:UIControlStateNormal];
    NSString *text3a = [NSString stringWithFormat:@"  C. %@",[list objectAtIndex:2]];
    Btn33.titleLabel.font = [UIFont systemFontOfSize: 40];
    Btn33.titleLabel.adjustsFontSizeToFitWidth = YES;
    wrongWords *wrong7 = [[wrongWords alloc]init];
    wrong7.randomTag = intString;
    wrong7.answare = [list objectAtIndex:2];
    wrong7.myword = word;
    Btn33.tag = (int)wrong7;
    Btn33.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft ;
    [Btn33 setTitle:text3a forState:UIControlStateNormal];
    [Btn33 addTarget:self action:@selector(buttonClickB:) forControlEvents:UIControlEventTouchUpInside];
    [Btn33 setTitleColor:[UIColor colorWithRed:37.99/255 green:37.99/255 blue:41.05/255 alpha:1]forState:UIControlStateNormal];
    
    UIButton *Btn44 =[UIButton buttonWithType:UIButtonTypeCustom];
    Btn44.frame=CGRectMake(50.0, 375.0, 880.0, 60.0);
    UIImage*btnImage4a =[UIImage imageNamed:@"memory_d_background"];
    [Btn44 setBackgroundImage:btnImage4a forState:UIControlStateNormal];
    NSString *text4a = [NSString stringWithFormat:@"  D. %@",[list objectAtIndex:3]];
    Btn44.titleLabel.font = [UIFont systemFontOfSize: 40];
    Btn44.titleLabel.adjustsFontSizeToFitWidth = YES;
    wrongWords *wrong8 = [[wrongWords alloc]init];
    wrong8.randomTag = intString;
    wrong8.answare = [list objectAtIndex:3];
    wrong8.myword = word;
    Btn44.tag = (int)wrong8;
    Btn44.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft ;
    [Btn44 setTitle:text4a forState:UIControlStateNormal];
    [Btn44 addTarget:self action:@selector(buttonClickB:) forControlEvents:UIControlEventTouchUpInside];
    [Btn44 setTitleColor:[UIColor colorWithRed:37.99/255 green:37.99/255 blue:41.05/255 alpha:1]forState:UIControlStateNormal];
    
    UIButton *Btne2=[UIButton buttonWithType:UIButtonTypeCustom];
    Btne2.frame=CGRectMake(50.0, 450.0, 880.0, 60.0);
    UIImage*btnImagee2 =[UIImage imageNamed:@"memory_d_background"];
    [Btne2 setBackgroundImage:btnImagee2 forState:UIControlStateNormal];
    NSString *texte2 = [NSString stringWithFormat:@"  E. %@",[list objectAtIndex:4]];
    Btne2.titleLabel.font = [UIFont systemFontOfSize: 40];
    Btne2.titleLabel.adjustsFontSizeToFitWidth = YES;
    wrongWords *wronge2 = [[wrongWords alloc]init];
    wronge2.randomTag = intString;
    wronge2.answare = [list objectAtIndex:4];
    wronge2.myword = word;
    Btne2.tag = (int)wronge2;
    Btne2.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft ;
    [Btne2 setTitle:texte2 forState:UIControlStateNormal];
    [Btne2 addTarget:self action:@selector(buttonClickB:) forControlEvents:UIControlEventTouchUpInside];
    [Btne2 setTitleColor:[UIColor colorWithRed:37.99/255 green:37.99/255 blue:41.05/255 alpha:1]forState:UIControlStateNormal];
    
    UIButton *Btn55=[UIButton buttonWithType:UIButtonTypeCustom];
    Btn55.frame=CGRectMake(730,560,200,43);
    wrongWords *wrong52 = [[wrongWords alloc]init];
    wrong52.randomTag = intString;//标记正确答案是第几个
    wrong52.answare = [list objectAtIndex:0];//保存正确答案
    wrong52.myword = word;
    Btn55.tag = (int)wrong52;
    UIImage*btnImage5a =[UIImage imageNamed:@"memory_i_do_not_know"];
    [Btn55 setBackgroundImage:btnImage5a forState:UIControlStateNormal];
    [Btn55 addTarget:self action:@selector(buttonClickBWrong:) forControlEvents:UIControlEventTouchUpInside];
    [ansView addSubview:label11];
    [ansView addSubview:label22];
    [ansView addSubview:Btn11];
    [ansView addSubview:Btn22];
    [ansView addSubview:Btn33];
    [ansView addSubview:Btn44];
    if (![word.word_b_explane compare:@""]==NSOrderedSame) {
        [dwView addSubview:Btne2];
//        [Btne2 release];
    }
    if (image3) {
        [ansView addSubview:imageView22];
    }
    [ansView addSubview:Btn55];
    [ansView addSubview:[self AddDictionaryLable2:word]];
    
    if ((viewNum - dwNum)>=24) {
        UIView *view = [viewArray objectAtIndex:dwNum+22];
        NSArray *views = [view subviews];
        if ([views count]==3) {
            [viewArray insertObject:ansView atIndex:dwNum+24];
            [self.view addSubview:ansView];
            [isAnsArray insertObject:@"0" atIndex:dwNum+24];
        }else{
            [viewArray insertObject:ansView atIndex:dwNum+23];
            [self.view addSubview:ansView];
            [isAnsArray insertObject:@"0" atIndex:dwNum+23];
        }
        
    }else{
        [viewArray insertObject:ansView atIndex:[viewArray count]];
        [self.view addSubview:ansView];
        [isAnsArray insertObject:@"0" atIndex:[isAnsArray count]];
    }
    viewNum +=1;
}

// C形题错误处理
-(void)addWrongViewC:(Word *) word{
    UIView *dwView1=[[UIView alloc] initWithFrame:CGRectMake(20, 50, 975.0, 620)];
    UIImage*img =[UIImage imageNamed:@"remember_words_background"];
    [dwView1 setBackgroundColor:[UIColor colorWithPatternImage:img]];
    dwView1.tag = (int)word;
    
    UILabel *label1 = [[UILabel alloc]initWithFrame:CGRectMake(52.0, 150.0, 900.0, 70.0)];
    NSString *text = [NSString stringWithFormat:@"%@",word.word_c_explan];
    label1.text = text;
    label1.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label1.textAlignment = UITextAlignmentCenter;
    label1.font = [UIFont fontWithName:@"Helvetica" size:50];
    label1.textColor = [UIColor blackColor];
    
    UILabel *label2 = [[UILabel alloc]initWithFrame:CGRectMake(52.0, 220.0, 900.0, 50.0)];
    NSString *text5 = [NSString stringWithFormat:@"%@",word.word_word_class];
    label2.text = text5;
    label2.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label2.textAlignment = UITextAlignmentCenter;
    label2.font = [UIFont fontWithName:@"Helvetica" size:30];
    label2.textColor = [UIColor blackColor];
    
    UITextField *textFiled = [[UITextField alloc] initWithFrame:CGRectMake(350, 300, 280.0f, 40)];
    [textFiled setBorderStyle:UITextBorderStyleRoundedRect]; //外框类型
    textFiled.font = [UIFont fontWithName:@"helvetica" size:20];
    textFiled.placeholder = @"请输入答案";
    textFiled.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    textFiled.textColor = [UIColor blackColor];
    textFiled.tag = (int)word;
    textFiled.delegate=self;
    [textFiled addTarget:self action:@selector(textFieldEnd:) forControlEvents:UIControlEventEditingDidEnd];
    textFiled.autocapitalizationType = UITextAutocapitalizationTypeNone;
    
    UILabel *label3 = [[UILabel alloc]initWithFrame:CGRectMake(640, 300, 280.0f, 34)];
    label3.text = @"";
    label3.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label3.font = [UIFont fontWithName:@"Helvetica" size:30];
    label3.textColor = [UIColor blackColor];

    UIImage *image2 = [self loadImage:word.word_d_picture1];//加载入图片
    int width3 = 160*(image2.size.width/image2.size.height);
    UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(360,350,width3,160)];

    [imageView2 initWithImage:image2];
    imageView2.tag = (int)image2;
    imageView2.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(LargeImage:)];
    [imageView2 addGestureRecognizer:singleTap];
    [singleTap release];
    [dwView1 bringSubviewToFront:imageView2];
    [dwView1 setUserInteractionEnabled:YES];
    
    UIButton *Btn1=[UIButton buttonWithType:UIButtonTypeCustom];
    Btn1.frame=CGRectMake(750,550,200,43);
    UIImage*btnImage5 =[UIImage imageNamed:@"memory_c_button"];
    [Btn1 setBackgroundImage:btnImage5 forState:UIControlStateNormal];
    QuestionC *qc = [[QuestionC alloc]init];
    qc.answareC = textFiled.text;
    qc.wordC = word;
    Btn1.tag=(int)qc;
    //[Btn1 setTitle:[titleArray objectAtIndex:i] forState:UIControlStateNormal];
    [Btn1 addTarget:self action:@selector(buttonClickC:) forControlEvents:UIControlEventTouchUpInside];
    
    [dwView1 addSubview:label1];
    [dwView1 addSubview:label2];
    [dwView1 addSubview:label3];
    [dwView1 addSubview:textFiled];
    if (image2) {
        [dwView1 addSubview:imageView2];
    }
    [dwView1 addSubview:Btn1];
    [dwView1 addSubview:[self AddDictionaryLable2:word]];
    int record;
    if ((viewNum - dwNum)>=12) {
        UIView *view = [viewArray objectAtIndex:dwNum+10];
        NSArray *views = [dwView1 subviews];
        if ([views count] == 3) {
            [viewArray insertObject:dwView1 atIndex:dwNum+12];
//            [self.view addSubview:dwView];
            [isAnsArray insertObject:@"0" atIndex:dwNum+12];
            record = dwNum+12;
        }else{
            [viewArray insertObject:dwView1 atIndex:dwNum+11];
//            [self.view addSubview:dwView];
            [isAnsArray insertObject:@"0" atIndex:dwNum+11];
            record =dwNum+11;
        }

    }else{
        [viewArray insertObject:dwView1 atIndex:viewNum];
//        [self.view addSubview:dwView];
        [isAnsArray insertObject:@"0" atIndex:viewNum];
        record = viewNum;
    }
    viewNum+=1;
    
    //第二次添加界面
    UIView *ansView=[[UIView alloc] initWithFrame:CGRectMake(20, 50, 975.0, 620)];
    UIImage*img2 =[UIImage imageNamed:@"remember_words_background"];
    [ansView setBackgroundColor:[UIColor colorWithPatternImage:img2]];
    ansView.tag = (int)word;
    
    UILabel *label11 = [[UILabel alloc]initWithFrame:CGRectMake(52.0, 150.0, 900.0, 70.0)];
    NSString *text2 = [NSString stringWithFormat:@"%@",word.word_c_explan];
    label11.text = text2;
    label11.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label11.textAlignment = UITextAlignmentCenter;
    label11.font = [UIFont fontWithName:@"Helvetica" size:50];
    label11.textColor = [UIColor blackColor];
    
    UILabel *label22 = [[UILabel alloc]initWithFrame:CGRectMake(52.0, 220.0, 900.0, 50.0)];
    NSString *text3 = [NSString stringWithFormat:@"%@",word.word_word_class];
    label22.text = text3;
    label22.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label22.textAlignment = UITextAlignmentCenter;
    label22.font = [UIFont fontWithName:@"Helvetica" size:30];
    label22.textColor = [UIColor blackColor];
    
    UITextField *textFiled2 = [[UITextField alloc] initWithFrame:CGRectMake(350, 300, 280.0f, 40)];
    [textFiled2 setBorderStyle:UITextBorderStyleRoundedRect]; //外框类型
    textFiled2.font = [UIFont fontWithName:@"helvetica" size:20];
    textFiled2.placeholder = @"请输入答案";
    textFiled2.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    textFiled2.textColor = [UIColor blackColor];
    textFiled2.tag = (int)word;
    textFiled2.delegate=self;
    [textFiled2 addTarget:self action:@selector(textFieldEnd:) forControlEvents:UIControlEventEditingDidEnd];
    textFiled.autocapitalizationType = UITextAutocapitalizationTypeNone;
    
    UILabel *label33 = [[UILabel alloc]initWithFrame:CGRectMake(640, 300, 280.0f, 34)];
    label33.text = @"";
    label33.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label33.font = [UIFont fontWithName:@"Helvetica" size:30];
    label33.textColor = [UIColor blackColor];

    UIImage *image4 = [self loadImage:word.word_d_picture1];//加载入图片
    int width4 = 160*(image4.size.width/image4.size.height);
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(360,350,width4,160)];
    [imageView2 initWithImage:image4];
    imageView2.tag = (int)image4;
    imageView2.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(LargeImage:)];
    [imageView2 addGestureRecognizer:singleTap2];
    [singleTap2 release];
    [ansView bringSubviewToFront:imageView2];
    [ansView setUserInteractionEnabled:YES];
    
    Btn1=[UIButton buttonWithType:UIButtonTypeCustom];
    Btn1.frame=CGRectMake(750,550,200,43);
    UIImage*btnImage2 =[UIImage imageNamed:@"memory_c_button"];
    [Btn1 setBackgroundImage:btnImage2 forState:UIControlStateNormal];
    QuestionC *qc2 = [[QuestionC alloc]init];
    qc2.answareC = textFiled.text;
    qc2.wordC = word;
    Btn1.tag=(int)qc2;
    //[Btn1 setTitle:[titleArray objectAtIndex:i] forState:UIControlStateNormal];
    [Btn1 addTarget:self action:@selector(buttonClickBWrong:) forControlEvents:UIControlEventTouchUpInside];
    
    [ansView addSubview:label11];
    [ansView addSubview:label22];
    [ansView addSubview:label33];
    [ansView addSubview:textFiled2];
    if (image4) {
        [ansView addSubview:imageView2];
    }
    [ansView addSubview:Btn1];
    [ansView addSubview:[self AddDictionaryLable2:word]];
    
    if ((viewNum - dwNum)>=24) {
        UIView *view = [viewArray objectAtIndex:dwNum+22];
        NSArray *views = [view subviews];
        if ([views count]==3) {
            [viewArray insertObject:ansView atIndex:dwNum+24];
            [self.view addSubview:ansView];
            [isAnsArray insertObject:@"0" atIndex:dwNum+24];
        }else{
            [viewArray insertObject:ansView atIndex:dwNum+23];
            [self.view addSubview:ansView];
            [isAnsArray insertObject:@"0" atIndex:dwNum+23];
        }
    }else{
        [viewArray insertObject:ansView atIndex:viewNum];
        [self.view addSubview:ansView];
        [isAnsArray insertObject:@"0" atIndex:viewNum];
    }
    
    viewNum +=1;
}

// D形题错误处理
-(void)addWrongViewD:(Word *) word wrongWordNum:(int) wrongNum{
    UIView *dwView1=[[UIView alloc] initWithFrame:CGRectMake(20, 50, 975.0, 620)];
    UIImage*img =[UIImage imageNamed:@"remember_words_background"];
    [dwView1 setBackgroundColor:[UIColor colorWithPatternImage:img]];
    dwView1.tag = (int)word;
    
    UILabel *label1 = [[UILabel alloc]initWithFrame:CGRectMake(40.0, 40.0, 900.0, 100.0)];
    label1.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label1.textAlignment = UITextAlignmentLeft;
    label1.font = [UIFont fontWithName:@"Helvetica" size:30];
    label1.textColor = [UIColor blackColor];
    label1.adjustsFontSizeToFitWidth = YES;
    label1.lineBreakMode = UILineBreakModeWordWrap;
    label1.numberOfLines = 0;
    NSString *wordString = [word.word_word stringByReplacingOccurrencesOfString:@"@$" withString:@"\n"];
    label1.text = wordString;
    
    UILabel *label2 = [[UILabel alloc]initWithFrame:CGRectMake(40.0, 140.0, 950.0, 30.0)];
    label2.adjustsFontSizeToFitWidth = YES;
    if (![word.word_word_class compare:@""]==NSOrderedSame) {
        NSString *text = [NSString stringWithFormat:@"%@.",word.word_word_class];
        label2.text = text;
    }
    label2.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label2.textAlignment = UITextAlignmentLeft;
    label2.font = [UIFont fontWithName:@"Helvetica" size:20];
    label2.textColor = [UIColor blackColor];
    
    UIImage *image4 = [self loadImage:word.word_d_picture1];
    
    int layoutWith = image4.size.width;
    int layoutHeight = image4.size.height;
    
    if (layoutWith > 700) {
        layoutWith = 700;
        layoutHeight = layoutHeight*700/layoutWith;
    }else if(layoutHeight > 450){
        layoutHeight = 450;
        layoutWith = layoutWith*450/layoutHeight;
    }
    int layoutX = (975.0 - layoutWith)/2;
    int layoutY = (620 - layoutHeight)/2;

    
    UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(layoutX,layoutY,layoutWith,layoutHeight)];
    [imageView2 initWithImage:image4];
    imageView2.tag = (long)image4;
    imageView2.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(LargeImage:)];
    [imageView2 addGestureRecognizer:singleTap];
    [singleTap release];
    [dwView1 addSubview:label1];
    [dwView1 addSubview:label2];
    [dwView1 bringSubviewToFront:imageView2];
    [dwView1 setUserInteractionEnabled:YES];
    
    if (image4) {
        [dwView1 addSubview:imageView2];
    }
    [dwView1 addSubview:[self AddDictionaryLable:word]];
    
    
    int record;
    if ((viewNum - dwNum) >=12) {//添加到第五个单词后面
        UIView *view = [viewArray objectAtIndex:dwNum +10];
        NSArray *views = [view subviews];
        if ([views count] == 3) {
            [viewArray insertObject:dwView1 atIndex:dwNum+12];
            [self.view addSubview:dwView1];
            [isAnsArray insertObject:@"1" atIndex:dwNum+12];
            record=dwNum+12;
        }else{
            [viewArray insertObject:dwView1 atIndex:dwNum+11];
            [self.view addSubview:dwView1];
            [isAnsArray insertObject:@"1" atIndex:dwNum+11];
            record=dwNum+11;
        }
    }else{
        record = viewNum;
        [viewArray insertObject:dwView1 atIndex:viewNum];
        [self.view addSubview:dwView1];
        [isAnsArray insertObject:@"1" atIndex:viewNum];
    }
    viewNum +=1;
    
    //体型d的答案界面
    UIView *dwView2=[[UIView alloc] initWithFrame:CGRectMake(20, 50, 975.0, 620)];
    UIImage*img2 =[UIImage imageNamed:@"remember_words_background"];
    [dwView2 setBackgroundColor:[UIColor colorWithPatternImage:img2]];
    dwView2.tag = (int)word;
    
    UILabel *label3 = [[UILabel alloc]initWithFrame:CGRectMake(40.0, 40.0, 950.0, 100.0)];
    label3.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label3.textAlignment = UITextAlignmentLeft;
    label3.adjustsFontSizeToFitWidth = YES;
    label3.font = [UIFont fontWithName:@"Helvetica" size:30];
    label3.textColor = [UIColor blackColor];
    label3.lineBreakMode = UILineBreakModeWordWrap;
    label3.numberOfLines = 0;
    NSString *wordString2 = [word.word_a_explan1 stringByReplacingOccurrencesOfString:@"@$" withString:@"\n"];
    label3.text = wordString2;
    
//    UILabel *label4 = [[UILabel alloc]initWithFrame:CGRectMake(40.0, 100.0, 950.0, 30.0)];
//    label4.backgroundColor = [UIColor clearColor]; //可以去掉背景色
//    label4.textAlignment = UITextAlignmentLeft;
//    label4.adjustsFontSizeToFitWidth = YES;
//    label4.font = [UIFont fontWithName:@"Helvetica" size:20];
//    label4.textColor = [UIColor blackColor];
//    NSString *wordString3 = [word.word_a_explan2 stringByReplacingOccurrencesOfString:@"@$" withString:@"\n"];
//    label4.text = wordString3;
    
    if (wrongNum ==1) {
        UILabel *label5 = [[UILabel alloc]initWithFrame:CGRectMake(40.0, 140.0, 950.0, 30.0)];
        label5.backgroundColor = [UIColor clearColor]; //可以去掉背景色
        label5.textAlignment = UITextAlignmentCenter;
        label5.adjustsFontSizeToFitWidth = YES;
        label5.font = [UIFont fontWithName:@"Helvetica" size:20];
        label5.textColor = [UIColor blackColor];
        NSArray *array = [word.word_a_explan2 componentsSeparatedByString:@"@$"]; //从字符A中分隔成2个元素的数组
        NSString *newString = [word.word_a_explan2 stringByReplacingOccurrencesOfString:@"@$" withString:@"\n"];
        label5.text = newString;
        [dwView2 addSubview:label5];
    }else if(wrongNum >= 2){
        UILabel *label5 = [[UILabel alloc]initWithFrame:CGRectMake(40.0, 140.0, 950.0, 30.0)];
        label5.backgroundColor = [UIColor clearColor]; //可以去掉背景色
        label5.textAlignment = UITextAlignmentCenter;
        label5.adjustsFontSizeToFitWidth = YES;
        label5.font = [UIFont fontWithName:@"Helvetica" size:20];
        label5.textColor = [UIColor blackColor];
        NSString *newString = [word.word_a_explan2 stringByReplacingOccurrencesOfString:@"@$" withString:@"\n"];
        label5.text = newString;
        [dwView2 addSubview:label5];
        
        UILabel *label6 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 170.0, 950.0, 30.0)];
        label6.backgroundColor = [UIColor clearColor]; //可以去掉背景色
        label6.textAlignment = UITextAlignmentCenter;
        label6.adjustsFontSizeToFitWidth = YES;
        label6.font = [UIFont fontWithName:@"Helvetica" size:20];
        label6.textColor = [UIColor blackColor];
        NSString *newString2 = [word.word_a_explan3 stringByReplacingOccurrencesOfString:@"@$" withString:@"\n"];
        label6.text = newString2;
        [dwView2 addSubview:label6];
    }

    UIImage *image = [self loadImage:word.word_d_picture2];
    
    int layoutWith2 = image.size.width;
    int layoutHeight2 = image.size.height;
    
    if (layoutWith2 > 700) {
        layoutWith2 = 700;
        layoutHeight2 = layoutHeight2*700/layoutWith2;
    }else if(layoutHeight2 > 450){
        layoutHeight2 = 450;
        layoutWith2 = layoutWith2*450/layoutHeight2;
    }
    int layoutX2 = (975.0 - layoutWith2)/2;
    int layoutY2 = (620 - layoutHeight2)/2;

    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(layoutX2,layoutY2,layoutWith2,layoutHeight2)];
    [imageView initWithImage:image];
    imageView.tag = (long)image;
    imageView.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(LargeImage:)];
    [imageView addGestureRecognizer:singleTap2];
    [singleTap2 release];
    [dwView2 bringSubviewToFront:imageView];
    [dwView2 setUserInteractionEnabled:YES];
    
    UIButton *Btn1=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    Btn1.frame=CGRectMake(10,565,200,43);
    UIImage*btnImage =[UIImage imageNamed:@"memory_easy"];
    [Btn1 setBackgroundImage:btnImage forState:UIControlStateNormal];
    Btn1.tag=(long)word;
    [Btn1 addTarget:self action:@selector(rightAnsware:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *Btn2=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    Btn2.frame=CGRectMake(762,565,200,43);
    Btn2.tag=(long)word;
    UIImage*btnImage2 =[UIImage imageNamed:@"memory_i_know"];
    [Btn2 setBackgroundImage:btnImage2 forState:UIControlStateNormal];
    [Btn2 addTarget:self action:@selector(wrongAnsware:) forControlEvents:UIControlEventTouchUpInside];
    
    [dwView2 addSubview:label3];
//    [dwView2 addSubview:label4];
    if (image) {
        [dwView2 addSubview:imageView];
    }
    [dwView2 addSubview:[self AddDictionaryLable2:word]];
    [dwView2 addSubview:Btn1];
    [dwView2 addSubview:Btn2];
    [viewArray insertObject:dwView2 atIndex:record+1];
    [self.view addSubview:dwView2];
    [isAnsArray insertObject:@"1" atIndex:record+1];
    viewNum +=1;
    
    //第二次添加
    UIView *ansView1=[[UIView alloc] initWithFrame:CGRectMake(20, 50, 975.0, 620)];
    UIImage*img3 =[UIImage imageNamed:@"remember_words_background"];
    [ansView1 setBackgroundColor:[UIColor colorWithPatternImage:img3]];
    ansView1.tag = (long)word;
    
    UILabel *label11 = [[UILabel alloc]initWithFrame:CGRectMake(40.0, 40.0, 900.0, 100.0)];
    label11.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label11.textAlignment = UITextAlignmentLeft;
    label11.font = [UIFont fontWithName:@"Helvetica" size:30];
    label11.textColor = [UIColor blackColor];
    label11.adjustsFontSizeToFitWidth = YES;
    NSString *wordString11 = [word.word_word stringByReplacingOccurrencesOfString:@"@$" withString:@"\n"];
    label11.text = wordString11;
    
    UILabel *label22 = [[UILabel alloc]initWithFrame:CGRectMake(40.0, 140.0, 950.0, 30.0)];
    label22.adjustsFontSizeToFitWidth = YES;
    if (![word.word_word_class compare:@""]==NSOrderedSame) {
        NSString *text = [NSString stringWithFormat:@"%@.",word.word_word_class];
        label22.text = text;
    }
    label22.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label22.textAlignment = UITextAlignmentLeft;
    label22.font = [UIFont fontWithName:@"Helvetica" size:20];
    label22.textColor = [UIColor blackColor];
    
    UIImage *image3 = [self loadImage:word.word_d_picture1];
    int layoutWith3 = image3.size.width;
    int layoutHeight3 = image3.size.height;
    
    if (layoutWith3 > 700) {
        layoutWith3 = 700;
        layoutHeight3 = layoutHeight3*700/layoutWith3;
    }else if(layoutHeight3 > 450){
        layoutHeight3 = 450;
        layoutWith3 = layoutWith3*450/layoutHeight3;
    }
    int layoutX3 = (975.0 - layoutWith3)/2;
    int layoutY3 = (620 - layoutHeight3)/2;
    
    UIImageView *imageView22 = [[UIImageView alloc] initWithFrame:CGRectMake(layoutX3,layoutY3,layoutWith3,layoutHeight3)];
    [imageView22 initWithImage:image3];
    imageView22.tag = (long)image3;
    imageView22.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(LargeImage:)];
    [imageView22 addGestureRecognizer:singleTap3];
    [singleTap3 release];
    [ansView1 addSubview:label11];
    [ansView1 addSubview:label22];
    [ansView1 bringSubviewToFront:imageView22];
    [ansView1 setUserInteractionEnabled:YES];
    
    if (image3) {
        [ansView1 addSubview:imageView2];
    }
    [ansView1 addSubview:[self AddDictionaryLable:word]];
    
    int record2;
    if ((viewNum - dwNum)>=24) {
        UIView *view = [viewArray objectAtIndex:dwNum+22];
        NSArray *views = [view subviews];
        if ([views count] == 3) {
            [viewArray insertObject:ansView1 atIndex:dwNum+24];
            [self.view addSubview:ansView1];
            [isAnsArray insertObject:@"1" atIndex:dwNum+24];
            record2 = dwNum+24;
        }else{
            [viewArray insertObject:ansView1 atIndex:dwNum+23];
            [self.view addSubview:ansView1];
            [isAnsArray insertObject:@"1" atIndex:dwNum+23];
            record2 = dwNum+23;
        }
        
    }else{
        record2 = viewNum;
        [viewArray insertObject:ansView1 atIndex:viewNum];
        [self.view addSubview:ansView1];
        [isAnsArray insertObject:@"1" atIndex:viewNum];
    }
    viewNum +=1;
    
    
    //体型d的答案界面
    UIView *ansView2=[[UIView alloc] initWithFrame:CGRectMake(20, 50, 975.0, 620)];
    UIImage*img4 =[UIImage imageNamed:@"remember_words_background"];
    [ansView2 setBackgroundColor:[UIColor colorWithPatternImage:img4]];
    ansView2.tag = (long)word;
    
    UILabel *label33 = [[UILabel alloc]initWithFrame:CGRectMake(40.0, 40.0, 950.0, 100.0)];
    label33.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    label33.textAlignment = UITextAlignmentLeft;
    label33.adjustsFontSizeToFitWidth = YES;
    label33.font = [UIFont fontWithName:@"Helvetica" size:30];
    label33.textColor = [UIColor blackColor];
    NSArray *array3 = [word.word_a_explan1 componentsSeparatedByString:@"@$"]; //从字符A中分隔成2个元素的数组
    NSString *newString3 = [[NSString alloc]init];
    for (int i = 0; i < [array3 count]; i++) {
        newString3 = [NSString stringWithFormat:@"%@%@",newString3,[array3 objectAtIndex:i]];
    }
    label33.text = newString3;
    
//    UILabel *label44 = [[UILabel alloc]initWithFrame:CGRectMake(40.0, 100.0, 950.0, 40.0)];
//    label44.backgroundColor = [UIColor clearColor]; //可以去掉背景色
//    label44.textAlignment = UITextAlignmentLeft;
//    label44.adjustsFontSizeToFitWidth = YES;
//    label44.font = [UIFont fontWithName:@"Helvetica" size:20];
//    label44.textColor = [UIColor blackColor];
//    NSArray *array4 = [word.word_a_explan2 componentsSeparatedByString:@"@$"]; //从字符A中分隔成2个元素的数组
//    NSString *newString4 = [[NSString alloc]init];
//    for (int i = 0; i < [array4 count]; i++) {
//        newString4 = [NSString stringWithFormat:@"%@%@",newString4,[array4 objectAtIndex:i]];
//    }
//    label44.text = newString4;
    
    if (wrongNum ==1) {
        UILabel *label5 = [[UILabel alloc]initWithFrame:CGRectMake(40.0, 140.0, 950.0, 30.0)];
        label5.backgroundColor = [UIColor clearColor]; //可以去掉背景色
        label5.textAlignment = UITextAlignmentCenter;
        label5.adjustsFontSizeToFitWidth = YES;
        label5.font = [UIFont fontWithName:@"Helvetica" size:20];
        label5.textColor = [UIColor blackColor];
        NSArray *array = [word.word_a_explan2 componentsSeparatedByString:@"@$"]; //从字符A中分隔成2个元素的数组
        NSString *newString = [[NSString alloc]init];
        for (int i = 0; i < [array count]; i++) {
            newString = [NSString stringWithFormat:@"%@%@",newString,[array objectAtIndex:i]];
        }
        label5.text = newString;
        [ansView2 addSubview:label5];
    }else if(wrongNum >= 2){
        UILabel *label5 = [[UILabel alloc]initWithFrame:CGRectMake(40.0, 140.0, 950.0, 30.0)];
        label5.backgroundColor = [UIColor clearColor]; //可以去掉背景色
        label5.textAlignment = UITextAlignmentCenter;
        label5.adjustsFontSizeToFitWidth = YES;
        label5.font = [UIFont fontWithName:@"Helvetica" size:20];
        label5.textColor = [UIColor blackColor];
        NSArray *array = [word.word_a_explan2 componentsSeparatedByString:@"@$"]; //从字符A中分隔成2个元素的数组
        NSString *newString = [[NSString alloc]init];
        for (int i = 0; i < [array count]; i++) {
            newString = [NSString stringWithFormat:@"%@%@",newString,[array objectAtIndex:i]];
        }
        label5.text = newString;
        [ansView2 addSubview:label5];
        
        UILabel *label6 = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 170.0, 950.0, 30.0)];
        label6.backgroundColor = [UIColor clearColor]; //可以去掉背景色
        label6.textAlignment = UITextAlignmentCenter;
        label6.adjustsFontSizeToFitWidth = YES;
        label6.font = [UIFont fontWithName:@"Helvetica" size:20];
        label6.textColor = [UIColor blackColor];
        NSArray *array2 = [word.word_a_explan3 componentsSeparatedByString:@"@$"]; //从字符A中分隔成2个元素的数组
        NSString *newString2 = [[NSString alloc]init];
        for (int i = 0; i < [array count]; i++) {
            newString2 = [NSString stringWithFormat:@"%@%@",newString2,[array2 objectAtIndex:i]];
        }
        label6.text = newString2;
        [ansView2 addSubview:label6];
    }
    
    UIImage *image2 = [self loadImage:word.word_d_picture2];
    
    int layoutWith4 = image2.size.width;
    int layoutHeight4 = image2.size.height;
    
    if (layoutWith4 > 700) {
        layoutWith4 = 700;
        layoutHeight4 = layoutHeight4*700/layoutWith4;
    }else if(layoutHeight4 > 450){
        layoutHeight4 = 450;
        layoutWith4 = layoutWith4*450/layoutHeight4;
    }
    int layoutX4 = (975.0 - layoutWith4)/2;
    int layoutY4 = (620 - layoutHeight4)/2;
    
    UIImageView *imageView4 = [[UIImageView alloc] initWithFrame:CGRectMake(layoutX4,layoutY4,layoutWith4,layoutHeight4)];
    [imageView4 initWithImage:image2];
    imageView4.tag = (long)image2;
    imageView4.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(LargeImage:)];
    [imageView4 addGestureRecognizer:singleTap4];
    [singleTap4 release];
    [ansView2 bringSubviewToFront:imageView4];
    [ansView2 setUserInteractionEnabled:YES];
    
    UIButton *Btn11=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    Btn11.frame=CGRectMake(10,565,200,43);
    UIImage*btnImage3 =[UIImage imageNamed:@"memory_easy"];
    [Btn11 setBackgroundImage:btnImage3 forState:UIControlStateNormal];
    Btn11.tag=(long)word;
    [Btn11 addTarget:self action:@selector(rightAnsware:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *Btn22=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    Btn22.frame=CGRectMake(762,565,200,43);
    Btn22.tag=(long)word;
    UIImage*btnImage4 =[UIImage imageNamed:@"memory_i_know"];
    [Btn22 setBackgroundImage:btnImage4 forState:UIControlStateNormal];
    [Btn22 addTarget:self action:@selector(wrongAnsware:) forControlEvents:UIControlEventTouchUpInside];
    
    [ansView2 addSubview:label33];
//    [ansView2 addSubview:label44];
    if (image2) {
        [ansView2 addSubview:imageView];
    }
    [ansView2 addSubview:[self AddDictionaryLable2:word]];
    [ansView2 addSubview:Btn11];
    [ansView2 addSubview:Btn22];
    [viewArray insertObject:ansView2 atIndex:record2+1];
    [self.view addSubview:ansView2];
    [isAnsArray insertObject:@"1" atIndex:record2+1];
    
    viewNum +=1;
}

//-----------A和D型题回答正确后处理
-(void)rightAnsware:(id) sender{
    [isAnsArray replaceObjectAtIndex:dwNum withObject:@"1"];
    Word *word = (Word *)[sender tag];
    NSString *text = [NSString stringWithFormat:@"%@",word.word_id];
    NSString *user_id = (NSString*)[tool readObkect:@"user_id"];
    [database wordDayPlus:text howManyPlus:@"1" userId:user_id];
    [self AnimationToLeft];
    
}

//-----------A型题回答错误处理
-(void)wrongAnsware:(id) sender{
    NSLog(@"viewNum = %d; dwNUm = %d; 数组viewArray = %d; 数组answareArray = %d",viewNum,dwNum,[viewArray count],[isAnsArray count]);
    [isAnsArray replaceObjectAtIndex:dwNum withObject:@"1"];//设置可以翻页
    //获得错误的单词
    Word *word = (Word *)[sender tag];
    //更新当日学习单词数组，并且保存
    [self updeateWordArray:word];
    //更新viewArray数组，同时更新viewNum
    [self updateWrongWordView:word];
    //界面跳转
    [self AnimationToLeft];
}

//-----------D型题错误处理事件
-(void)D_wrongAnsware:(id) sender{
    [isAnsArray replaceObjectAtIndex:dwNum withObject:@"1"];//设置可以翻页
    //获得错误的单词
    Word *word = (Word *)[sender tag];
    //更新当日学习单词数组，并且保存
    [self updeateWordArray:word];
    //更新viewArray数组，同时更新viewNum
    [self updateWrongWordView:word];
    //界面跳转
    [self AnimationToLeft];
}

// B型题选择后的处理
-(void)buttonClickB:(id)sender{
    [isAnsArray replaceObjectAtIndex:dwNum withObject:@"1"];//设置可以翻页
    wrongWords *getWord = (wrongWords *)[sender tag];//得到结构
    Word *word = getWord.myword;//得到单词
    
    UIView *view = [viewArray objectAtIndex:dwNum];
    NSArray *ui = [view subviews];
    int i = [getWord.randomTag intValue];//正确答案是第几个
    int j;
    
    switch (i) {
        case 0:
            //标记选择的答案
            if ([getWord.answare compare:word.word_b_explana]==NSOrderedSame) {
                j=1;
            }else if([getWord.answare compare:word.word_b_explanb]==NSOrderedSame){
                j=2;
            }else if([getWord.answare compare:word.word_b_explanc]==NSOrderedSame){
                j=3;
            }else if([getWord.answare compare:word.word_b_expland]==NSOrderedSame){
                j=4;
            }else {
                j=5;
            }
            break;
        case 1:
            //标记选择的答案
            if ([getWord.answare compare:word.word_b_explanb]==NSOrderedSame) {
                j=1;
            }else if([getWord.answare compare:word.word_b_explana]==NSOrderedSame){
                j=2;
            }else if([getWord.answare compare:word.word_b_explanc]==NSOrderedSame){
                j=3;
            }else if([getWord.answare compare:word.word_b_expland]==NSOrderedSame){
                j=4;
            }else {
                j=5;
            }
            break;
        case 2:
            //标记选择的答案
            if ([getWord.answare compare:word.word_b_explanc]==NSOrderedSame) {
                j=1;
            }else if([getWord.answare compare:word.word_b_explanb]==NSOrderedSame){
                j=2;
            }else if([getWord.answare compare:word.word_b_explana]==NSOrderedSame){
                j=3;
            }else if([getWord.answare compare:word.word_b_expland]==NSOrderedSame){
                j=4;
            }else {
                j=5;
            }
            break;
        case 3:
            //标记选择的答案
            if ([getWord.answare compare:word.word_b_expland]==NSOrderedSame) {
                j=1;
            }else if([getWord.answare compare:word.word_b_explanb]==NSOrderedSame){
                j=2;
            }else if([getWord.answare compare:word.word_b_explanc]==NSOrderedSame){
                j=3;
            }else if([getWord.answare compare:word.word_b_explane]==NSOrderedSame){
                j=4;
            }else {
                j=5;
            }
            break;
        case 4:
            //标记选择的答案
            if ([getWord.answare compare:word.word_b_explane]==NSOrderedSame) {
                j=1;
            }else if([getWord.answare compare:word.word_b_explanb]==NSOrderedSame){
                j=2;
            }else if([getWord.answare compare:word.word_b_explanc]==NSOrderedSame){
                j=3;
            }else if([getWord.answare compare:word.word_b_expland]==NSOrderedSame){
                j=4;
            }else {
                j=5;
            }
            break;
    }

    //将错误答案显示为红色
    UIButton *la2 = [ui objectAtIndex:j+1];
    [la2 setBackgroundColor:[UIColor redColor]];
    if ([[la2.titleLabel.text substringFromIndex:5] compare:word.word_b_right]==NSOrderedSame) {
        
    }else{
        [la2 addSubview:[self wrong]];
    }
    //将正确答案显示为绿色
    if (word.word_b_explane.length == 0) {
        for (int n =0; n <4; n++) {
            UIButton *bt = [ui objectAtIndex:n+2];
            NSString *title = [bt.titleLabel.text substringFromIndex:5];
            if ([title compare:word.word_b_right]==NSOrderedSame) {
                [bt setBackgroundColor:[UIColor greenColor]];
                [bt addSubview:[self right]];
            }
        }
    }else{
        for (int n =0; n <5; n++) {
            UIButton *bt = [ui objectAtIndex:n+2];
            NSString *title = [bt.titleLabel.text substringFromIndex:5];
            if ([title compare:word.word_b_right]==NSOrderedSame) {
                [bt setBackgroundColor:[UIColor greenColor]];
                [bt addSubview:[self right]];
            }
        }
    }
    
    NSTimer *timer;//选择后睡眠2秒
    
    UIView *view2 = [viewArray objectAtIndex:dwNum];
    NSArray *ui2 = [view2 subviews];
    //隐藏显示正确答案button
    UIButton *la22 = [ui objectAtIndex:[ui2 count]-2];
    [la22 setHidden:YES];
    
    // 选择一次后就不能再次被点击
    if ([ui count] == 8) {
        for (int h = 2 ; h < 6; h++) {
            UIButton *but = [ui objectAtIndex:h];
            [but setUserInteractionEnabled:NO];
        }
        UIButton *but = [ui objectAtIndex:7];
        [but setHidden:YES];
    }else if([ui count] == 9){
        for (int h = 2 ; h < 7; h++) {
            UIButton *but = [ui objectAtIndex:h];
            [but setUserInteractionEnabled:NO];
        }
        UIButton *but = [ui objectAtIndex:8];
        [but setHidden:YES];
    }
    
    NSLog(@"%@",word.word_analyze);
    if (word.word_analyze.length != 0) {
        // 将答案解析显示出来
        UILabel *analyze = [ui objectAtIndex:[ui count]-3];
        analyze.userInteractionEnabled = YES;
        [analyze setHidden:NO];
    }
    timer = [NSTimer scheduledTimerWithTimeInterval: 2.0
                                             target: self
                                           selector: @selector(handleTimer:)
                                           userInfo: getWord  
                                            repeats: NO];
}

-(void)handleTimer:(NSTimer *)sender{
    wrongWords *wrong = sender.userInfo;
    Word *word = wrong.myword;//得到单词
    //如果回答正确
    if ([wrong.answare compare:word.word_b_right]==NSOrderedSame) {
        
        NSString *text = [NSString stringWithFormat:@"%@",word.word_id];
        NSString *user_id = (NSString*)[tool readObkect:@"user_id"];
        [database wordDayPlus:text howManyPlus:@"1" userId:user_id];
        [self AnimationToLeft];
    }else{//如果回答错误
        b_word = [[Word alloc]init];
        b_word = word;
        b = true; // 将插入操作放到翻页中处理
        
        //更新当日学习单词数组，并且保存
//        [self updeateWordArray:word];
        //更新viewArray数组，同时更新viewNum
//        [self updateWrongWordView:word];
//        [self AnimationToLeft];
        //界面跳转
    }
}

// B型题选择我不知道后处理
-(void)buttonClickBWrong:(id)sender{
    [sender setHidden:true];
    [isAnsArray replaceObjectAtIndex:dwNum withObject:@"1"];//设置可以翻页
    wrongWords *getWord = (wrongWords *)[sender tag];//得到结构
    Word *word = getWord.myword;
    UIView *view = [viewArray objectAtIndex:dwNum];
    NSArray *ui = [view subviews];
    
    //将正确答案显示为红色
    int i = [getWord.randomTag intValue];//正确答案是第几个
    UIButton *la = [ui objectAtIndex:i+2];
    [la setBackgroundColor:[UIColor greenColor]];
    
    // 选择一次后就不能再次被点击
    if ([ui count] == 8) {
        for (int h = 2 ; h < 6; h++) {
            UIButton *but = [ui objectAtIndex:h];
            [but setUserInteractionEnabled:NO];
        }
        UIButton *but = [ui objectAtIndex:9];
        [but setHidden:YES];
    }else if([ui count] == 9){
        for (int h = 2 ; h < 7; h++) {
            UIButton *but = [ui objectAtIndex:h];
            [but setUserInteractionEnabled:NO];
        }
        UIButton *but = [ui objectAtIndex:8];
        [but setHidden:YES];
    }
    
    b_word = [[Word alloc]init];
    b_word = word;
    b = true; // 将插入操作放到翻页中处理
    
    if (word.word_analyze.length != 0) {
        // 将答案解析显示出来
        UILabel *analyze = [ui objectAtIndex:[ui count]-3];
        [analyze setHidden:NO];
    }
//    //更新viewArray数组，同时更新viewNum
//    [self updateWrongWordView:word];
    
    //更新当日学习单词数组，并且保存
//    [self updeateWordArray:word];
    
//    [self AnimationToLeft];
}

// C选择我不知道后的处理
-(void)buttonClickC:(id)sender{
    [sender setHidden:YES];
    [isAnsArray replaceObjectAtIndex:dwNum withObject:@"1"];//设置可以翻页
    NSLog(@"%d",dwNum);
    QuestionC *qc = (QuestionC *)[sender tag];
    Word *word = qc.wordC;
    
    UIView *view = [viewArray objectAtIndex:dwNum];
    NSArray *ui = [view subviews];
    UILabel *text = [ui objectAtIndex:2];
    text.text = word.word_c_answer;
    text.textColor = [UIColor redColor];
    
    c_word = [[Word alloc]init];
    c_word = word;
    c = true; // 将插入操作放到翻页中处理
    
    // 将答案解析显示出来
    UILabel *analyze = [ui objectAtIndex:[ui count]-3];
    [analyze setHidden:NO];
    
    //更新当日学习单词数组，并且保存
//    [self updeateWordArray:word];
    
    //更新viewArray数组，同时更新viewNum
//    [self updateWrongWordView:word];
//    textFiled.text = word.word_c_answer;
    //界面跳转
//    [self AnimationToLeft];
    
    
}

// C答案对textField的监听
-(void) textFieldEnd:(UITextField *) getTextField2{
    NSTimer *timer;//选择后睡眠2秒
    timer = [NSTimer scheduledTimerWithTimeInterval: 1.0
                                             target: self
                                           selector: @selector(CansRight:)
                                           userInfo: getTextField2
                                            repeats: NO];
}

-(void)CansRight:(NSTimer *)sender{
    UITextField *textfield = (UITextField*)[sender userInfo];
    Word *word = (Word *)[textfield tag];
    if(word==nil){
        NSLog(@"CansRight word nil return");
        return;
    }
    if ([word.word_c_answer compare:textfield.text]==NSOrderedSame) {
        NSString *text = [NSString stringWithFormat:@"%@",word.word_id];
        pubuser =[[User alloc]init];
        NSString *user_id = (NSString*)[tool readObkect:@"user_id"];
        [database wordDayPlus:text howManyPlus:@"1" userId:user_id];
        textfield.text = word.word_c_answer;
        textfield.textColor = [UIColor greenColor];
        [isAnsArray replaceObjectAtIndex:dwNum withObject:@"1"];//设置可以翻页
        [self AnimationToLeft];
        //查询单词目前的天数
        NSString *days = [database getWordDay:word.word_id userId:pubuser.user_id];
        int worday =0;
        @try {
            worday = [days intValue];

        }
        @catch (NSException *exception) {
            NSLog(@"error 1");
        }

    }else{
        UIView *view = [viewArray objectAtIndex:dwNum];
        NSArray *ui = [view subviews];
        UILabel *text = [ui objectAtIndex:2];
        text.text = word.word_c_answer;
        text.textColor = [UIColor redColor];
        
        // 将答案解析显示出来
        UILabel *analyze = [ui objectAtIndex:[ui count]-3];
        [analyze setHidden:NO];
        
        // 隐藏显示答案
        UIButton *showAnsware = [ui objectAtIndex:[ui count]-2];
        [showAnsware setHidden:YES];
        
        NSTimer *timer;//选择后睡眠2秒
        timer = [NSTimer scheduledTimerWithTimeInterval: 1.0
                                                 target: self
                                               selector: @selector(CansRight2:)
                                               userInfo: word
                                                repeats: NO];
    }
   }

-(void)CansRight2:(NSTimer *)sender{
    [isAnsArray replaceObjectAtIndex:dwNum withObject:@"1"];//设置可以翻页
    NSLog(@"%d",dwNum);
    Word *word = (Word*)[sender userInfo];
//    //更新当日学习单词数组，并且保存
//    [self updeateWordArray:word];
//    //更新viewArray数组，同时更新viewNum
//    [self updateWrongWordView:word];
    c_word = [[Word alloc]init];
    c_word = word;
    c = true;
}

// 键盘输入时自动调整屏幕高度
-(void)moveView:(UITextField *)textField leaveView:(BOOL)leave{
    float screenHeight = 1496; //屏幕尺寸，如果屏幕允许旋转，可根据旋转动态调整
    float keyboardHeight = 748; //键盘尺寸，如果屏幕允许旋转，可根据旋转动态调整
    float statusBarHeight,NavBarHeight,tableCellHeight,textFieldOriginY,textFieldFromButtomHeigth;
    int margin;
    statusBarHeight = [[UIApplication sharedApplication] statusBarFrame].size.height; //屏幕状态栏高度
    NavBarHeight = self.navigationController.navigationBar.frame.size.height; //获取导航栏高度
    
    UITableViewCell *tableViewCell=(UITableViewCell *)textField.superview;
    tableCellHeight = tableViewCell.frame.size.height; //获取单元格高度
    
    CGRect fieldFrame=[self.view convertRect:textField.frame fromView:tableViewCell];
    textFieldOriginY = fieldFrame.origin.y; //获取文本框相对本视图的y轴位置。
    
    //计算文本框到屏幕底部的高度（屏幕高度-顶部状态栏高度-导航栏高度-文本框的的相对y轴位置-单元格高度）
    textFieldFromButtomHeigth = screenHeight - statusBarHeight - NavBarHeight - textFieldOriginY - tableCellHeight;
    
    if(!leave) {
        if(textFieldFromButtomHeigth < keyboardHeight) { //如果文本框到屏幕底部的高度 < 键盘高度
            margin = keyboardHeight - textFieldFromButtomHeigth; // 则计算差距
            keyBoardMargin_ = margin; //keyBoardMargin_ 为成员变量，记录上一次移动的间距,用户离开文本时恢复视图高度
        } else {
            margin= 0;
            keyBoardMargin_ = 0;
        }
    }
    
    float movementDuration = 0.3f; // 动画时间
    
    int movement; //进入时根据差距移动视图，离开时恢复之前的高度
    UIInterfaceOrientation deviceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (deviceOrientation == 3) {
        movement = (leave ? keyBoardMargin_ : -margin); //进入时根据差距移动视图，离开时恢复之前的高度
    }else{
        movement = (leave ? -keyBoardMargin_ : margin); //进入时根据差距移动视图，离开时恢复之前的高度
    }

    [UIView beginAnimations: @"textFieldAnim" context: nil]; //添加动画
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, -movement/8, 0);
    [UIView commitAnimations];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    [self moveView:textField leaveView:NO];
}

-(void)textFieldDidEndEditing:(UITextField *)textField;{
    [self moveView:textField leaveView:YES];
}

// 点击后图片放大
-(void)LargeImage:(id)sender{
    UIImage *image = [(UIGestureRecognizer *)sender view].tag;
    int layoutWidth = image.size.width;
    int layoutHeight = image.size.height;
    if (layoutHeight > layoutWidth) {
        layoutWidth = 748*layoutWidth/layoutHeight;
        layoutHeight = 748;
    }else{
        layoutHeight = 1024*layoutHeight/layoutWidth;
        layoutWidth = 1024;
    }
    int layoutX = (1024 - layoutWidth)/2;
    int layoutY = (748 - layoutHeight)/2;
    bigImage = [[UIImageView alloc] initWithFrame:CGRectMake(layoutX,layoutY,layoutWidth,layoutHeight)];
    [bigImage initWithImage:image];
    
    bigImage.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(UesrClicked:)];
    [bigImage addGestureRecognizer:singleTap];
    [singleTap release];
    NSArray *selfViewArray = [self.view subviews];
    for (int i =0; i <[selfViewArray count]; i++) {
        [[selfViewArray objectAtIndex:i] setHidden:YES];
    }
    [self.view addSubview:bigImage];
}

// 取消图片放大
-(void)UesrClicked:(id)sender{
    [bigImage removeFromSuperview];
    NSArray *selfViewArray = [self.view subviews];
    for (int i =0; i <[selfViewArray count]; i++) {
        [[selfViewArray objectAtIndex:i] setHidden:NO];
    }
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
	return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    [database release];
    database = nil;
    [isAnsArray release];
    isAnsArray = nil;
    [tool release];
    tool = nil;
    [myView release];
    myView = nil;
    [bigImage release];
    bigImage = nil;
//    [wordsArray release];
//    wordsArray = nil;
    [GoOver release];
    GoOver = nil;
//    [wrongWordArray release];
    wordsArray = nil;
    [processing release];
    processing = nil;
    [processText release];
    processText = nil;
    [avAudioPlayer release];
    avAudioPlayer = nil;
    [pubuser release];
    pubuser = nil;
    [user release];
    user = nil;
//    [viewArray release];
//    viewArray = nil;
}

-(void)viewDidUnload{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    [database release];
    database = nil;
    [isAnsArray release];
    isAnsArray = nil;
    [tool release];
    tool = nil;
    [myView release];
    myView = nil;
    [bigImage release];
    bigImage = nil;
//    [wordsArray release];
    wordsArray = nil;
    [GoOver release];
    GoOver = nil;
//    [wrongWordArray release];
    wordsArray = nil;
    [processing release];
    processing = nil;
    [processText release];
    processText = nil;
    [avAudioPlayer release];
    avAudioPlayer = nil;
    [pubuser release];
    pubuser = nil;
    [user release];
    user = nil;
    [super dealloc];
    
}

-(void)dealloc {
    [database release];
    database = nil;
    [isAnsArray release];
    isAnsArray = nil;
    [tool release];
    tool = nil;
    [myView release];
    myView = nil;
    [bigImage release];
    bigImage = nil;
//    [wordsArray release];
    wordsArray = nil;
    [GoOver release];
    GoOver = nil;
//    [wrongWordArray release];
    wordsArray = nil;
    [processing release];
    processing = nil;
    [processText release];
    processText = nil;
    [avAudioPlayer release];
    avAudioPlayer = nil;
    [pubuser release];
    pubuser = nil;
    [user release];
    user = nil;
    [super dealloc];
}

-(UIImageView*)right{
    UIImage *image =[UIImage imageNamed:@"choice_right"];
    
    UIImageView *rightImageView = [[UIImageView alloc] initWithFrame:CGRectMake(800,5,50,50)];
    [rightImageView initWithImage:image];
    return rightImageView;
}

-(UIImageView*)wrong{
    UIImage *image =[UIImage imageNamed:@"choice_wrong"];
    
    UIImageView *rightImageView = [[UIImageView alloc] initWithFrame:CGRectMake(800,5,50,50)];
    [rightImageView initWithImage:image];
    return rightImageView;
}

@end
