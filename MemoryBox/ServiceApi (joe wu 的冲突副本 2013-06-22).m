//
//  ServiceApi.m
//  MemoryBox
//
//  Created by firefly on 13-6-12.
//
//

#import "ServiceApi.h"
#import "GDataXMLNode.h"
#import "UserDictionary.h"
#import "Constants.h"

@implementation ServiceApi
//读取XML，返回包含多个NSMutableDictionary的NSMutableArray
- (NSMutableArray *)readXML:(NSData *)data ElementName:(NSString *)EleName{
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:data  options:0 error:nil];
    GDataXMLElement *rootElement = [doc rootElement];
    NSArray *element = [rootElement elementsForName:EleName];
    
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:1];

    
    for (GDataXMLElement *ele in element) {
        NSMutableDictionary *KeyAndValue = [NSMutableDictionary dictionaryWithCapacity:1];

        for (int i=0; i<[ele childCount]; i++) {
            GDataXMLNode *subElement = [ele childAtIndex:i];
            NSString *key = [subElement name];
            NSString *value = [subElement stringValue];

            [KeyAndValue setObject:value forKey:key];
        }
        [array addObject:KeyAndValue];
    }

    return array;
    
}

- (NSMutableArray *) testgetUserHadBuyDictionary:(NSString *)userName{
    NSMutableArray *dics = [NSMutableArray arrayWithCapacity:1];
    NSString *url = [NSString stringWithFormat:@"%@%@%@",ApiUrl,@"getUserDictionary/user_name/",userName];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    NSMutableArray *data = [self readXML:response ElementName:@"dictionary"];
    
    
    
    for (NSMutableDictionary *dic in data) {
        NSArray *allKeys = [dic allKeys];
        for (NSString *key in allKeys) {
            NSString *TheKey = key;
            NSString *TheValue = [dic objectForKey:key];
            NSLog(@"%@:%@",TheKey,TheValue);
        }
        NSLog(@"--------");
    }
    return nil;
}

- (NSMutableArray *) getUserHadBuyDictionary:(NSString *)userName{
    NSMutableArray *dics = [NSMutableArray arrayWithCapacity:1];
    
    
    NSString *url = [NSString stringWithFormat:@"%@%@%@",ApiUrl,@"getUserDictionary/user_name/",userName];
    
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:response  options:0 error:nil];
    GDataXMLElement *rootElement = [doc rootElement];
    
    NSArray *dictionarys = [rootElement elementsForName:@"dictionary"];
    
    for (GDataXMLElement *dic in dictionarys) {
        UserDictionary *tempDic = [[UserDictionary alloc] init];
        
        NSString *dic_id = [[[dic elementsForName:@"dictionary_id"] objectAtIndex:0] stringValue];
        NSString *dic_buy = [[[dic elementsForName:@"buy"] objectAtIndex:0]stringValue];
        NSString *dic_type_id = [[[dic elementsForName:@"dictionary_type_id"] objectAtIndex:0] stringValue];
        NSString *dic_name = [[[dic elementsForName:@"dictionary_name"] objectAtIndex:0]stringValue];
        NSString *dic_abc = [[[dic elementsForName:@"dictionary_abc"] objectAtIndex:0] stringValue];
        NSString *dic_hasVoice = [[[dic elementsForName:@"dictionary_has_voice"] objectAtIndex:0]stringValue];
        NSString *dic_time = [[[dic elementsForName:@"dictionary_time"] objectAtIndex:0] stringValue];
        NSString *dic_hasStick = [[[dic elementsForName:@"dictionary_has_stick"] objectAtIndex:0]stringValue];
        NSString *dic_state = [[[dic elementsForName:@"dictionary_state"] objectAtIndex:0]stringValue];
        NSString *dic_type_name = [[[dic elementsForName:@"dictionary_type_name"] objectAtIndex:0]stringValue];

        tempDic.dictionary_id = dic_id;
        tempDic.buy = dic_buy;
        tempDic.dictionary_type_id = dic_type_id;
        tempDic.dictionary_name = dic_name;
        tempDic.dictionary_abc = dic_abc;
        tempDic.dictionary_has_voice = dic_hasVoice;
        tempDic.dictionary_time = dic_time;
        tempDic.dictionary_has_stick = dic_hasStick;
        tempDic.dictionary_state = dic_state;
        tempDic.dictionary_type_name = dic_type_name;
       
        [dics addObject:tempDic];
    }
    
    return dics;

}

- (BOOL)downloadDictionary:(NSString *)dictionaryUrl{
    ASINetworkQueue   *que = [[ASINetworkQueue alloc] init];
    NSString *name = [NSString stringWithFormat:@"%@.zip",dictionaryUrl];
    dictionaryUrl = [NSString stringWithFormat:@"%@%@.zip",DownloadUrl,dictionaryUrl];
    
    self.netWorkQueue = que;
    
    [que release];
    
    
    
    [self.netWorkQueue reset];
    
    [self.netWorkQueue setShowAccurateProgress:YES];
    
    [self.netWorkQueue go];
    
    //   创建存放路径
    
    //初始化Documents路径
    
    NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    
    //初始化临时文件路径
    
    NSString *folderPath = [path stringByAppendingPathComponent:@"temp"];
    NSLog(@"文件存放：%@",folderPath);
    //创建文件管理器
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    //判断temp文件夹是否存在
    
    BOOL fileExists = [fileManager fileExistsAtPath:folderPath];
    
    if (!fileExists) {//如果不存在说创建,因为下载时,不会自动创建文件夹
        
        [fileManager createDirectoryAtPath:folderPath
         
               withIntermediateDirectories:YES
         
                                attributes:nil
         
                                     error:nil];
        
    }
   
    
    NSLog(@"filePath=%@",dictionaryUrl);
    
    //初始下载路径
    
    NSURL *url = [NSURL URLWithString:dictionaryUrl];
    
    //设置下载路径
    
    ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:url];
    
    //设置ASIHTTPRequest代理
    
    request.delegate = self;
    
    //初始化保存ZIP文件路径
    
    NSString *savePath = [path stringByAppendingPathComponent:name];
    
    //初始化临时文件路径
    
    NSString *tempPath = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"temp/dictionary.zip.temp"]];
    
    //设置文件保存路径
    
    [request setDownloadDestinationPath:savePath];
    
    //设置临时文件路径
    
    [request setTemporaryFileDownloadPath:tempPath];
    
    
    
    //设置进度条的代理,
    
  
    
    //设置是是否支持断点下载
    
    [request setAllowResumeForFileDownloads:NO];
    [request setDelegate:self]; 
    [request setDidFinishSelector:@selector(requestDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    
    
    NSLog(@"UserInfo=%@",request.userInfo);
    
    //添加到ASINetworkQueue队列去下载
    
    [self.netWorkQueue addOperation:request];
    
    //收回request
    
    [request release];
    return YES;
    
}

- (BOOL)upZip:(NSString *)fileName{
    ZipArchive* zip = [[ZipArchive alloc] init];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentpath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    
    NSString* l_zipfile = [documentpath stringByAppendingString:[NSString stringWithFormat:@"/%@.zip",fileName]] ;
    NSString* unzipto = [documentpath stringByAppendingString:[NSString stringWithFormat:@"/%@",fileName]] ;
    
    if( [zip UnzipOpenFile:l_zipfile] )
    {
        BOOL ret = [zip UnzipFileTo:unzipto overWrite:YES];
        
        if( NO==ret )
        {
            return NO;
        }
        [zip UnzipCloseFile];
    }
    return YES;
    [zip release];
}

- (NSMutableArray *)readLocalWordXml:(NSString *)id{
    NSString *paths = [[NSString alloc]initWithFormat:@"%@/%@/word.xml",[self getDocumentsPath],id];
    NSData *data = [NSData dataWithContentsOfFile:paths];
    
    
    NSMutableArray *words = [self readXML:data ElementName:@"word"];
    return words;
    
}

//获取Document目录
- (NSString *)getDocumentsPath{
    NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [path objectAtIndex:0];
    return documentsDir;
    
    //获取临时文件tmp路径的代码：return NSTemporaryDirectory（）；
}
//下载成功回调函数
- (void)requestDone:(ASIHTTPRequest *)request
{
    NSLog(@"下载成功！");
}
//下载失败回调函数  
- (void)requestWentWrong:(ASIHTTPRequest *)request
{
    NSLog(@"下载失败！");
}
@end
