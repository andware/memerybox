//
//  Word.h
//  MemoryBox
//
//  Created by firefly on 13-6-22.
//
//

#define A    @"1"
#define B    @"2"
#define C    @"3"
#define AB   @"12"
#define AC   @"13"
#define BC   @"23"
#define ABC  @"123"

#define d  @"4"
#define ad  @"14"
#define bd  @"24"
#define cd  @"34"
#define abd  @"124"
#define acd  @"134"
#define bcd  @"234"
#define abcd  @"1234"
#import <Foundation/Foundation.h>


@interface Word : NSObject<NSCoding>
@property (nonatomic, retain) NSString *word_id;
@property (nonatomic, retain) NSString *word_word;
@property (nonatomic, retain) NSString *word_symbol;
@property (nonatomic, retain) NSString *word_us_voice1;
@property (nonatomic, retain) NSString *word_us_voice2;
@property (nonatomic, retain) NSString *word_uk_voice1;
@property (nonatomic, retain) NSString *word_uk_voice2;
@property (nonatomic, retain) NSString *word_pircture1;
@property (nonatomic, retain) NSString *word_pircture2;
@property (nonatomic, retain) NSString *word_word_class;
@property (nonatomic, retain) NSString *word_statement;
@property (nonatomic, retain) NSString *word_cn_statement;
@property (nonatomic, retain) NSString *word_a_explan1;
@property (nonatomic, retain) NSString *word_a_explan2;
@property (nonatomic, retain) NSString *word_a_explan3;
@property (nonatomic, retain) NSString *word_b_explana;
@property (nonatomic, retain) NSString *word_b_explanb;
@property (nonatomic, retain) NSString *word_b_explanc;
@property (nonatomic, retain) NSString *word_b_expland;
@property (nonatomic, retain) NSString *word_b_explane;
@property (nonatomic, retain) NSString *word_b_right;
@property (nonatomic, retain) NSString *word_c_explan;
@property (nonatomic, retain) NSString *word_c_answer;
@property (nonatomic, retain) NSString *word_analyze;

@property (nonatomic, retain) NSString *word_d_picture1;
@property (nonatomic, retain) NSString *word_d_picture2;

@property (nonatomic, retain) NSString *word_dictionary;

/*
 单词类型，一共6中类型
 a:1,
 b:2,
 c:3,
 ab:12,
 ac:13,
 bc:23,
 abc:123
 */

@property (nonatomic, retain) NSString *abc;//
@property (nonatomic, retain) NSString *dictionary_id;

//图片和声音的get方法
-(NSString *)word_pircture1;

-(NSString *)word_pircture2;

-(NSString *)word_us_voice1;

-(NSString *)word_us_voice2;

-(NSString *)word_uk_voice1;

-(NSString *)word_uk_voice2;

-(NSString *)word_d_picture1;

-(NSString *)word_d_picture2;

-(NSString *)word_dictionary;


@end
