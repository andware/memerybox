//
//  guide_page.m
//  MemoryBox
//
//  Created by joewu on 5/26/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "guide_page.h"
#import "xMainViewController.h"

@interface guide_page ()

@end

@implementation guide_page
@synthesize _scrollview;
@synthesize pageControl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    pageControl.numberOfPages = 3;
    pageControl.currentPage = 0;
    _scrollview.delegate = self;
    _scrollview.contentSize = CGSizeMake(self.view.frame.size.width * 3, self.view.frame.size.height);
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat pageWidth = self.view.frame.size.width;
    int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    pageControl.currentPage = page;
    NSLog(@"aaa-%d",page);
}

- (void)viewDidUnload
{
    [self set_scrollview:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

- (IBAction)start_to_use:(UIButton *)sender {
    xMainViewController *ickImageViewController = [[xMainViewController alloc] init];
    [self presentModalViewController:ickImageViewController animated:NO];
    [ickImageViewController release];
}
- (void)dealloc {
    [_scrollview release];
    [super dealloc];
}
@end
