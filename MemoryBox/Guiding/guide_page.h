//
//  guide_page.h
//  MemoryBox
//
//  Created by joewu on 5/26/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface guide_page : UIViewController<UIScrollViewDelegate>

- (IBAction)start_to_use:(UIButton *)sender;
@property (retain, nonatomic) IBOutlet UIScrollView *_scrollview;
@property (retain, nonatomic) IBOutlet UIPageControl *pageControl;

@end
