//
//  Algorithm.h
//  MemoryBoxRed
//
//  Created by YangJoe on 3/16/14.
//
//

#import <Foundation/Foundation.h>
#import "wordStructure.h"
#import "UserDictionary.h"

@interface Algorithm : NSObject
-(NSMutableArray*)DynamicArray;
-(NSMutableArray*)DynamicOneArray;
-(NSMutableArray*)UpdateOneDicArray:(NSMutableArray*)oldArray word:(wordStructure*)wordNum2;
-(NSMutableArray*)UpdateAllDicArray:(NSMutableArray*)oldArray word:(wordStructure*)wordNum2;
-(NSMutableArray*)WrongUpdateWordArray:(NSMutableArray*)oldArray word:(wordStructure*)wordNum2;
-(float)CountAllDicProgress:(NSMutableArray*)array;
-(float)CountDailyPlan;
-(void)DeleteWordFromArray:(UserDictionary*)dic;
-(void)UpdateDailyWordArray:(NSMutableArray*)array;
-(float)CountOneDicProgress:(NSMutableArray*)array word:(Word*)word;
@end
