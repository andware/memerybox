//
//  remember_view.h
//  MemoryBox
//
//  Created by joewu on 5/27/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "User.h"
#import "wrongWords.h"
#import "DataBase.h"

#define ViewNumber 10//定义界面的个数

@interface remember_view : UIViewController<UIAlertViewDelegate,AVAudioPlayerDelegate,UITextFieldDelegate>{
//    UIView *dwView,*ansView;//定义试图
    UIView *myView;
    UIImageView *bigImage;
    NSMutableArray *wordsArray,*GoOver;
    
    NSMutableArray *viewArray,*isAnsArray,*wrongWordArray,*wrongViewArray;//界面个数的数组和button名字的数组
    UIImageView *processing;
    UILabel *processText;
    AVAudioPlayer *avAudioPlayer;
    User *pubuser;
    DataBase *database ;
    User *user;
    double score;
    int keyBoardMargin_;
    
}
- (IBAction)playSound:(UIButton *)sender;
- (IBAction)back_to_home:(UIButton *)sender;
@property(nonatomic,assign)NSInteger dwNum,viewNum;//防止界面越界处理

@end
