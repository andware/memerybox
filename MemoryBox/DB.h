//
//  DB.h
//  MemoryBox
//
//  Created by Wooden on 1/18/14.
//
//

#import <Foundation/Foundation.h>
#import "sqlite3.h"

@interface DB : NSObject{
    sqlite3 *db;
}

@property (nonatomic, assign) sqlite3 *db;

+ (DB *)getDB;
+(void)close;

@end