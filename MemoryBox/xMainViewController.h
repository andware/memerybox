//
//  xMainViewController.h
//  MemoryBox
//
//  Created by joewu on 5/20/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface xMainViewController : UIViewController<UITextFieldDelegate>{
    
    IBOutlet UITextField *_userName; // user name text field
    IBOutlet UITextField *_password; // password text field
    int keyBoardMargin_; // transposite the when input
}

- (IBAction)_register:(UIButton *)sender;
- (IBAction)_loading:(UIButton *)sender;
- (IBAction)_rememberLoding:(UIButton *)sender;
@property (retain, nonatomic) IBOutlet UIButton *rememberLoding;

@end
