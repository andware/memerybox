//
//  wordStructure.h
//  MemoryBoxRed
//
//  Created by YangJoe on 3/15/14.
//
//

#import <Foundation/Foundation.h>
#import "Word.h"

@interface wordStructure : NSObject

@property (nonatomic, retain) NSString *wordNum;
@property (nonatomic, retain) NSString *right;
@property (nonatomic, retain) Word *word;

@end
