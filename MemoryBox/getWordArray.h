//
//  getWordArray.h
//  MemoryBoxRed
//
//  Created by YangJoe on 3/15/14.
//
//

#import <Foundation/Foundation.h>

@interface getWordArray : NSObject
-(NSMutableArray *)getNewWords;
-(NSMutableArray*)GetOldWord;
-(NSMutableArray*)getAllWord;
@end
