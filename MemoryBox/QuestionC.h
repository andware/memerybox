//
//  QuestionC.h
//  MemoryBox
//
//  Created by appstone on 13-7-2.
//
//

#import <Foundation/Foundation.h>
#import "Word.h"
@interface QuestionC : NSObject<NSCoding>{
    
}
@property (nonatomic, retain) NSString *answareC;
@property (nonatomic, retain) Word *wordC;
@end
