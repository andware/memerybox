//
//  xAppDelegate.m
//  MemoryBox
//
//  Created by joewu on 5/20/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "xAppDelegate.h"
#import "xMainViewController.h"
#import "DataBaseHelper.h"
#import "HomePage.h"
#import "Reachability.h"
#import "PurchaseTest.h"
#import "tools.h"
#import "DateUnits.h"
#import "Init.h"
#import "guide_page.h"

@implementation xAppDelegate

@synthesize window = _window;
@synthesize viewController = _viewController;

tools *tool;

#define IOS7_OR_LATER    ( [[[UIDevice currentDevice] systemVersion] compare:@"7.0"] != NSOrderedAscending )
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    // Override point for customization after application launch.
    
    /*init all database*/
    Init *init = [[Init alloc]init];
    [init Initdatabase];
    
    tool = [[tools alloc]init];
    //增加标识，用于判断是否是第一次启动应用...
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"everLaunched"]) { 
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"everLaunched"]; 
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"firstLaunch"]; 
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"everLoding"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"firstLoding"];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"everGuide"]; 
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"firstGuide"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"firstGuide2"];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"rememberLoding"];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"downLoading"];
        
        //设置相关
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"notification"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"sound"];//声音默认开启
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"autoplay"];//自动发音默认开启
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
        // 判断是不是IOS7版本
        if (IOS7_OR_LATER) {
            NSLog(@"this is IOS 7 system");
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"version"];
        }else{
            NSLog(@"this is not IOS 7 system");
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"version"];
        }
    }
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"downLoading"];
    
    // if it is the first time user use
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"firstLaunch"]) {
        
        guide_page *mainViewController = [[guide_page alloc] init];
        self.window.rootViewController=mainViewController;
        [mainViewController release];
        
//        // The alert for use internet
//        UIAlertView *alerView =  [[UIAlertView alloc] initWithTitle:@"提醒"
//                                                            message:@"程序将使用你的网络流量获取数据和下载词库数据包，以后不再提醒!"
//                                                           delegate:nil cancelButtonTitle:NSLocalizedString(@"关闭",nil) otherButtonTitles:nil];
//        [alerView show];
//        [alerView release];
    }else {
        HomePage *mainViewController = [[HomePage alloc] init];
        self.window.rootViewController=mainViewController;
        [mainViewController release];
    }
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)dealloc {
    [_window release];
    [_viewController release];
    [tool release];
    [super dealloc];
}

@end
