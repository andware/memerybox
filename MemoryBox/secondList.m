//
//  secondList.m
//  MemoryBox
//
//  Created by YangJoe on 10/22/13.
//
//

#import "secondList.h"

@implementation secondList
- (NSComparisonResult)compare:(secondList *)otherObject {
    return [self.firstLevel compare:otherObject.firstLevel];
}
- (id) initWithCoder: (NSCoder *)coder {
    if (self = [super init]){
        self.firstLevel = [coder decodeObjectForKey:@"firstLevel"];
        self.secondLevel = [coder decodeObjectForKey:@"secondLevel"];
    }
    return self;
}

- (void) encodeWithCoder: (NSCoder *)coder  {
    [coder encodeObject:self.firstLevel forKey:@"firstLevel"];
    [coder encodeObject:self.secondLevel forKey:@"secondLevel"];
}

@end
