//
//  DataBaseHelper.h
//  MemoryBox
//
//  Created by firefly on 13-6-14.
//
//
#define DBNAME    @"db.sqlite"
#define NAME      @"name"
#define AGE       @"age"
#define ADDRESS   @"address"
#define TABLENAME @"PERSONINFO"

#import <Foundation/Foundation.h>
#import "sqlite3.h"
#import "UserDictionary.h"
#import "ServiceApi.h"
#import "Word.h"
#import "DB.h"

@interface DataBaseHelper : NSObject
{
    sqlite3 *db;
}
- (BOOL)execSql:(NSString *)sql;//执行非查询的SQL语句

- (BOOL)openDB;//打开数据库，如果没有则创建

- (BOOL)initDictoinaryTable;//初始化词库表

- (BOOL)importWordXMLToDB:(UserDictionary *)dictionary userId:(NSString *)uid;//将id词库导入数据库

- (void)deleteDictionaryInDB:(NSString *)dictionaryId;//删除数据库中的词库数据

- (BOOL)isDictionaryExist:(NSString *)dictionaryId;//判断词库是否存在于数据库

- (void)pauseDictionary:(NSString *)dictionaryID userId:(NSString *)uid;//暂停某个用户的词库

- (void)unPauseDictionary:(NSString *)dictionaryID userId:(NSString *)uid;//开启某个用户的词库

- (BOOL)isDictionaryPause:(NSString *)dictionaryId userId:(NSString *)uid;//判断词库是否暂停

- (float)getDictionaryMemoryPercent:(NSString *)dictionaryId userId:(NSString *)uid;//获取单词背诵百分比

- (int)getDictionaryCompletedWords:(NSString *)dictionary_id userId:(NSString *)uid;//获取单词已背诵的

- (int)getDictionaryAllWordCount:(NSString *)dictionary_id userId:(NSString *)uid;//获取词库单词总数

- (void)getUserWordRecordFromService:(NSString *)userId;

- (BOOL)sync:(NSString *)user_id;//上传背诵数据，然后从服务器下载背诵记录，放在背地数据库 如果同步成功返回 YES 否则 NO

-(int)getWordDayNotEqualZero:(NSString *)dictionaryId userId:(NSString *)uid;

-(NSString *)getWordDictionary:(NSString *)word_id;//获取单词对应的词库名称
@end
