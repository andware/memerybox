//
//  xAppDelegate.h
//  MemoryBox
//
//  Created by joewu on 5/20/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class xViewController;

@interface xAppDelegate : UIResponder <UIApplicationDelegate>//定义一个类 继承uiresponder<采用的协议>

@property (strong, nonatomic) UIWindow *window;// strong == retain; nonatiomic 表示不进行多线程保护

@property (strong, nonatomic) xViewController *viewController;

@end
