//
//  Init.h
//  MemoryBoxRed
//
//  Created by YangJoe on 3/19/14.
//
//

#import <Foundation/Foundation.h>

@interface Init : NSObject
-(void)Initdatabase; // initialize database every time when we start the program
-(void)changeWordDay; // change the word's 'day' attribute
-(void)InitTodayWords; // initialize all word we should remember today in array, and save them to 'NSUserDefaults'
-(NSMutableArray*)GetTodayWords; // get to word array for today which had saved in to 'NSUserDefaults'
-(void)SaveTodayWords:(NSMutableArray*)array; // change word array for today when we are memorying
@end
