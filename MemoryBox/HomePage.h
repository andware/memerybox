//
//  HomePage.h
//  MemoryBox
//
//  Created by iMac on 13-8-30.
//
//

#import <UIKit/UIKit.h>
#import "UIDownloadBar.h"
#import "ASIHTTPRequest.h"
#import "ASINetworkQueue.h"
#import "MBProgressHUD.h"
#import "UserDictionary.h"
#import "EGORefreshTableHeaderView.h"

#import "HeadView.h"

#import "secondList.h"
@interface HomePage : UIViewController<UITableViewDelegate,UITableViewDataSource,UIApplicationDelegate, UIDownloadBarDelegate,MBProgressHUDDelegate,EGORefreshTableHeaderDelegate,EGORefreshTableHeaderDelegate,HeadViewDelegate>{
    NSMutableArray *dicarray,*dicType;
    NSMutableArray *percent;
    NSMutableArray *dicPause;
    UILabel *date;
    UILabel *count;
    MBProgressHUD *_progressHUD;
    BOOL isShow;
    NSMutableArray *viewArray4;
    NSArray *ui;
    UIImageView *bigImage;
    int afterBuyindex;
    
    //下拉视图
    EGORefreshTableHeaderView * _refreshHeaderView;
    //刷新标识，是否正在刷新过程中
    BOOL _reloading;
    
    // 二级菜单
    NSInteger _currentSection;
    NSInteger _currentRow;
    id record;
}

//@property(nonatomic,strong) UITableView *myTableView;
@property(nonatomic,retain)UIImageView *barView;
@property(nonatomic,retain)NSMutableArray *list;
@property(nonatomic,strong) UIImageView *start_dialog;
@property(nonatomic,strong) NSMutableArray *dics;
@property(nonatomic,strong) UIProgressView * progress;
@property(nonatomic,strong) UILabel *process;
@property (nonatomic, retain) MBProgressHUD *progressHUD;

// 二级菜单
@property(nonatomic, retain) NSMutableArray* headViewArray;
@property(nonatomic, retain) NSMutableArray* doubleStruct;
@property(nonatomic, retain) UITableView* tableView;

-(void)downLoadSuccess:(NSArray*)viewArray;
-(void)downLoadFailed:(NSArray*)viewArray;
-(void)changeDicToBuyState:(NSArray *)ViewArray;

- (void)reloadTableViewDataSource;
- (void)doneLoadingTableViewData;
@end
